package cn.com.libertymutual.sys.bean;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_sys_role_menu",  catalog = "")
@IdClass(SysRoleMenuPK.class)
public class SysRoleMenu   implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 2444639569795384925L;
	private String roleid;
    private int menuid;

    @Id
    @Column(name = "ROLEID", nullable = false, length = 16)
    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    @Id
    @Column(name = "MENUID", nullable = false)
    public int getMenuid() {
        return menuid;
    }

    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysRoleMenu that = (SysRoleMenu) o;

        if (menuid != that.menuid) return false;
        if (roleid != null ? !roleid.equals(that.roleid) : that.roleid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid != null ? roleid.hashCode() : 0;
        result = 31 * result + menuid;
        return result;
    }

	@Override
	public String toString() {
		return "SysRoleMenu [roleid=" + roleid + ", menuid=" + menuid + "]";
	}
}
