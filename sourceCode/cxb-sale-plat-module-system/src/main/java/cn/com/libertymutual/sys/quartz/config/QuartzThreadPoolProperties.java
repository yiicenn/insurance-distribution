package cn.com.libertymutual.sys.quartz.config;


public class QuartzThreadPoolProperties {
	private String className;	//: org.quartz.simpl.SimpleThreadPool
	private String threadCount;	//: 10
	private String threadPriority;	//: 5
	private boolean threadsInheritContextClassLoaderOfInitializingThread;	//: true
	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}
	/**
	 * @param className the className to set
	 */
	public void setClassName(String className) {
		this.className = className;
	}
	/**
	 * @return the threadCount
	 */
	public String getThreadCount() {
		return threadCount;
	}
	/**
	 * @param threadCount the threadCount to set
	 */
	public void setThreadCount(String threadCount) {
		this.threadCount = threadCount;
	}
	/**
	 * @return the threadPriority
	 */
	public String getThreadPriority() {
		return threadPriority;
	}
	/**
	 * @param threadPriority the threadPriority to set
	 */
	public void setThreadPriority(String threadPriority) {
		this.threadPriority = threadPriority;
	}
	/**
	 * @return the threadsInheritContextClassLoaderOfInitializingThread
	 */
	public boolean getThreadsInheritContextClassLoaderOfInitializingThread() {
		return threadsInheritContextClassLoaderOfInitializingThread;
	}
	/**
	 * @param threadsInheritContextClassLoaderOfInitializingThread the threadsInheritContextClassLoaderOfInitializingThread to set
	 */
	public void setThreadsInheritContextClassLoaderOfInitializingThread(
			boolean threadsInheritContextClassLoaderOfInitializingThread) {
		this.threadsInheritContextClassLoaderOfInitializingThread = threadsInheritContextClassLoaderOfInitializingThread;
	}
	
	
}
