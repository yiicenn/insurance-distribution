package cn.com.libertymutual.sys.dao;

import java.util.List;


import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sys.bean.SysRoleMenu;


@Repository
public interface ISysRoleMenuDao  extends PagingAndSortingRepository<SysRoleMenu, String>, JpaSpecificationExecutor<SysRoleMenu>
{
	
	
	@Query( "SELECT DISTINCT t1.menuid FROM SysRoleMenu t1 , SysMenu t2 where t1.menuid=t2.menuid and t1.roleid IN (?1) GROUP BY t1.menuid ORDER BY t2.orderid")
	List<Integer> findRoleMenu(List<String> roleList) ;
	
	@Query("select menuid from SysRoleMenu where roleid=?1 order by menuid")
	List<Integer> findMenuIdList( String roleId );

	@Query("select t from SysRoleMenu t where t.roleid=?1 and menuid = ?2")
	SysRoleMenu findByRoleAndMenuId(String roleId, int menuid);

	@Modifying
	@Transactional
	@Query("delete from SysRoleMenu where roleid = :roleId")
	void deleteByRoleId(@Param("roleId")String roleId);
}
