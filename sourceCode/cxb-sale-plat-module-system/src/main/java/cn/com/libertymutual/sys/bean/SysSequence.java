package cn.com.libertymutual.sys.bean;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_sys_sequence",  catalog = "")
public class SysSequence  implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -7961869524856726549L;
	private String name;
    private int currentValue;
    private int increment;
    private Date curDate;

    @Id
    @Column(name = "name", nullable = false, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "current_value", nullable = false)
    public int getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(int currentValue) {
        this.currentValue = currentValue;
    }

    @Basic
    @Column(name = "increment", nullable = false)
    public int getIncrement() {
        return increment;
    }

    public void setIncrement(int increment) {
        this.increment = increment;
    }

    @Basic
    @Column(name = "cur_date", nullable = true)
    public Date getCurDate() {
        return curDate;
    }

    public void setCurDate(Date curDate) {
        this.curDate = curDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysSequence that = (SysSequence) o;

        if (currentValue != that.currentValue) return false;
        if (increment != that.increment) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (curDate != null ? !curDate.equals(that.curDate) : that.curDate != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + currentValue;
        result = 31 * result + increment;
        result = 31 * result + (curDate != null ? curDate.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return "SysSequence [name=" + name + ", currentValue=" + currentValue
				+ ", increment=" + increment + ", curDate=" + curDate + "]";
	}
}
