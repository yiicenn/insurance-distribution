package cn.com.libertymutual.sys.bean;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_sys_menu",  catalog = "")
public class SysMenu  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 969414279254141598L;
	private int menuid;
    private int fmenuid;
    private String menuname;
    private String menunameen;
    private String menuurl;
    private String stdcode;
    private String icon;
    private String target;
    private String isleaf;
    private int orderid;
    private String loadflag;
    private String status;
   
    public List smenu;
    
    

    @Id    
//    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "MENUID", nullable = false)
    public int getMenuid() {
        return menuid;
    }

    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }

    @Basic
    @Column(name = "FMENUID", nullable = false)
    public int getFmenuid() {
        return fmenuid;
    }

    public void setFmenuid(int fmenuid) {
        this.fmenuid = fmenuid;
    }

    @Basic
    @Column(name = "MENUNAME", nullable = false, length = 32)
    public String getMenuname() {
        return menuname;
    }

    public void setMenuname(String menuname) {
        this.menuname = menuname;
    }

    @Basic
    @Column(name = "MENUNAMEEN", nullable = true, length = 32)
    public String getMenunameen() {
        return menunameen;
    }

    public void setMenunameen(String menunameen) {
        this.menunameen = menunameen;
    }

    @Basic
    @Column(name = "MENUURL", nullable = true, length = 100)
    public String getMenuurl() {
        return menuurl;
    }

    public void setMenuurl(String menuurl) {
        this.menuurl = menuurl;
    }

    @Basic
    @Column(name = "STDCODE", nullable = true, length = 16)
    public String getStdcode() {
        return stdcode;
    }

    public void setStdcode(String stdcode) {
        this.stdcode = stdcode;
    }

    @Basic
    @Column(name = "ICON", nullable = true, length = 64)
    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Basic
    @Column(name = "TARGET", nullable = true, length = 16)
    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    @Basic
    @Column(name = "ISLEAF", nullable = false, length = 1)
    public String getIsleaf() {
        return isleaf;
    }

    public void setIsleaf(String isleaf) {
        this.isleaf = isleaf;
    }

    @Basic
    @Column(name = "ORDERID", nullable = false)
    public int getOrderid() {
        return orderid;
    }

    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

    @Basic
    @Column(name = "LOADFLAG", nullable = false, length = 1)
    public String getLoadflag() {
        return loadflag;
    }

    public void setLoadflag(String loadflag) {
        this.loadflag = loadflag;
    }
    @Basic
    @Column(name = "STATUS", nullable = true, length = 1)
    public String getStatus() {
    	return status;
    }
    
    public void setStatus(String status) {
    	this.status = status;
    }

    
    @Transient
    public List getSmenu() {
		return smenu;
	}

	public void setSmenu(List smenu) {
		this.smenu = smenu;
	}
	
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof SysMenu))
			return false;
		SysMenu other = (SysMenu) obj;
		if (fmenuid != other.fmenuid)
			return false;
		if (icon == null) {
			if (other.icon != null)
				return false;
		} else if (!icon.equals(other.icon))
			return false;
		if (isleaf == null) {
			if (other.isleaf != null)
				return false;
		} else if (!isleaf.equals(other.isleaf))
			return false;
		if (loadflag == null) {
			if (other.loadflag != null)
				return false;
		} else if (!loadflag.equals(other.loadflag))
			return false;
		if (menuid != other.menuid)
			return false;
		if (menuname == null) {
			if (other.menuname != null)
				return false;
		} else if (!menuname.equals(other.menuname))
			return false;
		if (menunameen == null) {
			if (other.menunameen != null)
				return false;
		} else if (!menunameen.equals(other.menunameen))
			return false;
		if (menuurl == null) {
			if (other.menuurl != null)
				return false;
		} else if (!menuurl.equals(other.menuurl))
			return false;
		if (orderid != other.orderid)
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (stdcode == null) {
			if (other.stdcode != null)
				return false;
		} else if (!stdcode.equals(other.stdcode))
			return false;
		if (target == null) {
			if (other.target != null)
				return false;
		} else if (!target.equals(other.target))
			return false;
		return true;
	}

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fmenuid;
		result = prime * result + ((icon == null) ? 0 : icon.hashCode());
		result = prime * result + ((isleaf == null) ? 0 : isleaf.hashCode());
		result = prime * result
				+ ((loadflag == null) ? 0 : loadflag.hashCode());
		result = prime * result + menuid;
		result = prime * result
				+ ((menuname == null) ? 0 : menuname.hashCode());
		result = prime * result
				+ ((menunameen == null) ? 0 : menunameen.hashCode());
		result = prime * result + ((menuurl == null) ? 0 : menuurl.hashCode());
		result = prime * result + orderid;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((stdcode == null) ? 0 : stdcode.hashCode());
		result = prime * result + ((target == null) ? 0 : target.hashCode());
		return result;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SysMenu [menuid=").append(menuid).append(", fmenuid=")
				.append(fmenuid).append(", menuname=").append(menuname)
				.append(", menunameen=").append(menunameen)
				.append(", menuurl=").append(menuurl).append(", stdcode=")
				.append(stdcode).append(", icon=").append(icon)
				.append(", target=").append(target).append(", isleaf=")
				.append(isleaf).append(", orderid=").append(orderid)
				.append(", loadflag=").append(loadflag).append(", status=")
				.append(status).append("]");
		return builder.toString();
	}
}
