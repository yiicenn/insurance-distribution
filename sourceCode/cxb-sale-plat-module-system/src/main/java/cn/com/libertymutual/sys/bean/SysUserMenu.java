package cn.com.libertymutual.sys.bean;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-12-21.
 */
@Entity
@Table(name = "tb_sys_user_menu",  catalog = "")
@IdClass(SysUserMenuPK.class)
public class SysUserMenu {
    private String userid;
    private Integer menuid;

    @Id
    @Column(name = "USERID", nullable = false, length = 16)
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    @Id
    @Column(name = "MENUID", nullable = false, precision = 0)
    public Integer getMenuid() {
        return menuid;
    }

    public void setMenuid(Integer menuid) {
        this.menuid = menuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysUserMenu that = (SysUserMenu) o;

        if (userid != null ? !userid.equals(that.userid) : that.userid != null) return false;
        if (menuid != null ? !menuid.equals(that.menuid) : that.menuid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = userid != null ? userid.hashCode() : 0;
        result = 31 * result + (menuid != null ? menuid.hashCode() : 0);
        return result;
    }
}
