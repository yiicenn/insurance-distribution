package cn.com.libertymutual.sys.quartz.config;





public class QuartzSchedulerProperties {
	private String instanceName;
	private String instanceId;
	private boolean skipUpdateCheck;
	private String threadName;
	
	private boolean jmxExport;
	
	
	/**
	 * @return the instanceName
	 */
	public String getInstanceName() {
		return instanceName;
	}
	/**
	 * @param instanceName the instanceName to set
	 */
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	/**
	 * @return the threadName
	 */
	public String getThreadName() {
		return threadName;
	}
	/**
	 * @param threadName the threadName to set
	 */
	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}
	/**
	 * @return the instanceId
	 */
	public String getInstanceId() {
		return instanceId;
	}
	/**
	 * @param instanceId the instanceId to set
	 */
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	/**
	 * @return the skipUpdateCheck
	 */
	public boolean getSkipUpdateCheck() {
		return skipUpdateCheck;
	}
	/**
	 * @param skipUpdateCheck the skipUpdateCheck to set
	 */
	public void setSkipUpdateCheck(boolean skipUpdateCheck) {
		this.skipUpdateCheck = skipUpdateCheck;
	}
	/**
	 * @return the jmxExport
	 */
	public boolean getJmxExport() {
		return jmxExport;
	}
	/**
	 * @param jmxExport the jmxExport to set
	 */
	public void setJmxExport(boolean jmxExport) {
		this.jmxExport = jmxExport;
	}
	
}
