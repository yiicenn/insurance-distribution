package cn.com.libertymutual.sys.bean;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "tb_sys_pay_type",  catalog = "")
public class SysPayType implements Serializable{

	/**
	 * #if($payType.isDef=="1")
					#set($defPay = $payType)
				#end
				<tr id="tr_check$!{payType.payCode}" class="alipay_item_tr" style="" onclick="checkPayType('i_check$!{payType.payCode}', '$!{payType.payType}','$!{payType.platformId}')">
	                <td align="center" width="70"><img src="$contextPath$urls.getForLookupPath('$!{payType.img}')" class="moneyimg" style="width:58px;height:48px;" align="absmiddle"></td>
	                <td><b>$!{payType.payName}</b><br><span class="alipay_item_desc">$!{payType.payDesc}</span></td>
	            	#if($payType.isDef=="1"){
	               		<td align="right"><img src="$contextPath$urls.getForLookupPath('/payment/img/check.png')" id="i_check$!{payType.payCode}" class="checkimg" align="absmiddle"></td>
	            	#else
		                <td align="right"><img src="$contextPath$urls.getForLookupPath('/payment/img/uncheck.png')" id="i_check$!{payType.payCode}" class="checkimg" align="absmiddle"></td>
	            	#end
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	//支付码
	private String payCode;
	//支付类型 wap online
	private String payType;
	//支付方式    微信 支付宝
	private String platformId;
	//支付方式名称
	private String payName;
	//支付方式描述
	private String payDesc;
	//图片地址
	private String img;
	//是否默认支付方式
	private String isDef;
	//启用状态
	private String status;
	
    @Id    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	 @Basic
	 @Column(name = "pay_Code", nullable = false)
	public String getPayCode() {
		return payCode;
	}
	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}
	
	@Basic
	 @Column(name = "Pay_Type", nullable = false)
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	@Basic
	 @Column(name = "plat_form_Id", nullable = false)
	public String getPlatformId() {
		return platformId;
	}
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	
	@Basic
	@Column(name = "Pay_Name", nullable = false)
	public String getPayName() {
		return payName;
	}
	public void setPayName(String payName) {
		this.payName = payName;
	}
	
	@Basic
	@Column(name = "Pay_Desc", nullable = true, length = 512)
	public String getPayDesc() {
		return payDesc;
	}
	public void setPayDesc(String payDesc) {
		this.payDesc = payDesc;
	}
	
	@Basic
	@Column(name = "img", nullable = false, length = 512)
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	@Basic
	@Column(name = "is_Def", nullable = false)
	public String getIsDef() {
		return isDef;
	}
	public void setIsDef(String isDef) {
		this.isDef = isDef;
	}
	@Basic
	@Column(name = "status", nullable = false)
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((img == null) ? 0 : img.hashCode());
		result = prime * result + ((isDef == null) ? 0 : isDef.hashCode());
		result = prime * result + ((payCode == null) ? 0 : payCode.hashCode());
		result = prime * result + ((payDesc == null) ? 0 : payDesc.hashCode());
		result = prime * result + ((payName == null) ? 0 : payName.hashCode());
		result = prime * result + ((payType == null) ? 0 : payType.hashCode());
		result = prime * result
				+ ((platformId == null) ? 0 : platformId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SysPayType other = (SysPayType) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (img == null) {
			if (other.img != null)
				return false;
		} else if (!img.equals(other.img))
			return false;
		if (isDef == null) {
			if (other.isDef != null)
				return false;
		} else if (!isDef.equals(other.isDef))
			return false;
		if (payCode == null) {
			if (other.payCode != null)
				return false;
		} else if (!payCode.equals(other.payCode))
			return false;
		if (payDesc == null) {
			if (other.payDesc != null)
				return false;
		} else if (!payDesc.equals(other.payDesc))
			return false;
		if (payName == null) {
			if (other.payName != null)
				return false;
		} else if (!payName.equals(other.payName))
			return false;
		if (payType == null) {
			if (other.payType != null)
				return false;
		} else if (!payType.equals(other.payType))
			return false;
		if (platformId == null) {
			if (other.platformId != null)
				return false;
		} else if (!platformId.equals(other.platformId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "SysPayType [id=" + id + ", payCode=" + payCode + ", payType="
				+ payType + ", platformId=" + platformId + ", payName="
				+ payName + ", payDesc=" + payDesc + ", img=" + img
				+ ", isDef=" + isDef + "]";
	}
 

}
