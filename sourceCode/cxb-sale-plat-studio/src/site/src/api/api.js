import axios from 'axios';
import { Message } from 'element-ui';
// export let base = 'http://10.132.30.86:18008';
// export let base = 'https://uat-lm.libertymutual.com.cn';
// export let base = 'https://m.libao.cn';
export let base = '';

let userInfo = sessionStorage.user ? JSON.parse(sessionStorage.user) : '';
let admintoken = userInfo ? userInfo.token : '';

// export let picURL = 'https://jsonplaceholder.typicode.com/posts/';
// export let picURL = 'https://uat-lm.libertymutual.com.cn/cxb/admin/uploadWeb/many';
// export let picURL = 'https://uat-lm.libertymutual.com.cn/cxb/admin/uploadWeb/many';
export let picURL = `${base}/cxb/admin/uploadWeb/many`;
export let picURLs = `${base}/cxb/admin/scoreWeb/excelScore`;
// export const requestLogin1 = params => { return axios.post(`${base}/syslogin/login`, params).then(res => res.data); };
// export const requestLogin = params => {
//     return  axios({
//         url: `${base}/syslogin/login`,
//         method: 'post',
//         params,
//         headers: {
//           'Content-Type': 'application/x-www-form-urlencoded'
//         }
//       }).then(res => res.data);
//      };


axios.interceptors.response.use(

    response => {
        if (response.status == '200') {
            // //获取标识码
            // let identification_mark = response.headers[.IDENTIFICATION_MARK];
            // if (typeof (identification_mark) != 'undefined' && identification_mark.length > 0) {
            //     //有则保存到缓存
            //     sessionStorage[.IDENTIFICATION_MARK] = identification_mark;
            // }
            if (response.data.state == 3) {
                sessionStorage.setItem('loginStatus', '0');
                Message.error(response.data.result);
                let WEB_DOMAIN_FULL_PATH = window.location.protocol + "//" + window.location.hostname + "/admin-cxb/#/login";
                window.location.href = WEB_DOMAIN_FULL_PATH;
            }
            else {
                return response;
            }
        }
        return response;
    },
    error => {
        return Promise.reject(error)
    });


//Main
export const visitsCount = params => { return axios.post(`${base}/cxb/admin/home/visitsCount`, params); };

export const petConfigList = params => { return axios.post(`${base}/cxb/admin/question/petConfigList`, params); };

export const addPetConfig = params => { return axios.post(`${base}/cxb/admin/question/addPetConfig`, params); };

export const checkImgCode = params => { return axios.post(`${base}/cxb/nol/user/checkImgCode`, params); };

export const getSaleName = params => { return axios.post(`${base}/cxb/admin/branchSet/getSaleName`, params); };

export const requestLogin = params => { return axios.post(`${base}/cxb/web/login`, params); };

export const getUserList = params => { return axios.get(`${base}/cxb/user/list`, { params: params }); };

export const getUserListPage = params => { return axios.post(`${base}/cxb/admin/productWeb/productList`, params); };

export const removeProduct = params => { return axios.post(`${base}/cxb/admin/productWeb/deleteProduct`, params); };

export const rebackProduct = params => { return axios.post(`${base}/cxb/admin/productWeb/rebackProduct`, params); };

// export const batchRemoveUser = params => { return axios.post(`${base}/cxb/admin/productWeb/deleteProduct`, params); };

export const editProduct = params => { return axios.post(`${base}/cxb/admin/productWeb/updateProduct`, params); };

export const addProduct = params => { return axios.post(`${base}/cxb/admin/productWeb/addProduct`, params); };

export const editHot = params => { return axios.post(`${base}/cxb/admin/productWeb/setHot`, params); };

export const getHotProduct = params => { return axios.post(`${base}/cxb/admin/productWeb/getHotProduct`, params); };

export const planList = params => { return axios.post(`${base}/cxb/admin/productWeb/planList`, params); };

export const addPlans = params => { return axios.post(`${base}/cxb/admin/productWeb/addPlans`, params); };

export const getExclusive = params => { return axios.post(`${base}/cxb/admin/productWeb/getExclusive`, params); };

export const setExclusive = params => { return axios.post(`${base}/cxb/admin/productWeb/setExclusive`, params); };

//order
export const orderList = params => { return axios.post(`${base}/cxb/admin/orderWeb/orderList`, params); };

export const callBackUrl = params => { return axios.post(`${base}/cxb/admin/orderWeb/callBackUrl`, params); };

export const upOrder = params => { return axios.post(`${base}/cxb/admin/orderWeb/upOrder`, params); };

//Hot
export const branchHotProductList = params => { return axios.post(`${base}/cxb/admin/productWeb/branchHotProductList`, params); };

export const removeHotProduct = params => { return axios.post(`${base}/cxb/admin/productWeb/removeHotProduct`, params); };

export const addToAreaHot = params => { return axios.post(`${base}/cxb/admin/productWeb/addToAreaHot`, params); };

export const codeNodeList = params => { return axios.post(`${base}/cxb/admin/productWeb/codeNodeList`, params); };



export const updateProductSerialNo = params => { return axios.post(`${base}/cxb/admin/productWeb/updateProductSerialNo`, params); };

export const notConfiguredArea = params => { return axios.post(`${base}/cxb/admin/branchSet/notConfiguredArea`, params); };


//or
export const getAgreementNo = params => { return axios.post(`${base}/cxb/admin/branchSet/getAgreementNo`, params); };

export const getcxbName = params => { return axios.post(`${base}/cxb/admin/branchSet/getcxbName`, params); };

export const addNewArea = params => { return axios.post(`${base}/cxb/admin/branchSet/addNewArea`, params); };



//AD
export const getAdListPage = params => { return axios.post(`${base}/cxb/admin/advertWeb/advertList`, params); };

export const removeAd = params => { return axios.post(`${base}/cxb/admin/advertWeb/removeAdvert`, params); };

export const editAd = params => { return axios.post(`${base}/cxb/admin/advertWeb/updateAdvert`, params); };

export const addAd = params => { return axios.post(`${base}/cxb/admin/advertWeb/addAdvert`, params); };

export const getAdDetail = params => { return axios.post(`${base}/cxb/admin/advertWeb/getAdDetail`, params); };

export const updateAdDetail = params => { return axios.post(`${base}/cxb/admin/advertWeb/updateAdDetail`, params); };



//branch
export const areaList = params => { return axios.post(`${base}/cxb/admin/branchSet/areaList`, params); };

export const addArea = params => { return axios.post(`${base}/cxb/admin/branchSet/addArea`, params); };

export const removeArea = params => { return axios.post(`${base}/cxb/admin/branchSet/removeArea`, params); };

//Service
export const getServiceListPage = params => { return axios.post(`${base}/cxb/admin/serviceMenuWeb/menuList`, params); };

export const removeService = params => { return axios.post(`${base}/cxb/admin/serviceMenuWeb/removeService`, params); };

export const editService = params => { return axios.post(`${base}/cxb/admin/serviceMenuWeb/updateService`, params); };

export const addService = params => { return axios.post(`${base}/cxb/admin/serviceMenuWeb/addService`, params); };

//cusrom
export const userList = params => { return axios.post(`${base}/cxb/admin/userWeb/userList`, params); };

export const updateUser = params => { return axios.post(`${base}/cxb/admin/userWeb/updateUser`, params); };

export const createUsers = params => { return axios.post(`${base}/cxb/admin/batch_process_q_rcode_test/createUsers`, params); };

export const addUser = params => { return axios.post(`${base}/cxb/admin/userWeb/addUser`, params); };

export const updatePwdByUserCode = params => { return axios.post(`${base}/cxb/admin/userWeb/updatePwdByUserCode`, params); };

//user
export const adminList = params => { return axios.post(`${base}/cxb/admin/admin/adminList`, params); };

export const addAdmin = params => { return axios.post(`${base}/cxb/admin/admin/addAdmin`, params); };

export const LdapUser = params => { return axios.post(`${base}/cxb/admin/admin/LdapUser`, params); };

export const getRoleOfUser = params => { return axios.post(`${base}/cxb/admin/admin/getRoleOfUser`, params); };

export const setRoleToUser = params => { return axios.post(`${base}/cxb/admin/admin/setRoleToUser`, params); };

export const updateAdminStauts = params => { return axios.post(`${base}/cxb/admin/admin/updateAdminStauts`, params); };

//role
export const roleList = params => { return axios.post(`${base}/cxb/admin/role/roleList`, params); };

export const allMenuList = () => { return axios.post(`${base}/cxb/admin/role/allMenuList`); };

export const roleMenuList = params => { return axios.post(`${base}/cxb/admin/role/roleMenuList`, params); };

export const setPermission = params => { return axios.post(`${base}/cxb/admin/role/setPermission`, params); };

export const removeRole = params => { return axios.post(`${base}/cxb/admin/role/removeRole`, params); };

export const addRole = params => { return axios.post(`${base}/cxb/admin/role/addRole`, params); };

//Log
export const queryLog = params => { return axios.post(`${base}/cxb/admin/log/queryLog`, params); };

//menu
export const addMenu = params => { return axios.post(`${base}/cxb/admin/menu/addMenu`, params); };

export const removeMenu = params => { return axios.post(`${base}/cxb/admin/menu/removeMenu`, params); };

export const updateMenuName = params => { return axios.post(`${base}/cxb/admin/menu/updateMenuName`, params); };


//clause
export const clauseList = params => { return axios.post(`${base}/cxb/admin/clause/clauseList`, params); };

export const addClause = params => { return axios.post(`${base}/cxb/admin/clause/addClause`, params); };

export const removeClause = params => { return axios.post(`${base}/cxb/admin/clause/removeClause`, params); };

export const clauseTitleList = params => { return axios.post(`${base}/cxb/admin/clause/clauseTitleList`, params); };

//riskCode
export const riskList = params => { return axios.post(`${base}/cxb/admin/productWeb/riskList`, params); };

export const addRisk = params => { return axios.post(`${base}/cxb/admin/productWeb/addRisk`, params); };

export const removeRisk = params => { return axios.post(`${base}/cxb/admin/productWeb/removeRisk`, params); };

export const channelProductList = params => { return axios.post(`${base}/cxb/admin/productWeb/channelProductList`, params); };
//WxsystemLog

export const shareList = params => { return axios.post(`${base}/cxb/admin/share/shareList`, params); };

export const addShare = params => { return axios.post(`${base}/cxb/admin/share/addShare`, params); };

export const removeShare = params => { return axios.post(`${base}/cxb/admin/share/removeShare`, params); };

export const querySaleLog = params => { return axios.post(`${base}/cxb/admin/log/querySaleLog`, params); };

export const deleteSaleLog = params => { return axios.post(`${base}/cxb/admin/log/deleteSaleLog`, params); };

//article

export const articleList = params => { return axios.post(`${base}/cxb/admin/article/articleList`, params); };

export const addOrUpdateArticle = params => { return axios.post(`${base}/cxb/admin/article/addOrUpdateArticle`, params); };

export const productByRisk = params => { return axios.post(`${base}/cxb/admin/productWeb/productByRisk`, params); };


//poster
export const posterList = params => { return axios.post(`${base}/cxb/admin/posterWeb/posterList`, params); };

export const addOrUpdatePoster = params => { return axios.post(`${base}/cxb/admin/posterWeb/addOrUpdatePoster`, params); };

export const approve = params => { return axios.post(`${base}/cxb/admin/posterWeb/approve`, params); };


//cxblog
export const querycxbLog = params => { return axios.post(`${base}/cxb/admin/log/querycxbLog`, params); };

export const deletecxbLog = params => { return axios.post(`${base}/cxb/admin/log/deletecxbLog`, params); };

//approve

export const approveHis = params => { return axios.post(`${base}/cxb/admin/posterWeb/approveHis`, params); };

//serverinfo

export const serviceInfo = params => { return axios.post(`${base}/cxb/admin/serviceMenuWeb/serviceInfo`, params); };

export const updateServiceInfo = params => { return axios.post(`${base}/cxb/admin/serviceMenuWeb/updateServiceInfo`, params); };

//shield

export const allKeywords = params => { return axios.post(`${base}/cxb/admin/keyword/allKeywords`, params); };

export const addOrUpdateKeyword = params => { return axios.post(`${base}/cxb/admin/keyword/addOrUpdateKeyword`, params); };

export const weChatKeywords = params => { return axios.post(`${base}/cxb/admin/keyword/weChatKeywords`, params); };

export const updateKeywordWeChat = params => { return axios.post(`${base}/cxb/admin/keyword/updateKeywordWeChat`, params); };

export const addKeywordWeChat = params => { return axios.post(`${base}/cxb/admin/keyword/addKeywordWeChat`, params); };


//integral---activityGive
export const activityList = params => { return axios.post(`${base}/cxb/admin/active/activityList`, params); };

export const issueActivity = params => { return axios.post(`${base}/cxb/admin/active/issueActivity`, params); };

export const stopActivity = params => { return axios.post(`${base}/cxb/admin/active/stopActivity`, params); };

export const addBudget = params => { return axios.post(`${base}/cxb/admin/active/addBudget`, params); };

export const budgetHis = params => { return axios.post(`${base}/cxb/admin/active/budgetHis`, params); };

export const approveActivity = params => { return axios.post(`${base}/cxb/admin/scoreWeb/approve`, params); };

//integral---handworkGive
export const listByhand = params => { return axios.post(`${base}/cxb/admin/scoreWeb/listByhand`, params); };

export const beforeApprove = params => { return axios.post(`${base}/cxb/admin/scoreWeb/beforeApprove`, params); };

//integral---giveInquire
export const inScoreDetail = params => { return axios.post(`${base}/cxb/admin/scoreWeb/inScoreDetail`, params); };

//integral---lotteryQuire
export const drawLog = params => { return axios.post(`${base}/cxb/admin/scoreWeb/drawLog`, params); };

// integral----withdrawInquire
export const integralCashPatch = params => { return axios.post(`${base}/cxb/admin/scoreWeb/integralCashPatch`, params); };

export const findBankListAll = params => { return axios.post(`${base}/cxb/admin/userWeb/findBankListAll`, params); };

// integral---quickMoneyQuerry
export const queryCnp = params => { return axios.post(`${base}/cxb/admin/scoreWeb/queryCnp`, params); };

// integral----deduction
export const cashToInScoreDetail = params => { return axios.post(`${base}/cxb/admin/scoreWeb/cashToInScoreDetail`, params); };

//integral---useInquire
export const outScoreDetail = params => { return axios.post(`${base}/cxb/admin/scoreWeb/outScoreDetail`, params); };

//integral---withdrawInquire

export const cashLog = params => { return axios.post(`${base}/cxb/admin/scoreWeb/cashLog`, params); };

//integral---parameterConfigure
export const scoreConfig = () => { return axios.post(`${base}/cxb/admin/scoreWeb/scoreConfig`); };

export const updateScoreConfig = params => { return axios.post(`${base}/cxb/admin/scoreWeb/updateScoreConfig`, params); };

//integral---userInquire

export const userScore = params => { return axios.post(`${base}/cxb/admin/scoreWeb/userScore`, params); };

//integral---userInquire
export const reissue = params => { return axios.post(`${base}/cxb/admin/wechat/reissue`, params); };
//applicant

export const policyInfo = params => { return axios.post(`${base}/cxb/admin/orderWeb/policyInfo`, params); };

export const addPolicy = params => { return axios.post(`${base}/cxb/admin/orderWeb/addPolicy`, params); };


/////

//车牌
export const plateList = params => { return axios.post(`${base}/cxb/admin/carPlate/plateList`, params); };

export const addPlate = params => { return axios.post(`${base}/cxb/admin/carPlate/addPlate`, params); };

export const removePlate = params => { return axios.post(`${base}/cxb/admin/carPlate/removePlate`, params); };

//用户佣金比例
export const setShopRate = params => { return axios.post(`${base}/cxb/admin/userWeb/setShopRate`, params); };

export const shopRate = params => { return axios.post(`${base}/cxb/admin/userWeb/shopRate`, params); };

export const deleteShopRate = params => { return axios.post(`${base}/cxb/admin/userWeb/deleteShopRate`, params); };

export const waitAapprove = params => { return axios.post(`${base}/cxb/admin/posterWeb/waitAapprove`, params); };

export const agreeRate = params => { return axios.post(`${base}/cxb/admin/userWeb/agreeRate`, params); };

export const deleteAgreeRate = params => { return axios.post(`${base}/cxb/admin/userWeb/deleteAgreeRate`, params); };
//SQL

export const findBySql = params => { return axios.post(`${base}/cxb/admin/orderWeb/findBySql`, params); };

// export const uploadWeb = params => { return axios.get(`${base}/cxb/admin/uploadWeb/getByName?filepath=`+params); };

//login.vue
export const LOGIN_URL = "/soa/web/login";

export const LOGOUT_URL = "/soa/logout";

//home.vue
export const MENU_URL = "/soa/system/initLogin";

//loadmen.vue

export const LOADMEN_URL = params => { return axios.post(`${base}/cxb/systemCfg/reload`, params); };


export const clearAccessToken = params => { return axios.get(`${base}/cxb/admin/wechat/clearAccessToken`); };

export const refresh = params => { return axios.post(`${base}/cxb/admin/cache/refresh`, params); };

// export const LOADMEN_URL = "${base}/cxb/systemCfg/reload";

// branchManage
export const levelList = params => { return axios.post(`${base}/cxb/admin/level/levelList`, params); };

export const isHasIdnumber = params => { return axios.post(`${base}/cxb/admin/level/isHasIdnumber`, params); };

export const createChannelUser = params => { return axios.post(`${base}/cxb/admin/level/createChannelUser`, params); };

export const updateChannelUser = params => { return axios.post(`${base}/cxb/admin/level/updateChannelUser`, params); };

export const isMergeUser = params => { return axios.post(`${base}/cxb/admin/level/isMergeUser`, params); };

export const nextChannelList = params => { return axios.post(`${base}/cxb/admin/level/nextChannelList`, params); };

export const findByAgreementNo = params => { return axios.post(`${base}/cxb/admin/level/findByAgreementNo`, params); };
export const treeNextChannelList = params => { return axios.post(`${base}/cxb/admin/level/treeNextChannelList`, params); };

export const treeNextBranchList = params => { return axios.post(`${base}/cxb/admin/level/treeNextBranchList`, params); };

export const nextBranchList = params => { return axios.post(`${base}/cxb/admin/level/nextBranchList`, params); };

export const branchManage = params => { return axios.post(`${base}/cxb/admin/level/branchManage`, params); };

export const thisChannel = params => { return axios.post(`${base}/cxb/admin/level/thisChannel`, params); };

export const topChannelUser = params => { return axios.post(`${base}/cxb/admin/level/topChannelUser`, params); };

// guaranteeSlipManage
export const findAllHotArea = params => { return axios.post(`${base}/cxb/admin/orderWeb/findAllHotArea`, params); };

// authorization
export const authUserList = params => { return axios.post(`${base}/cxb/admin/level/authUserList`, params); };

export const orderAuth = params => { return axios.post(`${base}/cxb/admin/level/orderAuth`, params); };

export const setShopPro = params => { return axios.post(`${base}/cxb/admin/level/setShopPro`, params); };

export const updateOrderAuth = params => { return axios.post(`${base}/cxb/admin/level/updateOrderAuth`, params); };

export const addFlow = params => { return axios.post(`${base}/cxb/admin/flow/addFlow`, params); };
export const flowList = params => { return axios.post(`${base}/cxb/admin/flow/flowList`, params); };

export const flowChildNodeList = params => { return axios.post(`${base}/cxb/admin/flow/flowChildNodeList`, params); };

export const addChildNode = params => { return axios.post(`${base}/cxb/admin/flow/addChildNode`, params); };
export const nodeDetail = params => { return axios.post(`${base}/cxb/admin/flow/nodeDetail`, params); };

export const removeNode = params => { return axios.post(`${base}/cxb/admin/flow/removeNode`, params); };
// questionnaire
export const addQuestionnaire = params => { return axios.post(`${base}/cxb/admin/question/addQuestionnaire `, params); };

export const questionList = params => { return axios.post(`${base}/cxb/admin/question/questionList `, params); };

export const adminInfo = params => { return axios.post(`${base}/cxb/admin/admin/adminInfo `, params); };

// count
export const countProductList = params => { return axios.post(`${base}/cxb/admin/countWeb/countProductList`, params); };
export const branchProductList = params => { return axios.post(`${base}/cxb/admin/countWeb/branchProductList`, params); };
export const myProductCountList = params => { return axios.post(`${base}/cxb/admin/countWeb/myProductCountList`, params); };

//prodeucCompany
export const prodeucCompanyList = params => { return axios.post(`${base}/cxb/admin/productWeb/productCompanyList`, params); };
export const addProductCompany = params => { return axios.post(`${base}/cxb/admin/productWeb/addProductCompany`, params); };
export const deleteProductCompany = params => { return axios.post(`${base}/cxb/admin/productWeb/deleteProductCompany`, params); };
