/*****************************************************************************************************
 * DESC			:	API函数定义
 * Author		:	Steven.Li
 * Datetime		:	2017-07-06
 * ***************************************************************************************************
 * 函数组			函数名称			函数作用
 * 
 * 接口函数
 * 			           queryKindData		           按条件获取条款信息
 *                                             initRiskSelect                            初始化险种下拉框信息
 * ***************************************************************************************************
 */

import Constant from "../constant";
import Validations from "../validations";

const ItemAPI = {

            openAddDialog(_this) {
                    _this.showAddDialog = true;
            },

            openEditDialog(_this) {
                    if (_this.selectedRows.length > 1) {
                        _this.$message({type: 'error',message: '只能选择一行数据进行编辑'});
                        return;
                    } else if (_this.selectedRows.length < 1) {
                        _this.$message({type: 'error',message: '请选择责任！'});
                        return;
                    }
                    _this.showEditDialog = true;
            },

            itemFormValidation(_this) {
                //添加标的/责任，表单非空校验
                let requiredFields = [];
                // requiredFields.push( _this.formData.itemcode);
                requiredFields.push( _this.formData.itemcname);
                requiredFields.push( _this.formData.id);
                if(!Validations.required(requiredFields,_this)) {
                        return false;
                }
                return true;
            },

            add(_this) {
                _this.loading = true;
                let api = this;

                if (!api.itemFormValidation(_this)) {
                    _this.loading = false;
                    return ;
                 }

                $.ajax({
                    type: 'POST',
                    url: Constant.urls.addItem,
                    data: JSON.stringify(_this.formData),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                             _this.$message({type: 'success',message: '新增标的成功!'});
                             api.queryItem(_this.$parent);
                             _this.off();
                        } else {
                             alert(data.result);
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                    }
                });
                _this.loading = false;
            },

            edit(_this) {
                    let api = this;
                    if (!api.itemFormValidation(_this)) {
                        _this.loading = false;
                        return ;
                     }
                    _this.$confirm('此操作将修改该责任, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                         _this.loading = true;
                         let api = this;
                         $.ajax({
                            type: 'POST',
                            url: Constant.urls.updateItem,
                            data: JSON.stringify(_this.formData),
                            dataType: 'json',
                            contentType: 'application/json',
                            success: function(data) {
                                if(data.state === 3) {
                                    _this.$router.push('/login');
                                    _this.$message({type: 'error',message: data.result});
                                    return false;
                                }
                                if(data.state == 0) {
                                    _this.$message({type: 'success',message: '修改成功!'});
                                    api.queryItem(_this.$parent);
                                    _this.off();
                                } else {
                                     alert(data.result);
                                }
                            },
                            complete:function(XMLHttpRequest,textStatus){  
                                if(textStatus=='timeout'){
                                    _this.$message({type: 'error',message: '服务器连接超时!'});
                                }
                            },  
                            error:function(XMLHttpRequest, textStatus){  
                                console.log(XMLHttpRequest);
                                console.log(textStatus);
                               _this.$message({type: 'error',message: '服务器错误!'});
                            }
                        });
                         _this.loading = false;
                    }).catch(() => {
                        _this.$message({type: 'info',message: '已取消修改'});
                    });
            },

            //责任管理页面查询接口
            queryItem(_this) {
                _this.loading = true;
                let vm = _this;
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryItem,
                    data: vm.formData,
                    dataType: 'json',
                    success: function(data) {
                        _this.loading = false;
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.tableData = data.result;
                            _this.total = data.total;
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                        _this.loading = false;
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                       _this.loading = false;
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },

            query(_this) {
                _this.loading = true;
                let vm = _this;
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryPrpdItemLibrary,
                    data: JSON.stringify(vm.formData),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function(data) {
                        _this.loading = false;
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.tableData = data.result;
                            _this.total = data.total;
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                        _this.loading = false;
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                       _this.loading = false;
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },

            queryNotRiskLinked(_this) {
                let vm = _this;
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.findItemNotRiskLinked,
                    data: JSON.stringify(vm.formData),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.tableData = data.result;
                            _this.total = data.total;
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },

            linkItem(_this) {
                    let vm = _this;
                    _this.$confirm('此操作将关联选中标的/责任, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        $.ajax({
                            type: 'POST',
                            url: Constant.urls.linkItem,
                            data: JSON.stringify(vm.formData),
                            dataType: 'json',
                            contentType: 'application/json',
                            success: function(data) {
                                if(data.state === 3) {
                                    vm.$router.push('/login');
                                    vm.$message({type: 'error',message: data.result});
                                    return false;
                                }
                                if(data.state == 0) {
                                    vm.$message({type: 'success',message: '关联成功!'});
                                    vm.off();
                                } else {
                                   alert("错误提示: \n" + data.result);
                                }
                                vm.queryData(); //重新加载表格数据
                                vm.formData.selectedRows = [];
                            }
                        });
                    }).catch(() => {
                        _this.$message({type: 'info',message: '已取消关联'});
                    });
            },

            generateItemCode(_this) {
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.generateItemCode,
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.formData.itemcode = data.result;
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    }
                });
            },
            getItemLinked(_this) {
                _this.loading = true;
                let vm = _this;
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.getKindItem,
                    data: JSON.stringify(vm.formData),
                    dataType: 'json',
                    contentType: 'application/json',
                    success: function(data) {
                        _this.loading = false;
                        if(data.state === 3) {
                            vm.$router.push('/login');
                            vm.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            vm.tableData = data.result;
                            vm.total = data.total;
                        } else {
                           vm.$message({type: 'error',message: '调用接口错误！'});
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                        _this.loading = false;
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                       _this.loading = false;
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },
            delLink(_this) {
                    let vm = _this;
                     let api = this;
                    _this.$confirm('此操作将取消选中标的/责任的关联, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {

                        let beginUnLink = false;

                        let deletable = api.checkItemLinkedDeletable(vm.formData);
                        
                        if (deletable.flag === false) {
                            if(confirm(deletable.errorMsg)) {
                                beginUnLink = true;
                            }
                        } else {
                                beginUnLink = true;
                        }

                        if (beginUnLink) {
                                $.ajax({
                                    type: 'POST',
                                    url: Constant.urls.delItemLinked,
                                    data: JSON.stringify(vm.formData),
                                    dataType: 'json',
                                    contentType: 'application/json',
                                    success: function(data) {
                                        if(data.state === 3) {
                                            vm.$router.push('/login');
                                            vm.$message({type: 'error',message: data.result});
                                            return false;
                                        }
                                        if(data.state == 0) {
                                            vm.$message({type: 'success',message: '取消关联成功!'});
                                            vm.off();
                                        } else {
                                           alert("错误提示: \n" + data.result);
                                        }
                                        vm.queryData(); //重新加载表格数据
                                        vm.formData.selectedRows = [];
                                    }
                                });
                        }
                    }).catch(() => {
                        _this.$message({type: 'info',message: '已取消关联'});
                    });
            },
            linkItem2Risk(_this) {
                let vm = _this;
                    _this.$confirm('此操作将关联选中标的/责任, 是否继续?', '提示', {
                        confirmButtonText: '确定',
                        cancelButtonText: '取消',
                        type: 'warning'
                    }).then(() => {
                        $.ajax({
                            type: 'POST',
                            url: Constant.urls.linkItem2Risk,
                            data: JSON.stringify(vm.formData),
                            dataType: 'json',
                            contentType: 'application/json',
                            success: function(data) {
                                if(data.state === 3) {
                                    vm.$router.push('/login');
                                    vm.$message({type: 'error',message: data.result});
                                    return false;
                                }
                                if(data.state == 0) {
                                    vm.$message({type: 'success',message: '关联成功!'});
                                    vm.off();
                                } else {
                                   alert("错误提示: \n" + data.result);
                                }
                                vm.queryData(); //重新加载表格数据
                                vm.formData.selectedRows = [];
                            }
                        });
                    }).catch(() => {
                        _this.$message({type: 'info',message: '已取消关联'});
                    });
            },
            checkItemLinkedDeletable(request) {
                let deletable = {
                    flag: true,
                    errorMsg: '',
                };
                $.ajax({
                        type: 'POST',
                        url: Constant.urls.checkItemLinkedDeletable,
                        data: JSON.stringify(request),
                        dataType: 'json',
                        contentType : 'application/json',
                        async : false,
                        success: function(data) {
                            if(data.state == 1) {
                                deletable.flag = false;
                                deletable.errorMsg = data.result
                            } 
                        }
                });
                return deletable;
            },
            queryItemByParent(_this) {
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryParentItems,
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                             _this.itemOptions = data.result;
                        } else {
                            if (data.result.length > 40) {
                                alert(data.result);
                            } else {
                                _this.$message({type: 'error',message: data.result});
                            }
                        }
                    },
                });
            }
}

export default ItemAPI