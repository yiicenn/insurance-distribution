/*****************************************************************************************************
 * DESC         :   API函数定义
 * Author       :   Steven.Li
 * Datetime     :   2017-07-06
 * ***************************************************************************************************
 * 函数组          函数名称            函数作用
 *
 * 接口函数
 *                     queryKindData                   按条件获取条款信息
 *                                             initRiskSelect                            初始化险种下拉框信息
 * ***************************************************************************************************
 */

import Constant from "../constant";
import Validations from "../validations";

const ClauseAPI = {

    openAddDialog(_this) {
        _this.showAddDialog = true;
    },

    openEditDialog(_this) {
        if (_this.selectedRows.length > 1) {
            _this.$message({
                type: 'error',
                message: '只能选择一行数据进行编辑'
            });
            return;
        } else if (_this.selectedRows.length < 1) {
            _this.$message({
                type: 'error',
                message: '请选择特约！'
            });
            return;
        }
        _this.showEditDialog = true;
    },

    openRiskCodeSelectDialog(_this) {
        _this.showRiskCodeSelectDialog = true;
    },
    openDefaultRiskSelectDialog(_this) {
        if (_this.formData.riskcode == '') {
            _this.$message({
                type: 'error',
                message: '请选择适用险种!'
            });
            return;
        }
        _this.showDefaultRiskSelectDialog = true;
    },
    openPlanSelectDialog(_this) {
        if (_this.formData.defaultriskcode == '') {
            _this.$message({
                type: 'error',
                message: '请选择适用默认带出险种!'
            });
            return;
        }
        _this.showPlanSelectDialog = true;
    },
    openComSelectDialog(_this) {
        _this.showComSelectDialog = true;
    },
    openDefaultComSelectDialog(_this) {
        if (_this.formData.comcode == '') {
            _this.$message({
                type: 'error',
                message: '请选择适用分公司!'
            });
            return;
        }
        _this.showDefaultComSelectDialog = true;
    },
    openAgentSelectDialog(_this) {
        if (_this.formData.comcode == '') {
            _this.$message({
                type: 'error',
                message: '请选择适用分公司!'
            });
            return;
        }
        _this.showAgentSelectDialog = true;
    },

    add(_this) {
        _this.loading = true;
        let api = this;
        if (!api.clauseFormValidation(_this)) {
            _this.loading = false;
            return;
        }
        $.ajax({
            type: 'POST',
            url: Constant.urls.addClause,
            data: _this.formData,
            dataType: 'json',
            success: function(data) {
                if (data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({
                        type: 'error',
                        message: data.result
                    });
                    return false;
                }
                if (data.state == 0) {
                    _this.$message({
                        type: 'success',
                        message: '新增特约成功!'
                    });
                    api.query(_this.$parent);
                    _this.off();
                } else {
                    alert(data.result);
                }
            },
            complete: function(XMLHttpRequest, textStatus) {  
                if (textStatus == 'timeout') {
                    _this.$message({
                        type: 'error',
                        message: '服务器连接超时!'
                    });
                }        
            },
                      error: function(XMLHttpRequest,  textStatus) {  
                console.log(XMLHttpRequest);
                console.log(textStatus);
                _this.$message({
                    type: 'error',
                    message: '服务器错误!'
                });        
            }
        }); 
        _this.loading = false;
    },

    delete(_this) {
        if (_this.selectedRows.length < 1) {
            _this.$message({
                type: 'error',
                message: '请选择条款！'
            });
            return;
        }
        _this.$confirm('此操作将删除所选特约, 是否继续?', '提示', {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'warning'
        }).then(() => {
            // TODO 执行删除逻辑
            _this.$message({
                type: 'success',
                message: '删除成功!'
            });
            _this.queryData(); //重新加载表格数据
        }).catch(() => {
            _this.$message({
                type: 'info',
                message: '已取消删除'
            });
        });
    },

    edit(_this) {
        let api = this;
        if (!api.clauseFormValidation(_this)) {
            return;
        }
        _this.$confirm('此操作将修改该特约, 是否继续?', '提示', {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'warning'
        }).then(() => {
            _this.loading = true;
            let api = this;
            $.ajax({
                type: 'POST',
                url: Constant.urls.updatePrpdClause,
                data: _this.formData,
                dataType: 'json',
                success: function(data) {
                    if (data.state === 3) {
                        _this.$router.push('/login');
                        _this.$message({
                            type: 'error',
                            message: data.result
                        });
                        return false;
                    }
                    if (data.state == 0) {
                        _this.$message({
                            type: 'success',
                            message: '修改成功!'
                        });
                        api.query(_this.$parent);
                        _this.off();
                    } else {
                        alert(data.result);
                    }
                },
                complete: function(XMLHttpRequest, textStatus) {  
                    if (textStatus == 'timeout') {
                        _this.$message({
                            type: 'error',
                            message: '服务器连接超时!'
                        });
                    }        
                },
                          error: function(XMLHttpRequest,  textStatus) {  
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    _this.$message({
                        type: 'error',
                        message: '服务器错误!'
                    });        
                }
            });
            _this.loading = false;
        }).catch(() => {
            _this.$message({
                type: 'info',
                message: '已取消修改'
            });
        });
    },

    query(_this) {
        _this.loading = true;
        if (_this.formData.createdate === '') {
            _this.formData.createdate = undefined;
        }
        $.ajax({
            type: 'POST',
            url: Constant.urls.queryPrpdClause,
            data: _this.formData,
            dataType: 'json',
            success: function(data) {
                _this.loading = false;
                if (data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({
                        type: 'error',
                        message: data.result
                    });
                    return false;
                }
                if (data.state == 0) {
                    _this.tableData = data.result;
                    _this.total = data.total;
                } else {
                    _this.$message({
                        type: 'error',
                        message: '调用接口错误！'
                    });
                }
            },
            complete: function(XMLHttpRequest, textStatus) {  
                if (textStatus == 'timeout') {
                    _this.$message({
                        type: 'error',
                        message: '服务器连接超时!'
                    });
                }            
                _this.loading = false;        
            },
                      error: function(XMLHttpRequest,  textStatus) {  
                console.log(XMLHttpRequest);
                console.log(textStatus);
                _this.$message({
                    type: 'error',
                    message: '服务器错误!'
                });           
                _this.loading = false;        
            }
        });
        setTimeout(function() {
            _this.isSortRequest = true;
        }, 500);
    },

    queryPrpdClauseCodeNo(_this) {
        $.ajax({
            type: 'POST',
            url: Constant.urls.queryPrpdClauseCodeNo,
            data: {},
            dataType: 'json',
            success: function(data) {
                if (data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({
                        type: 'error',
                        message: data.result
                    });
                    return false;
                }
                if (data.state == 0) {
                    _this.formData.clausecode = data.result;
                } else {
                    _this.$message({
                        type: 'error',
                        message: '调用接口错误！'
                    });
                }
            }
        });
        setTimeout(function() {
            _this.isSortRequest = true;
        }, 500);
    },

    queryPlanOptions(_this) {
        let planOptions = [];
        $.ajax({
            type: 'POST',
            url: Constant.urls.queryPlansByRisks,
            data: {
                riskCodes: _this.formData.defaultriskcode,
            },
            dataType: 'json',
            async: false,
            success: function(data) {
                if (data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({
                        type: 'error',
                        message: data.result
                    });
                    return false;
                }
                if (data.state == 0) {
                    planOptions = data.result;
                }
            }
        });
        return planOptions;
    },

    queryPlansByRisks(_this) {
        $.ajax({
            type: 'POST',
            url: Constant.urls.queryPlansByRisks,
            data: {
                riskCodes: _this.$parent.formData.defaultriskcode,
            },
            dataType: 'json',
            success: function(data) {
                if (data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({
                        type: 'error',
                        message: data.result
                    });
                    return false;
                }
                if (data.state == 0) {
                  _this.planOptions = data.result;
                  _this.planOptionsAll.push({riskcode:"ALL",plancode:"ALL",plancname:"全部方案"});
                  for(let i = 0; i < data.result.length; i++) {
                    _this.planOptionsAll.push(data.result[i]);
                  }
                } else {
                    _this.$message({
                        type: 'error',
                        message: '调用接口错误！'
                    });
                }
            }
        });
        setTimeout(function() {
            _this.isSortRequest = true;
        }, 500);
    },
    queryCodeRiskByClause(_this) {
        let api = this;
        $.ajax({
            type: 'POST',
            url: Constant.urls.queryCodeRiskByClause,
            data: {
                clausecode: _this.formData.clausecode,
            },
            dataType: 'json',
            success: function(data) {
                if (data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({
                        type: 'error',
                        message: data.result
                    });
                    return false;
                }
                if (data.state == 0) {
                    _this.riskCodesLabel = data.result;
                    if (_this.riskCodesLabel != null && _this.riskCodesLabel != '') {
                        _this.arrayRiskcodes = _this.riskCodesLabel.split(',');
                    }
                    api.queryRiskClauseByClause(_this);
                } else {
                    _this.$message({
                        type: 'error',
                        message: '调用接口错误！'
                    });
                }
            }
        });
        setTimeout(function() {
            _this.isSortRequest = true;
        }, 500);
    },

    queryRiskClauseByClause(_this) {
        $.ajax({
            type: 'POST',
            url: Constant.urls.queryRiskClauseByClause,
            data: {
                clausecode: _this.formData.clausecode,
            },
            dataType: 'json',
            success: function(data) {
                if (data.state === 3) {
                    _this.$router.push('/login');
                    _this.$message({
                        type: 'error',
                        message: data.result
                    });
                    return false;
                }
                if (data.state == 0) {
                    if (data.result != null && data.result != '') {
                        let defaultRiskLabel = '';
                        let riskClause = {};
                        for (let i = 0; i < data.result.length; i++) {
                            defaultRiskLabel += data.result[i].riskcode + ',';
                            riskClause = data.result[i];
                        }
                        //设置默认带出险种
                        _this.defaultRiskLabel = defaultRiskLabel.substring(0, defaultRiskLabel.length - 1);
                        if (_this.defaultRiskLabel != null && _this.defaultRiskLabel != '') {
                            _this.arrayDefaultRisk = _this.defaultRiskLabel.split(',');
                        }
                        //设置适用默认带出险种方案
                        _this.planLabel = riskClause.plancode;
                        if (_this.planLabel != null && _this.planLabel != '') {
                            _this.arrayPlancodes = _this.planLabel.split(',');
                        }
                        //设置默认带出分公司
                        _this.defaultComLabel = riskClause.comcode;
                        if (_this.defaultComLabel != null && _this.defaultComLabel != '') {
                            _this.arrayDefaultCom = _this.defaultComLabel.split(',');
                        }

                        _this.originalRisk = _this.riskCodesLabel;
                        _this.originalDefaultRisk = _this.defaultRiskLabel;
                        _this.originalCom = _this.comLabel;
                        _this.originalDefaultCom = _this.defaultComLabel;
                    }
                } else {
                    _this.$message({
                        type: 'error',
                        message: '调用接口错误！'
                    });
                }
            }
        });
        setTimeout(function() {
            _this.isSortRequest = true;
        }, 500);
    },
    openLogDialog(_this) {
        if (_this.selectedRows.length > 1) {
            _this.$message({
                type: 'error',
                message: '只能选择一行数据进行查看'
            });
            return;
        } else if (_this.selectedRows.length < 1) {
            _this.$message({
                type: 'error',
                message: '请选择特约！'
            });
            return;
        }
        _this.logBusinessKey = _this.selectedRows[0].clausename;
        _this.logBusinessKeyValue = _this.selectedRows[0].clausecode;
        _this.showLogDialog = true;
    },
    clauseFormValidation(_this) {
        let requiredFields = [];
        // requiredFields.push( _this.formData.clausecode);
        requiredFields.push(_this.formData.clausename);
        requiredFields.push(_this.formData.language);
        requiredFields.push(_this.formData.validstatus);
        requiredFields.push(_this.formData.riskcode);
        requiredFields.push(_this.formData.comcode);
        requiredFields.push(_this.formData.context);
        if (!Validations.required(requiredFields, _this)) {
            return false;
        }
        return true;
    },
}

export
default ClauseAPI
