/*****************************************************************************************************
 * DESC			:	API函数定义
 * Author		:	Steven.Li
 * Datetime		:	2017-07-06
 * ***************************************************************************************************
 * 函数组			函数名称			函数作用
 * 
 * 接口函数
 * 			           queryKindData		           按条件获取条款信息
 *                                             initRiskSelect                            初始化险种下拉框信息
 * ***************************************************************************************************
 */

import Constant from "../constant";

const PropsAPI = {

            initPropsSelect(_this) {
                    $.ajax({
                    type: 'POST',
                    url: Constant.urls.findProps,
                    dataType: 'json',
                    async : false,
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                             _this.propsOptions = data.result;
                        } else {
                             if (data.result.length > 40) {
                                alert(data.result);
                            } else {
                                _this.$message({type: 'error',message: data.result});
                            }
                        }
                    }
                });
            },
            initPlanProps(_this) {
                    _this.formData.plan.createdate = undefined;
                    _this.formData.plan.updatedate = undefined;
                    $.ajax({
                    type: 'POST',
                    url: Constant.urls.findPlanProps,
                    data: _this.formData.plan,
                    dataType: 'json',
                    async : false,
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            let props = data.result
                            _this.index = props.length;
                            for (let i = 0; i < props.length; i++) {
                                props[i].rowId = i + 1;
                                _this.formData.props.push(props[i]);
                            }
                        } else {
                             if (data.result.length > 40) {
                                alert(data.result);
                            } else {
                                _this.$message({type: 'error',message: data.result});
                            }
                        }
                    }
                });
            },
            add(_this) {
                 $.ajax({
                    type: 'POST',
                    url: Constant.urls.addProps,
                    data: JSON.stringify(_this.formData),
                    dataType: 'json',
                    contentType: 'application/json',
                    async : false,
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.$message({type: 'success',message: '方案因子配置成功!'});
                            _this.off();
                        } else {
                             if (data.result.length > 40) {
                                alert(data.result);
                            } else {
                                _this.$message({type: 'error',message: data.result});
                            }
                        }
                    },
                    complete:function(XMLHttpRequest,textStatus){  
                        if(textStatus=='timeout'){
                            _this.$message({type: 'error',message: '服务器连接超时!'});
                        }
                    },  
                    error:function(XMLHttpRequest, textStatus){  
                        console.log(XMLHttpRequest);
                        console.log(textStatus);
                       _this.$message({type: 'error',message: '服务器错误!'});
                    }
                });
            },
           
}

export default PropsAPI