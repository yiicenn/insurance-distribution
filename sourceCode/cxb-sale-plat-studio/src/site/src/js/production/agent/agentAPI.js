/*****************************************************************************************************
 * DESC			:	API函数定义
 * Author		:	Steven.Li
 * Datetime		:	2017-07-06
 * ***************************************************************************************************
 * 函数组			函数名称			函数作用
 * 
 * 接口函数
 * 			           queryKindData		           按条件获取条款信息
 *                                             initRiskSelect                            初始化险种下拉框信息
 * ***************************************************************************************************
 */

import Constant from "../constant";
import Validations from "../validations";

const AgentAPI = {
	
          queryAgent(_this) {
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryAgentByComCode,
                    data: {
                        comCodes :  _this.$parent.formData.comcode
                    },
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            // _this.agentOptions = data.result;
                            for(let index in data.result) {
                                _this.agentOptions.push({
                                    userCode: data.result[index].userCode,
                                    userName: data.result[index].userName,
                                });
                            }
                        } else {
                          alert(data.result);
                        }
                    }
                });
                 setTimeout(function(){
                    _this.isSortRequest = true;
                },500);
            },

            queryAgentByChange(_this) {
                let agents = [];
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryAgentByComCode,
                    data: {
                        comCodes :  _this.formData.comcode
                    },
                    dataType: 'json',
                    async: false,
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            agents = data.result;
                        }
                    }
                });
                return agents;
            },
}

export default AgentAPI