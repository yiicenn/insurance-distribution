import {  requestLogin,checkImgCode} from '../api/api';
import common from '../common/js/common';

export default {
  login(_this) {
    try {
      _this.$refs.ruleForm2.validate((valid) => {
        if (valid) {
          //_this.$router.replace('/table');
          _this.logining = true;

          let para = new FormData();
          para.append("imgCode", _this.ruleForm2.imgCode);

          checkImgCode(para).then((res) => {
            _this.logining = false;
            console.log(res);
            if(res.data.state != 0){
              _this.$message({
                message: res.data.result,
                type: 'error'
              });
              _this.changeImgCode();//更换图形码
            }else{
              let para = new FormData();
              para.append("username", _this.ruleForm2.account);
              para.append("password", _this.ruleForm2.checkPass);
              //NProgress.start();
              requestLogin(para).then((res) => {
                _this.logining = false;
                //let loginmsg = xhr.getResponseHeader("userInfo");
                //sessionStorage.setItem('user', JSON.stringify(loginmsg));

                if (!res.data) {
                  _this.$message({
                    message: '帐号或密码错误',
                    type: 'error'
                  });
                } else {
                  if (res.headers.success == "true") {
                    sessionStorage.setItem('user', JSON.stringify(res.data));
                    if (!common.isEmpty(res.data.menus)) {
                      for (var i in res.data.menus) {
                        if (!common.isEmpty(res.data.menus[i].child)) {
                          let firstChild = res.data.menus[i].child;
                          for (var j in firstChild) {
                            if (!common.isEmpty(firstChild[j].resIds)) {
                              sessionStorage.setItem(firstChild[j].menuid, JSON.stringify(firstChild[j].resIds));
                            }
                          }

                        }
                      }

                    }
                    _this.$router.push({ path: '/index' });

                  }
                  else {
                    if (data.success == false) {
                      _this.$message({
                        message: data.result,
                        type: 'error'
                      });

                    }
                  }
                }
                console.log(res);
                this.listLoading = false;
                //NProgress.done();
              }).catch(() => {
                this.listLoading = false;
              });
            }
            this.listLoading = false;
          }).catch(() => {
            this.listLoading = false;
          });
        } else {
          console.log('error submit!!');
          return false;
        }
      });
    } catch (err) {
    }
  }
}
