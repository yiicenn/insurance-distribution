DELIMITER $$

USE `cxb_plat`$$

DROP FUNCTION IF EXISTS `getChildList`$$

CREATE DEFINER=`cxb_uat`@`%` FUNCTION `getChildList`(rootId VARCHAR(1000)) RETURNS VARCHAR(1000) CHARSET utf8
BEGIN 
       DECLARE pTemp VARCHAR(1000);  
       DECLARE cTemp VARCHAR(1000);  
       SET pTemp = '$';  
       SET cTemp =CAST(rootId AS CHAR);  
       WHILE cTemp IS NOT NULL DO  
         SET pTemp = CONCAT(pTemp,',',cTemp); 
         SELECT GROUP_CONCAT(BRANCH_NO) INTO cTemp FROM tb_sys_branch   
         WHERE FIND_IN_SET(UPPER_BRANCH_NO,cTemp)>0; 
       END WHILE;  
       RETURN pTemp;  
     END$$

DELIMITER ;