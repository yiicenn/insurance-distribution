package cn.com.libertymutual.sp.dto.queryplans;

import java.io.Serializable;
import java.util.List;

import cn.com.libertymutual.core.base.dto.ResponseBaseDto;

import com.google.common.collect.Lists;


public class QueryPlansResponseDTO extends ResponseBaseDto implements
Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<CrossSalePlan> planLists;

	public List<CrossSalePlan> getPlanLists() {
		if(null==planLists){
			planLists=Lists.newArrayList();
		}
		return planLists;
	}

	public void setPlanLists(List<CrossSalePlan> planLists) {
		this.planLists = planLists;
	}
	
}
