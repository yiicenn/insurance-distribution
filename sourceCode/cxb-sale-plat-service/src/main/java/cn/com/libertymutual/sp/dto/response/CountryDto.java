package cn.com.libertymutual.sp.dto.response;

public class CountryDto {
	private String cName;
	private String eName;
	public String getcName() {
		return cName;
	}
	public void setcName(String cName) {
		this.cName = cName;
	}
	public String geteName() {
		return eName;
	}
	public void seteName(String eName) {
		this.eName = eName;
	}
	
}
