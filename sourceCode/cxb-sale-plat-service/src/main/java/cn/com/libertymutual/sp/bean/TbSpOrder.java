package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "tb_sp_order")
public class TbSpOrder implements Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -1309299682620082102L;
	/**
	 * 
	 */
	public static final String ERROR_01 = "01:投保失败,日期错误";
	public static final String ERROR_02 = "02:投保失败,重新计算价格错误";
	public static final String ERROR_03 = "03:投保失败,请求超时";
	public static final String ERROR_04 = "04:投保失败,投保失败,请求超时或者返回信息不存在";
	public static final String ERROR_05 = "05:返回信息不存在responseJson";
	public static final String ERROR_06 = "06:信息表回写失败";
	public static final String ERROR_07 = "07:订单表回写失败";

	public static final String STATUS_UNDERWRITING = "7"; // 核保

	public static final String STATUS_NO_POLIICY = "18"; // 有效
	public static final String STATUS_FAIL = "0"; // 支付失败
	public static final String STATUS_EFFECTIVE = "1"; // 有效
	public static final String STATUS_NO_PAY = "2"; // 未支付
	public static final String STATUS_INVALID = "3"; // 无效

	public static final String STATUS_ALLOW = "1"; // 允许
	public static final String STATUS_PROHIBIT = "0"; // 禁止

	private Integer id;
	private String orderNo;
	private String userId;
	private String status;
	private Double oldPremium;
	private Double amount;
	private String riskName;
	private String riskCode;
	private String planCode;
	private String planName;
	private String planId;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date startDate;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date endDate;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;
	private String remark;
	private String orderDetail;
	private String productId;
	private String productName;
	private String houseAddress;
	private String destination;
	private String payWay;
	private String paymentNo;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date payDate;
	private String refereeId;
	private String branchCode;
	private String agreementNo;
	private String additionalInfo;
	private String applicantName;
	private String saleType;
	private String houseType;
	private String address;
	private String policyNo;
	private String proposalNo;
	private String idNo;
	private String clauseUrl;
	private String travelDestination;// 旅行目的地
	private String branchOffice;// 落地分公司
	private String insureCity;
	private String plateNum;// 车牌号
	private String frameNum;// 车架号
	private String comCode;// 归属机构
	private String agentName;

	private String relationId;

	private String planRiskCode;// 车架号

	private Integer count;

	private List<TbSpApplicant> applicant;
	private List<Object> upUserSocre;

	private String invoiceStatus; // 是否显示发票
	private String insurancePolicyStatus; // 是否显示保单
	private TbSpUser user;// 推荐人信息
	private TbSpUser refereeUser;// 推荐人信息
	private String callBackUrl;
	private String singleMember;
	private String successfulUrl;
	private String shareUid;
	private String dealUuid;
	// private String accessUrl;
	private String channelType;// 客户单位类型
	private String customName;// 客户单位名称
	private String commissionRate;// 佣金比例
	private String endorseNo;
	private String relState;
	private String salesmanName;
	private String salesmanCode;

	@Column(name = "SALESMAN_NAME")
	public String getSalesmanName() {
		return salesmanName;
	}

	public void setSalesmanName(String salesmanName) {
		this.salesmanName = salesmanName;
	}

	@Column(name = "SALESMAN_CODE")
	public String getSalesmanCode() {
		return salesmanCode;
	}

	public void setSalesmanCode(String salesmanCode) {
		this.salesmanCode = salesmanCode;
	}

	@Transient
	public String getRelState() {
		return relState;
	}

	public void setRelState(String relState) {
		this.relState = relState;
	}

	@Column(name = "AGENT_NAME")
	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	@Column(name = "RELATION_ID")
	public String getRelationId() {
		return relationId;
	}

	public void setRelationId(String relationId) {
		this.relationId = relationId;
	}

	@Column(name = "ADDITIONAL_INFO")
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	@Column(name = "PLAN_RISK_CODE")
	public String getPlanRiskCode() {
		return planRiskCode;
	}

	public void setPlanRiskCode(String planRiskCode) {
		this.planRiskCode = planRiskCode;
	}

	@Column(name = "ENDORSE_NO")
	public String getEndorseNo() {
		return endorseNo;
	}

	public void setEndorseNo(String endorseNo) {
		this.endorseNo = endorseNo;
	}

	@Column(name = "COMMISSION_RATE")
	public String getCommissionRate() {
		return commissionRate;
	}

	public void setCommissionRate(String commissionRate) {
		this.commissionRate = commissionRate;
	}

	@Column(name = "CHANNEL_TYPE")
	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	@Column(name = "CUSTOM_NAME")
	public String getCustomName() {
		return customName;
	}

	public void setCustomName(String customName) {
		this.customName = customName;
	}

	@Column(name = "ID_NO")
	public String getIdNo() {
		return idNo;
	}

	public void setIdNo(String idNo) {
		this.idNo = idNo;
	}

	@Column(name = "DEAL_UUID")
	public String getDealUuid() {
		return dealUuid;
	}

	public void setDealUuid(String dealUuid) {
		this.dealUuid = dealUuid;
	}

	@Column(name = "COM_CODE")
	public String getComCode() {
		return comCode;
	}

	public void setComCode(String comCode) {
		this.comCode = comCode;
	}

	@Column(name = "PLAN_ID")
	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	@Column(name = "INSURE_CITY")
	public String getInsureCity() {
		return insureCity;
	}

	// @Column(name = "ACCESS_URL")
	// public String getAccessUrl() {
	// return accessUrl;
	// }
	//
	// public void setAccessUrl(String accessUrl) {
	// this.accessUrl = accessUrl;
	// }

	public void setInsureCity(String insureCity) {
		this.insureCity = insureCity;
	}

	@Column(name = "CALLBACK_URL")
	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	@Column(name = "SUCCESSFUL_URL")
	public String getSuccessfulUrl() {
		return successfulUrl;
	}

	public void setSuccessfulUrl(String successfulUrl) {
		this.successfulUrl = successfulUrl;
	}

	@Column(name = "SHARE_UID")
	public String getShareUid() {
		return shareUid;
	}

	public void setShareUid(String shareUid) {
		this.shareUid = shareUid;
	}

	@Column(name = "SINGLE_MEMBER")
	public String getSingleMember() {
		return singleMember;
	}

	public void setSingleMember(String singleMember) {
		this.singleMember = singleMember;
	}

	@Column(name = "PLATE_NUM")
	public String getPlateNum() {
		return plateNum;
	}

	public void setPlateNum(String plateNum) {
		this.plateNum = plateNum;
	}

	@Column(name = "FRAME_NUM")
	public String getFrameNum() {
		return frameNum;
	}

	public void setFrameNum(String frameNum) {
		this.frameNum = frameNum;
	}

	@Column(name = "clause_url")
	public String getClauseUrl() {
		return clauseUrl;
	}

	public void setClauseUrl(String clauseUrl) {
		this.clauseUrl = clauseUrl;
	}

	// Constructors
	@Column(name = "Policy_No")
	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	/** default constructor */
	public TbSpOrder() {
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "ORDER_NO")
	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Column(name = "USER_ID")
	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "STATUS")
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "OLD_PREMIUM", precision = 22, scale = 0)
	public Double getOldPremium() {
		return oldPremium;
	}

	public void setOldPremium(Double oldPremium) {
		this.oldPremium = oldPremium;
	}

	@Column(name = "AMOUNT", precision = 22, scale = 0)
	public Double getAmount() {
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	@Column(name = "RISK_NAME")
	public String getRiskName() {
		return this.riskName;
	}

	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}

	@Column(name = "RISK_CODE")
	public String getRiskCode() {
		return this.riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	@Column(name = "PLAN_CODE")
	public String getPlanCode() {
		return this.planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	@Column(name = "PLAN_NAME")
	public String getPlanName() {
		return this.planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	// @Column(name = "OWNER", length = 100)
	// public String getOwner() {
	// return this.owner;
	// }
	//
	// public void setOwner(String owner) {
	// this.owner = owner;
	// }

	@Column(name = "START_DATE")
	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Column(name = "END_DATE")
	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Column(name = "CREATE_TIME")
	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "UPDATE_TIME")
	public Date getUpdateTime() {
		return this.updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name = "REMARK")
	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column(name = "ORDER_DETAIL")
	public String getOrderDetail() {
		return this.orderDetail;
	}

	public void setOrderDetail(String orderDetail) {
		this.orderDetail = orderDetail;
	}

	@Column(name = "PRODUCT_ID")
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME")
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "HOUSE_ADDRESS")
	public String getHouseAddress() {
		return houseAddress;
	}

	public void setHouseAddress(String houseAddress) {
		this.houseAddress = houseAddress;
	}

	@Column(name = "Destination")
	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		destination = destination;
	}

	@Column(name = "PAYWAY")
	public String getPayWay() {
		return payWay;
	}

	public void setPayWay(String payWay) {
		this.payWay = payWay;
	}

	@Column(name = "PAYMENT_NO")
	public String getPaymentNo() {
		return paymentNo;
	}

	public void setPaymentNo(String paymentNo) {
		this.paymentNo = paymentNo;
	}

	@Column(name = "PAY_DATE")
	public Date getPayDate() {
		return payDate;
	}

	public void setPayDate(Date payDate) {
		this.payDate = payDate;
	}

	@Column(name = "Referee_ID")
	public String getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(String refereeId) {
		this.refereeId = refereeId;
	}

	@Column(name = "BRANCH_CODE")
	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	@Column(name = "AGREEMENT_NO")
	public String getAgreementNo() {
		return agreementNo;
	}

	public void setAgreementNo(String agreementNo) {
		this.agreementNo = agreementNo;
	}

	@Column(name = "APPLICANT_NAME")
	public String getApplicantName() {
		return applicantName;
	}

	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}

	@Column(name = "SALE_TYPE")
	public String getSaleType() {
		return saleType;
	}

	public void setSaleType(String saleType) {
		this.saleType = saleType;
	}

	@Column(name = "HOUSE_TYPE")
	public String getHouseType() {
		return houseType;
	}

	@Column(name = "Proposal_No")
	public String getProposalNo() {
		return proposalNo;
	}

	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}

	@Column(name = "Travel_Destination")
	public String getTravelDestination() {
		return travelDestination;
	}

	public void setTravelDestination(String travelDestination) {
		this.travelDestination = travelDestination;
	}

	@Column(name = "Branch_Office")
	public String getBranchOffice() {
		return branchOffice;
	}

	public void setBranchOffice(String branchOffice) {
		this.branchOffice = branchOffice;
	}

	@Transient
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	@Transient
	public List<TbSpApplicant> getApplicant() {
		return applicant;
	}

	public void setApplicant(List<TbSpApplicant> applicant) {
		this.applicant = applicant;
	}

	@Transient
	public String getInvoiceStatus() {
		return invoiceStatus;
	}

	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}

	@Transient
	public String getInsurancePolicyStatus() {
		return insurancePolicyStatus;
	}

	public void setInsurancePolicyStatus(String insurancePolicyStatus) {
		this.insurancePolicyStatus = insurancePolicyStatus;
	}

	public TbSpOrder(String orderNo, String userId, String status, Double amount, String riskName, String riskCode, String planCode, String planName,
			Date startDate, Date endDate, Date createTime, Date updateTime, String remark, String orderDetail, String productId, String productName,
			String houseAddress, String destination, String payWay, String paymentNo, Date payDate, String refereeId, String branchCode,
			String agreementNo, String applicantName, String saleType, String houseType, String address) {
		super();
		this.orderNo = orderNo;
		this.userId = userId;
		this.status = status;
		this.amount = amount;
		this.riskName = riskName;
		this.riskCode = riskCode;
		this.planCode = planCode;
		this.planName = planName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.createTime = createTime;
		this.updateTime = updateTime;
		this.remark = remark;
		this.orderDetail = orderDetail;
		this.productId = productId;
		this.productName = productName;
		this.houseAddress = houseAddress;
		destination = destination;
		this.payWay = payWay;
		this.paymentNo = paymentNo;
		this.payDate = payDate;
		this.refereeId = refereeId;
		this.branchCode = branchCode;
		this.agreementNo = agreementNo;
		this.applicantName = applicantName;
		this.saleType = saleType;
		this.houseType = houseType;
		this.address = address;
	}

	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}

	@Column(name = "ADDRESS")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Transient
	public TbSpUser getRefereeUser() {
		return refereeUser;
	}

	public void setRefereeUser(TbSpUser refereeUser) {
		this.refereeUser = refereeUser;
	}

	@Transient
	public TbSpUser getUser() {
		return user;
	}

	public void setUser(TbSpUser user) {
		this.user = user;
	}

	// private boolean scoreU = true;
	//
	// @Transient
	// public boolean getScoreU() {
	// return scoreU;
	// }
	//
	// public void setScoreU(boolean scoreU) {
	// this.scoreU = scoreU;
	// }

	@Transient
	public List<Object> getUpUserSocre() {
		return upUserSocre;
	}

	public void setUpUserSocre(List<Object> upUserSocre) {
		this.upUserSocre = upUserSocre;
	}

}
