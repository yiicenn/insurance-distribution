package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrpdriskplanExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpdriskplanExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRiskcodeIsNull() {
            addCriterion("RISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIsNotNull() {
            addCriterion("RISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeEqualTo(String value) {
            addCriterion("RISKCODE =", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotEqualTo(String value) {
            addCriterion("RISKCODE <>", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThan(String value) {
            addCriterion("RISKCODE >", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThanOrEqualTo(String value) {
            addCriterion("RISKCODE >=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThan(String value) {
            addCriterion("RISKCODE <", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThanOrEqualTo(String value) {
            addCriterion("RISKCODE <=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLike(String value) {
            addCriterion("RISKCODE like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotLike(String value) {
            addCriterion("RISKCODE not like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIn(List<String> values) {
            addCriterion("RISKCODE in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotIn(List<String> values) {
            addCriterion("RISKCODE not in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeBetween(String value1, String value2) {
            addCriterion("RISKCODE between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotBetween(String value1, String value2) {
            addCriterion("RISKCODE not between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskversionIsNull() {
            addCriterion("RISKVERSION is null");
            return (Criteria) this;
        }

        public Criteria andRiskversionIsNotNull() {
            addCriterion("RISKVERSION is not null");
            return (Criteria) this;
        }

        public Criteria andRiskversionEqualTo(String value) {
            addCriterion("RISKVERSION =", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotEqualTo(String value) {
            addCriterion("RISKVERSION <>", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionGreaterThan(String value) {
            addCriterion("RISKVERSION >", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionGreaterThanOrEqualTo(String value) {
            addCriterion("RISKVERSION >=", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLessThan(String value) {
            addCriterion("RISKVERSION <", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLessThanOrEqualTo(String value) {
            addCriterion("RISKVERSION <=", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLike(String value) {
            addCriterion("RISKVERSION like", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotLike(String value) {
            addCriterion("RISKVERSION not like", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionIn(List<String> values) {
            addCriterion("RISKVERSION in", values, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotIn(List<String> values) {
            addCriterion("RISKVERSION not in", values, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionBetween(String value1, String value2) {
            addCriterion("RISKVERSION between", value1, value2, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotBetween(String value1, String value2) {
            addCriterion("RISKVERSION not between", value1, value2, "riskversion");
            return (Criteria) this;
        }

        public Criteria andPlancodeIsNull() {
            addCriterion("PLANCODE is null");
            return (Criteria) this;
        }

        public Criteria andPlancodeIsNotNull() {
            addCriterion("PLANCODE is not null");
            return (Criteria) this;
        }

        public Criteria andPlancodeEqualTo(String value) {
            addCriterion("PLANCODE =", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotEqualTo(String value) {
            addCriterion("PLANCODE <>", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeGreaterThan(String value) {
            addCriterion("PLANCODE >", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeGreaterThanOrEqualTo(String value) {
            addCriterion("PLANCODE >=", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLessThan(String value) {
            addCriterion("PLANCODE <", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLessThanOrEqualTo(String value) {
            addCriterion("PLANCODE <=", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLike(String value) {
            addCriterion("PLANCODE like", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotLike(String value) {
            addCriterion("PLANCODE not like", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeIn(List<String> values) {
            addCriterion("PLANCODE in", values, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotIn(List<String> values) {
            addCriterion("PLANCODE not in", values, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeBetween(String value1, String value2) {
            addCriterion("PLANCODE between", value1, value2, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotBetween(String value1, String value2) {
            addCriterion("PLANCODE not between", value1, value2, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancnameIsNull() {
            addCriterion("PLANCNAME is null");
            return (Criteria) this;
        }

        public Criteria andPlancnameIsNotNull() {
            addCriterion("PLANCNAME is not null");
            return (Criteria) this;
        }

        public Criteria andPlancnameEqualTo(String value) {
            addCriterion("PLANCNAME =", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameNotEqualTo(String value) {
            addCriterion("PLANCNAME <>", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameGreaterThan(String value) {
            addCriterion("PLANCNAME >", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameGreaterThanOrEqualTo(String value) {
            addCriterion("PLANCNAME >=", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameLessThan(String value) {
            addCriterion("PLANCNAME <", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameLessThanOrEqualTo(String value) {
            addCriterion("PLANCNAME <=", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameLike(String value) {
            addCriterion("PLANCNAME like", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameNotLike(String value) {
            addCriterion("PLANCNAME not like", value, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameIn(List<String> values) {
            addCriterion("PLANCNAME in", values, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameNotIn(List<String> values) {
            addCriterion("PLANCNAME not in", values, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameBetween(String value1, String value2) {
            addCriterion("PLANCNAME between", value1, value2, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlancnameNotBetween(String value1, String value2) {
            addCriterion("PLANCNAME not between", value1, value2, "plancname");
            return (Criteria) this;
        }

        public Criteria andPlanenameIsNull() {
            addCriterion("PLANENAME is null");
            return (Criteria) this;
        }

        public Criteria andPlanenameIsNotNull() {
            addCriterion("PLANENAME is not null");
            return (Criteria) this;
        }

        public Criteria andPlanenameEqualTo(String value) {
            addCriterion("PLANENAME =", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameNotEqualTo(String value) {
            addCriterion("PLANENAME <>", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameGreaterThan(String value) {
            addCriterion("PLANENAME >", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameGreaterThanOrEqualTo(String value) {
            addCriterion("PLANENAME >=", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameLessThan(String value) {
            addCriterion("PLANENAME <", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameLessThanOrEqualTo(String value) {
            addCriterion("PLANENAME <=", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameLike(String value) {
            addCriterion("PLANENAME like", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameNotLike(String value) {
            addCriterion("PLANENAME not like", value, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameIn(List<String> values) {
            addCriterion("PLANENAME in", values, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameNotIn(List<String> values) {
            addCriterion("PLANENAME not in", values, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameBetween(String value1, String value2) {
            addCriterion("PLANENAME between", value1, value2, "planename");
            return (Criteria) this;
        }

        public Criteria andPlanenameNotBetween(String value1, String value2) {
            addCriterion("PLANENAME not between", value1, value2, "planename");
            return (Criteria) this;
        }

        public Criteria andSerialnoIsNull() {
            addCriterion("SERIALNO is null");
            return (Criteria) this;
        }

        public Criteria andSerialnoIsNotNull() {
            addCriterion("SERIALNO is not null");
            return (Criteria) this;
        }

        public Criteria andSerialnoEqualTo(Long value) {
            addCriterion("SERIALNO =", value, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoNotEqualTo(Long value) {
            addCriterion("SERIALNO <>", value, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoGreaterThan(Long value) {
            addCriterion("SERIALNO >", value, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoGreaterThanOrEqualTo(Long value) {
            addCriterion("SERIALNO >=", value, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoLessThan(Long value) {
            addCriterion("SERIALNO <", value, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoLessThanOrEqualTo(Long value) {
            addCriterion("SERIALNO <=", value, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoIn(List<Long> values) {
            addCriterion("SERIALNO in", values, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoNotIn(List<Long> values) {
            addCriterion("SERIALNO not in", values, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoBetween(Long value1, Long value2) {
            addCriterion("SERIALNO between", value1, value2, "serialno");
            return (Criteria) this;
        }

        public Criteria andSerialnoNotBetween(Long value1, Long value2) {
            addCriterion("SERIALNO not between", value1, value2, "serialno");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNull() {
            addCriterion("VALIDSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNotNull() {
            addCriterion("VALIDSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidstatusEqualTo(String value) {
            addCriterion("VALIDSTATUS =", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotEqualTo(String value) {
            addCriterion("VALIDSTATUS <>", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThan(String value) {
            addCriterion("VALIDSTATUS >", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS >=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThan(String value) {
            addCriterion("VALIDSTATUS <", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS <=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLike(String value) {
            addCriterion("VALIDSTATUS like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotLike(String value) {
            addCriterion("VALIDSTATUS not like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusIn(List<String> values) {
            addCriterion("VALIDSTATUS in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotIn(List<String> values) {
            addCriterion("VALIDSTATUS not in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS not between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andPlandescIsNull() {
            addCriterion("PLANDESC is null");
            return (Criteria) this;
        }

        public Criteria andPlandescIsNotNull() {
            addCriterion("PLANDESC is not null");
            return (Criteria) this;
        }

        public Criteria andPlandescEqualTo(String value) {
            addCriterion("PLANDESC =", value, "plandesc");
            return (Criteria) this;
        }

        public Criteria andPlandescNotEqualTo(String value) {
            addCriterion("PLANDESC <>", value, "plandesc");
            return (Criteria) this;
        }

        public Criteria andPlandescGreaterThan(String value) {
            addCriterion("PLANDESC >", value, "plandesc");
            return (Criteria) this;
        }

        public Criteria andPlandescGreaterThanOrEqualTo(String value) {
            addCriterion("PLANDESC >=", value, "plandesc");
            return (Criteria) this;
        }

        public Criteria andPlandescLessThan(String value) {
            addCriterion("PLANDESC <", value, "plandesc");
            return (Criteria) this;
        }

        public Criteria andPlandescLessThanOrEqualTo(String value) {
            addCriterion("PLANDESC <=", value, "plandesc");
            return (Criteria) this;
        }

        public Criteria andPlandescLike(String value) {
            addCriterion("PLANDESC like", value, "plandesc");
            return (Criteria) this;
        }

        public Criteria andPlandescNotLike(String value) {
            addCriterion("PLANDESC not like", value, "plandesc");
            return (Criteria) this;
        }

        public Criteria andPlandescIn(List<String> values) {
            addCriterion("PLANDESC in", values, "plandesc");
            return (Criteria) this;
        }

        public Criteria andPlandescNotIn(List<String> values) {
            addCriterion("PLANDESC not in", values, "plandesc");
            return (Criteria) this;
        }

        public Criteria andPlandescBetween(String value1, String value2) {
            addCriterion("PLANDESC between", value1, value2, "plandesc");
            return (Criteria) this;
        }

        public Criteria andPlandescNotBetween(String value1, String value2) {
            addCriterion("PLANDESC not between", value1, value2, "plandesc");
            return (Criteria) this;
        }

        public Criteria andPlantypeIsNull() {
            addCriterion("PLANTYPE is null");
            return (Criteria) this;
        }

        public Criteria andPlantypeIsNotNull() {
            addCriterion("PLANTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andPlantypeEqualTo(String value) {
            addCriterion("PLANTYPE =", value, "plantype");
            return (Criteria) this;
        }

        public Criteria andPlantypeNotEqualTo(String value) {
            addCriterion("PLANTYPE <>", value, "plantype");
            return (Criteria) this;
        }

        public Criteria andPlantypeGreaterThan(String value) {
            addCriterion("PLANTYPE >", value, "plantype");
            return (Criteria) this;
        }

        public Criteria andPlantypeGreaterThanOrEqualTo(String value) {
            addCriterion("PLANTYPE >=", value, "plantype");
            return (Criteria) this;
        }

        public Criteria andPlantypeLessThan(String value) {
            addCriterion("PLANTYPE <", value, "plantype");
            return (Criteria) this;
        }

        public Criteria andPlantypeLessThanOrEqualTo(String value) {
            addCriterion("PLANTYPE <=", value, "plantype");
            return (Criteria) this;
        }

        public Criteria andPlantypeLike(String value) {
            addCriterion("PLANTYPE like", value, "plantype");
            return (Criteria) this;
        }

        public Criteria andPlantypeNotLike(String value) {
            addCriterion("PLANTYPE not like", value, "plantype");
            return (Criteria) this;
        }

        public Criteria andPlantypeIn(List<String> values) {
            addCriterion("PLANTYPE in", values, "plantype");
            return (Criteria) this;
        }

        public Criteria andPlantypeNotIn(List<String> values) {
            addCriterion("PLANTYPE not in", values, "plantype");
            return (Criteria) this;
        }

        public Criteria andPlantypeBetween(String value1, String value2) {
            addCriterion("PLANTYPE between", value1, value2, "plantype");
            return (Criteria) this;
        }

        public Criteria andPlantypeNotBetween(String value1, String value2) {
            addCriterion("PLANTYPE not between", value1, value2, "plantype");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtIsNull() {
            addCriterion("AUTOUNDWRT is null");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtIsNotNull() {
            addCriterion("AUTOUNDWRT is not null");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtEqualTo(String value) {
            addCriterion("AUTOUNDWRT =", value, "autoundwrt");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtNotEqualTo(String value) {
            addCriterion("AUTOUNDWRT <>", value, "autoundwrt");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtGreaterThan(String value) {
            addCriterion("AUTOUNDWRT >", value, "autoundwrt");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtGreaterThanOrEqualTo(String value) {
            addCriterion("AUTOUNDWRT >=", value, "autoundwrt");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtLessThan(String value) {
            addCriterion("AUTOUNDWRT <", value, "autoundwrt");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtLessThanOrEqualTo(String value) {
            addCriterion("AUTOUNDWRT <=", value, "autoundwrt");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtLike(String value) {
            addCriterion("AUTOUNDWRT like", value, "autoundwrt");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtNotLike(String value) {
            addCriterion("AUTOUNDWRT not like", value, "autoundwrt");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtIn(List<String> values) {
            addCriterion("AUTOUNDWRT in", values, "autoundwrt");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtNotIn(List<String> values) {
            addCriterion("AUTOUNDWRT not in", values, "autoundwrt");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtBetween(String value1, String value2) {
            addCriterion("AUTOUNDWRT between", value1, value2, "autoundwrt");
            return (Criteria) this;
        }

        public Criteria andAutoundwrtNotBetween(String value1, String value2) {
            addCriterion("AUTOUNDWRT not between", value1, value2, "autoundwrt");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeIsNull() {
            addCriterion("PERIODTYPE is null");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeIsNotNull() {
            addCriterion("PERIODTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeEqualTo(String value) {
            addCriterion("PERIODTYPE =", value, "periodtype");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeNotEqualTo(String value) {
            addCriterion("PERIODTYPE <>", value, "periodtype");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeGreaterThan(String value) {
            addCriterion("PERIODTYPE >", value, "periodtype");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeGreaterThanOrEqualTo(String value) {
            addCriterion("PERIODTYPE >=", value, "periodtype");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeLessThan(String value) {
            addCriterion("PERIODTYPE <", value, "periodtype");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeLessThanOrEqualTo(String value) {
            addCriterion("PERIODTYPE <=", value, "periodtype");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeLike(String value) {
            addCriterion("PERIODTYPE like", value, "periodtype");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeNotLike(String value) {
            addCriterion("PERIODTYPE not like", value, "periodtype");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeIn(List<String> values) {
            addCriterion("PERIODTYPE in", values, "periodtype");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeNotIn(List<String> values) {
            addCriterion("PERIODTYPE not in", values, "periodtype");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeBetween(String value1, String value2) {
            addCriterion("PERIODTYPE between", value1, value2, "periodtype");
            return (Criteria) this;
        }

        public Criteria andPeriodtypeNotBetween(String value1, String value2) {
            addCriterion("PERIODTYPE not between", value1, value2, "periodtype");
            return (Criteria) this;
        }

        public Criteria andPeriodIsNull() {
            addCriterion("PERIOD is null");
            return (Criteria) this;
        }

        public Criteria andPeriodIsNotNull() {
            addCriterion("PERIOD is not null");
            return (Criteria) this;
        }

        public Criteria andPeriodEqualTo(Integer value) {
            addCriterion("PERIOD =", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotEqualTo(Integer value) {
            addCriterion("PERIOD <>", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodGreaterThan(Integer value) {
            addCriterion("PERIOD >", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodGreaterThanOrEqualTo(Integer value) {
            addCriterion("PERIOD >=", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLessThan(Integer value) {
            addCriterion("PERIOD <", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLessThanOrEqualTo(Integer value) {
            addCriterion("PERIOD <=", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodIn(List<Integer> values) {
            addCriterion("PERIOD in", values, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotIn(List<Integer> values) {
            addCriterion("PERIOD not in", values, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodBetween(Integer value1, Integer value2) {
            addCriterion("PERIOD between", value1, value2, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotBetween(Integer value1, Integer value2) {
            addCriterion("PERIOD not between", value1, value2, "period");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeIsNull() {
            addCriterion("OCCUPATIONCODE is null");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeIsNotNull() {
            addCriterion("OCCUPATIONCODE is not null");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeEqualTo(String value) {
            addCriterion("OCCUPATIONCODE =", value, "occupationcode");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeNotEqualTo(String value) {
            addCriterion("OCCUPATIONCODE <>", value, "occupationcode");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeGreaterThan(String value) {
            addCriterion("OCCUPATIONCODE >", value, "occupationcode");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeGreaterThanOrEqualTo(String value) {
            addCriterion("OCCUPATIONCODE >=", value, "occupationcode");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeLessThan(String value) {
            addCriterion("OCCUPATIONCODE <", value, "occupationcode");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeLessThanOrEqualTo(String value) {
            addCriterion("OCCUPATIONCODE <=", value, "occupationcode");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeLike(String value) {
            addCriterion("OCCUPATIONCODE like", value, "occupationcode");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeNotLike(String value) {
            addCriterion("OCCUPATIONCODE not like", value, "occupationcode");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeIn(List<String> values) {
            addCriterion("OCCUPATIONCODE in", values, "occupationcode");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeNotIn(List<String> values) {
            addCriterion("OCCUPATIONCODE not in", values, "occupationcode");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeBetween(String value1, String value2) {
            addCriterion("OCCUPATIONCODE between", value1, value2, "occupationcode");
            return (Criteria) this;
        }

        public Criteria andOccupationcodeNotBetween(String value1, String value2) {
            addCriterion("OCCUPATIONCODE not between", value1, value2, "occupationcode");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNull() {
            addCriterion("CREATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNotNull() {
            addCriterion("CREATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedateEqualTo(Date value) {
            addCriterion("CREATEDATE =", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotEqualTo(Date value) {
            addCriterion("CREATEDATE <>", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThan(Date value) {
            addCriterion("CREATEDATE >", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE >=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThan(Date value) {
            addCriterion("CREATEDATE <", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE <=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateIn(List<Date> values) {
            addCriterion("CREATEDATE in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotIn(List<Date> values) {
            addCriterion("CREATEDATE not in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE not between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UPDATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UPDATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UPDATEDATE =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UPDATEDATE <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UPDATEDATE >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UPDATEDATE >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UPDATEDATE <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UPDATEDATE <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UPDATEDATE in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UPDATEDATE not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UPDATEDATE between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UPDATEDATE not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andAutodocIsNull() {
            addCriterion("AUTODOC is null");
            return (Criteria) this;
        }

        public Criteria andAutodocIsNotNull() {
            addCriterion("AUTODOC is not null");
            return (Criteria) this;
        }

        public Criteria andAutodocEqualTo(String value) {
            addCriterion("AUTODOC =", value, "autodoc");
            return (Criteria) this;
        }

        public Criteria andAutodocNotEqualTo(String value) {
            addCriterion("AUTODOC <>", value, "autodoc");
            return (Criteria) this;
        }

        public Criteria andAutodocGreaterThan(String value) {
            addCriterion("AUTODOC >", value, "autodoc");
            return (Criteria) this;
        }

        public Criteria andAutodocGreaterThanOrEqualTo(String value) {
            addCriterion("AUTODOC >=", value, "autodoc");
            return (Criteria) this;
        }

        public Criteria andAutodocLessThan(String value) {
            addCriterion("AUTODOC <", value, "autodoc");
            return (Criteria) this;
        }

        public Criteria andAutodocLessThanOrEqualTo(String value) {
            addCriterion("AUTODOC <=", value, "autodoc");
            return (Criteria) this;
        }

        public Criteria andAutodocLike(String value) {
            addCriterion("AUTODOC like", value, "autodoc");
            return (Criteria) this;
        }

        public Criteria andAutodocNotLike(String value) {
            addCriterion("AUTODOC not like", value, "autodoc");
            return (Criteria) this;
        }

        public Criteria andAutodocIn(List<String> values) {
            addCriterion("AUTODOC in", values, "autodoc");
            return (Criteria) this;
        }

        public Criteria andAutodocNotIn(List<String> values) {
            addCriterion("AUTODOC not in", values, "autodoc");
            return (Criteria) this;
        }

        public Criteria andAutodocBetween(String value1, String value2) {
            addCriterion("AUTODOC between", value1, value2, "autodoc");
            return (Criteria) this;
        }

        public Criteria andAutodocNotBetween(String value1, String value2) {
            addCriterion("AUTODOC not between", value1, value2, "autodoc");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}