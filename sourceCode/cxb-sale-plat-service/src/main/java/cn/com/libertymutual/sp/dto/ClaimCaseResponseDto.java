package cn.com.libertymutual.sp.dto;

import java.util.List;

import com.google.common.collect.Lists;

import cn.com.libertymutual.core.base.dto.ResponseBaseDto;

public class ClaimCaseResponseDto extends ResponseBaseDto{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6208918887971748792L;

	private List<TClaimCase> claimList;

	public List<TClaimCase> getClaimList() {
		if(null==claimList){
			claimList=Lists.newArrayList();
		}
		return claimList;
	}

	public void setClaimList(List<TClaimCase> claimList) {
		this.claimList = claimList;
	}
	
	
	
}
