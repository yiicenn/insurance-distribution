package cn.com.libertymutual.sp.dto.wechat;

import java.io.Serializable;

import cn.com.libertymutual.core.base.dto.RequestBaseDto;


public class TQueryRuleMsgRequestDto  extends RequestBaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2815752105230321699L;
	
	private Message message;

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}


}
