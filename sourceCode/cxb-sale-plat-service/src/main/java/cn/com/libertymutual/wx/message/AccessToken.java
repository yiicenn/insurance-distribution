package cn.com.libertymutual.wx.message;

import java.io.Serializable;

public class AccessToken  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = -8505925608301364788L;
	// 获取到的凭证
    private String token;
    // 凭证有效时间，单位：秒
    private int expiresIn;

	public AccessToken() {
		super();
	}

	public AccessToken(String token, int expiresIn) {
		super();
		this.token = token;
		this.expiresIn = expiresIn;
	}

	public String getToken() {
        return token;
    }
 
    public void setToken(String token) {
        this.token = token;
    }
 
    public int getExpiresIn() {
        return expiresIn;
    }
 
    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("token=" + token + ", expiresIn = " + expiresIn);
		return sb.toString();
	}
}

