package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrpdriskExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpdriskExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRiskcodeIsNull() {
            addCriterion("RISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIsNotNull() {
            addCriterion("RISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeEqualTo(String value) {
            addCriterion("RISKCODE =", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotEqualTo(String value) {
            addCriterion("RISKCODE <>", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThan(String value) {
            addCriterion("RISKCODE >", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThanOrEqualTo(String value) {
            addCriterion("RISKCODE >=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThan(String value) {
            addCriterion("RISKCODE <", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThanOrEqualTo(String value) {
            addCriterion("RISKCODE <=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLike(String value) {
            addCriterion("RISKCODE like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotLike(String value) {
            addCriterion("RISKCODE not like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIn(List<String> values) {
            addCriterion("RISKCODE in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotIn(List<String> values) {
            addCriterion("RISKCODE not in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeBetween(String value1, String value2) {
            addCriterion("RISKCODE between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotBetween(String value1, String value2) {
            addCriterion("RISKCODE not between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskversionIsNull() {
            addCriterion("RISKVERSION is null");
            return (Criteria) this;
        }

        public Criteria andRiskversionIsNotNull() {
            addCriterion("RISKVERSION is not null");
            return (Criteria) this;
        }

        public Criteria andRiskversionEqualTo(String value) {
            addCriterion("RISKVERSION =", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotEqualTo(String value) {
            addCriterion("RISKVERSION <>", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionGreaterThan(String value) {
            addCriterion("RISKVERSION >", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionGreaterThanOrEqualTo(String value) {
            addCriterion("RISKVERSION >=", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLessThan(String value) {
            addCriterion("RISKVERSION <", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLessThanOrEqualTo(String value) {
            addCriterion("RISKVERSION <=", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLike(String value) {
            addCriterion("RISKVERSION like", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotLike(String value) {
            addCriterion("RISKVERSION not like", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionIn(List<String> values) {
            addCriterion("RISKVERSION in", values, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotIn(List<String> values) {
            addCriterion("RISKVERSION not in", values, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionBetween(String value1, String value2) {
            addCriterion("RISKVERSION between", value1, value2, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotBetween(String value1, String value2) {
            addCriterion("RISKVERSION not between", value1, value2, "riskversion");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeIsNull() {
            addCriterion("STATRISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeIsNotNull() {
            addCriterion("STATRISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeEqualTo(String value) {
            addCriterion("STATRISKCODE =", value, "statriskcode");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeNotEqualTo(String value) {
            addCriterion("STATRISKCODE <>", value, "statriskcode");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeGreaterThan(String value) {
            addCriterion("STATRISKCODE >", value, "statriskcode");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeGreaterThanOrEqualTo(String value) {
            addCriterion("STATRISKCODE >=", value, "statriskcode");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeLessThan(String value) {
            addCriterion("STATRISKCODE <", value, "statriskcode");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeLessThanOrEqualTo(String value) {
            addCriterion("STATRISKCODE <=", value, "statriskcode");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeLike(String value) {
            addCriterion("STATRISKCODE like", value, "statriskcode");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeNotLike(String value) {
            addCriterion("STATRISKCODE not like", value, "statriskcode");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeIn(List<String> values) {
            addCriterion("STATRISKCODE in", values, "statriskcode");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeNotIn(List<String> values) {
            addCriterion("STATRISKCODE not in", values, "statriskcode");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeBetween(String value1, String value2) {
            addCriterion("STATRISKCODE between", value1, value2, "statriskcode");
            return (Criteria) this;
        }

        public Criteria andStatriskcodeNotBetween(String value1, String value2) {
            addCriterion("STATRISKCODE not between", value1, value2, "statriskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcnameIsNull() {
            addCriterion("RISKCNAME is null");
            return (Criteria) this;
        }

        public Criteria andRiskcnameIsNotNull() {
            addCriterion("RISKCNAME is not null");
            return (Criteria) this;
        }

        public Criteria andRiskcnameEqualTo(String value) {
            addCriterion("RISKCNAME =", value, "riskcname");
            return (Criteria) this;
        }

        public Criteria andRiskcnameNotEqualTo(String value) {
            addCriterion("RISKCNAME <>", value, "riskcname");
            return (Criteria) this;
        }

        public Criteria andRiskcnameGreaterThan(String value) {
            addCriterion("RISKCNAME >", value, "riskcname");
            return (Criteria) this;
        }

        public Criteria andRiskcnameGreaterThanOrEqualTo(String value) {
            addCriterion("RISKCNAME >=", value, "riskcname");
            return (Criteria) this;
        }

        public Criteria andRiskcnameLessThan(String value) {
            addCriterion("RISKCNAME <", value, "riskcname");
            return (Criteria) this;
        }

        public Criteria andRiskcnameLessThanOrEqualTo(String value) {
            addCriterion("RISKCNAME <=", value, "riskcname");
            return (Criteria) this;
        }

        public Criteria andRiskcnameLike(String value) {
            addCriterion("RISKCNAME like", value, "riskcname");
            return (Criteria) this;
        }

        public Criteria andRiskcnameNotLike(String value) {
            addCriterion("RISKCNAME not like", value, "riskcname");
            return (Criteria) this;
        }

        public Criteria andRiskcnameIn(List<String> values) {
            addCriterion("RISKCNAME in", values, "riskcname");
            return (Criteria) this;
        }

        public Criteria andRiskcnameNotIn(List<String> values) {
            addCriterion("RISKCNAME not in", values, "riskcname");
            return (Criteria) this;
        }

        public Criteria andRiskcnameBetween(String value1, String value2) {
            addCriterion("RISKCNAME between", value1, value2, "riskcname");
            return (Criteria) this;
        }

        public Criteria andRiskcnameNotBetween(String value1, String value2) {
            addCriterion("RISKCNAME not between", value1, value2, "riskcname");
            return (Criteria) this;
        }

        public Criteria andRiskenameIsNull() {
            addCriterion("RISKENAME is null");
            return (Criteria) this;
        }

        public Criteria andRiskenameIsNotNull() {
            addCriterion("RISKENAME is not null");
            return (Criteria) this;
        }

        public Criteria andRiskenameEqualTo(String value) {
            addCriterion("RISKENAME =", value, "riskename");
            return (Criteria) this;
        }

        public Criteria andRiskenameNotEqualTo(String value) {
            addCriterion("RISKENAME <>", value, "riskename");
            return (Criteria) this;
        }

        public Criteria andRiskenameGreaterThan(String value) {
            addCriterion("RISKENAME >", value, "riskename");
            return (Criteria) this;
        }

        public Criteria andRiskenameGreaterThanOrEqualTo(String value) {
            addCriterion("RISKENAME >=", value, "riskename");
            return (Criteria) this;
        }

        public Criteria andRiskenameLessThan(String value) {
            addCriterion("RISKENAME <", value, "riskename");
            return (Criteria) this;
        }

        public Criteria andRiskenameLessThanOrEqualTo(String value) {
            addCriterion("RISKENAME <=", value, "riskename");
            return (Criteria) this;
        }

        public Criteria andRiskenameLike(String value) {
            addCriterion("RISKENAME like", value, "riskename");
            return (Criteria) this;
        }

        public Criteria andRiskenameNotLike(String value) {
            addCriterion("RISKENAME not like", value, "riskename");
            return (Criteria) this;
        }

        public Criteria andRiskenameIn(List<String> values) {
            addCriterion("RISKENAME in", values, "riskename");
            return (Criteria) this;
        }

        public Criteria andRiskenameNotIn(List<String> values) {
            addCriterion("RISKENAME not in", values, "riskename");
            return (Criteria) this;
        }

        public Criteria andRiskenameBetween(String value1, String value2) {
            addCriterion("RISKENAME between", value1, value2, "riskename");
            return (Criteria) this;
        }

        public Criteria andRiskenameNotBetween(String value1, String value2) {
            addCriterion("RISKENAME not between", value1, value2, "riskename");
            return (Criteria) this;
        }

        public Criteria andClasscodeIsNull() {
            addCriterion("CLASSCODE is null");
            return (Criteria) this;
        }

        public Criteria andClasscodeIsNotNull() {
            addCriterion("CLASSCODE is not null");
            return (Criteria) this;
        }

        public Criteria andClasscodeEqualTo(String value) {
            addCriterion("CLASSCODE =", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotEqualTo(String value) {
            addCriterion("CLASSCODE <>", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeGreaterThan(String value) {
            addCriterion("CLASSCODE >", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeGreaterThanOrEqualTo(String value) {
            addCriterion("CLASSCODE >=", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeLessThan(String value) {
            addCriterion("CLASSCODE <", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeLessThanOrEqualTo(String value) {
            addCriterion("CLASSCODE <=", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeLike(String value) {
            addCriterion("CLASSCODE like", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotLike(String value) {
            addCriterion("CLASSCODE not like", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeIn(List<String> values) {
            addCriterion("CLASSCODE in", values, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotIn(List<String> values) {
            addCriterion("CLASSCODE not in", values, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeBetween(String value1, String value2) {
            addCriterion("CLASSCODE between", value1, value2, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotBetween(String value1, String value2) {
            addCriterion("CLASSCODE not between", value1, value2, "classcode");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeIsNull() {
            addCriterion("PRODUCTTYPECODE is null");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeIsNotNull() {
            addCriterion("PRODUCTTYPECODE is not null");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeEqualTo(String value) {
            addCriterion("PRODUCTTYPECODE =", value, "producttypecode");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeNotEqualTo(String value) {
            addCriterion("PRODUCTTYPECODE <>", value, "producttypecode");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeGreaterThan(String value) {
            addCriterion("PRODUCTTYPECODE >", value, "producttypecode");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeGreaterThanOrEqualTo(String value) {
            addCriterion("PRODUCTTYPECODE >=", value, "producttypecode");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeLessThan(String value) {
            addCriterion("PRODUCTTYPECODE <", value, "producttypecode");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeLessThanOrEqualTo(String value) {
            addCriterion("PRODUCTTYPECODE <=", value, "producttypecode");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeLike(String value) {
            addCriterion("PRODUCTTYPECODE like", value, "producttypecode");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeNotLike(String value) {
            addCriterion("PRODUCTTYPECODE not like", value, "producttypecode");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeIn(List<String> values) {
            addCriterion("PRODUCTTYPECODE in", values, "producttypecode");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeNotIn(List<String> values) {
            addCriterion("PRODUCTTYPECODE not in", values, "producttypecode");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeBetween(String value1, String value2) {
            addCriterion("PRODUCTTYPECODE between", value1, value2, "producttypecode");
            return (Criteria) this;
        }

        public Criteria andProducttypecodeNotBetween(String value1, String value2) {
            addCriterion("PRODUCTTYPECODE not between", value1, value2, "producttypecode");
            return (Criteria) this;
        }

        public Criteria andGroupcodeIsNull() {
            addCriterion("GROUPCODE is null");
            return (Criteria) this;
        }

        public Criteria andGroupcodeIsNotNull() {
            addCriterion("GROUPCODE is not null");
            return (Criteria) this;
        }

        public Criteria andGroupcodeEqualTo(String value) {
            addCriterion("GROUPCODE =", value, "groupcode");
            return (Criteria) this;
        }

        public Criteria andGroupcodeNotEqualTo(String value) {
            addCriterion("GROUPCODE <>", value, "groupcode");
            return (Criteria) this;
        }

        public Criteria andGroupcodeGreaterThan(String value) {
            addCriterion("GROUPCODE >", value, "groupcode");
            return (Criteria) this;
        }

        public Criteria andGroupcodeGreaterThanOrEqualTo(String value) {
            addCriterion("GROUPCODE >=", value, "groupcode");
            return (Criteria) this;
        }

        public Criteria andGroupcodeLessThan(String value) {
            addCriterion("GROUPCODE <", value, "groupcode");
            return (Criteria) this;
        }

        public Criteria andGroupcodeLessThanOrEqualTo(String value) {
            addCriterion("GROUPCODE <=", value, "groupcode");
            return (Criteria) this;
        }

        public Criteria andGroupcodeLike(String value) {
            addCriterion("GROUPCODE like", value, "groupcode");
            return (Criteria) this;
        }

        public Criteria andGroupcodeNotLike(String value) {
            addCriterion("GROUPCODE not like", value, "groupcode");
            return (Criteria) this;
        }

        public Criteria andGroupcodeIn(List<String> values) {
            addCriterion("GROUPCODE in", values, "groupcode");
            return (Criteria) this;
        }

        public Criteria andGroupcodeNotIn(List<String> values) {
            addCriterion("GROUPCODE not in", values, "groupcode");
            return (Criteria) this;
        }

        public Criteria andGroupcodeBetween(String value1, String value2) {
            addCriterion("GROUPCODE between", value1, value2, "groupcode");
            return (Criteria) this;
        }

        public Criteria andGroupcodeNotBetween(String value1, String value2) {
            addCriterion("GROUPCODE not between", value1, value2, "groupcode");
            return (Criteria) this;
        }

        public Criteria andCalculatorIsNull() {
            addCriterion("CALCULATOR is null");
            return (Criteria) this;
        }

        public Criteria andCalculatorIsNotNull() {
            addCriterion("CALCULATOR is not null");
            return (Criteria) this;
        }

        public Criteria andCalculatorEqualTo(Long value) {
            addCriterion("CALCULATOR =", value, "calculator");
            return (Criteria) this;
        }

        public Criteria andCalculatorNotEqualTo(Long value) {
            addCriterion("CALCULATOR <>", value, "calculator");
            return (Criteria) this;
        }

        public Criteria andCalculatorGreaterThan(Long value) {
            addCriterion("CALCULATOR >", value, "calculator");
            return (Criteria) this;
        }

        public Criteria andCalculatorGreaterThanOrEqualTo(Long value) {
            addCriterion("CALCULATOR >=", value, "calculator");
            return (Criteria) this;
        }

        public Criteria andCalculatorLessThan(Long value) {
            addCriterion("CALCULATOR <", value, "calculator");
            return (Criteria) this;
        }

        public Criteria andCalculatorLessThanOrEqualTo(Long value) {
            addCriterion("CALCULATOR <=", value, "calculator");
            return (Criteria) this;
        }

        public Criteria andCalculatorIn(List<Long> values) {
            addCriterion("CALCULATOR in", values, "calculator");
            return (Criteria) this;
        }

        public Criteria andCalculatorNotIn(List<Long> values) {
            addCriterion("CALCULATOR not in", values, "calculator");
            return (Criteria) this;
        }

        public Criteria andCalculatorBetween(Long value1, Long value2) {
            addCriterion("CALCULATOR between", value1, value2, "calculator");
            return (Criteria) this;
        }

        public Criteria andCalculatorNotBetween(Long value1, Long value2) {
            addCriterion("CALCULATOR not between", value1, value2, "calculator");
            return (Criteria) this;
        }

        public Criteria andSalestartdateIsNull() {
            addCriterion("SALESTARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andSalestartdateIsNotNull() {
            addCriterion("SALESTARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andSalestartdateEqualTo(Date value) {
            addCriterion("SALESTARTDATE =", value, "salestartdate");
            return (Criteria) this;
        }

        public Criteria andSalestartdateNotEqualTo(Date value) {
            addCriterion("SALESTARTDATE <>", value, "salestartdate");
            return (Criteria) this;
        }

        public Criteria andSalestartdateGreaterThan(Date value) {
            addCriterion("SALESTARTDATE >", value, "salestartdate");
            return (Criteria) this;
        }

        public Criteria andSalestartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("SALESTARTDATE >=", value, "salestartdate");
            return (Criteria) this;
        }

        public Criteria andSalestartdateLessThan(Date value) {
            addCriterion("SALESTARTDATE <", value, "salestartdate");
            return (Criteria) this;
        }

        public Criteria andSalestartdateLessThanOrEqualTo(Date value) {
            addCriterion("SALESTARTDATE <=", value, "salestartdate");
            return (Criteria) this;
        }

        public Criteria andSalestartdateIn(List<Date> values) {
            addCriterion("SALESTARTDATE in", values, "salestartdate");
            return (Criteria) this;
        }

        public Criteria andSalestartdateNotIn(List<Date> values) {
            addCriterion("SALESTARTDATE not in", values, "salestartdate");
            return (Criteria) this;
        }

        public Criteria andSalestartdateBetween(Date value1, Date value2) {
            addCriterion("SALESTARTDATE between", value1, value2, "salestartdate");
            return (Criteria) this;
        }

        public Criteria andSalestartdateNotBetween(Date value1, Date value2) {
            addCriterion("SALESTARTDATE not between", value1, value2, "salestartdate");
            return (Criteria) this;
        }

        public Criteria andSaleenddateIsNull() {
            addCriterion("SALEENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andSaleenddateIsNotNull() {
            addCriterion("SALEENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andSaleenddateEqualTo(Date value) {
            addCriterion("SALEENDDATE =", value, "saleenddate");
            return (Criteria) this;
        }

        public Criteria andSaleenddateNotEqualTo(Date value) {
            addCriterion("SALEENDDATE <>", value, "saleenddate");
            return (Criteria) this;
        }

        public Criteria andSaleenddateGreaterThan(Date value) {
            addCriterion("SALEENDDATE >", value, "saleenddate");
            return (Criteria) this;
        }

        public Criteria andSaleenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("SALEENDDATE >=", value, "saleenddate");
            return (Criteria) this;
        }

        public Criteria andSaleenddateLessThan(Date value) {
            addCriterion("SALEENDDATE <", value, "saleenddate");
            return (Criteria) this;
        }

        public Criteria andSaleenddateLessThanOrEqualTo(Date value) {
            addCriterion("SALEENDDATE <=", value, "saleenddate");
            return (Criteria) this;
        }

        public Criteria andSaleenddateIn(List<Date> values) {
            addCriterion("SALEENDDATE in", values, "saleenddate");
            return (Criteria) this;
        }

        public Criteria andSaleenddateNotIn(List<Date> values) {
            addCriterion("SALEENDDATE not in", values, "saleenddate");
            return (Criteria) this;
        }

        public Criteria andSaleenddateBetween(Date value1, Date value2) {
            addCriterion("SALEENDDATE between", value1, value2, "saleenddate");
            return (Criteria) this;
        }

        public Criteria andSaleenddateNotBetween(Date value1, Date value2) {
            addCriterion("SALEENDDATE not between", value1, value2, "saleenddate");
            return (Criteria) this;
        }

        public Criteria andEnddateflagIsNull() {
            addCriterion("ENDDATEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andEnddateflagIsNotNull() {
            addCriterion("ENDDATEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andEnddateflagEqualTo(String value) {
            addCriterion("ENDDATEFLAG =", value, "enddateflag");
            return (Criteria) this;
        }

        public Criteria andEnddateflagNotEqualTo(String value) {
            addCriterion("ENDDATEFLAG <>", value, "enddateflag");
            return (Criteria) this;
        }

        public Criteria andEnddateflagGreaterThan(String value) {
            addCriterion("ENDDATEFLAG >", value, "enddateflag");
            return (Criteria) this;
        }

        public Criteria andEnddateflagGreaterThanOrEqualTo(String value) {
            addCriterion("ENDDATEFLAG >=", value, "enddateflag");
            return (Criteria) this;
        }

        public Criteria andEnddateflagLessThan(String value) {
            addCriterion("ENDDATEFLAG <", value, "enddateflag");
            return (Criteria) this;
        }

        public Criteria andEnddateflagLessThanOrEqualTo(String value) {
            addCriterion("ENDDATEFLAG <=", value, "enddateflag");
            return (Criteria) this;
        }

        public Criteria andEnddateflagLike(String value) {
            addCriterion("ENDDATEFLAG like", value, "enddateflag");
            return (Criteria) this;
        }

        public Criteria andEnddateflagNotLike(String value) {
            addCriterion("ENDDATEFLAG not like", value, "enddateflag");
            return (Criteria) this;
        }

        public Criteria andEnddateflagIn(List<String> values) {
            addCriterion("ENDDATEFLAG in", values, "enddateflag");
            return (Criteria) this;
        }

        public Criteria andEnddateflagNotIn(List<String> values) {
            addCriterion("ENDDATEFLAG not in", values, "enddateflag");
            return (Criteria) this;
        }

        public Criteria andEnddateflagBetween(String value1, String value2) {
            addCriterion("ENDDATEFLAG between", value1, value2, "enddateflag");
            return (Criteria) this;
        }

        public Criteria andEnddateflagNotBetween(String value1, String value2) {
            addCriterion("ENDDATEFLAG not between", value1, value2, "enddateflag");
            return (Criteria) this;
        }

        public Criteria andEffectivestartdateIsNull() {
            addCriterion("EFFECTIVESTARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andEffectivestartdateIsNotNull() {
            addCriterion("EFFECTIVESTARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andEffectivestartdateEqualTo(Date value) {
            addCriterion("EFFECTIVESTARTDATE =", value, "effectivestartdate");
            return (Criteria) this;
        }

        public Criteria andEffectivestartdateNotEqualTo(Date value) {
            addCriterion("EFFECTIVESTARTDATE <>", value, "effectivestartdate");
            return (Criteria) this;
        }

        public Criteria andEffectivestartdateGreaterThan(Date value) {
            addCriterion("EFFECTIVESTARTDATE >", value, "effectivestartdate");
            return (Criteria) this;
        }

        public Criteria andEffectivestartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("EFFECTIVESTARTDATE >=", value, "effectivestartdate");
            return (Criteria) this;
        }

        public Criteria andEffectivestartdateLessThan(Date value) {
            addCriterion("EFFECTIVESTARTDATE <", value, "effectivestartdate");
            return (Criteria) this;
        }

        public Criteria andEffectivestartdateLessThanOrEqualTo(Date value) {
            addCriterion("EFFECTIVESTARTDATE <=", value, "effectivestartdate");
            return (Criteria) this;
        }

        public Criteria andEffectivestartdateIn(List<Date> values) {
            addCriterion("EFFECTIVESTARTDATE in", values, "effectivestartdate");
            return (Criteria) this;
        }

        public Criteria andEffectivestartdateNotIn(List<Date> values) {
            addCriterion("EFFECTIVESTARTDATE not in", values, "effectivestartdate");
            return (Criteria) this;
        }

        public Criteria andEffectivestartdateBetween(Date value1, Date value2) {
            addCriterion("EFFECTIVESTARTDATE between", value1, value2, "effectivestartdate");
            return (Criteria) this;
        }

        public Criteria andEffectivestartdateNotBetween(Date value1, Date value2) {
            addCriterion("EFFECTIVESTARTDATE not between", value1, value2, "effectivestartdate");
            return (Criteria) this;
        }

        public Criteria andInvalidenddateIsNull() {
            addCriterion("INVALIDENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andInvalidenddateIsNotNull() {
            addCriterion("INVALIDENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andInvalidenddateEqualTo(Date value) {
            addCriterion("INVALIDENDDATE =", value, "invalidenddate");
            return (Criteria) this;
        }

        public Criteria andInvalidenddateNotEqualTo(Date value) {
            addCriterion("INVALIDENDDATE <>", value, "invalidenddate");
            return (Criteria) this;
        }

        public Criteria andInvalidenddateGreaterThan(Date value) {
            addCriterion("INVALIDENDDATE >", value, "invalidenddate");
            return (Criteria) this;
        }

        public Criteria andInvalidenddateGreaterThanOrEqualTo(Date value) {
            addCriterion("INVALIDENDDATE >=", value, "invalidenddate");
            return (Criteria) this;
        }

        public Criteria andInvalidenddateLessThan(Date value) {
            addCriterion("INVALIDENDDATE <", value, "invalidenddate");
            return (Criteria) this;
        }

        public Criteria andInvalidenddateLessThanOrEqualTo(Date value) {
            addCriterion("INVALIDENDDATE <=", value, "invalidenddate");
            return (Criteria) this;
        }

        public Criteria andInvalidenddateIn(List<Date> values) {
            addCriterion("INVALIDENDDATE in", values, "invalidenddate");
            return (Criteria) this;
        }

        public Criteria andInvalidenddateNotIn(List<Date> values) {
            addCriterion("INVALIDENDDATE not in", values, "invalidenddate");
            return (Criteria) this;
        }

        public Criteria andInvalidenddateBetween(Date value1, Date value2) {
            addCriterion("INVALIDENDDATE between", value1, value2, "invalidenddate");
            return (Criteria) this;
        }

        public Criteria andInvalidenddateNotBetween(Date value1, Date value2) {
            addCriterion("INVALIDENDDATE not between", value1, value2, "invalidenddate");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeIsNull() {
            addCriterion("INSURANCEPERIODTYPE is null");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeIsNotNull() {
            addCriterion("INSURANCEPERIODTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeEqualTo(String value) {
            addCriterion("INSURANCEPERIODTYPE =", value, "insuranceperiodtype");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeNotEqualTo(String value) {
            addCriterion("INSURANCEPERIODTYPE <>", value, "insuranceperiodtype");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeGreaterThan(String value) {
            addCriterion("INSURANCEPERIODTYPE >", value, "insuranceperiodtype");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeGreaterThanOrEqualTo(String value) {
            addCriterion("INSURANCEPERIODTYPE >=", value, "insuranceperiodtype");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeLessThan(String value) {
            addCriterion("INSURANCEPERIODTYPE <", value, "insuranceperiodtype");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeLessThanOrEqualTo(String value) {
            addCriterion("INSURANCEPERIODTYPE <=", value, "insuranceperiodtype");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeLike(String value) {
            addCriterion("INSURANCEPERIODTYPE like", value, "insuranceperiodtype");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeNotLike(String value) {
            addCriterion("INSURANCEPERIODTYPE not like", value, "insuranceperiodtype");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeIn(List<String> values) {
            addCriterion("INSURANCEPERIODTYPE in", values, "insuranceperiodtype");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeNotIn(List<String> values) {
            addCriterion("INSURANCEPERIODTYPE not in", values, "insuranceperiodtype");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeBetween(String value1, String value2) {
            addCriterion("INSURANCEPERIODTYPE between", value1, value2, "insuranceperiodtype");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodtypeNotBetween(String value1, String value2) {
            addCriterion("INSURANCEPERIODTYPE not between", value1, value2, "insuranceperiodtype");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodIsNull() {
            addCriterion("INSURANCEPERIOD is null");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodIsNotNull() {
            addCriterion("INSURANCEPERIOD is not null");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodEqualTo(Long value) {
            addCriterion("INSURANCEPERIOD =", value, "insuranceperiod");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodNotEqualTo(Long value) {
            addCriterion("INSURANCEPERIOD <>", value, "insuranceperiod");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodGreaterThan(Long value) {
            addCriterion("INSURANCEPERIOD >", value, "insuranceperiod");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodGreaterThanOrEqualTo(Long value) {
            addCriterion("INSURANCEPERIOD >=", value, "insuranceperiod");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodLessThan(Long value) {
            addCriterion("INSURANCEPERIOD <", value, "insuranceperiod");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodLessThanOrEqualTo(Long value) {
            addCriterion("INSURANCEPERIOD <=", value, "insuranceperiod");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodIn(List<Long> values) {
            addCriterion("INSURANCEPERIOD in", values, "insuranceperiod");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodNotIn(List<Long> values) {
            addCriterion("INSURANCEPERIOD not in", values, "insuranceperiod");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodBetween(Long value1, Long value2) {
            addCriterion("INSURANCEPERIOD between", value1, value2, "insuranceperiod");
            return (Criteria) this;
        }

        public Criteria andInsuranceperiodNotBetween(Long value1, Long value2) {
            addCriterion("INSURANCEPERIOD not between", value1, value2, "insuranceperiod");
            return (Criteria) this;
        }

        public Criteria andStarthourIsNull() {
            addCriterion("STARTHOUR is null");
            return (Criteria) this;
        }

        public Criteria andStarthourIsNotNull() {
            addCriterion("STARTHOUR is not null");
            return (Criteria) this;
        }

        public Criteria andStarthourEqualTo(Long value) {
            addCriterion("STARTHOUR =", value, "starthour");
            return (Criteria) this;
        }

        public Criteria andStarthourNotEqualTo(Long value) {
            addCriterion("STARTHOUR <>", value, "starthour");
            return (Criteria) this;
        }

        public Criteria andStarthourGreaterThan(Long value) {
            addCriterion("STARTHOUR >", value, "starthour");
            return (Criteria) this;
        }

        public Criteria andStarthourGreaterThanOrEqualTo(Long value) {
            addCriterion("STARTHOUR >=", value, "starthour");
            return (Criteria) this;
        }

        public Criteria andStarthourLessThan(Long value) {
            addCriterion("STARTHOUR <", value, "starthour");
            return (Criteria) this;
        }

        public Criteria andStarthourLessThanOrEqualTo(Long value) {
            addCriterion("STARTHOUR <=", value, "starthour");
            return (Criteria) this;
        }

        public Criteria andStarthourIn(List<Long> values) {
            addCriterion("STARTHOUR in", values, "starthour");
            return (Criteria) this;
        }

        public Criteria andStarthourNotIn(List<Long> values) {
            addCriterion("STARTHOUR not in", values, "starthour");
            return (Criteria) this;
        }

        public Criteria andStarthourBetween(Long value1, Long value2) {
            addCriterion("STARTHOUR between", value1, value2, "starthour");
            return (Criteria) this;
        }

        public Criteria andStarthourNotBetween(Long value1, Long value2) {
            addCriterion("STARTHOUR not between", value1, value2, "starthour");
            return (Criteria) this;
        }

        public Criteria andStartminuteIsNull() {
            addCriterion("STARTMINUTE is null");
            return (Criteria) this;
        }

        public Criteria andStartminuteIsNotNull() {
            addCriterion("STARTMINUTE is not null");
            return (Criteria) this;
        }

        public Criteria andStartminuteEqualTo(Long value) {
            addCriterion("STARTMINUTE =", value, "startminute");
            return (Criteria) this;
        }

        public Criteria andStartminuteNotEqualTo(Long value) {
            addCriterion("STARTMINUTE <>", value, "startminute");
            return (Criteria) this;
        }

        public Criteria andStartminuteGreaterThan(Long value) {
            addCriterion("STARTMINUTE >", value, "startminute");
            return (Criteria) this;
        }

        public Criteria andStartminuteGreaterThanOrEqualTo(Long value) {
            addCriterion("STARTMINUTE >=", value, "startminute");
            return (Criteria) this;
        }

        public Criteria andStartminuteLessThan(Long value) {
            addCriterion("STARTMINUTE <", value, "startminute");
            return (Criteria) this;
        }

        public Criteria andStartminuteLessThanOrEqualTo(Long value) {
            addCriterion("STARTMINUTE <=", value, "startminute");
            return (Criteria) this;
        }

        public Criteria andStartminuteIn(List<Long> values) {
            addCriterion("STARTMINUTE in", values, "startminute");
            return (Criteria) this;
        }

        public Criteria andStartminuteNotIn(List<Long> values) {
            addCriterion("STARTMINUTE not in", values, "startminute");
            return (Criteria) this;
        }

        public Criteria andStartminuteBetween(Long value1, Long value2) {
            addCriterion("STARTMINUTE between", value1, value2, "startminute");
            return (Criteria) this;
        }

        public Criteria andStartminuteNotBetween(Long value1, Long value2) {
            addCriterion("STARTMINUTE not between", value1, value2, "startminute");
            return (Criteria) this;
        }

        public Criteria andEndhourIsNull() {
            addCriterion("ENDHOUR is null");
            return (Criteria) this;
        }

        public Criteria andEndhourIsNotNull() {
            addCriterion("ENDHOUR is not null");
            return (Criteria) this;
        }

        public Criteria andEndhourEqualTo(Long value) {
            addCriterion("ENDHOUR =", value, "endhour");
            return (Criteria) this;
        }

        public Criteria andEndhourNotEqualTo(Long value) {
            addCriterion("ENDHOUR <>", value, "endhour");
            return (Criteria) this;
        }

        public Criteria andEndhourGreaterThan(Long value) {
            addCriterion("ENDHOUR >", value, "endhour");
            return (Criteria) this;
        }

        public Criteria andEndhourGreaterThanOrEqualTo(Long value) {
            addCriterion("ENDHOUR >=", value, "endhour");
            return (Criteria) this;
        }

        public Criteria andEndhourLessThan(Long value) {
            addCriterion("ENDHOUR <", value, "endhour");
            return (Criteria) this;
        }

        public Criteria andEndhourLessThanOrEqualTo(Long value) {
            addCriterion("ENDHOUR <=", value, "endhour");
            return (Criteria) this;
        }

        public Criteria andEndhourIn(List<Long> values) {
            addCriterion("ENDHOUR in", values, "endhour");
            return (Criteria) this;
        }

        public Criteria andEndhourNotIn(List<Long> values) {
            addCriterion("ENDHOUR not in", values, "endhour");
            return (Criteria) this;
        }

        public Criteria andEndhourBetween(Long value1, Long value2) {
            addCriterion("ENDHOUR between", value1, value2, "endhour");
            return (Criteria) this;
        }

        public Criteria andEndhourNotBetween(Long value1, Long value2) {
            addCriterion("ENDHOUR not between", value1, value2, "endhour");
            return (Criteria) this;
        }

        public Criteria andEndminuteIsNull() {
            addCriterion("ENDMINUTE is null");
            return (Criteria) this;
        }

        public Criteria andEndminuteIsNotNull() {
            addCriterion("ENDMINUTE is not null");
            return (Criteria) this;
        }

        public Criteria andEndminuteEqualTo(Long value) {
            addCriterion("ENDMINUTE =", value, "endminute");
            return (Criteria) this;
        }

        public Criteria andEndminuteNotEqualTo(Long value) {
            addCriterion("ENDMINUTE <>", value, "endminute");
            return (Criteria) this;
        }

        public Criteria andEndminuteGreaterThan(Long value) {
            addCriterion("ENDMINUTE >", value, "endminute");
            return (Criteria) this;
        }

        public Criteria andEndminuteGreaterThanOrEqualTo(Long value) {
            addCriterion("ENDMINUTE >=", value, "endminute");
            return (Criteria) this;
        }

        public Criteria andEndminuteLessThan(Long value) {
            addCriterion("ENDMINUTE <", value, "endminute");
            return (Criteria) this;
        }

        public Criteria andEndminuteLessThanOrEqualTo(Long value) {
            addCriterion("ENDMINUTE <=", value, "endminute");
            return (Criteria) this;
        }

        public Criteria andEndminuteIn(List<Long> values) {
            addCriterion("ENDMINUTE in", values, "endminute");
            return (Criteria) this;
        }

        public Criteria andEndminuteNotIn(List<Long> values) {
            addCriterion("ENDMINUTE not in", values, "endminute");
            return (Criteria) this;
        }

        public Criteria andEndminuteBetween(Long value1, Long value2) {
            addCriterion("ENDMINUTE between", value1, value2, "endminute");
            return (Criteria) this;
        }

        public Criteria andEndminuteNotBetween(Long value1, Long value2) {
            addCriterion("ENDMINUTE not between", value1, value2, "endminute");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeIsNull() {
            addCriterion("TEMPLETRISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeIsNotNull() {
            addCriterion("TEMPLETRISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeEqualTo(String value) {
            addCriterion("TEMPLETRISKCODE =", value, "templetriskcode");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeNotEqualTo(String value) {
            addCriterion("TEMPLETRISKCODE <>", value, "templetriskcode");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeGreaterThan(String value) {
            addCriterion("TEMPLETRISKCODE >", value, "templetriskcode");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeGreaterThanOrEqualTo(String value) {
            addCriterion("TEMPLETRISKCODE >=", value, "templetriskcode");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeLessThan(String value) {
            addCriterion("TEMPLETRISKCODE <", value, "templetriskcode");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeLessThanOrEqualTo(String value) {
            addCriterion("TEMPLETRISKCODE <=", value, "templetriskcode");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeLike(String value) {
            addCriterion("TEMPLETRISKCODE like", value, "templetriskcode");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeNotLike(String value) {
            addCriterion("TEMPLETRISKCODE not like", value, "templetriskcode");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeIn(List<String> values) {
            addCriterion("TEMPLETRISKCODE in", values, "templetriskcode");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeNotIn(List<String> values) {
            addCriterion("TEMPLETRISKCODE not in", values, "templetriskcode");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeBetween(String value1, String value2) {
            addCriterion("TEMPLETRISKCODE between", value1, value2, "templetriskcode");
            return (Criteria) this;
        }

        public Criteria andTempletriskcodeNotBetween(String value1, String value2) {
            addCriterion("TEMPLETRISKCODE not between", value1, value2, "templetriskcode");
            return (Criteria) this;
        }

        public Criteria andRiskflagIsNull() {
            addCriterion("RISKFLAG is null");
            return (Criteria) this;
        }

        public Criteria andRiskflagIsNotNull() {
            addCriterion("RISKFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andRiskflagEqualTo(String value) {
            addCriterion("RISKFLAG =", value, "riskflag");
            return (Criteria) this;
        }

        public Criteria andRiskflagNotEqualTo(String value) {
            addCriterion("RISKFLAG <>", value, "riskflag");
            return (Criteria) this;
        }

        public Criteria andRiskflagGreaterThan(String value) {
            addCriterion("RISKFLAG >", value, "riskflag");
            return (Criteria) this;
        }

        public Criteria andRiskflagGreaterThanOrEqualTo(String value) {
            addCriterion("RISKFLAG >=", value, "riskflag");
            return (Criteria) this;
        }

        public Criteria andRiskflagLessThan(String value) {
            addCriterion("RISKFLAG <", value, "riskflag");
            return (Criteria) this;
        }

        public Criteria andRiskflagLessThanOrEqualTo(String value) {
            addCriterion("RISKFLAG <=", value, "riskflag");
            return (Criteria) this;
        }

        public Criteria andRiskflagLike(String value) {
            addCriterion("RISKFLAG like", value, "riskflag");
            return (Criteria) this;
        }

        public Criteria andRiskflagNotLike(String value) {
            addCriterion("RISKFLAG not like", value, "riskflag");
            return (Criteria) this;
        }

        public Criteria andRiskflagIn(List<String> values) {
            addCriterion("RISKFLAG in", values, "riskflag");
            return (Criteria) this;
        }

        public Criteria andRiskflagNotIn(List<String> values) {
            addCriterion("RISKFLAG not in", values, "riskflag");
            return (Criteria) this;
        }

        public Criteria andRiskflagBetween(String value1, String value2) {
            addCriterion("RISKFLAG between", value1, value2, "riskflag");
            return (Criteria) this;
        }

        public Criteria andRiskflagNotBetween(String value1, String value2) {
            addCriterion("RISKFLAG not between", value1, value2, "riskflag");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeIsNull() {
            addCriterion("NEWRISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeIsNotNull() {
            addCriterion("NEWRISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeEqualTo(String value) {
            addCriterion("NEWRISKCODE =", value, "newriskcode");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeNotEqualTo(String value) {
            addCriterion("NEWRISKCODE <>", value, "newriskcode");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeGreaterThan(String value) {
            addCriterion("NEWRISKCODE >", value, "newriskcode");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeGreaterThanOrEqualTo(String value) {
            addCriterion("NEWRISKCODE >=", value, "newriskcode");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeLessThan(String value) {
            addCriterion("NEWRISKCODE <", value, "newriskcode");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeLessThanOrEqualTo(String value) {
            addCriterion("NEWRISKCODE <=", value, "newriskcode");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeLike(String value) {
            addCriterion("NEWRISKCODE like", value, "newriskcode");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeNotLike(String value) {
            addCriterion("NEWRISKCODE not like", value, "newriskcode");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeIn(List<String> values) {
            addCriterion("NEWRISKCODE in", values, "newriskcode");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeNotIn(List<String> values) {
            addCriterion("NEWRISKCODE not in", values, "newriskcode");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeBetween(String value1, String value2) {
            addCriterion("NEWRISKCODE between", value1, value2, "newriskcode");
            return (Criteria) this;
        }

        public Criteria andNewriskcodeNotBetween(String value1, String value2) {
            addCriterion("NEWRISKCODE not between", value1, value2, "newriskcode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeIsNull() {
            addCriterion("ARTICLECODE is null");
            return (Criteria) this;
        }

        public Criteria andArticlecodeIsNotNull() {
            addCriterion("ARTICLECODE is not null");
            return (Criteria) this;
        }

        public Criteria andArticlecodeEqualTo(String value) {
            addCriterion("ARTICLECODE =", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeNotEqualTo(String value) {
            addCriterion("ARTICLECODE <>", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeGreaterThan(String value) {
            addCriterion("ARTICLECODE >", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeGreaterThanOrEqualTo(String value) {
            addCriterion("ARTICLECODE >=", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeLessThan(String value) {
            addCriterion("ARTICLECODE <", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeLessThanOrEqualTo(String value) {
            addCriterion("ARTICLECODE <=", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeLike(String value) {
            addCriterion("ARTICLECODE like", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeNotLike(String value) {
            addCriterion("ARTICLECODE not like", value, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeIn(List<String> values) {
            addCriterion("ARTICLECODE in", values, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeNotIn(List<String> values) {
            addCriterion("ARTICLECODE not in", values, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeBetween(String value1, String value2) {
            addCriterion("ARTICLECODE between", value1, value2, "articlecode");
            return (Criteria) this;
        }

        public Criteria andArticlecodeNotBetween(String value1, String value2) {
            addCriterion("ARTICLECODE not between", value1, value2, "articlecode");
            return (Criteria) this;
        }

        public Criteria andManageflagIsNull() {
            addCriterion("MANAGEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andManageflagIsNotNull() {
            addCriterion("MANAGEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andManageflagEqualTo(String value) {
            addCriterion("MANAGEFLAG =", value, "manageflag");
            return (Criteria) this;
        }

        public Criteria andManageflagNotEqualTo(String value) {
            addCriterion("MANAGEFLAG <>", value, "manageflag");
            return (Criteria) this;
        }

        public Criteria andManageflagGreaterThan(String value) {
            addCriterion("MANAGEFLAG >", value, "manageflag");
            return (Criteria) this;
        }

        public Criteria andManageflagGreaterThanOrEqualTo(String value) {
            addCriterion("MANAGEFLAG >=", value, "manageflag");
            return (Criteria) this;
        }

        public Criteria andManageflagLessThan(String value) {
            addCriterion("MANAGEFLAG <", value, "manageflag");
            return (Criteria) this;
        }

        public Criteria andManageflagLessThanOrEqualTo(String value) {
            addCriterion("MANAGEFLAG <=", value, "manageflag");
            return (Criteria) this;
        }

        public Criteria andManageflagLike(String value) {
            addCriterion("MANAGEFLAG like", value, "manageflag");
            return (Criteria) this;
        }

        public Criteria andManageflagNotLike(String value) {
            addCriterion("MANAGEFLAG not like", value, "manageflag");
            return (Criteria) this;
        }

        public Criteria andManageflagIn(List<String> values) {
            addCriterion("MANAGEFLAG in", values, "manageflag");
            return (Criteria) this;
        }

        public Criteria andManageflagNotIn(List<String> values) {
            addCriterion("MANAGEFLAG not in", values, "manageflag");
            return (Criteria) this;
        }

        public Criteria andManageflagBetween(String value1, String value2) {
            addCriterion("MANAGEFLAG between", value1, value2, "manageflag");
            return (Criteria) this;
        }

        public Criteria andManageflagNotBetween(String value1, String value2) {
            addCriterion("MANAGEFLAG not between", value1, value2, "manageflag");
            return (Criteria) this;
        }

        public Criteria andSettletypeIsNull() {
            addCriterion("SETTLETYPE is null");
            return (Criteria) this;
        }

        public Criteria andSettletypeIsNotNull() {
            addCriterion("SETTLETYPE is not null");
            return (Criteria) this;
        }

        public Criteria andSettletypeEqualTo(String value) {
            addCriterion("SETTLETYPE =", value, "settletype");
            return (Criteria) this;
        }

        public Criteria andSettletypeNotEqualTo(String value) {
            addCriterion("SETTLETYPE <>", value, "settletype");
            return (Criteria) this;
        }

        public Criteria andSettletypeGreaterThan(String value) {
            addCriterion("SETTLETYPE >", value, "settletype");
            return (Criteria) this;
        }

        public Criteria andSettletypeGreaterThanOrEqualTo(String value) {
            addCriterion("SETTLETYPE >=", value, "settletype");
            return (Criteria) this;
        }

        public Criteria andSettletypeLessThan(String value) {
            addCriterion("SETTLETYPE <", value, "settletype");
            return (Criteria) this;
        }

        public Criteria andSettletypeLessThanOrEqualTo(String value) {
            addCriterion("SETTLETYPE <=", value, "settletype");
            return (Criteria) this;
        }

        public Criteria andSettletypeLike(String value) {
            addCriterion("SETTLETYPE like", value, "settletype");
            return (Criteria) this;
        }

        public Criteria andSettletypeNotLike(String value) {
            addCriterion("SETTLETYPE not like", value, "settletype");
            return (Criteria) this;
        }

        public Criteria andSettletypeIn(List<String> values) {
            addCriterion("SETTLETYPE in", values, "settletype");
            return (Criteria) this;
        }

        public Criteria andSettletypeNotIn(List<String> values) {
            addCriterion("SETTLETYPE not in", values, "settletype");
            return (Criteria) this;
        }

        public Criteria andSettletypeBetween(String value1, String value2) {
            addCriterion("SETTLETYPE between", value1, value2, "settletype");
            return (Criteria) this;
        }

        public Criteria andSettletypeNotBetween(String value1, String value2) {
            addCriterion("SETTLETYPE not between", value1, value2, "settletype");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNull() {
            addCriterion("VALIDSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNotNull() {
            addCriterion("VALIDSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidstatusEqualTo(String value) {
            addCriterion("VALIDSTATUS =", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotEqualTo(String value) {
            addCriterion("VALIDSTATUS <>", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThan(String value) {
            addCriterion("VALIDSTATUS >", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS >=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThan(String value) {
            addCriterion("VALIDSTATUS <", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS <=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLike(String value) {
            addCriterion("VALIDSTATUS like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotLike(String value) {
            addCriterion("VALIDSTATUS not like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusIn(List<String> values) {
            addCriterion("VALIDSTATUS in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotIn(List<String> values) {
            addCriterion("VALIDSTATUS not in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS not between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwIsNull() {
            addCriterion("USECOMMONUW is null");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwIsNotNull() {
            addCriterion("USECOMMONUW is not null");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwEqualTo(String value) {
            addCriterion("USECOMMONUW =", value, "usecommonuw");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwNotEqualTo(String value) {
            addCriterion("USECOMMONUW <>", value, "usecommonuw");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwGreaterThan(String value) {
            addCriterion("USECOMMONUW >", value, "usecommonuw");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwGreaterThanOrEqualTo(String value) {
            addCriterion("USECOMMONUW >=", value, "usecommonuw");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwLessThan(String value) {
            addCriterion("USECOMMONUW <", value, "usecommonuw");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwLessThanOrEqualTo(String value) {
            addCriterion("USECOMMONUW <=", value, "usecommonuw");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwLike(String value) {
            addCriterion("USECOMMONUW like", value, "usecommonuw");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwNotLike(String value) {
            addCriterion("USECOMMONUW not like", value, "usecommonuw");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwIn(List<String> values) {
            addCriterion("USECOMMONUW in", values, "usecommonuw");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwNotIn(List<String> values) {
            addCriterion("USECOMMONUW not in", values, "usecommonuw");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwBetween(String value1, String value2) {
            addCriterion("USECOMMONUW between", value1, value2, "usecommonuw");
            return (Criteria) this;
        }

        public Criteria andUsecommonuwNotBetween(String value1, String value2) {
            addCriterion("USECOMMONUW not between", value1, value2, "usecommonuw");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeIsNull() {
            addCriterion("CALCULATEPREMIUMTYPE is null");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeIsNotNull() {
            addCriterion("CALCULATEPREMIUMTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeEqualTo(String value) {
            addCriterion("CALCULATEPREMIUMTYPE =", value, "calculatepremiumtype");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeNotEqualTo(String value) {
            addCriterion("CALCULATEPREMIUMTYPE <>", value, "calculatepremiumtype");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeGreaterThan(String value) {
            addCriterion("CALCULATEPREMIUMTYPE >", value, "calculatepremiumtype");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeGreaterThanOrEqualTo(String value) {
            addCriterion("CALCULATEPREMIUMTYPE >=", value, "calculatepremiumtype");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeLessThan(String value) {
            addCriterion("CALCULATEPREMIUMTYPE <", value, "calculatepremiumtype");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeLessThanOrEqualTo(String value) {
            addCriterion("CALCULATEPREMIUMTYPE <=", value, "calculatepremiumtype");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeLike(String value) {
            addCriterion("CALCULATEPREMIUMTYPE like", value, "calculatepremiumtype");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeNotLike(String value) {
            addCriterion("CALCULATEPREMIUMTYPE not like", value, "calculatepremiumtype");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeIn(List<String> values) {
            addCriterion("CALCULATEPREMIUMTYPE in", values, "calculatepremiumtype");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeNotIn(List<String> values) {
            addCriterion("CALCULATEPREMIUMTYPE not in", values, "calculatepremiumtype");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeBetween(String value1, String value2) {
            addCriterion("CALCULATEPREMIUMTYPE between", value1, value2, "calculatepremiumtype");
            return (Criteria) this;
        }

        public Criteria andCalculatepremiumtypeNotBetween(String value1, String value2) {
            addCriterion("CALCULATEPREMIUMTYPE not between", value1, value2, "calculatepremiumtype");
            return (Criteria) this;
        }

        public Criteria andCompositeflagIsNull() {
            addCriterion("COMPOSITEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andCompositeflagIsNotNull() {
            addCriterion("COMPOSITEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andCompositeflagEqualTo(String value) {
            addCriterion("COMPOSITEFLAG =", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagNotEqualTo(String value) {
            addCriterion("COMPOSITEFLAG <>", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagGreaterThan(String value) {
            addCriterion("COMPOSITEFLAG >", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagGreaterThanOrEqualTo(String value) {
            addCriterion("COMPOSITEFLAG >=", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagLessThan(String value) {
            addCriterion("COMPOSITEFLAG <", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagLessThanOrEqualTo(String value) {
            addCriterion("COMPOSITEFLAG <=", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagLike(String value) {
            addCriterion("COMPOSITEFLAG like", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagNotLike(String value) {
            addCriterion("COMPOSITEFLAG not like", value, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagIn(List<String> values) {
            addCriterion("COMPOSITEFLAG in", values, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagNotIn(List<String> values) {
            addCriterion("COMPOSITEFLAG not in", values, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagBetween(String value1, String value2) {
            addCriterion("COMPOSITEFLAG between", value1, value2, "compositeflag");
            return (Criteria) this;
        }

        public Criteria andCompositeflagNotBetween(String value1, String value2) {
            addCriterion("COMPOSITEFLAG not between", value1, value2, "compositeflag");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}