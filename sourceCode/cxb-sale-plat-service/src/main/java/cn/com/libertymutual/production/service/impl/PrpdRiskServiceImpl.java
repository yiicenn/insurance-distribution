package cn.com.libertymutual.production.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdkindMapper;
import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdriskMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkind;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkindExample;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdrisk;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskExample.Criteria;
import cn.com.libertymutual.production.pojo.request.LinkRiskRequest;
import cn.com.libertymutual.production.pojo.request.PrpdRiskRequest;
import cn.com.libertymutual.production.service.api.PrpdRiskService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class PrpdRiskServiceImpl implements PrpdRiskService {

	@Autowired
	private PrpdriskMapper prpdriskMapper;
	@Autowired
	private PrpdkindMapper prpdkindMapper;

	@Override
	public List<Prpdrisk> findAllPrpdRisk() {
		PrpdriskExample prpdriskExampl = new PrpdriskExample();
		prpdriskExampl.createCriteria().andValidstatusEqualTo("1");
		prpdriskExampl.setOrderByClause("riskcode");
		return prpdriskMapper.selectByExample(prpdriskExampl);
	}
	

	@Override
	public List<Prpdrisk> fetchRisks(PrpdRiskRequest request) {
		PrpdriskExample example = new PrpdriskExample();
		Criteria criteria = example.createCriteria();
		if(!StringUtils.isEmpty(request.getRiskcode())) {
			criteria.andRiskcodeEqualTo(request.getRiskcode());
		}
		if(!StringUtils.isEmpty(request.getRiskversion())) {
			criteria.andRiskversionEqualTo(request.getRiskversion());
		}
		return prpdriskMapper.selectByExample(example);
	}


	@Override
	public PageInfo<Prpdrisk> findAllByPage(PrpdRiskRequest request) {
		
		//查询条款已关联的险种代码
		List<String> linkedRiskCodes = new ArrayList<>();
		if (request.getKind() != null) {
			PrpdkindExample example = new PrpdkindExample();
			example.createCriteria().andKindcodeEqualTo(request.getKind().getKindcode())
			.andKindversionEqualTo(request.getKind().getKindversion());
			List<Prpdkind> prpdkinds = prpdkindMapper.selectByExample(example);
			for (Prpdkind kind : prpdkinds) {
				if (!linkedRiskCodes.contains(kind.getRiskcode())) {
					linkedRiskCodes.add(kind.getRiskcode());
				}
			}
		}
		
		//查询条款还没有关联的险种
		PrpdriskExample prpdriskExample = new PrpdriskExample();
		Criteria criteria = prpdriskExample.createCriteria();
		String riskCode = request.getRiskcode();
		String riskCName = request.getRiskcname();
		String validStatus = request.getValidstatus();
		String orderBy = request.getOrderBy();
		String order = request.getOrder();
		
		if(!StringUtils.isEmpty(riskCode)) {
			criteria.andRiskcodeLike("%" + riskCode.toUpperCase() + "%");
		}
		if(!StringUtils.isEmpty(riskCName)) {
			criteria.andRiskcnameLike("%" + riskCName + "%");
		}
		if(!StringUtils.isEmpty(validStatus)) {
			criteria.andValidstatusEqualTo(validStatus);
		}
		if (!StringUtils.isEmpty(orderBy) && !"undefined".equals(orderBy)) {
			prpdriskExample.setOrderByClause(orderBy + " " + order);
		}
		//排除已关联的险种代码
		if (!linkedRiskCodes.isEmpty()) {
			criteria.andRiskcodeNotIn(linkedRiskCodes);
		}
		
		PageHelper.startPage(request.getCurrentPage(), request.getPageSize());
		PageInfo<Prpdrisk> pageInfo = new PageInfo<>(prpdriskMapper.selectByExample(prpdriskExample));
		
		return pageInfo;
	}

	@Override
	public PageInfo<Prpdrisk> selectByPage(List<String> riskCodes, LinkRiskRequest linkRiskRequest) {
		PrpdriskExample example = new PrpdriskExample();
		Criteria criteria = example.createCriteria();
		criteria.andRiskcodeIn(riskCodes);
		PageHelper.startPage(linkRiskRequest.getCurrentPage(), linkRiskRequest.getPageSize());
		PageInfo<Prpdrisk> pageInfo = new PageInfo<>(prpdriskMapper.selectByExample(example));
		return pageInfo;
	}

	@Override
	public PageInfo<Prpdrisk> findByClassAndComposite(PrpdRiskRequest request, List<String> compositeClasses) {
		//查询条款已关联的险种代码
		List<String> linkedRiskCodes = new ArrayList<>();
		String classCode = "";
		if (request.getKind() != null) {
			classCode = request.getKind().getOwnerriskcode();
			PrpdkindExample example = new PrpdkindExample();
			example.createCriteria().andKindcodeEqualTo(request.getKind().getKindcode()).andKindversionEqualTo(request.getKind().getKindversion());
			List<Prpdkind> prpdkinds = prpdkindMapper.selectByExample(example);
			for (Prpdkind kind : prpdkinds) {
				if (!linkedRiskCodes.contains(kind.getRiskcode())) {
					String prefixCode = kind.getRiskcode().substring(0, 2);  //组合险可以多次关联，不需要排除
					if (!compositeClasses.contains(prefixCode)) {
						linkedRiskCodes.add(kind.getRiskcode());
					}
				}
			}
		}
		
		//查询条款还没有关联的险种
		PrpdriskExample prpdriskExample = new PrpdriskExample();
		Criteria criteria = prpdriskExample.createCriteria();
		String riskCode = request.getRiskcode();
		String riskCName = request.getRiskcname();
		String validStatus = request.getValidstatus();
		String orderBy = request.getOrderBy();
		String order = request.getOrder();
		
		if(!StringUtils.isEmpty(riskCode)) {
			criteria.andRiskcodeLike("%" + riskCode.toUpperCase() + "%");
		}
		if(!StringUtils.isEmpty(riskCName)) {
			criteria.andRiskcnameLike("%" + riskCName + "%");
		}
		if(!StringUtils.isEmpty(validStatus)) {
			criteria.andValidstatusEqualTo(validStatus);
		}
		if(!StringUtils.isEmpty(classCode)) {
			if(compositeClasses == null) {
				compositeClasses = new ArrayList<String>();
			}
			compositeClasses.add(classCode);
		}
		if(!compositeClasses.isEmpty()) {
			/**
			 * 条款关联险种时，查询出所有还未关联的险种，故移除以下代码
			 */
//			criteria.andClasscodeIn(compositeClasses);
		}
		if (!StringUtils.isEmpty(orderBy) && !"undefined".equals(orderBy)) {
			prpdriskExample.setOrderByClause(orderBy + " " + order);
		}
		//排除已关联的险种代码
		if (!linkedRiskCodes.isEmpty()) {
			criteria.andRiskcodeNotIn(linkedRiskCodes);
		}
		
		//////////////////////////////////////////////////////////////////
		//06条款特殊处理（06条款可关联66险种）
		if ("06".equals(classCode) && !linkedRiskCodes.contains("6666")) {
			prpdriskExample.or().andRiskcodeEqualTo("6666").andValidstatusEqualTo("1");
		}
		//////////////////////////////////////////////////////////////////
		
		prpdriskExample.setOrderByClause("riskcode");
		PageHelper.startPage(request.getCurrentPage(), request.getPageSize());
		PageInfo<Prpdrisk> pageInfo = new PageInfo<>(prpdriskMapper.selectByExample(prpdriskExample));
		
		return pageInfo;
	}

	@Override
	public List<Prpdrisk> findByClass(String classcode) {
		PrpdriskExample example = new PrpdriskExample();
		Criteria criteria = example.createCriteria();
		/**
		 * 条款关联险种时，给组合险指定归属险种时，查询出所有非组合险种
		 */
//		criteria.andClasscodeEqualTo(classcode);
		criteria.andCompositeflagEqualTo("0");
		criteria.andValidstatusEqualTo("1");
		example.setOrderByClause("riskcode");
		return prpdriskMapper.selectByExample(example);
	}

	@Override
	public boolean cheakRiskCodeUsed(String riskcode) {
		boolean isUsed = false;
		if(!StringUtils.isEmpty(riskcode)) {
			PrpdriskExample example = new PrpdriskExample();
			Criteria criteria = example.createCriteria();
			criteria.andRiskcodeEqualTo(riskcode);
			List<Prpdrisk> list = prpdriskMapper.selectByExample(example);
			if(list.size() > 0) {
				isUsed = true;
			}
		}
		return isUsed;
	}

	@Override
	public void insert(Prpdrisk record) {
		prpdriskMapper.insertSelective(record);
	}

	@Override
	public void update(Prpdrisk record) {
		PrpdriskExample example = new PrpdriskExample();
		Criteria criteria = example.createCriteria();
		criteria.andRiskcodeEqualTo(record.getRiskcode()).andRiskversionEqualTo(record.getRiskversion());
		prpdriskMapper.updateByExampleSelective(record, example);
	}

	@Override
	public List<Prpdrisk> queryRiskByCodes(List<String> riskcodes) {
		PrpdriskExample example = new PrpdriskExample();
		Criteria criteria = example.createCriteria();
		criteria.andRiskcodeIn(riskcodes);
		criteria.andValidstatusEqualTo("1");
		example.setOrderByClause("riskcode");
		return prpdriskMapper.selectByExample(example);
	}
}
