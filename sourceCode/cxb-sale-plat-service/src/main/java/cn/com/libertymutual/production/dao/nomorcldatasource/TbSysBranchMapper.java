package cn.com.libertymutual.production.dao.nomorcldatasource;

import cn.com.libertymutual.production.model.nomorcldatasource.TbSysBranch;
import cn.com.libertymutual.production.model.nomorcldatasource.TbSysBranchExample;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface TbSysBranchMapper {
    int countByExample(TbSysBranchExample example);

    int deleteByExample(TbSysBranchExample example);

    int deleteByPrimaryKey(String branchNo);

    int insert(TbSysBranch record);

    int insertSelective(TbSysBranch record);

    List<TbSysBranch> selectByExample(TbSysBranchExample example);

    TbSysBranch selectByPrimaryKey(String branchNo);

    int updateByExampleSelective(@Param("record") TbSysBranch record, @Param("example") TbSysBranchExample example);

    int updateByExample(@Param("record") TbSysBranch record, @Param("example") TbSysBranchExample example);

    int updateByPrimaryKeySelective(TbSysBranch record);

    int updateByPrimaryKey(TbSysBranch record);
}