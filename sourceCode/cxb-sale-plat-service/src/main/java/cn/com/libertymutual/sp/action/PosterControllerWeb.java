package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpPoster;
import cn.com.libertymutual.sp.service.api.PosterService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/posterWeb")
public class PosterControllerWeb {

	
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private PosterService posterService;
	
	/*
	 * 海报查询
	 */
	@ApiOperation(value = "海报查询", notes = "海报查询")
	@PostMapping(value = "/posterList")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "name", value = "海报名称", required = false, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "type", value = "海报类型", required = false, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "status", value = "海报状态", required = false, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
		@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), 
	})
	public ServiceResult posterList(String productName, String riskCode, String name,String type,String status,int pageNumber, int pageSize) {
		ServiceResult sr = new ServiceResult();
		
		sr = posterService.posterList(productName,riskCode,name,type,status,pageNumber,pageSize);
		return sr;

	}
	
	
	/*
	 * 新增或者修改文章
	 */

	@ApiOperation(value = "新增文章", notes = "新增文章")
	@PostMapping(value = "/addOrUpdatePoster")
	public ServiceResult addOrUpdatePoster(@RequestBody @ApiParam(name = "tbSpPoster", value = "tbSpPoster", required = true)TbSpPoster tbSpPoster) {
		ServiceResult sr = new ServiceResult();
		
		sr = posterService.addOrUpdatePoster(tbSpPoster);
		return sr;

	}
	
	
	/*
	 * 审批
	 */
	
	@ApiOperation(value = "审批", notes = "审批")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "approveType", value = "审批类型", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "Long"),
		@ApiImplicitParam(name = "type", value = "通过与否", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "remark", value = "备注", required = true, paramType = "query", dataType = "String"),
	})
	@PostMapping(value = "/approve")
	public ServiceResult approve(String approveType,Integer id,String type,String remark) {
		ServiceResult sr = new ServiceResult();
		
		sr = posterService.approve(approveType,id,type,remark);
		return sr;

	}
	
	
	
	
	@ApiOperation(value = "审批历史", notes = "审批历史")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "approveType", value = "审批类型", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "Long"),
		@ApiImplicitParam(name = "pageNumber", value = "", required = true, paramType = "query", dataType = "Long"),
		@ApiImplicitParam(name = "pageSize", value = "", required = true, paramType = "query", dataType = "Long"),
	})
	@PostMapping(value = "/approveHis")
	public ServiceResult approveHistory(String approveType,Integer id,Integer pageNumber,Integer pageSize) {
		ServiceResult sr = new ServiceResult();
		sr = posterService.approveHistory(approveType,id,pageNumber,pageSize);
		return sr;

	}
	
	
	@ApiOperation(value = "待审批记录", notes = "待审批记录")
	@ApiImplicitParams(value = {
		@ApiImplicitParam(name = "approveType", value = "审批类型", required = true, paramType = "query", dataType = "String"),
		@ApiImplicitParam(name = "pageNumber", value = "", required = true, paramType = "query", dataType = "Long"),
		@ApiImplicitParam(name = "pageSize", value = "", required = true, paramType = "query", dataType = "Long"),
	})
	@PostMapping(value = "/waitAapprove")
	public ServiceResult waitAapprove(String approveType,Integer pageNumber,Integer pageSize) {
		ServiceResult sr = new ServiceResult();
		sr = posterService.waitAapprove(approveType,Constants.APPROVE_WAIT,pageNumber,pageSize);
		return sr;

	}
	
	
	
}
