package cn.com.libertymutual.sp.dto;

import java.io.File;
import java.util.List;

import cn.com.libertymutual.core.base.dto.RequestBaseDto;

public class FileUploadDto extends RequestBaseDto {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String businessNo;
	private String sort;
	private String code;
	private String fileTitle;
	private String fromSystem;
	private List<File> fileList;

	public String getBusinessNo() {
		return businessNo;
	}

	public void setBusinessNo(String businessNo) {
		this.businessNo = businessNo;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFileTitle() {
		return fileTitle;
	}

	public void setFileTitle(String fileTitle) {
		this.fileTitle = fileTitle;
	}

	public String getFromSystem() {
		return fromSystem;
	}

	public void setFromSystem(String fromSystem) {
		this.fromSystem = fromSystem;
	}

	public List<File> getFileList() {
		return fileList;
	}

	public void setFileList(List<File> fileList) {
		this.fileList = fileList;
	}

}
