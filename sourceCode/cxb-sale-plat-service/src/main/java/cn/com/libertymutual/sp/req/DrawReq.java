package cn.com.libertymutual.sp.req;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
@ApiModel
public class DrawReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2771233047939581200L;

	private Integer id;
	private Double  budget;
	private Integer addDrawTimes;
	private Integer addSpecialTimes;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getBudget() {
		return budget;
	}
	public void setBudget(Double budget) {
		this.budget = budget;
	}
	public Integer getAddDrawTimes() {
		return addDrawTimes;
	}
	public void setAddDrawTimes(Integer addDrawTimes) {
		this.addDrawTimes = addDrawTimes;
	}
	public Integer getAddSpecialTimes() {
		return addSpecialTimes;
	}
	public void setAddSpecialTimes(Integer addSpecialTimes) {
		this.addSpecialTimes = addSpecialTimes;
	}
	
	
}
