package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhtreaty;

/**
 * @author steven.li
 * @create 2017/12/20
 */
public interface FhtreatyService {

    void insert(Fhtreaty record);

    List<Fhtreaty> findAll(Fhtreaty where);
}
