package cn.com.libertymutual.sp.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sp.bean.TbSpPlan;


@Repository
public interface SpPlanDao extends PagingAndSortingRepository<TbSpPlan, Integer>, JpaSpecificationExecutor<TbSpPlan>
{

	@Query("select t from TbSpPlan t where t.productId = ?1 and t.status = 1 order by t.serialNo")
	List<TbSpPlan> findByProductId(String productId);

	@Query("select t from TbSpPlan t where t.productId = ?1 order by t.serialNo")
	List<TbSpPlan> findAllByProductId(String productId);
	
	@Transactional
	@Modifying
	@Query("delete from TbSpPlan where productId = ?1 ")
	void deleteByProduct(String productId);

	@Query("select t.id  from TbSpPlan t where t.productId = ?1 and t.status = 1")
	List<Integer> findIdsByProductId(String productId);

	@Transactional
	@Modifying
	@Query("delete from TbSpPlan where id = ?1 ")
	void deleteById(Integer id);

	@Query("from TbSpPlan where status = 1")
	List<TbSpPlan> findAllPlan();
}
