package cn.com.libertymutual.production.dao.nomorcldatasource;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpditemlibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemlibraryExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpditemlibraryWithBLOBs;
import cn.com.libertymutual.production.pojo.response.ItemKind;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
@Mapper
public interface PrpditemlibraryMapper {
    int countByExample(PrpditemlibraryExample example);

    int deleteByExample(PrpditemlibraryExample example);

    int deleteByPrimaryKey(String itemcode);

    int insert(PrpditemlibraryWithBLOBs record);

    int insertSelective(PrpditemlibraryWithBLOBs record);

    List<PrpditemlibraryWithBLOBs> selectByExampleWithBLOBs(PrpditemlibraryExample example);

    List<Prpditemlibrary> selectByExample(PrpditemlibraryExample example);

    PrpditemlibraryWithBLOBs selectByPrimaryKey(String itemcode);

    int updateByExampleSelective(@Param("record") PrpditemlibraryWithBLOBs record, @Param("example") PrpditemlibraryExample example);

    int updateByExampleWithBLOBs(@Param("record") PrpditemlibraryWithBLOBs record, @Param("example") PrpditemlibraryExample example);

    int updateByExample(@Param("record") Prpditemlibrary record, @Param("example") PrpditemlibraryExample example);

    int updateByPrimaryKeySelective(PrpditemlibraryWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(PrpditemlibraryWithBLOBs record);

    int updateByPrimaryKey(Prpditemlibrary record);
    
    @Select({"<script>",
		 "SELECT * FROM prpditemlibrary b where b.validstatus = '1' ",
		 "and (b.id in ('1','2') or b.id is null) ",
		 "<when test='itemCode!=null'>",
		 	"and b.itemcode like '%'||#{itemCode}||'%' ",
		 "</when>",
		 "<when test='itemCName!=null'>",
		 	"and b.itemcname like '%'||#{itemCName}||'%' ",
		 "</when>",
		 " and not exists ",
		 "(SELECT 1 FROM prpditem a ",
		 "where a.itemcode = b.itemcode ",
		 "and a.riskcode = #{riskCode} ",
		 "and a.riskversion = #{riskVersion} ",
		 "and a.plancode = #{planCode})",
		"</script>"})
List<PrpditemlibraryWithBLOBs> selectNotRiskLinked(@Param("riskCode") String riskCode,
										  @Param("riskVersion") String riskVersion,
										  @Param("planCode") String planCode,
										  @Param("itemCode") String itemCode,
										  @Param("itemCName") String itemCName,
										  @Param("pageNum") int pageNum,
										  @Param("pageSize") int pageSize);

@Select(
	    "SELECT DISTINCT A.*, B.KINDCODE, B.KINDVERSION, concat(A.ITEMCODE,B.KINDCODE) AS 'KEY' "
	    		 + "  FROM PRPDITEMLIBRARY A, PRPDKINDITEM B"
	    		 + " WHERE A.ITEMCODE = B.ITEMCODE"
	    		 + "           AND B.RISKCODE = #{riskCode}"
	    		 + "           AND B.RISKVERSION = #{riskVersion}"
	    		 + "           AND B.KINDCODE = #{kindCode}"
	    		 + "           AND B.KINDVERSION = #{kindVersion}"
		)
List<ItemKind> selectByRiskKindLinked(@Param("riskCode") String riskCode,
									  @Param("riskVersion") String riskVersion,
									  @Param("kindCode") String kindCode,
									  @Param("kindVersion") String kindVersion);


@Select("SELECT DISTINCT B.*, A.RISKCODE, A.FLAG AS OWNERITEMCODE FROM PRPDKINDITEM A LEFT JOIN PRPDITEMLIBRARY B "
		+ "ON B.ITEMCODE = A.ITEMCODE AND B.VALIDSTATUS != '0' "
		+ "WHERE  A.KINDCODE = #{kindCode} AND A.KINDVERSION = #{kindVersion}")
List<ItemKind> selectWithItemKind(@Param("kindCode") String kindCode,
								  @Param("kindVersion") String kindVersion,
								  @Param("pageNum") int pageNum,
								  @Param("pageSize") int pageSize);
}