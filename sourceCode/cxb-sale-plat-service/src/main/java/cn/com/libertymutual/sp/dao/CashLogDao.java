package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sp.bean.TbSpCashLog;

public interface CashLogDao extends PagingAndSortingRepository<TbSpCashLog, Integer>, JpaSpecificationExecutor<TbSpCashLog> {

	@Query(" from TbSpCashLog where businessNo= ?1 and status='9'")
	List<TbSpCashLog> findFailBybusinessNo(String businessNo);

	@Query(" from TbSpCashLog where userCode= ?1 or userCode= ?2  and status='00'")
	List<TbSpCashLog> findByUserCode(String userCode, String userCodeBs);

	@Query(" from TbSpCashLog where phoneNo= ?1 and status='00'")
	List<TbSpCashLog> findByMobile(String mobile);

	@Query(" from TbSpCashLog where businessNo= ?1 ")
	TbSpCashLog findBybusinessNo(String businessNo);

	// 修改用户编码
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update TbSpCashLog set userCode = :userCode, phoneNo = :phoneNo where userCode = :userCode1 and phoneNo = :phoneNo1")
	public int updateUserCodeByUserCode(@Param("userCode") String userCode, @Param("phoneNo") String phoneNo, @Param("userCode1") String userCode1,
			@Param("phoneNo1") String phoneNo1);
}
