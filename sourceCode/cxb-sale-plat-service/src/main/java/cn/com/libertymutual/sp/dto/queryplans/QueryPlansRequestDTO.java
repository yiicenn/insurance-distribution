package cn.com.libertymutual.sp.dto.queryplans;

import java.io.Serializable;
import java.util.Map;

import cn.com.libertymutual.core.base.dto.RequestBaseDto;



public class QueryPlansRequestDTO  extends RequestBaseDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4327671355137839832L;
    private  String companyCode;//分公司代码
	private String agreementCode;//非车业务关系代码
   
	private int seatCount;//座位数 
	private int age;//被保人年龄
	
	//private  String startDate;//起保时间
	//private String endDate;//终保时间
	//private String idNo;//被保人身份证
	//private String startHour;// 起保小时
	// String endHour;// 终保小时
	
	/*有以下两个请求参数之一，优先按照以下条件查询 */
	private String[]  ipaPlanId;//3105 百万宝 计划ID
	private String[]  gpaPlanCode;//2724安行天下  计划code
	private String queryType;// 查询方式： 1-以id 或code查询 

	private String agentCode;//代理人代码
	private String[] products;//查询产品：2724、3105
	/*新查询方式：以代理人代码和动态参数 查询
	 * 动态参数  key:value,比如： 00005:1
	 * 00005 车辆类型(0-货运车,1-乘用车)
	 * 00006 车辆使用性质(0-营业,1-非营业)
	 * 00007 车辆吨位数
	 * 00001 座位
	 * 00002 天
	 * 00003 年龄
	 * 00004 地区
	 * */
	private  Map<String,String> paraMap;
    


	public String getAgreementCode() {
		return agreementCode;
	}

	public void setAgreementCode(String agreementCode) {
		this.agreementCode = agreementCode;
	}

	public int getSeatCount() {
		return seatCount;
	}

	public void setSeatCount(int seatCount) {
		this.seatCount = seatCount;
	}


	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String[] getIpaPlanId() {
		return ipaPlanId;
	}

	public void setIpaPlanId(String[] ipaPlanId) {
		this.ipaPlanId = ipaPlanId;
	}

	public String[] getGpaPlanCode() {
		return gpaPlanCode;
	}

	public void setGpaPlanCode(String[] gpaPlanCode) {
		this.gpaPlanCode = gpaPlanCode;
	}

	public String getQueryType() {
		return queryType;
	}

	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}

	

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public Map<String, String> getParaMap() {
		return paraMap;
	}

	public void setParaMap(Map<String, String> paraMap) {
		this.paraMap = paraMap;
	}

	public String[] getProducts() {
		return products;
	}

	public void setProducts(String[] products) {
		this.products = products;
	}
	
	
}
