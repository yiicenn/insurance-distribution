
package cn.com.libertymutual.sp.webService.policy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for policyDetailInquiry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="policyDetailInquiry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="policyDetailInquiryRequestDto" type="{http://prpall.liberty.com/all/cb/policyDetailInquiry/bean}policyDetailInquiryRequestDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "policyDetailInquiry", namespace = "http://prpall.liberty.com/all/cb/policyDetailInquiry/intf/policyDetailInquiry", propOrder = {
    "policyDetailInquiryRequestDto"
})
public class PolicyDetailInquiry {

    protected PolicyDetailInquiryRequestDto policyDetailInquiryRequestDto;

    /**
     * Gets the value of the policyDetailInquiryRequestDto property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDetailInquiryRequestDto }
     *     
     */
    public PolicyDetailInquiryRequestDto getPolicyDetailInquiryRequestDto() {
        return policyDetailInquiryRequestDto;
    }

    /**
     * Sets the value of the policyDetailInquiryRequestDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDetailInquiryRequestDto }
     *     
     */
    public void setPolicyDetailInquiryRequestDto(PolicyDetailInquiryRequestDto value) {
        this.policyDetailInquiryRequestDto = value;
    }

}
