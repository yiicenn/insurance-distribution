package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sp.bean.TbSpDrawLog;

@Repository
public interface DrawLogDao extends PagingAndSortingRepository<TbSpDrawLog, Integer>, JpaSpecificationExecutor<TbSpDrawLog> {

	/**
	 * 查询未抽奖记录
	 * @return
	 */
	@Query(value = " select * from tb_sp_drawlog t ,tb_sp_activity ac where t.user_code= :userCode and t.`STATUS` ='1' and t.ACTIVITY_NO=ac.activity_code and ac.`status` in ('1','3','4') and t.effictive_date <= date_format(SYSDATE(),'%Y-%m-%d') and t.invalid_date >= date_format(SYSDATE(),'%Y-%m-%d')", nativeQuery = true)
	List<TbSpDrawLog> findByUserCode(@Param("userCode") String userCode);

	/**
	 * 查询特殊金额记录数
	 * @return
	 */
	// @Query(" from TbSpDrawLog t where t.userCode= ?1 and t.status ='2' and
	// t.activityNo = ?2 and t.amount= ?3")
	// List<TbSpDrawLog> findSpecialByUsercode(String userCode,String
	// activityNo,double specialAmount);

	/**
	 * 查询特殊金额记录数
	 * @return
	 */
	@Query(" from TbSpDrawLog t  where  t.status ='2' and t.activityNo = ?1 and  t.amount= ?2")
	List<TbSpDrawLog> findSpecial(String activityNo, double specialAmount);

	// 修改用户编码
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update TbSpDrawLog set userCode = :userCode, telephone = :telephone where userCode = :userCode1 and telephone = :telephone1")
	public int updateUserCodeByUserCode(@Param("userCode") String userCode, @Param("telephone") String telephone,
			@Param("userCode1") String userCode1, @Param("telephone1") String phoneNo1);

}
