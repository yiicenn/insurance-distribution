package cn.com.libertymutual.sp.action;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import cn.com.libertymutual.core.annotation.SystemValidate;
import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.CodeRelateService;
import cn.com.libertymutual.sp.service.api.QueryPrdCodeService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/prpd")
public class QueryPrpdCode {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private CodeRelateService codeRelateService;

	@Resource
	RestTemplate restTemplate;
	@Resource
	private RedisUtils redisUtils;
	@Autowired
	private QueryPrdCodeService queryPrdCodeService;

	@ApiOperation(value = "前端职业类别接口", notes = "前端职业类别接口")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "level", value = "级别", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "codeCode", value = "职业编码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "levelMax", value = "最大工种等级", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "levelMin", value = "最小工种等级", required = false, paramType = "query", dataType = "Long") })
	@PostMapping(value = "/queryCode")
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult queryCode(String level, String codeCode, int levelMax, int levelMin, boolean type) {

		return codeRelateService.queryCode(level, codeCode, levelMax, levelMin, type);
	}

	@ApiOperation(value = "获取验车码", notes = "微信自动回复一串6位数的随机编码 & 更新编码列表（含历史编码）， 系统触发邮件给George zhu、Leo Wang、William Guo、Menny Du")
	@PostMapping(value = "/queryCarCode")
	public ServiceResult queryCarCode() {
		ServiceResult sr = queryPrdCodeService.queryCheckCode();
		return sr;
	}
}
