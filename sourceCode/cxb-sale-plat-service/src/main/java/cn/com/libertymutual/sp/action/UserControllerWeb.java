package cn.com.libertymutual.sp.action;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpUser;
import cn.com.libertymutual.sp.req.StoreRateReq;
import cn.com.libertymutual.sp.service.api.ShopService;
import cn.com.libertymutual.sp.service.api.UserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/userWeb")
public class UserControllerWeb {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private UserService userService;
	@Autowired
	private ShopService shopService;
	/*
	 * 后台分类查询所有的用户
	 */

	@ApiOperation(value = "后台分类查询所有的用户", notes = "后台分类查询所有的用户")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "storeStartDate", value = "开店日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "storeEndDate", value = "开店日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "signStartTime", value = "注册日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "signEndTime", value = "注册日期", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userCode", value = "用户id", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userName", value = "客户名", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "mobile", value = "手机号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "nickName", value = "真实姓名", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "branchCode", value = "分公司", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "agrementNo", value = "业务关系代码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "userType", value = "用户分类", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "points", value = "积分余额", required = false, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "storeFlag", value = "是否店主", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "agrementNo", value = "业务关系代码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "分页", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "分页", required = true, paramType = "query", dataType = "Long"), })
	@PostMapping(value = "/userList")
	public ServiceResult userList(String storeStartDate,String storeEndDate,String userCode, String signStartTime, String signEndTime, String userName, String mobile, String nickName,
			String branchCode, String agrementNo, String userType, Integer Maxpoints, Integer Minpoints, String storeFlag, Integer pageNumber,
			Integer pageSize) {

		try {
			return userService.findAllUser(storeStartDate,storeEndDate,userCode, signStartTime, signEndTime, userName, mobile, nickName, branchCode, agrementNo, userType,
					Maxpoints, Minpoints, storeFlag, pageNumber, pageSize);
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * 修改客户的真实姓名和证件号 LJ
	 */

	@ApiOperation(value = "修改客户的真实姓名和证件号", notes = "修改客户的真实姓名和证件号")
	@PostMapping(value = "/updateUser")
	public ServiceResult updateUser(@RequestBody @ApiParam(name = "tbSpUser", value = "tbSpUser", required = true) TbSpUser tbSpUser) {

		try {
			return userService.updateUser(tbSpUser);
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	@ApiOperation(value = "新增用户", notes = "新增用户")
	@PostMapping(value = "/addUser")
	public ServiceResult addUser(@RequestBody @ApiParam(name = "tbSpUser", value = "tbSpUser", required = true) TbSpUser tbSpUser) {
		try {
			return userService.addUser(tbSpUser);
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * 查询所有店主 LJ
	 */

	@ApiOperation(value = "后台分类查询所有的用户", notes = "后台分类查询所有的用户")
	@PostMapping(value = "/allShopUser")
	public ServiceResult findAllShopUser() {

		try {
			return userService.findAllShopUser();
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * 配置店铺佣金比例
	 */

	@ApiOperation(value = "配置店铺佣金比例", notes = "配置店铺佣金比例")
	@PostMapping(value = "/setShopRate")
	public ServiceResult setShopRate(@RequestBody @ApiParam(name = "storeRate", value = "storeRate", required = true) StoreRateReq storeRate) {
		ServiceResult sr = new ServiceResult();
		if (StringUtils.isEmpty(storeRate.getUserCodes())) {
			sr.setFail();
			sr.setResult("信息不能为空！");
			return sr;
		}
		try {
			sr = userService.setShopRate(storeRate);
			return sr;
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	@ApiOperation(value = "配置店铺佣金比例", notes = "配置店铺佣金比例")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "agrementNo", value = "业务关系代码", required = false, paramType = "query", dataType = "String"), })
	@PostMapping(value = "/shopRates")
	public ServiceResult shopRate(String agrementNo) {
		ServiceResult sr = new ServiceResult();
		if (StringUtils.isEmpty(agrementNo)) {
			sr.setFail();
			sr.setResult("信息不能为空！");
			return sr;
		}
		try {
			sr.setResult(shopService.findAgreementNo(agrementNo));
			return sr;
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * 店铺佣金比例列表
	 */

	@ApiOperation(value = "店铺佣金比例列表", notes = "店铺佣金比例列表")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "分页", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "分页", required = true, paramType = "query", dataType = "Long"), })
	@PostMapping(value = "/shopRate")
	public ServiceResult shopRate(String userCode, int pageNumber, int pageSize) {

		try {
			return userService.shopRate(userCode, pageNumber, pageSize);
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * 删除店铺佣金比例列表
	 */

	@ApiOperation(value = "删除店铺佣金比例列表", notes = "删除店铺佣金比例列表")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "Long"), })
	@PostMapping(value = "/deleteShopRate")
	public ServiceResult deleteShopRate(Integer id) {

		try {
			return userService.deleteShopRate(id, "shop");
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * 业务关系代码佣金比例列表
	 */

	@ApiOperation(value = "业务关系代码佣金比例列表", notes = "业务关系代码佣金比例列表")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "agrementNo", value = "业务关系代码", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "分页", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "分页", required = true, paramType = "query", dataType = "Long"), })
	@PostMapping(value = "/agreeRate")
	public ServiceResult agreeRate(String agrementNo, int pageNumber, int pageSize) {

		try {
			return userService.agreeRate(agrementNo, pageNumber, pageSize);
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * 删除业务关系代码佣金比例
	 */

	@ApiOperation(value = "删除业务关系代码佣金比例", notes = "删除业务关系代码佣金比例")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "Long"), })
	@PostMapping(value = "/deleteAgreeRate")
	public ServiceResult deleteAgreeRate(Integer id) {

		try {
			return userService.deleteShopRate(id, "agree");
		} catch (Exception e) {
			log.info(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}

	/*
	 * 修改客户密码
	 */

	@ApiOperation(value = "修改客户密码", notes = "修改客户密码")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "userCode", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "password", value = "password", required = true, paramType = "query", dataType = "String"), })
	@PostMapping(value = "/updatePwdByUserCode")
	public ServiceResult updatePwdByUserCode(String userCode, String password) {
		ServiceResult sr = new ServiceResult();
		try {
			return userService.updatePwdByUserCode(sr, userCode, password);
		} catch (Exception e) {
			log.info("修改客户密码异常：{}", e.toString());
			e.printStackTrace();
		}
		return sr;
	}

	@ApiOperation(value = "查询某用户所有有效银行卡", notes = "测试")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String") })
	@RequestMapping(value = "/findBankListAll")
	public ServiceResult findBankListAll(String userCode) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_EXCEPTION);
		try {
			return userService.findBankListAll(sr, userCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sr;
	}
}
