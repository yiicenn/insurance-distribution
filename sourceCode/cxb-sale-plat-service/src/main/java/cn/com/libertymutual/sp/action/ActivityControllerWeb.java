package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpActivity;
import cn.com.libertymutual.sp.req.DrawReq;
import cn.com.libertymutual.sp.service.api.ActivityService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/active")
public class ActivityControllerWeb {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private ActivityService activityService;

	/*
	 * 活动发放查询
	 */

	@ApiOperation(value = "活动发放查询", notes = "活动发放查询")
	@PostMapping(value = "/activityList")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "activityName", value = "活动名称", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "planStartTime", value = "计划开始时间", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "planEndTime", value = "计划结束时间", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "activityCode", value = "活动编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "status", value = "状态", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult activityList(String activityName, String planStartTime, String planEndTime,String activityCode, String status, int pageNumber, int pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = activityService.activityList(activityName, planStartTime, planEndTime,activityCode, status, pageNumber, pageSize);
		return sr;

	}

	/*
	 * 新增或者修改活动发放规则
	 * 
	 */

	@ApiOperation(value = "新增或者修改活动发放规则", notes = "新增或者修改活动发放规则")
	@PostMapping(value = "/issueActivity")
	public ServiceResult issueActivity(@RequestBody @ApiParam(name = "activity", value = "activity", required = true) TbSpActivity activity) {
		ServiceResult sr = new ServiceResult();

		sr = activityService.issueActivity(activity);
		return sr;

	}

//	/*
//	 * 追加预算
//	 */
//
//	@ApiOperation(value = "追加预算", notes = "追加预算")
//	@PostMapping(value = "/addBudget")
//	@ApiImplicitParams(value = { @ApiImplicitParam(name = "id", value = "活动id", required = true, paramType = "query", dataType = "Long"),
//			@ApiImplicitParam(name = "budget", value = "预算额", required = true, paramType = "query", dataType = "Long"), })
//	public ServiceResult addBudget(Integer id, Double budget) {
//		ServiceResult sr = new ServiceResult();
//
//		sr = activityService.addBudget(id, budget);
//		return sr;
//
//	}
	
	/*
	 * 追加预算
	 */

	@ApiOperation(value = "追加预算", notes = "追加预算")
	@PostMapping(value = "/addBudget")
	public ServiceResult addBudget(@RequestBody @ApiParam(name = "drawReq", value = "drawReq", required = true) DrawReq drawReq) {
		ServiceResult sr = new ServiceResult();

		sr = activityService.addBudget(drawReq);
		return sr;

	}

	/*
	 * 停止活动
	 */

	@ApiOperation(value = "停止活动", notes = "停止活动")
	@PostMapping(value = "/stopActivity")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "id", value = "活动id", required = true, paramType = "query", dataType = "Long"),
			// @ApiImplicitParam(name = "status", value = "预算额", required = true, paramType
			// = "query", dataType = "Long"),
	})
	public ServiceResult stopActivity(Integer id) {
		ServiceResult sr = new ServiceResult();

		sr = activityService.stopActivity(id);
		return sr;

	}

	/*
	 * 预算添加记录
	 */

	@ApiOperation(value = "预算添加记录", notes = "预算添加记录")
	@PostMapping(value = "/budgetHis")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "type", value = "type", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult budgetHis(Integer id, String type, Integer pageNumber, Integer pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = activityService.approveHistory(type, id, pageNumber, pageSize);
		return sr;

	}

}
