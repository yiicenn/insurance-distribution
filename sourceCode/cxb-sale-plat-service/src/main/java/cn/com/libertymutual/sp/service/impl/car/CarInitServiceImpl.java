package cn.com.libertymutual.sp.service.impl.car;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dao.car.PrpKindDao;
import cn.com.libertymutual.sp.dao.car.PrpKindRelationDao;
import cn.com.libertymutual.sp.dao.car.PrpRateDao;
import cn.com.libertymutual.sp.dto.SelectDto;
import cn.com.libertymutual.sp.service.api.CodeRelateService;
import cn.com.libertymutual.sp.service.api.car.CarInitService;
import cn.com.libertymutual.sys.bean.SysCodeRelate;
import cn.com.libertymutual.sys.dao.ISysCodeRelateDao;

@Service("CarInitService")
public class CarInitServiceImpl implements CarInitService {
	@Autowired
	private PrpKindDao prpKindDao;
	@Autowired
	private PrpKindRelationDao prpKindRelationDao;
	@Autowired
	private PrpRateDao prpRateDao;
	@Autowired
	private ISysCodeRelateDao iSysCodeRelateDao;
	@Resource
	private RedisUtils redisUtils;
	@Resource
	private CodeRelateService codeRelateService;
	@Resource
	private RestTemplate restTemplate;

	@Autowired
	private JdbcTemplate readJdbcTemplate;

	public ServiceResult getInitOffer(String riskCode) {
		ServiceResult sr = new ServiceResult();
		Map<String, Object> carInit = (Map<String, Object>) redisUtils.get(Constants.CAR_OFFER_INIT);
		if (carInit == null) {
			carInit = new HashMap<String, Object>();
			carInit.put("kind", prpKindDao.findAllKind(riskCode));
			carInit.put("seleRate", prpRateDao.findRate(riskCode));
			carInit.put("relation", prpKindRelationDao.findRelation(riskCode));
			redisUtils.set(Constants.CAR_OFFER_INIT, carInit);
		}
		sr.setResult(carInit);
		sr.setSuccess();
		return sr;
	}

	@Override
	public ServiceResult querySeleInit(String riskCode, String branchCode) {
		ServiceResult sr = new ServiceResult();
		String carKindSt = Constants.CAR_KIND + riskCode;
		String fuelType = Constants.FUEL_TYPE + branchCode;
		String vehicleCatSt = Constants.VEHICLE_CATEGORY + branchCode;
		String licenseSt = Constants.LICENSE_KIND_CODE + branchCode;
		String packAge = Constants.CAR_PACKAGE + riskCode + branchCode;

		Map<String, Object> initSeleMap = new HashMap<String, Object>();
		// 获得车辆种类
		List<SelectDto> carKindList = (List<SelectDto>) redisUtils.get(carKindSt);
		// if (null == carKindList) {
		carKindList = codeRelateService.getCodeRelateRisk(Constants.CAR_KIND, riskCode);
		redisUtils.set(carKindSt, carKindList);
		// }
		initSeleMap.put(Constants.CAR_KIND, carKindList);

		// 获得能源种类
		List<SelectDto> fuelTypeList = (List<SelectDto>) redisUtils.get(fuelType);
		// if (null == fuelTypeList) {
		// 判断能源种类地区
		if ("11000000".equals(branchCode)) {
			fuelTypeList = codeRelateService.getCodeRelateBranch(Constants.FUEL_TYPE, branchCode);
		} else {
			fuelTypeList = codeRelateService.getCodeRelateBranch(Constants.FUEL_TYPE, "00000000");
		}
		redisUtils.set(fuelType, fuelTypeList);
		// }
		initSeleMap.put(Constants.FUEL_TYPE, fuelTypeList);

		// 交管车种类
		List<SelectDto> vehicleCategoryList = (List<SelectDto>) redisUtils.get(vehicleCatSt);
		// if (null == vehicleCategoryList) {
		// 判断能源种类地区
		vehicleCategoryList = codeRelateService.getCodeRelateBranch(Constants.VEHICLE_CATEGORY, branchCode);
		redisUtils.set(vehicleCatSt, vehicleCategoryList);
		// }
		initSeleMap.put(Constants.VEHICLE_CATEGORY, vehicleCategoryList);

		// 号牌种类
		List<SelectDto> licenseKindCodeList = (List<SelectDto>) redisUtils.get(licenseSt);
		// if (null == licenseKindCodeList) {
		// 判断能源种类地区
		if ("11000000".equals(branchCode)) {
			licenseKindCodeList = codeRelateService.getCodeRelateBranch(Constants.LICENSE_KIND_CODE, branchCode);
		} else {
			licenseKindCodeList = codeRelateService.getCodeRelateBranch(Constants.LICENSE_KIND_CODE, "00000000");
		}
		redisUtils.set(licenseSt, licenseKindCodeList);
		// }
		initSeleMap.put(Constants.LICENSE_KIND_CODE, licenseKindCodeList);

		List<Map<String, Object>> packageList = (List<Map<String, Object>>) redisUtils.get(packAge);
		// if (null == packageList) {
		String sql = "SELECT m.PK_PACKAGE_NAME as packName, k.PACK_KIND as packKind ,k.PACK_FOREHEAD as forehead, k.DEDUCTIBLE_VALUE as deductible FROM tb_mng_package_main AS m, tb_mng_package_kind AS k WHERE m.PK_BRANCH_NO = ? AND m.PACK_RISK = ?  and k.PK_ID=m.PK_ID;";
		packageList = readJdbcTemplate.queryForList(sql, new Object[] { branchCode, riskCode });
		redisUtils.set(packAge, packageList);
		// }
		initSeleMap.put(Constants.CAR_PACKAGE, packageList);
		sr.setSuccess();
		sr.setResult(initSeleMap);
		return sr;
	}

	@Override
	public ServiceResult queryUseNature(String insuredNature, String carKind, String riskCode) {
		ServiceResult sr = new ServiceResult();
		String queryStr = Constants.USE_NATURE + insuredNature + carKind + riskCode;

		List<SelectDto> userNatureSele = (List<SelectDto>) redisUtils.get(queryStr);
		// if (null == userNatureSele) {
		userNatureSele = new ArrayList<SelectDto>();
		List<SysCodeRelate> codeRelaList = iSysCodeRelateDao.findUseNatureCode(insuredNature, carKind, riskCode);
		SysCodeRelate sysCode = new SysCodeRelate();
		for (int i = 0; i < codeRelaList.size(); i++) {
			sysCode = codeRelaList.get(i);
			SelectDto sele = new SelectDto();
			sysCode = codeRelaList.get(i);
			sele.setValue(sysCode.getCodeCname2());
			sele.setKey(sysCode.getCode2());
			userNatureSele.add(sele);
		}
		redisUtils.set(queryStr, userNatureSele);
		// }
		sr.setSuccess();
		sr.setResult(userNatureSele);

		return sr;
	}

}
