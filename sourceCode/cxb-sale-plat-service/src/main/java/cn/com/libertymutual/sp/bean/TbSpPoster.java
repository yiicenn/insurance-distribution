package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "t_sp_poster")
public class TbSpPoster implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5116410968767081923L;
	private Integer id;
	private String name;
	private String type;
	private String riskCode;
	private Integer productId;
	private String productName;
	private String status;
	private String bigImgUrl;
	private String summaryInfo;
	private String imgUrl;
	private Integer views;
	private String userId;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createDate;
	private String modifyUser;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date modifyTime;
	private String approveStatus;
	// 二维码位置和宽高
	private Integer coordinateXQRCode;
	private Integer coordinateYQRCode;
	private Integer widthQRCode;
	private Integer heightQRCode;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "NAME", length = 100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "type", length = 10)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "RISKCODE", length = 10)
	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	@Column(name = "PRODUCT_ID", length = 11)
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	@Column(name = "PRODUCT_NAME", length = 100)
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "STATUS", length = 1)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "BIGIMG_URL", length = 100)
	public String getBigImgUrl() {
		return bigImgUrl;
	}

	public void setBigImgUrl(String bigImgUrl) {
		this.bigImgUrl = bigImgUrl;
	}

	@Column(name = "SUMMARY_INFO")
	public String getSummaryInfo() {
		return summaryInfo;
	}

	public void setSummaryInfo(String summaryInfo) {
		this.summaryInfo = summaryInfo;
	}

	@Column(name = "img_url", length = 100)
	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	@Column(name = "views", length = 10)
	public Integer getViews() {
		return views;
	}

	public void setViews(Integer views) {
		this.views = views;
	}

	@Column(name = "USER_ID", length = 10)
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Column(name = "Create_date")
	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Column(name = "MODIFY_USER", length = 10)
	public String getModifyUser() {
		return modifyUser;
	}

	public void setModifyUser(String modifyUser) {
		this.modifyUser = modifyUser;
	}

	@Column(name = "MODIFY_TIME")
	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}

	@Column(name = "Approve_Status", length = 1)
	public String getApproveStatus() {
		return approveStatus;
	}

	public void setApproveStatus(String approveStatus) {
		this.approveStatus = approveStatus;
	}

	@Column(name = "COORDINATE_X_Q_R_CODE")
	public Integer getCoordinateXQRCode() {
		return coordinateXQRCode;
	}

	public void setCoordinateXQRCode(Integer coordinateXQRCode) {
		this.coordinateXQRCode = coordinateXQRCode;
	}

	@Column(name = "COORDINATE_Y_Q_R_CODE")
	public Integer getCoordinateYQRCode() {
		return coordinateYQRCode;
	}

	public void setCoordinateYQRCode(Integer coordinateYQRCode) {
		this.coordinateYQRCode = coordinateYQRCode;
	}

	@Column(name = "WIDTH_Q_R_CODE")
	public Integer getWidthQRCode() {
		return widthQRCode;
	}

	public void setWidthQRCode(Integer widthQRCode) {
		this.widthQRCode = widthQRCode;
	}

	@Column(name = "HEIGHT_Q_R_CODE")
	public Integer getHeightQRCode() {
		return heightQRCode;
	}

	public void setHeightQRCode(Integer heightQRCode) {
		this.heightQRCode = heightQRCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((approveStatus == null) ? 0 : approveStatus.hashCode());
		result = prime * result + ((bigImgUrl == null) ? 0 : bigImgUrl.hashCode());
		result = prime * result + ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((imgUrl == null) ? 0 : imgUrl.hashCode());
		result = prime * result + ((modifyTime == null) ? 0 : modifyTime.hashCode());
		result = prime * result + ((modifyUser == null) ? 0 : modifyUser.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((productId == null) ? 0 : productId.hashCode());
		result = prime * result + ((productName == null) ? 0 : productName.hashCode());
		result = prime * result + ((riskCode == null) ? 0 : riskCode.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		result = prime * result + ((views == null) ? 0 : views.hashCode());
		return result;
	}

	public boolean differ(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TbSpPoster other = (TbSpPoster) obj;
		// if (approveStatus == null) {
		// if (other.approveStatus != null)
		// return false;
		// } else if (!approveStatus.equals(other.approveStatus))
		// return false;
		if (bigImgUrl == null) {
			if (other.bigImgUrl != null)
				return false;
		} else if (!bigImgUrl.equals(other.bigImgUrl))
			return false;
		// if (createDate == null) {
		// if (other.createDate != null)
		// return false;
		// } else if (!createDate.equals(other.createDate))
		// return false;
		// if (id == null) {
		// if (other.id != null)
		// return false;
		// } else if (!id.equals(other.id))
		// return false;
		if (imgUrl == null) {
			if (other.imgUrl != null)
				return false;
		} else if (!imgUrl.equals(other.imgUrl))
			return false;
		// if (modifyTime == null) {
		// if (other.modifyTime != null)
		// return false;
		// } else if (!modifyTime.equals(other.modifyTime))
		// return false;
		// if (modifyUser == null) {
		// if (other.modifyUser != null)
		// return false;
		// } else if (!modifyUser.equals(other.modifyUser))
		// return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (productId == null) {
			if (other.productId != null)
				return false;
		} else if (!productId.equals(other.productId))
			return false;
		if (productName == null) {
			if (other.productName != null)
				return false;
		} else if (!productName.equals(other.productName))
			return false;
		if (riskCode == null) {
			if (other.riskCode != null)
				return false;
		} else if (!riskCode.equals(other.riskCode))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		// if (userId == null) {
		// if (other.userId != null)
		// return false;
		// } else if (!userId.equals(other.userId))
		// return false;
		// if (views == null) {
		// if (other.views != null)
		// return false;
		// } else if (!views.equals(other.views))
		// return false;
		return true;
	}

	public TbSpPoster propertyDiffer(Object obj) {
		TbSpPoster other = (TbSpPoster) obj;
		if (!bigImgUrl.equals(other.bigImgUrl)) {
			this.setBigImgUrl(other.bigImgUrl);
		}

		if (!imgUrl.equals(other.imgUrl)) {
			this.setImgUrl(other.imgUrl);
		}

		if (!name.equals(other.name)) {
			this.setName(other.name);
		}

		if (!productId.equals(other.productId)) {
			this.setProductId(other.productId);
		}
		if (!productName.equals(other.productName)) {
			this.setProductName(other.productName);
		}
		if (!riskCode.equals(other.riskCode)) {
			this.setRiskCode(other.riskCode);
		}
		if (!type.equals(other.type)) {
			this.setType(other.type);
		}
		return this;
	}

	@Override
	public String toString() {
		return "{id:" + id + ", name:" + name + ", type:" + type + ", riskCode:" + riskCode + ", productId:" + productId + ", productName:"
				+ productName + ", status:" + status + ", bigImgUrl:" + bigImgUrl + ", imgUrl:" + imgUrl + ", views:" + views + ", userId:" + userId
				+ ", createDate:" + createDate + ", modifyUser:" + modifyUser + ", modifyTime:" + modifyTime + ", approveStatus:" + approveStatus
				+ "}";
	}

	private TbSpApprove tbSpApprove;

	@Transient
	public TbSpApprove getTbSpApprove() {
		return tbSpApprove;
	}

	public void setTbSpApprove(TbSpApprove tbSpApprove) {
		this.tbSpApprove = tbSpApprove;
	}

	private List<PosterAnimatedText> animatedTexts;

	@Transient
	public List<PosterAnimatedText> getAnimatedTexts() {
		return animatedTexts;
	}

	public void setAnimatedTexts(List<PosterAnimatedText> animatedTexts) {
		this.animatedTexts = animatedTexts;
	}
	private String approveAuth;
	@Transient
	public String getApproveAuth() {
		return approveAuth;
	}

	public void setApproveAuth(String approveAuth) {
		this.approveAuth = approveAuth;
	}
	private Double rate;

	@Transient
	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

}
