package cn.com.libertymutual.production.pojo.request;

import java.util.List;

/**
 * 险种关联条款请求对象
 * @author GuoYue
 *
 */
public class LinkKindRequest extends Request {

	private PrpdRiskRequest risk;
	private List<PrpdKindLibraryRequest> selectedRows;
	private String shortratetype;
	public PrpdRiskRequest getRisk() {
		return risk;
	}
	public void setRisk(PrpdRiskRequest risk) {
		this.risk = risk;
	}
	public List<PrpdKindLibraryRequest> getSelectedRows() {
		return selectedRows;
	}
	public void setSelectedRows(List<PrpdKindLibraryRequest> selectedRows) {
		this.selectedRows = selectedRows;
	}
	public String getShortratetype() {
		return shortratetype;
	}
	public void setShortratetype(String shortratetype) {
		this.shortratetype = shortratetype;
	}
	
}
