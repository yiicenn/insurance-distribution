package cn.com.libertymutual.saleplat.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;



@Configuration
@EnableSwagger2
public class Swagger2Config {

	@Bean
    public Docket createRestApi() {
		
		Contact contact = new Contact("bob.kuang", "", "bob.kuang@libertymutual.com.cn");
		
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("自营平台-销售工具 API")
                .description("由于移动互联网对传统业务的冲击，现公司决定开发自己的移动销售工具。")
                .termsOfServiceUrl("https://m.libao.cn")
                .contact(contact)
                .version("1.0.0")
                .build();

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.basePackage("cn.com.libertymutual.saleplat.controller"))
                .paths(PathSelectors.any())
                .build();
    }
}
