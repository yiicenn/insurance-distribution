package cn.com.libertymutual.sp.dto.car.dto;

import java.io.Serializable;

import cn.com.libertymutual.core.base.dto.RequestBaseDto;

public class TVerifyRequstDTO extends RequestBaseDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String renewalFlag; // 转保类型
	private String querySequenceNo; // 转保问题序列号
	private String answer;// 转保问题回答

	public String getRenewalFlag() {
		return renewalFlag;
	}

	public void setRenewalFlag(String renewalFlag) {
		this.renewalFlag = renewalFlag;
	}

	public String getQuerySequenceNo() {
		return querySequenceNo;
	}

	public void setQuerySequenceNo(String querySequenceNo) {
		this.querySequenceNo = querySequenceNo;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

}
