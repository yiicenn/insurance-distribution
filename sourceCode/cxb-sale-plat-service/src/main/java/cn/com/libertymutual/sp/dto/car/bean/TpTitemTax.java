package cn.com.libertymutual.sp.dto.car.bean;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/**
 * Created by Ryan on 2016-09-05.
 */
@Entity
@javax.persistence.Table(name = "tb_tp_titem_tax",  catalog = "")
public class TpTitemTax  implements Serializable{
	 /*protected String annualTaxAmount;
	    protected String annualTaxDue;
	    protected String calculateMode;
	    protected String declareDate;
	    protected String declareStatusIA;
	    protected String deduction;
	    protected String deductionDocumentNumber;
	    protected String deductionDueCode;
	    protected String deductionType;
	    protected String exceedDate;
	    protected String exceedDaysCount;
	    protected String overDue;
	    protected String policyNo;
	    protected String proposalNo;
	    protected String sumOverdue;
	    protected String sumTaxDefault;
	    protected String tax1Due;
	    protected String tax1EndDate;
	    protected String tax1StartDate;
	    protected String taxDepartment;
	    protected String taxDepartmentCode;
	    protected String taxDocumentNumber;
	    protected String taxDue;
	    protected String taxLocationCode;
	    protected String taxRegistryNumber;
	    protected String taxStartDate;
	    protected String taxTermTypeCode;
	    protected String taxUnitTypeCode;
	    protected String taxpayerName;
	    protected String total1Amount;
	    protected String totalAmount;
	    protected String unitRate;*/
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8931608463001431667L;
	private int id;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    private String flowId;
    public String getFlowId() {
        return flowId;
    }
    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }
    private String taxConditionCode;
    public String getTaxConditionCode() {
		return taxConditionCode;
	}
	public void setTaxConditionCode(String taxConditionCode) {
		this.taxConditionCode = taxConditionCode;
	}
	
	private String taxpayerName;
	public String getTaxpayerName() {
		return taxpayerName;
	}
	
	public void setTaxpayerName(String taxpayerName) {
		this.taxpayerName = taxpayerName;
	}
	 private String taxPayerIdentificationCode;
    public String getTaxPayerIdentificationCode() {
        return taxPayerIdentificationCode;
    }
	public void setTaxPayerIdentificationCode(String taxPayerIdentificationCode) {
        this.taxPayerIdentificationCode = taxPayerIdentificationCode;
    }
    private String vehicleStatus;
    public String getVehicleStatus() {
        return vehicleStatus;
    }
    public void setVehicleStatus(String vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }
    private Date cutoffDate;
    public Date getCutoffDate() {
        return cutoffDate;
    }
    public void setCutoffDate(Date cutoffDate) {
        this.cutoffDate = cutoffDate;
    }
    private Date taxEndDate;
    public Date getTaxEndDate() {
        return taxEndDate;
    }
    public void setTaxEndDate(Date taxEndDate) {
        this.taxEndDate = taxEndDate;
    }
    private Double outstandingTax;
    public Double getOutstandingTax() {
        return outstandingTax;
    }
    public void setOutstandingTax(Double outstandingTax) {
        this.outstandingTax = outstandingTax;
    }
    private Double dealyPayPenylty;
    public Double getDealyPayPenylty() {
        return dealyPayPenylty;
    }
    public void setDealyPayPenylty(Double dealyPayPenylty) {
        this.dealyPayPenylty = dealyPayPenylty;
    }
    private Double vehicleTax;
    public Double getVehicleTax() {
        return vehicleTax;
    }
    public void setVehicleTax(Double vehicleTax) {
        this.vehicleTax = vehicleTax;
    }
    private Double sumTax;
    public Double getSumTax() {
 		return sumTax;
 	}
 	public void setSumTax(Double sumTax) {
 		this.sumTax = sumTax;
 	}
    private String taxCutsPlan;
 
    public String getTaxCutsPlan() {
        return taxCutsPlan;
    }
    public void setTaxCutsPlan(String taxCutsPlan) {
        this.taxCutsPlan = taxCutsPlan;
    }
    private Double taxCutsAmount;
    public Double getTaxCutsAmount() {
        return taxCutsAmount;
    }
    public void setTaxCutsAmount(Double taxCutsAmount) {
        this.taxCutsAmount = taxCutsAmount;
    }
    private String taxAuthorityName;
    public String getTaxAuthorityName() {
        return taxAuthorityName;
    }
    public void setTaxAuthorityName(String taxAuthorityName) {
        this.taxAuthorityName = taxAuthorityName;
    }
    private String taxCutsReason;
    public String getTaxCutsReason() {
        return taxCutsReason;
    }
    public void setTaxCutsReason(String taxCutsReason) {
        this.taxCutsReason = taxCutsReason;
    }
    private String taxCutsCertificateNumber;
    public String getTaxCutsCertificateNumber() {
        return taxCutsCertificateNumber;
    }
    public void setTaxCutsCertificateNumber(String taxCutsCertificateNumber) {
        this.taxCutsCertificateNumber = taxCutsCertificateNumber;
    }
    private Double deductionRate;
    public Double getDeductionRate() {
        return deductionRate;
    }
    public void setDeductionRate(Double deductionRate) {
        this.deductionRate = deductionRate;
    }
    private String calcTaxFlag;
    public String getCalcTaxFlag() {
        return calcTaxFlag;
    }
    public void setCalcTaxFlag(String calcTaxFlag) {
        this.calcTaxFlag = calcTaxFlag;
    }
    private String declareStatusIa;
    public String getDeclareStatusIa() {
        return declareStatusIa;
    }
    public void setDeclareStatusIa(String declareStatusIa) {
        this.declareStatusIa = declareStatusIa;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TpTitemTax that = (TpTitemTax) o;
        if (id != that.id) return false;
        if (flowId != null ? !flowId.equals(that.flowId) : that.flowId != null) return false;
        if (taxConditionCode != null ? !taxConditionCode.equals(that.taxConditionCode) : that.taxConditionCode != null)
            return false;
        if (taxPayerIdentificationCode != null ? !taxPayerIdentificationCode.equals(that.taxPayerIdentificationCode) : that.taxPayerIdentificationCode != null)
            return false;
        if (vehicleStatus != null ? !vehicleStatus.equals(that.vehicleStatus) : that.vehicleStatus != null)
            return false;
        if (cutoffDate != null ? !cutoffDate.equals(that.cutoffDate) : that.cutoffDate != null) return false;
        if (taxEndDate != null ? !taxEndDate.equals(that.taxEndDate) : that.taxEndDate != null) return false;
        if (outstandingTax != null ? !outstandingTax.equals(that.outstandingTax) : that.outstandingTax != null)
            return false;
        if (dealyPayPenylty != null ? !dealyPayPenylty.equals(that.dealyPayPenylty) : that.dealyPayPenylty != null)
            return false;
        if (vehicleTax != null ? !vehicleTax.equals(that.vehicleTax) : that.vehicleTax != null) return false;
        if (sumTax != null ? !sumTax.equals(that.sumTax) : that.sumTax != null) return false;
        if (taxCutsPlan != null ? !taxCutsPlan.equals(that.taxCutsPlan) : that.taxCutsPlan != null) return false;
        if (taxCutsAmount != null ? !taxCutsAmount.equals(that.taxCutsAmount) : that.taxCutsAmount != null)
            return false;
        if (taxAuthorityName != null ? !taxAuthorityName.equals(that.taxAuthorityName) : that.taxAuthorityName != null)
            return false;
        if (taxCutsReason != null ? !taxCutsReason.equals(that.taxCutsReason) : that.taxCutsReason != null)
            return false;
        if (taxCutsCertificateNumber != null ? !taxCutsCertificateNumber.equals(that.taxCutsCertificateNumber) : that.taxCutsCertificateNumber != null)
            return false;
        if (deductionRate != null ? !deductionRate.equals(that.deductionRate) : that.deductionRate != null)
            return false;
        if (calcTaxFlag != null ? !calcTaxFlag.equals(that.calcTaxFlag) : that.calcTaxFlag != null) return false;
        if (declareStatusIa != null ? !declareStatusIa.equals(that.declareStatusIa) : that.declareStatusIa != null)
            return false;
        return true;
    }
    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (flowId != null ? flowId.hashCode() : 0);
        result = 31 * result + (taxConditionCode != null ? taxConditionCode.hashCode() : 0);
        result = 31 * result + (taxPayerIdentificationCode != null ? taxPayerIdentificationCode.hashCode() : 0);
        result = 31 * result + (vehicleStatus != null ? vehicleStatus.hashCode() : 0);
        result = 31 * result + (cutoffDate != null ? cutoffDate.hashCode() : 0);
        result = 31 * result + (taxEndDate != null ? taxEndDate.hashCode() : 0);
        result = 31 * result + (outstandingTax != null ? outstandingTax.hashCode() : 0);
        result = 31 * result + (dealyPayPenylty != null ? dealyPayPenylty.hashCode() : 0);
        result = 31 * result + (vehicleTax != null ? vehicleTax.hashCode() : 0);
        result = 31 * result + (sumTax != null ? sumTax.hashCode() : 0);
        result = 31 * result + (taxCutsPlan != null ? taxCutsPlan.hashCode() : 0);
        result = 31 * result + (taxCutsAmount != null ? taxCutsAmount.hashCode() : 0);
        result = 31 * result + (taxAuthorityName != null ? taxAuthorityName.hashCode() : 0);
        result = 31 * result + (taxCutsReason != null ? taxCutsReason.hashCode() : 0);
        result = 31 * result + (taxCutsCertificateNumber != null ? taxCutsCertificateNumber.hashCode() : 0);
        result = 31 * result + (deductionRate != null ? deductionRate.hashCode() : 0);
        result = 31 * result + (calcTaxFlag != null ? calcTaxFlag.hashCode() : 0);
        result = 31 * result + (declareStatusIa != null ? declareStatusIa.hashCode() : 0);
        return result;
    }
	@Override
	public String toString() {
		return "TpTitemTax [id=" + id + ", flowId=" + flowId
				+ ", taxConditionCode=" + taxConditionCode
				+ ", taxPayerIdentificationCode=" + taxPayerIdentificationCode
				+ ", vehicleStatus=" + vehicleStatus + ", cutoffDate="
				+ cutoffDate + ", taxEndDate=" + taxEndDate
				+ ", outstandingTax=" + outstandingTax + ", dealyPayPenylty="
				+ dealyPayPenylty + ", vehicleTax=" + vehicleTax
				+ ", sumTax=" + sumTax + ", taxCutsPlan=" + taxCutsPlan
				+ ", taxCutsAmount=" + taxCutsAmount + ", taxAuthorityName="
				+ taxAuthorityName + ", taxCutsReason=" + taxCutsReason
				+ ", taxCutsCertificateNumber=" + taxCutsCertificateNumber
				+ ", deductionRate=" + deductionRate + ", calcTaxFlag="
				+ calcTaxFlag + ", declareStatusIa=" + declareStatusIa + "]";
	}
}