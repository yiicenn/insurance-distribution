package cn.com.libertymutual.sp.dto.car.dto;

public class TProposalSaveResponseDTO extends RequestBaseDto {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String proposalFlowId;
	private String proposalMTPLNo;
	private String proposalNo;
	private String proposalStatus;
	private String resultCode;
	private String resultMessage;
	private Boolean status;
	private String uuId;

	public String getProposalFlowId() {
		return proposalFlowId;
	}

	public void setProposalFlowId(String proposalFlowId) {
		this.proposalFlowId = proposalFlowId;
	}

	public String getProposalMTPLNo() {
		return proposalMTPLNo;
	}

	public void setProposalMTPLNo(String proposalMTPLNo) {
		this.proposalMTPLNo = proposalMTPLNo;
	}

	public String getProposalNo() {
		return proposalNo;
	}

	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}

	public String getProposalStatus() {
		return proposalStatus;
	}

	public void setProposalStatus(String proposalStatus) {
		this.proposalStatus = proposalStatus;
	}

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMessage() {
		return resultMessage;
	}

	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public String getUuId() {
		return uuId;
	}

	public void setUuId(String uuId) {
		this.uuId = uuId;
	}

}
