package cn.com.libertymutual.sp.action.production;

import cn.com.libertymutual.production.pojo.request.PlanAddRequest2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.production.pojo.request.PlanAddRequest;
import cn.com.libertymutual.production.pojo.request.PrpdRiskPlanRequest;
import cn.com.libertymutual.production.pojo.request.RiskKindRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.PlanBusinessService;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@RestController
@RequestMapping("plan")
public class PlanController {
	
	@Autowired
	protected PlanBusinessService planBusinessService;
	
	/**
	 * 方案管理-获取当前险种下所有条款
	 * @return
	 */
	@RequestMapping("queryRiskKind")
	public Response queryRiskKind(String riskCode, String riskVersion) {
		Response result = planBusinessService.findKindsByRisk(riskCode, riskVersion);
		return result;
	}
	
	/**
	 * 获取方案
	 * @param PrpdRiskPlanRequest
	 * @return
	 */
	@RequestMapping("queryPrpdRiskPlan")
	public Response queryPrpdRiskPlan(PrpdRiskPlanRequest prpdRiskPlanRequest) {
		Response result = planBusinessService.findPrpdRiskPlan(prpdRiskPlanRequest);
		return result;
	}
	
	/**
	 * 生成方案代码
	 * @param riskcode
	 * @return
	 */
	@RequestMapping("generatePlanCode")
	public Response generatePlanCode(String riskcode) {
		Response result = new Response();
		try {
			result = planBusinessService.getNewPlanSerialno(riskcode);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	/**
	 * 方案管理-获取条款关联的标的/责任
	 * @param request
	 * @return
	 */
	@RequestMapping("getItemLinked")
	public Response getItemLinked(@RequestBody RiskKindRequest request) {
		return planBusinessService.findPrpdItemLibrary(request);
	}
	
	/**
	 * 方案管理-新增方案
	 * @param request
	 * @return
	 */
	@RequestMapping("addPlan")
	public Response addPlan(@RequestBody PlanAddRequest request) {
		Response result = new Response();
		try {
			String plancode = planBusinessService.getNewPlanSerialno(request.getRisk().getRiskcode()).getResult().toString();
			request.setPlancode(plancode);
			result = planBusinessService.insertPlanInfo(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	/**
	 * 方案管理-新增方案（费率因子）
	 * @param request
	 * @return
	 */
	@RequestMapping("addPlan2")
	public Response addPlan2(@RequestBody PlanAddRequest2 request) {
		Response result = new Response();
		try {
			String plancode = planBusinessService.getNewPlanSerialno(request.getRisk().getRiskcode()).getResult().toString();
			request.setPlancode(plancode);
			result = planBusinessService.insertPlanInfo2(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	/**
	 * 方案管理-修改方案
	 * @param request
	 * @return
	 */
	@RequestMapping("editPlan")
	public Response updatePlan(@RequestBody PlanAddRequest request) {
		Response result = new Response();
		try {
			result = planBusinessService.updatePlanInfo(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	/**
	 * 方案管理-修改方案(费率因子)
	 * @param request
	 * @return
	 */
	@RequestMapping("editPlan2")
	public Response updatePlan2(@RequestBody PlanAddRequest2 request) {
		Response result = new Response();
		try {
			result = planBusinessService.updatePlanInfo2(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	/**
	 * 方案管理-根据方案代码获取方案的条款及标的责任
	 * @param plancode
	 * @return
	 */
	@RequestMapping("queryKindItems")
	public Response queryKindItems(String plancode) {
		return planBusinessService.findKindItems(plancode);
	}

	/**
	 * 方案管理-根据方案代码获取方案的条款及标的责任（费率因子）
	 * @param plancode
	 * @return
	 */
	@RequestMapping("queryKindItems2")
	public Response queryKindItems2(String plancode) {
		return planBusinessService.findKindItems2(plancode);
	}

	/**
	 * 方案管理-校验方案是否配置费率因子
	 * @param plancode
	 * @return
	 */
	@RequestMapping("queryPlansub")
	public Response queryPlansub(String plancode) {
		return planBusinessService.queryPlansub(plancode);

	}

}
