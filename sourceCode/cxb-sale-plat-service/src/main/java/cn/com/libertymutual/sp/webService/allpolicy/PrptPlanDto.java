
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for prptPlanDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptPlanDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="delinquentFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="endorseNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MTPLDelinquentFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MTPLFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="payRefFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="planFee" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planFeePercent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="planStartDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proposalNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prpPlanCurrency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serialNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptPlanDto", propOrder = {
    "currency",
    "delinquentFee",
    "endorseNo",
    "flag",
    "mtplDelinquentFee",
    "mtplFee",
    "payNo",
    "payReason",
    "payRefFee",
    "planDate",
    "planFee",
    "planFeePercent",
    "planStartDate",
    "proposalNo",
    "prpPlanCurrency",
    "serialNo"
})
public class PrptPlanDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String currency;
    protected String delinquentFee;
    protected String endorseNo;
    protected String flag;
    @XmlElement(name = "MTPLDelinquentFee")
    protected String mtplDelinquentFee;
    @XmlElement(name = "MTPLFee")
    protected String mtplFee;
    protected String payNo;
    protected String payReason;
    protected String payRefFee;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar planDate;
    protected String planFee;
    protected String planFeePercent;
    protected String planStartDate;
    protected String proposalNo;
    protected String prpPlanCurrency;
    protected String serialNo;

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the delinquentFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDelinquentFee() {
        return delinquentFee;
    }

    /**
     * Sets the value of the delinquentFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDelinquentFee(String value) {
        this.delinquentFee = value;
    }

    /**
     * Gets the value of the endorseNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEndorseNo() {
        return endorseNo;
    }

    /**
     * Sets the value of the endorseNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEndorseNo(String value) {
        this.endorseNo = value;
    }

    /**
     * Gets the value of the flag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlag() {
        return flag;
    }

    /**
     * Sets the value of the flag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlag(String value) {
        this.flag = value;
    }

    /**
     * Gets the value of the mtplDelinquentFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMTPLDelinquentFee() {
        return mtplDelinquentFee;
    }

    /**
     * Sets the value of the mtplDelinquentFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMTPLDelinquentFee(String value) {
        this.mtplDelinquentFee = value;
    }

    /**
     * Gets the value of the mtplFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMTPLFee() {
        return mtplFee;
    }

    /**
     * Sets the value of the mtplFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMTPLFee(String value) {
        this.mtplFee = value;
    }

    /**
     * Gets the value of the payNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayNo() {
        return payNo;
    }

    /**
     * Sets the value of the payNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayNo(String value) {
        this.payNo = value;
    }

    /**
     * Gets the value of the payReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayReason() {
        return payReason;
    }

    /**
     * Sets the value of the payReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayReason(String value) {
        this.payReason = value;
    }

    /**
     * Gets the value of the payRefFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayRefFee() {
        return payRefFee;
    }

    /**
     * Sets the value of the payRefFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayRefFee(String value) {
        this.payRefFee = value;
    }

    /**
     * Gets the value of the planDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPlanDate() {
        return planDate;
    }

    /**
     * Sets the value of the planDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPlanDate(XMLGregorianCalendar value) {
        this.planDate = value;
    }

    /**
     * Gets the value of the planFee property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanFee() {
        return planFee;
    }

    /**
     * Sets the value of the planFee property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanFee(String value) {
        this.planFee = value;
    }

    /**
     * Gets the value of the planFeePercent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanFeePercent() {
        return planFeePercent;
    }

    /**
     * Sets the value of the planFeePercent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanFeePercent(String value) {
        this.planFeePercent = value;
    }

    /**
     * Gets the value of the planStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlanStartDate() {
        return planStartDate;
    }

    /**
     * Sets the value of the planStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanStartDate(String value) {
        this.planStartDate = value;
    }

    /**
     * Gets the value of the proposalNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposalNo() {
        return proposalNo;
    }

    /**
     * Sets the value of the proposalNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposalNo(String value) {
        this.proposalNo = value;
    }

    /**
     * Gets the value of the prpPlanCurrency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrpPlanCurrency() {
        return prpPlanCurrency;
    }

    /**
     * Sets the value of the prpPlanCurrency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrpPlanCurrency(String value) {
        this.prpPlanCurrency = value;
    }

    /**
     * Gets the value of the serialNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerialNo() {
        return serialNo;
    }

    /**
     * Sets the value of the serialNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerialNo(String value) {
        this.serialNo = value;
    }

}
