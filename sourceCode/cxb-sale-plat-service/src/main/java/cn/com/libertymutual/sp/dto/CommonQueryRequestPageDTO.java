package cn.com.libertymutual.sp.dto;

import java.io.Serializable;

import cn.com.libertymutual.core.base.dto.RequestBasePageDto;


public class CommonQueryRequestPageDTO  extends RequestBasePageDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4327671355137839832L;

	private String documentNo;//业务号：投保单号、批单号、保单号等
	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}
   
	
	

	
}
