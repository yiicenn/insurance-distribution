package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhexitemkind;

/**
 * Created by steven.li on 2017/12/18.
 */
public interface FhexitemkindService {

    /**
     * 新增Fhexitemkind
     * @param record
     */
    void insert(Fhexitemkind record);

    List<Fhexitemkind> findAll(String treatyno, String sectionno);

    List<Fhexitemkind> findByRisk(String riskcode);
}
