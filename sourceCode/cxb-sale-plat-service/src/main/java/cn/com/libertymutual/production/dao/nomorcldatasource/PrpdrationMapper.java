package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkindlibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdration;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdrationExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdrationKey;
@Mapper
public interface PrpdrationMapper {
    int countByExample(PrpdrationExample example);

    int deleteByExample(PrpdrationExample example);

    int deleteByPrimaryKey(PrpdrationKey key);

    int insert(Prpdration record);

    int insertSelective(Prpdration record);

    List<Prpdration> selectByExample(PrpdrationExample example);

    Prpdration selectByPrimaryKey(PrpdrationKey key);

    int updateByExampleSelective(@Param("record") Prpdration record, @Param("example") PrpdrationExample example);

    int updateByExample(@Param("record") Prpdration record, @Param("example") PrpdrationExample example);

    int updateByPrimaryKeySelective(Prpdration record);

    int updateByPrimaryKey(Prpdration record);
    
    @Select(
    		"SELECT * FROM PRPDKINDLIBRARY A "
    				 + "WHERE  A.KINDCODE = #{kindCode} "
    				 + "AND A.KINDVERSION = #{kindVersion} "
    				 + "AND EXISTS ("
    				 + "    SELECT 1 FROM PRPDKIND B"
    				 + "    WHERE B.KINDCODE = A.KINDCODE"
    				 + "    AND   B.KINDVERSION = B.KINDVERSION"
    				 + "    AND EXISTS ("
    				 + "        SELECT 1 FROM PRPDRATION C"
    				 + "        WHERE  C.KINDCODE = B.KINDCODE"
    				 + "        AND C.KINDVERSION = B.KINDVERSION"
    				 + "        AND C.RISKCODE = B.RISKCODE"
    				 + "        AND C.RISKVERSION = B.RISKVERSION"
    				 + "        AND C.VALIDSTATUS = '1' "
    				 + "        AND EXISTS (SELECT 1 "
                     + "               FROM PRPDRISKPLAN D "
                     + "              WHERE C.PLANCODE = D.PLANCODE "
                     + "                AND C.RISKCODE = D.RISKCODE "
                     + "                AND C.RISKVERSION = D.RISKVERSION "
                     + "                AND D.VALIDSTATUS = '1') "
    				 + "    )"
    				 + ")"
    		)
    List<Prpdkindlibrary> checkKindDeletable(@Param("kindCode") String kindCode, @Param("kindVersion") String kindVersion);
    
    @Select(
    		"SELECT MAX(SERIALNO) FROM PRPDRATION "
    		+ "WHERE PLANCODE = #{planCode} "
    		+ " AND RISKCODE = #{riskCode} "
    		+ " AND RISKVERSION = #{riskVersion} "
    		)
	List<Integer> getMaxSerialNum(@Param("planCode") String planCode,
								  @Param("riskCode") String riskCode,
								  @Param("riskVersion") String riskVersion);

    @Update("UPDATE PRPDRATION T SET T.VALIDSTATUS = '0' WHERE T.KINDCODE = #{kindCode} AND T.KINDVERSION = #{kindVersion}")
	void inActiveRation(@Param("kindCode") String kindCode, @Param("kindVersion") String kindVersion);
   
    @Select("select * from Prpdration where IFNULL(freeitem4,plancode) = #{planSubCode}  and validstatus ='1' and  kindcode <>'27S0054'  and SYSDATE() >= startdate and SYSDATE()<= enddate order by plancode asc , kindcode asc, serialno asc")
	List<Prpdration> findByPlanSubcode(@Param("planSubCode") String planSubCode);
}