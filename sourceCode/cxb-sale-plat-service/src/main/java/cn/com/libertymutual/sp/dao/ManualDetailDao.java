package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sp.bean.TbSpManualDetail;

@Repository
public interface ManualDetailDao extends PagingAndSortingRepository<TbSpManualDetail, Integer>, JpaSpecificationExecutor<TbSpManualDetail> {

	List<TbSpManualDetail> findByTaskId(Integer id);

	// 修改用户编码
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update TbSpManualDetail set userCode = :userCode, phoneNo = :phoneNo where userCode = :userCode1 and phoneNo = :phoneNo1")
	public int updateUserCodeByUserCode(@Param("userCode") String userCode, @Param("phoneNo") String phoneNo, @Param("userCode1") String userCode1,
			@Param("phoneNo1") String phoneNo1);

}
