package cn.com.libertymutual.sp.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.MenuService;
import cn.com.libertymutual.sys.bean.SysMenu;
import cn.com.libertymutual.sys.bean.SysMenuRes;
import cn.com.libertymutual.sys.dao.ISysMenuDao;
import cn.com.libertymutual.sys.dao.ISysMenuResDao;
import cn.com.libertymutual.sys.dao.ISysRoleMenuResDao;

@Service("menuService")
public class MenuServiceImpl implements MenuService {

	private Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	ISysRoleMenuResDao iSysRoleMenuResDao;
	@Autowired
	private ISysMenuResDao menuResDao;

	@Autowired
	private ISysMenuDao iSysMenuDao;

	@Transactional(propagation = Propagation.REQUIRED) // 事务
	@Override
	public ServiceResult addMenu(Integer fmenuId, String type, String menuname,String resName, String menunameen, String menuurl, String icon) {
		ServiceResult sr = new ServiceResult();
		log.info("参数fmenuId===》：" + String.valueOf(fmenuId));
		log.info("参数type===》：" + type);
		try {
			if ("menu".equals(type)) {// 添加菜单
				int menuid = 0;
				SysMenu maxmenuidSM = iSysMenuDao.findMaxMenuid(fmenuId);
				if (null != maxmenuidSM) {
					SysMenu dbmenu = iSysMenuDao.findByMenuId(maxmenuidSM.getMenuid() + 1);
					if (null!=dbmenu) {
						menuid = Integer.parseInt(String.valueOf(maxmenuidSM.getMenuid()) + "0");
					}else {
					    menuid = maxmenuidSM.getMenuid() + 1;
					}
				} else {
					menuid = Integer.parseInt((String.valueOf(fmenuId) + "0"));
				}
				log.info("menuid--->:" + String.valueOf(menuid));
				SysMenu sm = new SysMenu();
				sm.setFmenuid(fmenuId);
				sm.setMenuid(menuid);
				sm.setMenuname(menuname);
				sm.setMenunameen(menunameen);
				sm.setMenuurl(menuurl);
				sm.setIcon(icon);
				sm.setIsleaf("0");
				sm.setLoadflag("1");
				sm.setOrderid(0);
				sm.setStatus("1");
				sr.setResult(iSysMenuDao.save(sm));
			} else {// 添加按钮
				SysMenuRes dbbutton = menuResDao.findByResid(resName);
				if (null!=dbbutton) {
					sr.setFail();
					sr.setResult("按钮唯一标识已存在！");
					return sr;
				}
//				List<SysMenuRes> smrList = menuResDao.findByMenuid(fmenuId);
//				int maxResid = 0;
//				if (null != smrList && smrList.size() != 0) {
//					log.info("smrList--->:" + smrList.toString());
//					HashSet<Integer> set = new HashSet<Integer>();
//					for (SysMenuRes sm : smrList) {
//						set.add(Integer.parseInt(sm.getResid()));
//					}
//					log.info("add menu set--->:" + set.toString());
//					maxResid = Collections.max(set) + 1;
//				} else {
//					maxResid = Integer.parseInt((String.valueOf(fmenuId) + "01"));
//				}
				SysMenuRes smr = new SysMenuRes();
				smr.setMenuid(fmenuId);
				log.info("smr.getMenuid()===>:" + smr.getMenuid());
//				smr.setResid(String.valueOf(maxResid));
				smr.setResid(resName);
				smr.setResname(menuname);
				sr.setResult(menuResDao.save(smr));
			}
		} catch (Exception e) {
			log.info(e.getMessage());
			sr.setResult("数据异常！");
		}
		log.info("sr--->:" + sr.toString());
		return sr;
	}

	@Transactional(propagation = Propagation.REQUIRED) // 事务
	@Override
	public ServiceResult removeMenu(Integer menuId, String type) {
		ServiceResult sr = new ServiceResult();

		if ("menu".equals(type)) {

			iSysMenuDao.updateStatus("0", menuId);
			menuResDao.deleteByMenuId(menuId);
		} else {
			menuResDao.deleteByResId(String.valueOf(menuId));
			// iSysRoleMenuResDao.deleteByResId(String.valueOf(menuId));
		}
		return sr;
	}

	@Transactional(propagation = Propagation.REQUIRED) // 事务
	@Override
	public ServiceResult updateMenuName(Integer menuId, String menuname, String menunameen, String menuurl, String icon) {
		ServiceResult sr = new ServiceResult();
		log.info(menuId + "--------" + menuname);
		SysMenuRes dbSysMenuRes = menuResDao.findByResid(String.valueOf(menuId));

		if (null != dbSysMenuRes) {
			dbSysMenuRes.setResname(menuname);
			menuResDao.save(dbSysMenuRes);
		} else {
			SysMenu dbSysMenu = iSysMenuDao.findByMenuId(menuId);
			if (null != dbSysMenu) {
				dbSysMenu.setMenuname(menuname);
				dbSysMenu.setMenunameen(menunameen);
				dbSysMenu.setIcon(icon);
				dbSysMenu.setMenuurl(menuurl);
				iSysMenuDao.save(dbSysMenu);
			}
		}
		sr.setSuccess();
		return sr;
	}
}
