package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdcompanyWithBLOBs extends Prpdcompany {
    private String addresscname;

    private String printcomname;

    private String printaddress;

    private String pringpostcode;

    public String getAddresscname() {
        return addresscname;
    }

    public void setAddresscname(String addresscname) {
        this.addresscname = addresscname == null ? null : addresscname.trim();
    }

    public String getPrintcomname() {
        return printcomname;
    }

    public void setPrintcomname(String printcomname) {
        this.printcomname = printcomname == null ? null : printcomname.trim();
    }

    public String getPrintaddress() {
        return printaddress;
    }

    public void setPrintaddress(String printaddress) {
        this.printaddress = printaddress == null ? null : printaddress.trim();
    }

    public String getPringpostcode() {
        return pringpostcode;
    }

    public void setPringpostcode(String pringpostcode) {
        this.pringpostcode = pringpostcode == null ? null : pringpostcode.trim();
    }
}