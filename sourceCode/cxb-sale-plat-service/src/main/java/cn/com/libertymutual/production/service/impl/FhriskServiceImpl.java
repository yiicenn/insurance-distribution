package cn.com.libertymutual.production.service.impl;

import cn.com.libertymutual.production.dao.nomorcldatasource.FhriskMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Fhrisk;
import cn.com.libertymutual.production.model.nomorcldatasource.FhriskExample;
import cn.com.libertymutual.production.service.api.FhriskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author steven.li
 */
@Service
public class FhriskServiceImpl implements FhriskService {

    @Autowired
    private FhriskMapper fhriskMapper;

    @Override
    public void insert(Fhrisk record) {
        fhriskMapper.insertSelective(record);
    }

    @Override
    public List<Fhrisk> findAll() {
        FhriskExample criteria = new FhriskExample();
        return fhriskMapper.selectByExample(criteria);
    }

    @Override
    public List<Fhrisk> findByRisk(String riskcode) {
        FhriskExample criteria = new FhriskExample();
        criteria.createCriteria().andRiskcodeEqualTo(riskcode);
        return fhriskMapper.selectByExample(criteria);
    }
}
