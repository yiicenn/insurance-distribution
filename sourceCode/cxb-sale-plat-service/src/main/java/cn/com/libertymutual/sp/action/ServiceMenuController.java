package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.ServiceMenuService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/serviceMenu")
public class ServiceMenuController {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private ServiceMenuService serviceMenuService;
	
	
	@ApiOperation(value = "前端服务菜单列表", notes = "前端服务菜单列表")
	@PostMapping(value = "/menuList")
	@ApiImplicitParams(value = {
            @ApiImplicitParam(name = "isFirst", value = "是否首页", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "sortfield", value = "排序字段", required = true, paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "sorttype", value = "排序方式", required = true, paramType = "query", dataType = "String"),
    })
	public ServiceResult menuList(String isFirst,String sortfield,String sorttype){
		ServiceResult sr = new ServiceResult();
		
		sr = serviceMenuService.menuList(sorttype);
		return sr;
		
	}
	
}
