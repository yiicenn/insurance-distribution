package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.HomePageService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/admin/home")
public class HomePageController {

	private Logger log = LoggerFactory.getLogger(getClass());
	
	
	@Autowired
	private HomePageService homePageService;
	/*
	 * 产品销量统计
	 */
	@ApiOperation(value = "产品销量统计", notes = "产品销量统计")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "startDate", value = "开始时间", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endDate", value = "结束时间", required = false, paramType = "query", dataType = "String"),
//			@ApiImplicitParam(name = "pageNumber", value = "分页", required = true, paramType = "query", dataType = "Long"),
//			@ApiImplicitParam(name = "pageSize", value = "分页", required = true, paramType = "query", dataType = "Long"),
	})
	@PostMapping(value = "/salesCount")
	public ServiceResult salesCount(String startDate,String endDate) {
		
		return homePageService.salesCount(startDate,endDate);
	}
	
	
	@ApiOperation(value = "访问量统计", notes = "访问量统计")
	@ApiImplicitParams(value = {
			@ApiImplicitParam(name = "type", value = "开始时间", required = true, paramType = "query", dataType = "String"),
	})
	@PostMapping(value = "/visitsCount")
	public ServiceResult visitsCount(String type) {
		return homePageService.visitsCount(type);
	}
	
	
	
	
	@ApiOperation(value = "访问量统计", notes = "访问量统计")
	@PostMapping(value = "/task")
	public ServiceResult task() {
		ServiceResult sr = new ServiceResult();
		homePageService.task();
		return sr;
	}
}
