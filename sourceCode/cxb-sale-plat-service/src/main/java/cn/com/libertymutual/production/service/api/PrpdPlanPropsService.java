package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdplanprops;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskplan;

public interface PrpdPlanPropsService {
	
	public void insert(Prpdplanprops record);

	public List<Prpdplanprops> select(Prpdriskplan plan);

	public void deleteByPlan(Prpdriskplan plan);
	
}
