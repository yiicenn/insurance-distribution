package cn.com.libertymutual.sp.bean.car;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "tb_prp_kind", catalog = "")
@IdClass(PrpKindPK.class)
public class PrpKind implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4586494442200127581L;
	private String kindCode;
	private String riskCode;
	private String kindCName;
	private String kindEName;
	private java.util.Date startDate;
	private java.util.Date endDate;
	private String validStatus;
	private String remark;

	@Id
	@Column(name = "KIND_CODE", nullable = false, length = 10)
	public String getKindCode() {
		return kindCode;
	}

	public void setKindCode(String kindCode) {
		this.kindCode = kindCode;
	}

	@Id
	@Column(name = "RISK_CODE", nullable = false, length = 4)
	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	@Basic
	@Column(name = "KIND_CNAME", nullable = false, length = 255)
	public String getkindCName() {
		return kindCName;
	}

	public void setkindCName(String kindCName) {
		this.kindCName = kindCName;
	}

	@Basic
	@Column(name = "KIND_ENAME", nullable = false, length = 255)
	public String getkindEName() {
		return kindEName;
	}

	public void setkindEName(String kindEName) {
		this.kindEName = kindEName;
	}

	@Basic
	@Column(name = "START_DATE", nullable = false)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Basic
	@Column(name = "END_DATE", nullable = false)
	public java.util.Date getEndDate() {
		return endDate;
	}

	public void setEndDate(java.util.Date endDate) {
		this.endDate = endDate;
	}

	@Basic
	@Column(name = "VALID_STATUS", nullable = false, length = 1)
	public String getValidStatus() {
		return validStatus;
	}

	public void setValidStatus(String validStatus) {
		this.validStatus = validStatus;
	}

	@Basic
	@Column(name = "REMARK", nullable = true, length = 300)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		PrpKind prpKind = (PrpKind) o;
		if (kindCode != null ? !kindCode.equals(prpKind.kindCode) : prpKind.kindCode != null)
			return false;
		if (riskCode != null ? !riskCode.equals(prpKind.riskCode) : prpKind.riskCode != null)
			return false;
		if (kindCName != null ? !kindCName.equals(prpKind.kindCName) : prpKind.kindCName != null)
			return false;
		if (kindEName != null ? !kindEName.equals(prpKind.kindEName) : prpKind.kindEName != null)
			return false;
		if (startDate != null ? !startDate.equals(prpKind.startDate) : prpKind.startDate != null)
			return false;
		if (endDate != null ? !endDate.equals(prpKind.endDate) : prpKind.endDate != null)
			return false;
		if (validStatus != null ? !validStatus.equals(prpKind.validStatus) : prpKind.validStatus != null)
			return false;
		if (remark != null ? !remark.equals(prpKind.remark) : prpKind.remark != null)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int result = kindCode != null ? kindCode.hashCode() : 0;
		result = 31 * result + (riskCode != null ? riskCode.hashCode() : 0);
		result = 31 * result + (kindCName != null ? kindCName.hashCode() : 0);
		result = 31 * result + (kindEName != null ? kindEName.hashCode() : 0);
		result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
		result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
		result = 31 * result + (validStatus != null ? validStatus.hashCode() : 0);
		result = 31 * result + (remark != null ? remark.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "PrpKind [kindCode=" + kindCode + ", riskCode=" + riskCode + ", kindCName=" + kindCName + ", kindEName=" + kindEName + ", startDate="
				+ startDate + ", endDate=" + endDate + ", validStatus=" + validStatus + ", remark=" + remark + "]";
	}
}