package cn.com.libertymutual.production.service.impl;

import cn.com.libertymutual.production.dao.nomorcldatasource.FhtreatyMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Fhtreaty;
import cn.com.libertymutual.production.model.nomorcldatasource.FhtreatyExample;
import cn.com.libertymutual.production.service.api.FhtreatyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author steven.li
 * @create 2017/12/20
 */
@Service
public class FhtreatyServiceImpl implements FhtreatyService {

    @Autowired
    private FhtreatyMapper fhtreatyMapper;

    @Override
    public void insert(Fhtreaty record) {
        fhtreatyMapper.insertSelective(record);
    }

    @Override
    public List<Fhtreaty> findAll(Fhtreaty where) {
        return fhtreatyMapper.findAllBySectionExisted(where.getUwyear());
    }
}
