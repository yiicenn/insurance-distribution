package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdplanpropsMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdplanprops;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdplanpropsExample;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskplan;
import cn.com.libertymutual.production.service.api.PrpdPlanPropsService;

@Service
public class PrpdPlanPropsServiceImpl implements PrpdPlanPropsService{

	@Autowired
	private PrpdplanpropsMapper prpdplanpropsMapper;
	
	@Override
	public void insert(Prpdplanprops record) {
		prpdplanpropsMapper.insertSelective(record);
	}

	@Override
	public List<Prpdplanprops> select(Prpdriskplan plan) {
		
		PrpdplanpropsExample example = new PrpdplanpropsExample();
		example.createCriteria().andPlancodeEqualTo(plan.getPlancode())
								.andRiskcodeEqualTo(plan.getRiskcode())
								.andRiskversionEqualTo(plan.getRiskversion())
								.andValidstatusEqualTo("1");
		return prpdplanpropsMapper.selectByExample(example );
	}

	@Override
	public void deleteByPlan(Prpdriskplan plan) {
		PrpdplanpropsExample example = new PrpdplanpropsExample();
		example.createCriteria().andPlancodeEqualTo(plan.getPlancode())
								.andRiskcodeEqualTo(plan.getRiskcode())
								.andRiskversionEqualTo(plan.getRiskversion());
		prpdplanpropsMapper.deleteByExample(example );
	}
	
}
