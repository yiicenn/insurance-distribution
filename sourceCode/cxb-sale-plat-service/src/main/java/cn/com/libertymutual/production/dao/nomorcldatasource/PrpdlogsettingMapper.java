package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdlogsetting;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdlogsettingExample;
@Mapper
public interface PrpdlogsettingMapper {
    int countByExample(PrpdlogsettingExample example);

    int deleteByExample(PrpdlogsettingExample example);

    int deleteByPrimaryKey(BigDecimal rowidObject);

    int insert(Prpdlogsetting record);

    int insertSelective(Prpdlogsetting record);

    List<Prpdlogsetting> selectByExample(PrpdlogsettingExample example);

    Prpdlogsetting selectByPrimaryKey(BigDecimal rowidObject);

    int updateByExampleSelective(@Param("record") Prpdlogsetting record, @Param("example") PrpdlogsettingExample example);

    int updateByExample(@Param("record") Prpdlogsetting record, @Param("example") PrpdlogsettingExample example);

    int updateByPrimaryKeySelective(Prpdlogsetting record);

    int updateByPrimaryKey(Prpdlogsetting record);
    
    @Select("SELECT a.rowid_object as rowidobject, a.businessname, a.createdby, a.createdate, a.tablename FROM prpdlogsetting a WHERE EXISTS ("
    		+ "SELECT 1 FROM prpdlogoperation b "
    		+ "WHERE  a.rowid_object = b.logid AND b.businesskeyvalue=#{keyValue}) ORDER BY  CREATEDATE DESC")
List<Prpdlogsetting> selectByBusinessKey(String keyValue);
}