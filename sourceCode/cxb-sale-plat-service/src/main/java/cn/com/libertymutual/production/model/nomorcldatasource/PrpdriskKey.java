package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdriskKey {
    private String riskcode;

    private String riskversion;

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode == null ? null : riskcode.trim();
    }

    public String getRiskversion() {
        return riskversion;
    }

    public void setRiskversion(String riskversion) {
        this.riskversion = riskversion == null ? null : riskversion.trim();
    }
}