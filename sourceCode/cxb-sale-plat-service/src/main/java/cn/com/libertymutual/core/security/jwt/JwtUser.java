package cn.com.libertymutual.core.security.jwt;

import java.util.Collection;
import java.util.Date;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by stephan on 20.03.16.
 */
public class JwtUser implements UserDetails {

    private final Long id;
    private final String username;	//对应的user_code
    private final String userCodeBs;	//对应的user_code_bs
    private final String wxOpenId;
    private final String name;	//对应数据库中的user_name
    private final String nickName;
    private final String password;
    private final String email;
    private final String userType;
    private final Collection<? extends GrantedAuthority> authorities;
    private final boolean enabled;
    private final Date lastPasswordResetDate;

    public JwtUser(
          Long id,
          String userCode,
          String userCodeBs,
          String wxOpenId,
          String userName,
          String nickName,
          String password,
          String email,
          String userType, Collection<? extends GrantedAuthority> authorities,
          boolean enabled,
          Date lastPasswordResetDate
    ) {
        this.id = id;
        this.username = userCode;
        this.userCodeBs = userCodeBs;
        this.wxOpenId = wxOpenId;
        this.name = userName;
        this.nickName = nickName;
        this.email = email;
        this.password = password;
        this.userType = userType;
        this.authorities = authorities;
        this.enabled = enabled;
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    public String getWxOpenId() {
        return wxOpenId;
    }

    public String getName() {
        return name;
    }
    
    public String getNickName() {
    	return nickName;
    }

    public String getEmail() {
        return email;
    }
    public String getUserType() {
        return userType;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @JsonIgnore
    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }
}
