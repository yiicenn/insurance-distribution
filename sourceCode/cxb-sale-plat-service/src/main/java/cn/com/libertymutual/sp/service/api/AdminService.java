package cn.com.libertymutual.sp.service.api;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sys.bean.SysUser;

public interface AdminService {


	ServiceResult adminList(String userid,String username,Integer roleid, int pageNumber, int pageSize);

	ServiceResult addAdmin(SysUser sysUser);

	ServiceResult getUserInfoFromLdap(String userId);

	ServiceResult setRoleToUser(String userId, Integer roleId);

	ServiceResult getRoleOfUser(String userId);

	ServiceResult updateAdminStauts(String userid,String type);

}
