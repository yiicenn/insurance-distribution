package cn.com.libertymutual.sp.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.common.collect.Lists;

public class TPolicyDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String policyno;// 保单号
	private String proposalNo;// 投保单号
	private String applicode;// 投保人编码
	private String appliIdentifynumber;// 投保人证件号
	private String appliname;// 投保人姓名

	private String insuredcode;
	private String insuredIdentifynumber;
	private String insuredname;

	private String licenseno;// 车牌号
	private String brandname;// 车款
	private String seatcount;// 座位数

	private String sumpremium;// 保费
	private String carTax;// 车船税
	private String riskcode;// 险种
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private String startdate;// 起保时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private String enddate;// 终保时间

	private String status;// 有效状态

	private String riskcname;
	private String polisource;// 数据来源
	private String underwriteflag;// 投保单状态：8等待支付，9等待核保 ，1完成，2核保未通过 ，0投保中

	private List<TItemkindDto> itemKinds;// 险别列表

	public List<TItemkindDto> getItemKinds() {
		if (null == itemKinds) {
			itemKinds = Lists.newArrayList();
		}
		return itemKinds;
	}

	public void setItemKinds(List<TItemkindDto> itemKinds) {
		this.itemKinds = itemKinds;
	}

	public String getPolicyno() {
		return policyno;
	}

	public void setPolicyno(String policyno) {
		this.policyno = policyno;
	}

	public String getApplicode() {
		return applicode;
	}

	public void setApplicode(String applicode) {
		this.applicode = applicode;
	}

	public String getAppliIdentifynumber() {
		return appliIdentifynumber;
	}

	public void setAppliIdentifynumber(String appliIdentifynumber) {
		this.appliIdentifynumber = appliIdentifynumber;
	}

	public String getAppliname() {
		return appliname;
	}

	public void setAppliname(String appliname) {
		this.appliname = appliname;
	}

	public String getInsuredcode() {
		return insuredcode;
	}

	public void setInsuredcode(String insuredcode) {
		this.insuredcode = insuredcode;
	}

	public String getInsuredIdentifynumber() {
		return insuredIdentifynumber;
	}

	public void setInsuredIdentifynumber(String insuredIdentifynumber) {
		this.insuredIdentifynumber = insuredIdentifynumber;
	}

	public String getInsuredname() {
		return insuredname;
	}

	public void setInsuredname(String insuredname) {
		this.insuredname = insuredname;
	}

	public String getLicenseno() {
		return licenseno;
	}

	public void setLicenseno(String licenseno) {
		this.licenseno = licenseno;
	}

	public String getBrandname() {
		return brandname;
	}

	public void setBrandname(String brandname) {
		this.brandname = brandname;
	}

	public String getSumpremium() {
		return sumpremium;
	}

	public void setSumpremium(String sumpremium) {
		this.sumpremium = sumpremium;
	}

	public String getRiskcode() {
		return riskcode;
	}

	public void setRiskcode(String riskcode) {
		this.riskcode = riskcode;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String endDate) {
		this.enddate = endDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRiskcname() {
		return riskcname;
	}

	public void setRiskcname(String riskcname) {
		this.riskcname = riskcname;
	}

	public String getProposalNo() {
		return proposalNo;
	}

	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}

	public String getPolisource() {
		return polisource;
	}

	public void setPolisource(String polisource) {
		this.polisource = polisource;
	}

	public String getUnderwriteflag() {
		return underwriteflag;
	}

	public void setUnderwriteflag(String underwriteflag) {
		this.underwriteflag = underwriteflag;
	}

	public String getSeatcount() {
		return seatcount;
	}

	public void setSeatcount(String seatcount) {
		this.seatcount = seatcount;
	}

	public String getCarTax() {
		return carTax;
	}

	public void setCarTax(String carTax) {
		this.carTax = carTax;
	}

}
