
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptCommissionDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptCommissionDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agentCodeName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="agreeMentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="agreementCodeNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="commissionRate" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="commissionValue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proposalNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptCommissionDto", propOrder = {
    "agentCodeName",
    "agreeMentType",
    "agreementCodeNo",
    "commissionRate",
    "commissionValue",
    "proposalNo"
})
public class PrptCommissionDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String agentCodeName;
    protected String agreeMentType;
    protected String agreementCodeNo;
    protected double commissionRate;
    protected String commissionValue;
    protected String proposalNo;

    /**
     * Gets the value of the agentCodeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgentCodeName() {
        return agentCodeName;
    }

    /**
     * Sets the value of the agentCodeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgentCodeName(String value) {
        this.agentCodeName = value;
    }

    /**
     * Gets the value of the agreeMentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgreeMentType() {
        return agreeMentType;
    }

    /**
     * Sets the value of the agreeMentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgreeMentType(String value) {
        this.agreeMentType = value;
    }

    /**
     * Gets the value of the agreementCodeNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgreementCodeNo() {
        return agreementCodeNo;
    }

    /**
     * Sets the value of the agreementCodeNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgreementCodeNo(String value) {
        this.agreementCodeNo = value;
    }

    /**
     * Gets the value of the commissionRate property.
     * 
     */
    public double getCommissionRate() {
        return commissionRate;
    }

    /**
     * Sets the value of the commissionRate property.
     * 
     */
    public void setCommissionRate(double value) {
        this.commissionRate = value;
    }

    /**
     * Gets the value of the commissionValue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommissionValue() {
        return commissionValue;
    }

    /**
     * Sets the value of the commissionValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommissionValue(String value) {
        this.commissionValue = value;
    }

    /**
     * Gets the value of the proposalNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProposalNo() {
        return proposalNo;
    }

    /**
     * Sets the value of the proposalNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProposalNo(String value) {
        this.proposalNo = value;
    }

}
