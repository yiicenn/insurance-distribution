package cn.com.libertymutual.sp.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpSaleLog;

@Repository
public interface SpSaleLogDao extends PagingAndSortingRepository<TbSpSaleLog, Integer>, JpaSpecificationExecutor<TbSpSaleLog>
{

}
