package cn.com.libertymutual.production.service.api;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpmaxno;


public interface PrpMaxNoService {
	
	/**
	 * 获取序列号
	 * @return
	 */
	public String findMaxNo(String tableName);
	
	/**
	 * 更新序列号
	 */
	public void updateMaxNo(Prpmaxno record, String tableName);
}
