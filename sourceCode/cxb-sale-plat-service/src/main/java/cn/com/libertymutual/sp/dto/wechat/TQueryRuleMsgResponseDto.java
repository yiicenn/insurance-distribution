package cn.com.libertymutual.sp.dto.wechat;

import java.io.Serializable;
import java.util.List;

import cn.com.libertymutual.core.base.dto.ResponseBaseDto;

import com.google.common.collect.Lists;


public class TQueryRuleMsgResponseDto extends ResponseBaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7368108134350867792L;

	private List<RuleReplyMsg> ruleReplyMsgs;
	
	
	public List<RuleReplyMsg> getRuleReplyMsgs() {
		if(null==ruleReplyMsgs){
			ruleReplyMsgs=Lists.newArrayList();
		}
		return ruleReplyMsgs;
	}

	public void setRuleReplyMsgs(List<RuleReplyMsg> ruleReplyMsgs) {
		this.ruleReplyMsgs = ruleReplyMsgs;
	}
	
}
