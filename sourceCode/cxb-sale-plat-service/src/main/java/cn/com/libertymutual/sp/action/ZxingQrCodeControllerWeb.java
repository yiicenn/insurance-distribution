package cn.com.libertymutual.sp.action;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.ZxingQrCodeService;

@RestController
@RequestMapping(value = "/admin/batch_process_q_rcode_test")
public class ZxingQrCodeControllerWeb {
	// 注入原子性业务逻辑
	@Resource
	private ZxingQrCodeService zxingQrCodeService;

	@RequestMapping(value = "/findUsers")
	public ServiceResult findUsers(HttpServletRequest request, HttpServletResponse response, String code, Integer s_userid, Integer e_userid) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_APP_EXCEPTION);
		try {
			sr = zxingQrCodeService.findUsers(request, response, code, s_userid, e_userid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sr;
	}

	@RequestMapping(value = "/createUsers", method = RequestMethod.POST)
	public ServiceResult createUsers(HttpServletRequest request, HttpServletResponse response, String code, String productids) {
		ServiceResult sr = new ServiceResult(Constants.SYS_ERROR_MSG, ServiceResult.STATE_APP_EXCEPTION);
		try {
			sr = zxingQrCodeService.createUsers(request, response, code, productids);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sr;
	}
}
