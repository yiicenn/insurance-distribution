package cn.com.libertymutual.sp.dto;

import java.util.ArrayList;
import java.util.List;

import cn.com.libertymutual.core.base.dto.ResponseBaseDto;

public class PolicysResponseDto extends ResponseBaseDto{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6208918887971748792L;

	private List<PrpCmain> prpCmainList;

	public List<PrpCmain> getPrpCmainList() {
		if(null==prpCmainList){
			prpCmainList=new ArrayList();
		}
		return prpCmainList;
	}

	public void setPrpCmainList(List<PrpCmain> prpCmainList) {
		this.prpCmainList = prpCmainList;
	}
	
}
