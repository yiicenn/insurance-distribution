package cn.com.libertymutual.production.controller;

import cn.com.libertymutual.production.pojo.request.LogRequest;
import cn.com.libertymutual.production.pojo.request.PrpdRiskRequest;
import cn.com.libertymutual.production.pojo.request.Request;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.CommonBusinessService;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@RestController
@RequestMapping("common")
public class CommonController {
	
	@Autowired
	protected CommonBusinessService commonBusinessService;
	
	/**
	 * 获取所有险别
	 * @return
	 */
	@RequestMapping("queryAllPrpdClass")
	public Response queryAllPrpdClass() {
		Response result = commonBusinessService.findAllPrpdClass();
		return result;
	}
	
	/**
	 * 获取不是组合险的险别
	 * @return
	 */
	@RequestMapping("queryClassNotComposite")
	public Response queryClassNotComposite() {
		Response result = commonBusinessService.findClassNotComposite();
		return result;
	}
	
	/**
	 * 获取所有险种
	 * @return
	 */
	@RequestMapping("queryAllPrpdRisk")
	public Response queryAllPrpdRisk() {
		Response result = commonBusinessService.findAllPrpdRisk();
		return result;
	}
	
	/**
	 * 获取所有险种(分页)
	 * @return
	 */
	@RequestMapping("queryRisks")
	public Response queryRisks(@RequestBody PrpdRiskRequest request) {
		Response result = commonBusinessService.findRisksByPage(request);
		return result;
	}
	
	/**
	 * 获取所属险类及组合险的所有险种(分页)
	 * @param request
	 * @return
	 */
	@RequestMapping("queryByClassAndComposite")
	public Response queryByClassAndComposite(@RequestBody PrpdRiskRequest request) {
		Response result = commonBusinessService.findByClassAndComposite(request);
		return result;
	}
	
	/**
	 * 查询机构信息
	 * @return
	 */
	@RequestMapping("queryPrpdCompany")
	public Response queryPrpdCompany(Request request) {
//		Response result = commonBusinessService.findPrpdCompany(request);
		Response result = commonBusinessService.findBranch(request);
		return result;
	}
	
	/**
	 * 获取操作日志
	 * @param logRequest
	 * @return
	 */
	@RequestMapping("findLogs")
	public Response findLogs(LogRequest logRequest) {
		Response result = new Response();
		result = commonBusinessService.findLogs(logRequest);
		return result;
	}
	
	/**
	 * 获取日志详情
	 * @param logRequest
	 * @return
	 */
	@RequestMapping("findLogDetail")
	public Response findLogDetail(LogRequest logRequest) {
		Response result = new Response();
		result = commonBusinessService.findLogDetail(logRequest);
		return result;
	}
	
	/**
	 * 根据机构代码查询渠道信息
	 * @return
	 */
	@RequestMapping("queryAgentByComCode")
	public Response queryAgentByComCode(String comCodes, Request request) {
		Response result = new Response();
//		result = commonBusinessService.findPrpsAgentByComCode(comCodes, request);
		result = commonBusinessService.findUsersAgentByComCode(comCodes, request);
		return result;
	}
	
	/**
	 * 查询险种关联的方案
	 * @param riskCodes
	 * @return
	 */
	@RequestMapping("queryPlansByRisks")
	public Response queryPlansByRisks(String riskCodes) {
		Response result = new Response();
		result = commonBusinessService.findPlansByRisks(riskCodes);
		return result;
	}
	
	/**
	 * 查询分支机构
	 * @return
	 */
	@RequestMapping("queryCompanys")
	public Response queryCompanys() {
		Response result = new Response();
//		result = commonBusinessService.findCompanys();
		result = commonBusinessService.findNextBranchs();
		return result;
	}

	/**
	 * 查询分支机构2
	 * @return
	 */
	@RequestMapping("queryCompanysBycodes")
	public Response queryCompanysBycodes(@RequestParam(value = "comcodes[]") List<String> comcodes) {
		Response result = commonBusinessService.queryBranchsBycodes(comcodes);
//		Response result = commonBusinessService.queryCompanysBycodes(comcodes);
		return result;
	}

	@Bean
    public HttpMessageConverters fastjsonHttpMessageConverter(){
        //定义一个转换消息的对象
        FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();

        //在转换器中添加配置信息
        //添加fastjson的配置信息 比如 ：是否要格式化返回的json数据
        fastConverter.setFeatures(SerializerFeature.PrettyFormat);
        fastConverter.setFeatures(SerializerFeature.WriteMapNullValue);

        HttpMessageConverter<?> converter = fastConverter;

        return new HttpMessageConverters(converter);

    }
	
	/**
	 * 查询险别下的险种
	 * @param classcode
	 * @return
	 */
	@RequestMapping("queryRiskByClass")
	public Response queryRiskByClass(String classcode) {
		Response result = new Response();
		result = commonBusinessService.findRiskByClass(classcode);
		return result;
	}
}
