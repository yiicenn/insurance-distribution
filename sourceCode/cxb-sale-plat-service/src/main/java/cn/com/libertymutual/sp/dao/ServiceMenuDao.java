package cn.com.libertymutual.sp.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.com.libertymutual.sp.bean.TbSpServiceconfig;

public interface ServiceMenuDao extends PagingAndSortingRepository<TbSpServiceconfig, Integer>, JpaSpecificationExecutor<TbSpServiceconfig>{

	Optional<TbSpServiceconfig> findById(Integer id);

}
