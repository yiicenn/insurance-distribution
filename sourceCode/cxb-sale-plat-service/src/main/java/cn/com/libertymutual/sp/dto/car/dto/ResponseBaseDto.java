package cn.com.libertymutual.sp.dto.car.dto;

import java.io.Serializable;
public class ResponseBaseDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5568034665646299947L;
	private Boolean status; // 状态 Boolean true 成功;false:失败
	private String resultCode;// 返回类型代码 错误码
	private String resultMessage;// 错误信息 错误信息
	private String flowId;// 业务流程ID String 每次业务请求返回本次请求的流程ID
	private String uuId;// 返回的唯一序列码 String
	private String pdk;  //加密后验证码
	
	/**
	 * 成功
	 */
	public static final int STATE_SUCCESS = 0;
	/**
	 * 应用异常
	 */
	public static final int STATE_APP_EXCEPTION = 1;
	/**
	 * 其他异常
	 */
	public static final int STATE_EXCEPTION = 2;
	/**
	 * 没有登录
	 */
	public static final int STATE_NO_SESSION = 3;
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		//如果成功，设置默认值0
		if(status){
			this.resultCode= String.valueOf(STATE_SUCCESS);
		}
		
		this.status = status;
	}
	public String getResultCode() {
		return resultCode;
	}
	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}
	public String getResultMessage() {
		return resultMessage;
	}
	public void setResultMessage(String resultMessage) {
		this.resultMessage = resultMessage;
	}
	public String getFlowId() {
		return flowId;
	}
	public void setFlowId(String flowId) {
		this.flowId = flowId;
	}
	public String getUuId() {
		return uuId;
	}
	public void setUuId(String uuId) {
		this.uuId = uuId;
	}
	
	
	@Override
	public String toString() {
		return "ResponseBaseDto [status=" + status + ", resultCode=" + resultCode + ", resultMessage=" + resultMessage
				+ ", flowId=" + flowId + ", uuId=" + uuId + ", pdk=" + pdk + "]";
	}
	public String getPdk() {
		return pdk;
	}
	public void setPdk(String pdk) {
		this.pdk = pdk;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((flowId == null) ? 0 : flowId.hashCode());
		result = prime * result + ((pdk == null) ? 0 : pdk.hashCode());
		result = prime * result
				+ ((resultCode == null) ? 0 : resultCode.hashCode());
		result = prime * result
				+ ((resultMessage == null) ? 0 : resultMessage.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((uuId == null) ? 0 : uuId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResponseBaseDto other = (ResponseBaseDto) obj;
		if (flowId == null) {
			if (other.flowId != null)
				return false;
		} else if (!flowId.equals(other.flowId))
			return false;
		if (pdk == null) {
			if (other.pdk != null)
				return false;
		} else if (!pdk.equals(other.pdk))
			return false;
		if (resultCode == null) {
			if (other.resultCode != null)
				return false;
		} else if (!resultCode.equals(other.resultCode))
			return false;
		if (resultMessage == null) {
			if (other.resultMessage != null)
				return false;
		} else if (!resultMessage.equals(other.resultMessage))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		if (uuId == null) {
			if (other.uuId != null)
				return false;
		} else if (!uuId.equals(other.uuId))
			return false;
		return true;
	}
}