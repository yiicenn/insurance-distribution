package cn.com.libertymutual.sp.service.impl;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpApplicant;
import cn.com.libertymutual.sp.dao.ApplicantDao;
import cn.com.libertymutual.sp.dao.OrderDao;
import cn.com.libertymutual.sp.service.api.ApplicantService;

@Service("ApplicantService")
public class ApplicantServiceImpl implements ApplicantService {

	@Autowired
	private ApplicantDao applicantDao;

	
	@Override
	public ServiceResult findAppliIdCar(String idCar) {
		ServiceResult sr = new ServiceResult();
		List<TbSpApplicant> appList = applicantDao.findId(idCar);
		if (CollectionUtils.isNotEmpty(appList)) {
			sr.setSuccess();
			sr.setResult(appList.get(0));
		} else {
			sr.setFail();
		}
		// TODO Auto-generated method stub
		return sr;
	}


}
