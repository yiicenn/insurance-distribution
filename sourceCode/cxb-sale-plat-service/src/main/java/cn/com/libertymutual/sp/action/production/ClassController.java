package cn.com.libertymutual.sp.action.production;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.production.pojo.request.PrpdClassRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.ClassBusinessService;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@RestController
@RequestMapping("class")
public class ClassController {
	
	@Autowired
	protected ClassBusinessService classBusinessService;
	
	/**
	 * 获取险类
	 * @param PrpdClassLibraryRequest
	 * @return
	 */
	@RequestMapping("queryPrpdClass")
	public Response queryPrpdClause(PrpdClassRequest request) {
		Response result = new Response();
		result = classBusinessService.findPrpdClass(request);
		return result;
	}
	
	@RequestMapping("checkClassCodeUsed")
	public Response checkClassCodeUsed(String classcode) {
		Response result = new Response();
		result = classBusinessService.checkClassCodeUsed(classcode);
		return result;
	}
	
	@RequestMapping("insertPrpdClass")
	public Response insertPrpdClass(PrpdClassRequest request) {
		Response result = new Response();
		try {
			result = classBusinessService.insert(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
	
	@RequestMapping("updatePrpdClass")
	public Response updatePrpdClass(PrpdClassRequest request) {
		Response result = new Response();
		try {
			result = classBusinessService.update(request);
			result.setSuccess();
		} catch (Exception e) {
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}
}
