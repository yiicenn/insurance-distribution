
package cn.com.libertymutual.sp.webService.allpolicy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptInsuredDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptInsuredDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="identifyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="identifyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredAddress3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredAddress4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="insuredType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="jobTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="occupationSortName3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prpInsuredInsuredCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prpInsuredInsuredName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="roomAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="roomPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="roomPostCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serialNo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="serviceMark" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="stature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unitAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="weight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptInsuredDto", propOrder = {
    "email",
    "identifyNumber",
    "identifyType",
    "insuredAddress",
    "insuredAddress1",
    "insuredAddress2",
    "insuredAddress3",
    "insuredAddress4",
    "insuredIdentity",
    "insuredType",
    "jobTitle",
    "mobile",
    "occupationSortName3",
    "prpInsuredInsuredCode",
    "prpInsuredInsuredName",
    "roomAddress",
    "roomPhone",
    "roomPostCode",
    "serialNo",
    "serviceMark",
    "stature",
    "unit",
    "unitAddress",
    "weight"
})
public class PrptInsuredDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String email;
    protected String identifyNumber;
    protected String identifyType;
    protected String insuredAddress;
    protected String insuredAddress1;
    protected String insuredAddress2;
    protected String insuredAddress3;
    protected String insuredAddress4;
    protected String insuredIdentity;
    protected String insuredType;
    protected String jobTitle;
    protected String mobile;
    protected String occupationSortName3;
    protected String prpInsuredInsuredCode;
    protected String prpInsuredInsuredName;
    protected String roomAddress;
    protected String roomPhone;
    protected String roomPostCode;
    protected int serialNo;
    protected String serviceMark;
    protected String stature;
    protected String unit;
    protected String unitAddress;
    protected String weight;

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the identifyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentifyNumber() {
        return identifyNumber;
    }

    /**
     * Sets the value of the identifyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentifyNumber(String value) {
        this.identifyNumber = value;
    }

    /**
     * Gets the value of the identifyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentifyType() {
        return identifyType;
    }

    /**
     * Sets the value of the identifyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentifyType(String value) {
        this.identifyType = value;
    }

    /**
     * Gets the value of the insuredAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredAddress() {
        return insuredAddress;
    }

    /**
     * Sets the value of the insuredAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredAddress(String value) {
        this.insuredAddress = value;
    }

    /**
     * Gets the value of the insuredAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredAddress1() {
        return insuredAddress1;
    }

    /**
     * Sets the value of the insuredAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredAddress1(String value) {
        this.insuredAddress1 = value;
    }

    /**
     * Gets the value of the insuredAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredAddress2() {
        return insuredAddress2;
    }

    /**
     * Sets the value of the insuredAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredAddress2(String value) {
        this.insuredAddress2 = value;
    }

    /**
     * Gets the value of the insuredAddress3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredAddress3() {
        return insuredAddress3;
    }

    /**
     * Sets the value of the insuredAddress3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredAddress3(String value) {
        this.insuredAddress3 = value;
    }

    /**
     * Gets the value of the insuredAddress4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredAddress4() {
        return insuredAddress4;
    }

    /**
     * Sets the value of the insuredAddress4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredAddress4(String value) {
        this.insuredAddress4 = value;
    }

    /**
     * Gets the value of the insuredIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredIdentity() {
        return insuredIdentity;
    }

    /**
     * Sets the value of the insuredIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredIdentity(String value) {
        this.insuredIdentity = value;
    }

    /**
     * Gets the value of the insuredType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuredType() {
        return insuredType;
    }

    /**
     * Sets the value of the insuredType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuredType(String value) {
        this.insuredType = value;
    }

    /**
     * Gets the value of the jobTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Sets the value of the jobTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJobTitle(String value) {
        this.jobTitle = value;
    }

    /**
     * Gets the value of the mobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Sets the value of the mobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobile(String value) {
        this.mobile = value;
    }

    /**
     * Gets the value of the occupationSortName3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupationSortName3() {
        return occupationSortName3;
    }

    /**
     * Sets the value of the occupationSortName3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupationSortName3(String value) {
        this.occupationSortName3 = value;
    }

    /**
     * Gets the value of the prpInsuredInsuredCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrpInsuredInsuredCode() {
        return prpInsuredInsuredCode;
    }

    /**
     * Sets the value of the prpInsuredInsuredCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrpInsuredInsuredCode(String value) {
        this.prpInsuredInsuredCode = value;
    }

    /**
     * Gets the value of the prpInsuredInsuredName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrpInsuredInsuredName() {
        return prpInsuredInsuredName;
    }

    /**
     * Sets the value of the prpInsuredInsuredName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrpInsuredInsuredName(String value) {
        this.prpInsuredInsuredName = value;
    }

    /**
     * Gets the value of the roomAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomAddress() {
        return roomAddress;
    }

    /**
     * Sets the value of the roomAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomAddress(String value) {
        this.roomAddress = value;
    }

    /**
     * Gets the value of the roomPhone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomPhone() {
        return roomPhone;
    }

    /**
     * Sets the value of the roomPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomPhone(String value) {
        this.roomPhone = value;
    }

    /**
     * Gets the value of the roomPostCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoomPostCode() {
        return roomPostCode;
    }

    /**
     * Sets the value of the roomPostCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoomPostCode(String value) {
        this.roomPostCode = value;
    }

    /**
     * Gets the value of the serialNo property.
     * 
     */
    public int getSerialNo() {
        return serialNo;
    }

    /**
     * Sets the value of the serialNo property.
     * 
     */
    public void setSerialNo(int value) {
        this.serialNo = value;
    }

    /**
     * Gets the value of the serviceMark property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceMark() {
        return serviceMark;
    }

    /**
     * Sets the value of the serviceMark property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceMark(String value) {
        this.serviceMark = value;
    }

    /**
     * Gets the value of the stature property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStature() {
        return stature;
    }

    /**
     * Sets the value of the stature property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStature(String value) {
        this.stature = value;
    }

    /**
     * Gets the value of the unit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets the value of the unit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnit(String value) {
        this.unit = value;
    }

    /**
     * Gets the value of the unitAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitAddress() {
        return unitAddress;
    }

    /**
     * Sets the value of the unitAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitAddress(String value) {
        this.unitAddress = value;
    }

    /**
     * Gets the value of the weight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeight() {
        return weight;
    }

    /**
     * Sets the value of the weight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeight(String value) {
        this.weight = value;
    }

}
