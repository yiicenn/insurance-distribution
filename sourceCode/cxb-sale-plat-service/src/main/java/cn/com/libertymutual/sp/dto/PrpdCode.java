package cn.com.libertymutual.sp.dto;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "PRPDCODE", schema = "", catalog = "")
@IdClass(PrpdCodePK.class)
public class PrpdCode implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1955253886696291803L;
	private String codeType;
	private String codeCode;
	private String codeCName;
	private String codeEName;
	private String newCodeCode;
	private String validStatus;
	private String flag;
	
	@Id
    @Column(name = "CODETYPE")
	public String getCodeType() {
		return codeType;
	}
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}
	@Id
    @Column(name = "CODECODE")
	public String getCodeCode() {
		return codeCode;
	}
	public void setCodeCode(String codeCode) {
		this.codeCode = codeCode;
	}
	 @Basic
	    @Column(name = "CODECNAME")
	public String getCodeCName() {
		return codeCName;
	}
	public void setCodeCName(String codeCName) {
		this.codeCName = codeCName;
	}
	@Basic
    @Column(name = "CODEENAME")
	public String getCodeEName() {
		return codeEName;
	}
	public void setCodeEName(String codeEName) {
		this.codeEName = codeEName;
	}
	@Basic
    @Column(name = "NEWCODECODE")
	public String getNewCodeCode() {
		return newCodeCode;
	}
	public void setNewCodeCode(String newCodeCode) {
		this.newCodeCode = newCodeCode;
	}
	@Basic
    @Column(name = "VALIDSTATUS")
	public String getValidStatus() {
		return validStatus;
	}
	public void setValidStatus(String validStatus) {
		this.validStatus = validStatus;
	}
	@Basic
    @Column(name = "FLAG")
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
}

