package cn.com.libertymutual.sp.dto.axtx;

import java.io.Serializable;

public class PolicyDataDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8263771600285530094L;
	private String riskCode;//
	private String StartDate;// 保险起期
	private String inputDate;// 录入日期
	private String productCode;// 计划代码
	private Double premium;// 保费
	private String agreementCode; // 业务关系代码
	private String agentCode; // 代理人代码
	private String saleCode; // 销售人员代码
	private String salerName; // 销售人员名称 20171113 bob.kuang
	// 第三方平台销售人员代码
	private String trdSalesCode;

	private String comm1Rate;

	public String getComm1Rate() {
		return comm1Rate;
	}

	public void setComm1Rate(String comm1Rate) {
		this.comm1Rate = comm1Rate;
	}

	// 数据修改回调地址
	private String callBackUrl;

	public String getTrdSalesCode() {
		return trdSalesCode;
	}

	public void setTrdSalesCode(String trdSalesCode) {
		this.trdSalesCode = trdSalesCode;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	public String getStartDate() {
		return StartDate;
	}

	public void setStartDate(String startDate) {
		StartDate = startDate;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Double getPremium() {
		return premium;
	}

	public void setPremium(Double premium) {
		this.premium = premium;
	}

	public String getAgreementCode() {
		return agreementCode;
	}

	public void setAgreementCode(String agreementCode) {
		this.agreementCode = agreementCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getSaleCode() {
		return saleCode;
	}

	public void setSaleCode(String saleCode) {
		this.saleCode = saleCode;
	}

	public String getSalerName() {
		return salerName;
	}

	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((StartDate == null) ? 0 : StartDate.hashCode());
		result = prime * result + ((agentCode == null) ? 0 : agentCode.hashCode());
		result = prime * result + ((agreementCode == null) ? 0 : agreementCode.hashCode());
		result = prime * result + ((inputDate == null) ? 0 : inputDate.hashCode());
		result = prime * result + ((premium == null) ? 0 : premium.hashCode());
		result = prime * result + ((productCode == null) ? 0 : productCode.hashCode());
		result = prime * result + ((riskCode == null) ? 0 : riskCode.hashCode());
		result = prime * result + ((saleCode == null) ? 0 : saleCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PolicyDataDto other = (PolicyDataDto) obj;
		if (StartDate == null) {
			if (other.StartDate != null)
				return false;
		} else if (!StartDate.equals(other.StartDate))
			return false;
		if (agentCode == null) {
			if (other.agentCode != null)
				return false;
		} else if (!agentCode.equals(other.agentCode))
			return false;
		if (agreementCode == null) {
			if (other.agreementCode != null)
				return false;
		} else if (!agreementCode.equals(other.agreementCode))
			return false;
		if (inputDate == null) {
			if (other.inputDate != null)
				return false;
		} else if (!inputDate.equals(other.inputDate))
			return false;
		if (premium == null) {
			if (other.premium != null)
				return false;
		} else if (!premium.equals(other.premium))
			return false;
		if (productCode == null) {
			if (other.productCode != null)
				return false;
		} else if (!productCode.equals(other.productCode))
			return false;
		if (riskCode == null) {
			if (other.riskCode != null)
				return false;
		} else if (!riskCode.equals(other.riskCode))
			return false;
		if (saleCode == null) {
			if (other.saleCode != null)
				return false;
		} else if (!saleCode.equals(other.saleCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PolicyDataDto [riskCode=" + riskCode + ", StartDate=" + StartDate + ", inputDate=" + inputDate + ", productCode=" + productCode
				+ ", premium=" + premium + ", agreementCode=" + agreementCode + ", agentCode=" + agentCode + ", saleCode=" + saleCode + ", salerName="
				+ salerName + "]";
	}
}
