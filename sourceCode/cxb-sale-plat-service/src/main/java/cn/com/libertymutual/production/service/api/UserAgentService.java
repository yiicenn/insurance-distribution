package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpsagent;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpsagentWithBLOBs;
import cn.com.libertymutual.production.model.nomorcldatasource.TbSpUser;
import cn.com.libertymutual.production.pojo.request.Request;

public interface UserAgentService {
	
	public List<TbSpUser> findUsersAgentByComCode(List<String> comCodes, Request request);
}
