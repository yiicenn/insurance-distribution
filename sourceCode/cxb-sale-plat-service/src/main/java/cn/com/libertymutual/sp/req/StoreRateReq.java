package cn.com.libertymutual.sp.req;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;

import cn.com.libertymutual.sp.bean.TbSpStoreConfig;
import io.swagger.annotations.ApiModel;

@ApiModel
public class StoreRateReq implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8686033805625491897L;

	private String userCodes;
	private List<TbSpStoreConfig> storeConfig;
	public String getUserCodes() {
		return userCodes;
	}
	public void setUserCodes(String userCodes) {
		this.userCodes = userCodes;
	}
	public List<TbSpStoreConfig> getStoreConfig() {
		return storeConfig;
	}
	public void setStoreConfig(List<TbSpStoreConfig> storeConfig) {
		this.storeConfig = storeConfig;
	}
	
	
}
