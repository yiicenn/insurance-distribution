package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.Date;

import cn.com.libertymutual.production.utils.LogFiled;

public class Prpdclause extends PrpdclauseKey {

	@LogFiled(chineseName = "特约名称")
	private String clausename;
	@LogFiled(chineseName = "语种")
	private String language;

	private String titleflag;
	@LogFiled(chineseName = "内容")
	private String context;

	private String newclausecode;
	@LogFiled(chineseName = "有效标识")
	private String validstatus;

	private String flag;
	@LogFiled(chineseName = "适用分公司")
	private String comcode;
	@LogFiled(chineseName = "内容是否可修改标志位")
	private String contflag;
	@LogFiled(chineseName = "是否可删除标志位")
	private String delflag;
	@LogFiled(chineseName = "初始化默认带出标志位")
	private String iniflag;
	@LogFiled(chineseName = "审核人备注")
	private String remark;
	@LogFiled(chineseName = "适用代理人")
	private String agentcode;

	private Date createdate;

	private Date updatedate;

	public String getClausename() {
		return clausename;
	}

	public void setClausename(String clausename) {
		this.clausename = clausename == null ? null : clausename.trim();
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language == null ? null : language.trim();
	}

	public String getTitleflag() {
		return titleflag;
	}

	public void setTitleflag(String titleflag) {
		this.titleflag = titleflag == null ? null : titleflag.trim();
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context == null ? null : context;
	}

	public String getNewclausecode() {
		return newclausecode;
	}

	public void setNewclausecode(String newclausecode) {
		this.newclausecode = newclausecode == null ? null : newclausecode
				.trim();
	}

	public String getValidstatus() {
		return validstatus;
	}

	public void setValidstatus(String validstatus) {
		this.validstatus = validstatus == null ? null : validstatus.trim();
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag == null ? null : flag.trim();
	}

	public String getComcode() {
		return comcode;
	}

	public void setComcode(String comcode) {
		this.comcode = comcode == null ? null : comcode.trim();
	}

	public String getContflag() {
		return contflag;
	}

	public void setContflag(String contflag) {
		this.contflag = contflag == null ? null : contflag.trim();
	}

	public String getDelflag() {
		return delflag;
	}

	public void setDelflag(String delflag) {
		this.delflag = delflag == null ? null : delflag.trim();
	}

	public String getIniflag() {
		return iniflag;
	}

	public void setIniflag(String iniflag) {
		this.iniflag = iniflag == null ? null : iniflag.trim();
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark == null ? null : remark.trim();
	}

	public String getAgentcode() {
		return agentcode;
	}

	public void setAgentcode(String agentcode) {
		this.agentcode = agentcode == null ? null : agentcode.trim();
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public Date getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}
}