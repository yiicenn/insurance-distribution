package cn.com.libertymutual.sp.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import cn.com.libertymutual.core.redis.util.RedisUtils;
import cn.com.libertymutual.core.util.DateUtil;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpProduct;
import cn.com.libertymutual.sp.bean.TbWebRequestLog;
import cn.com.libertymutual.sp.dao.OrderDao;
import cn.com.libertymutual.sp.dao.ProductDao;
import cn.com.libertymutual.sp.dao.TbWebRequestLogDao;
import cn.com.libertymutual.sp.service.api.HomePageService;

@Service("homePageService")
public class HomePageServiceImpl implements HomePageService {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private OrderDao orderDao;
	@Resource
	private RedisUtils redisUtils;
	@Autowired
	private ProductDao productDao;

	@Autowired
	private TbWebRequestLogDao webRequestLogDao;

	@Override
	public ServiceResult salesCount(String startDate, String endDate) {
		ServiceResult sr = new ServiceResult();
		// List<String> productId = orderDao.findDistinctProduct();
		List<TbSpProduct> list = Lists.newArrayList();
		// List<Object> list = Lists.newArrayList();
		int count = 0;

		if (StringUtils.isNotEmpty(startDate) && StringUtils.isNotEmpty(endDate)) {
			List<String> productId1 = orderDao.findDistinctProduct(startDate, endDate);
			for (String pId : productId1) {
				count = orderDao.findSalesCountByDate(pId, startDate, endDate);
				TbSpProduct tso = productDao.findById(Integer.parseInt(pId)).get();
				tso.setCount(count);
				list.add(tso);
			}
		} else {
			List<String> productId2 = orderDao.findDistinctProduct();
			for (String pId : productId2) {
				count = orderDao.findSalesCount(pId);
				log.info(pId + "======" + count);
				TbSpProduct tso = productDao.findById(Integer.parseInt(pId)).get();
				tso.setCount(count);
				list.add(tso);
			}
		}
		// if (StringUtils.isNotEmpty(startDate) &&
		// StringUtils.isNotEmpty(endDate)) {
		// list = orderDao.findOrderCountByDate(startDate, endDate);
		// } else {
		// list = orderDao.findOrderCount();
		// }
		sr.setResult(list);
		return sr;
	}

	// @Override
	// public ServiceResult visitsCount(String startDate, String endDate) {
	// ServiceResult sr = new ServiceResult();
	// List<Object> list = Lists.newArrayList();
	// if (StringUtils.isEmpty(startDate) && StringUtils.isEmpty(endDate)) {
	// log.info("-------------------date null-----------------");
	// list = webRequestLogDao.findCityall();
	// } else {
	// log.info("-----------------date not null------------------");
	// list = webRequestLogDao.findCity(startDate, endDate);
	// }
	// sr.setResult(list);
	// return sr;
	// }

	@Override
	public ServiceResult visitsCount(String type) {
		ServiceResult sr = new ServiceResult();
		
		List<TbWebRequestLog> list = (List<TbWebRequestLog>) redisUtils.get("visitCount"+type);
		if (CollectionUtils.isEmpty(list)) {
			SimpleDateFormat df = new SimpleDateFormat(DateUtil.DATE_TIME_PATTERN2);// 设置日期格式
			if ("year".equals(type)) {
				list = this.visitTask(DateUtil.getThisMonthFirstDate(),DateUtil.getThisYearLastDate(),type);
			}
			if ("month".equals(type)) {
				list = this.visitTask(DateUtil.getThisMonthFirstDate(),df.format(DateUtil.getLastDayOfThisMonth()),type);
			}
			if ("week".equals(type)) {
				list = this.visitTask(df.format(DateUtil.getWeekStartDate()),df.format(DateUtil.dayOffset(DateUtil.getWeekStartDate(),7)),type);
			}
		}
		sr.setResult(list);
		return sr;
	}

	
	@Override
	public List<TbWebRequestLog> visitTask(String startDate, String endDate,String type) {
		List<TbWebRequestLog> list = Lists.newArrayList();
		List<String> city = webRequestLogDao.findDistinctCity();
		log.info("------city-----" + city);
		for (String cityId : city) {
			log.info("-----cityId-----" + cityId);
			TbWebRequestLog trl = new TbWebRequestLog();
			List<TbWebRequestLog> req = webRequestLogDao.findByCityId(cityId);
			trl.setCityId(cityId);
			trl.setLatitude(req.get(0).getLatitude());
			trl.setLongitude(req.get(0).getLongitude());
			if (StringUtils.isEmpty(startDate) && StringUtils.isEmpty(endDate)) {
				log.info("date null");
				int count = webRequestLogDao.findCountByCityId(cityId);
				trl.setCount(count);
			} else {
				log.info("date not null");
				int count = webRequestLogDao.findCountByCityIdDate(cityId, startDate, endDate);
				trl.setCount(count);
			}
			list.add(trl);
		}
		redisUtils.set("visitCount"+type, list);
		return list;
	}

	@Override
	public void task() {
		SimpleDateFormat df = new SimpleDateFormat(DateUtil.DATE_TIME_PATTERN2);// 设置日期格式
		String startYear = DateUtil.getThisYearFirstDate();
		log.info("本年开始：{}",startYear);
		String endYear = DateUtil.getThisYearLastDate();
		log.info("本年结束：{}",endYear);
		this.visitTask(startYear,endYear,"year");
		String startMonth = DateUtil.getThisMonthFirstDate();
		log.info("本月开始：{}",startMonth);
		String endMonth = df.format(DateUtil.getLastDayOfThisMonth());
		log.info("本月结束：{}",endMonth);
		this.visitTask(startMonth,endMonth,"month");
		String startWeek = df.format(DateUtil.getWeekStartDate());
		log.info("本周开始：{}",startWeek);
		String endWeek = df.format(DateUtil.dayOffset(DateUtil.getWeekStartDate(),7));
		log.info("本周结束：{}",endWeek);
		this.visitTask(startWeek,endWeek,"week");
	}
	
}
