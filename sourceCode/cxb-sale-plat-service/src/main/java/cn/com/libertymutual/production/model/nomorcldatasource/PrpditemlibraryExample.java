package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrpditemlibraryExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpditemlibraryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andItemcodeIsNull() {
            addCriterion("ITEMCODE is null");
            return (Criteria) this;
        }

        public Criteria andItemcodeIsNotNull() {
            addCriterion("ITEMCODE is not null");
            return (Criteria) this;
        }

        public Criteria andItemcodeEqualTo(String value) {
            addCriterion("ITEMCODE =", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeNotEqualTo(String value) {
            addCriterion("ITEMCODE <>", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeGreaterThan(String value) {
            addCriterion("ITEMCODE >", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeGreaterThanOrEqualTo(String value) {
            addCriterion("ITEMCODE >=", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeLessThan(String value) {
            addCriterion("ITEMCODE <", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeLessThanOrEqualTo(String value) {
            addCriterion("ITEMCODE <=", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeLike(String value) {
            addCriterion("ITEMCODE like", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeNotLike(String value) {
            addCriterion("ITEMCODE not like", value, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeIn(List<String> values) {
            addCriterion("ITEMCODE in", values, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeNotIn(List<String> values) {
            addCriterion("ITEMCODE not in", values, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeBetween(String value1, String value2) {
            addCriterion("ITEMCODE between", value1, value2, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcodeNotBetween(String value1, String value2) {
            addCriterion("ITEMCODE not between", value1, value2, "itemcode");
            return (Criteria) this;
        }

        public Criteria andItemcnameIsNull() {
            addCriterion("ITEMCNAME is null");
            return (Criteria) this;
        }

        public Criteria andItemcnameIsNotNull() {
            addCriterion("ITEMCNAME is not null");
            return (Criteria) this;
        }

        public Criteria andItemcnameEqualTo(String value) {
            addCriterion("ITEMCNAME =", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameNotEqualTo(String value) {
            addCriterion("ITEMCNAME <>", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameGreaterThan(String value) {
            addCriterion("ITEMCNAME >", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameGreaterThanOrEqualTo(String value) {
            addCriterion("ITEMCNAME >=", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameLessThan(String value) {
            addCriterion("ITEMCNAME <", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameLessThanOrEqualTo(String value) {
            addCriterion("ITEMCNAME <=", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameLike(String value) {
            addCriterion("ITEMCNAME like", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameNotLike(String value) {
            addCriterion("ITEMCNAME not like", value, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameIn(List<String> values) {
            addCriterion("ITEMCNAME in", values, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameNotIn(List<String> values) {
            addCriterion("ITEMCNAME not in", values, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameBetween(String value1, String value2) {
            addCriterion("ITEMCNAME between", value1, value2, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemcnameNotBetween(String value1, String value2) {
            addCriterion("ITEMCNAME not between", value1, value2, "itemcname");
            return (Criteria) this;
        }

        public Criteria andItemenameIsNull() {
            addCriterion("ITEMENAME is null");
            return (Criteria) this;
        }

        public Criteria andItemenameIsNotNull() {
            addCriterion("ITEMENAME is not null");
            return (Criteria) this;
        }

        public Criteria andItemenameEqualTo(String value) {
            addCriterion("ITEMENAME =", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameNotEqualTo(String value) {
            addCriterion("ITEMENAME <>", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameGreaterThan(String value) {
            addCriterion("ITEMENAME >", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameGreaterThanOrEqualTo(String value) {
            addCriterion("ITEMENAME >=", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameLessThan(String value) {
            addCriterion("ITEMENAME <", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameLessThanOrEqualTo(String value) {
            addCriterion("ITEMENAME <=", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameLike(String value) {
            addCriterion("ITEMENAME like", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameNotLike(String value) {
            addCriterion("ITEMENAME not like", value, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameIn(List<String> values) {
            addCriterion("ITEMENAME in", values, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameNotIn(List<String> values) {
            addCriterion("ITEMENAME not in", values, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameBetween(String value1, String value2) {
            addCriterion("ITEMENAME between", value1, value2, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemenameNotBetween(String value1, String value2) {
            addCriterion("ITEMENAME not between", value1, value2, "itemename");
            return (Criteria) this;
        }

        public Criteria andItemflagIsNull() {
            addCriterion("ITEMFLAG is null");
            return (Criteria) this;
        }

        public Criteria andItemflagIsNotNull() {
            addCriterion("ITEMFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andItemflagEqualTo(String value) {
            addCriterion("ITEMFLAG =", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagNotEqualTo(String value) {
            addCriterion("ITEMFLAG <>", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagGreaterThan(String value) {
            addCriterion("ITEMFLAG >", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagGreaterThanOrEqualTo(String value) {
            addCriterion("ITEMFLAG >=", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagLessThan(String value) {
            addCriterion("ITEMFLAG <", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagLessThanOrEqualTo(String value) {
            addCriterion("ITEMFLAG <=", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagLike(String value) {
            addCriterion("ITEMFLAG like", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagNotLike(String value) {
            addCriterion("ITEMFLAG not like", value, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagIn(List<String> values) {
            addCriterion("ITEMFLAG in", values, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagNotIn(List<String> values) {
            addCriterion("ITEMFLAG not in", values, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagBetween(String value1, String value2) {
            addCriterion("ITEMFLAG between", value1, value2, "itemflag");
            return (Criteria) this;
        }

        public Criteria andItemflagNotBetween(String value1, String value2) {
            addCriterion("ITEMFLAG not between", value1, value2, "itemflag");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeIsNull() {
            addCriterion("NEWITEMCODE is null");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeIsNotNull() {
            addCriterion("NEWITEMCODE is not null");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeEqualTo(String value) {
            addCriterion("NEWITEMCODE =", value, "newitemcode");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeNotEqualTo(String value) {
            addCriterion("NEWITEMCODE <>", value, "newitemcode");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeGreaterThan(String value) {
            addCriterion("NEWITEMCODE >", value, "newitemcode");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeGreaterThanOrEqualTo(String value) {
            addCriterion("NEWITEMCODE >=", value, "newitemcode");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeLessThan(String value) {
            addCriterion("NEWITEMCODE <", value, "newitemcode");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeLessThanOrEqualTo(String value) {
            addCriterion("NEWITEMCODE <=", value, "newitemcode");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeLike(String value) {
            addCriterion("NEWITEMCODE like", value, "newitemcode");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeNotLike(String value) {
            addCriterion("NEWITEMCODE not like", value, "newitemcode");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeIn(List<String> values) {
            addCriterion("NEWITEMCODE in", values, "newitemcode");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeNotIn(List<String> values) {
            addCriterion("NEWITEMCODE not in", values, "newitemcode");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeBetween(String value1, String value2) {
            addCriterion("NEWITEMCODE between", value1, value2, "newitemcode");
            return (Criteria) this;
        }

        public Criteria andNewitemcodeNotBetween(String value1, String value2) {
            addCriterion("NEWITEMCODE not between", value1, value2, "newitemcode");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNull() {
            addCriterion("VALIDSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNotNull() {
            addCriterion("VALIDSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidstatusEqualTo(String value) {
            addCriterion("VALIDSTATUS =", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotEqualTo(String value) {
            addCriterion("VALIDSTATUS <>", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThan(String value) {
            addCriterion("VALIDSTATUS >", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS >=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThan(String value) {
            addCriterion("VALIDSTATUS <", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS <=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLike(String value) {
            addCriterion("VALIDSTATUS like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotLike(String value) {
            addCriterion("VALIDSTATUS not like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusIn(List<String> values) {
            addCriterion("VALIDSTATUS in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotIn(List<String> values) {
            addCriterion("VALIDSTATUS not in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS not between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNull() {
            addCriterion("STARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNotNull() {
            addCriterion("STARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andStartdateEqualTo(Date value) {
            addCriterion("STARTDATE =", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotEqualTo(Date value) {
            addCriterion("STARTDATE <>", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThan(Date value) {
            addCriterion("STARTDATE >", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("STARTDATE >=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThan(Date value) {
            addCriterion("STARTDATE <", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThanOrEqualTo(Date value) {
            addCriterion("STARTDATE <=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateIn(List<Date> values) {
            addCriterion("STARTDATE in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotIn(List<Date> values) {
            addCriterion("STARTDATE not in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateBetween(Date value1, Date value2) {
            addCriterion("STARTDATE between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotBetween(Date value1, Date value2) {
            addCriterion("STARTDATE not between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andEnddateIsNull() {
            addCriterion("ENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andEnddateIsNotNull() {
            addCriterion("ENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andEnddateEqualTo(Date value) {
            addCriterion("ENDDATE =", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotEqualTo(Date value) {
            addCriterion("ENDDATE <>", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateGreaterThan(Date value) {
            addCriterion("ENDDATE >", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateGreaterThanOrEqualTo(Date value) {
            addCriterion("ENDDATE >=", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateLessThan(Date value) {
            addCriterion("ENDDATE <", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateLessThanOrEqualTo(Date value) {
            addCriterion("ENDDATE <=", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateIn(List<Date> values) {
            addCriterion("ENDDATE in", values, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotIn(List<Date> values) {
            addCriterion("ENDDATE not in", values, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateBetween(Date value1, Date value2) {
            addCriterion("ENDDATE between", value1, value2, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotBetween(Date value1, Date value2) {
            addCriterion("ENDDATE not between", value1, value2, "enddate");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNull() {
            addCriterion("CREATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNotNull() {
            addCriterion("CREATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedateEqualTo(Date value) {
            addCriterion("CREATEDATE =", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotEqualTo(Date value) {
            addCriterion("CREATEDATE <>", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThan(Date value) {
            addCriterion("CREATEDATE >", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE >=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThan(Date value) {
            addCriterion("CREATEDATE <", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE <=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateIn(List<Date> values) {
            addCriterion("CREATEDATE in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotIn(List<Date> values) {
            addCriterion("CREATEDATE not in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE not between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UPDATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UPDATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UPDATEDATE =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UPDATEDATE <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UPDATEDATE >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UPDATEDATE >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UPDATEDATE <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UPDATEDATE <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UPDATEDATE in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UPDATEDATE not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UPDATEDATE between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UPDATEDATE not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}