package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpmaxnoKey {
    private String groupno;

    private String tablename;

    private String maxno;

    public String getGroupno() {
        return groupno;
    }

    public void setGroupno(String groupno) {
        this.groupno = groupno == null ? null : groupno.trim();
    }

    public String getTablename() {
        return tablename;
    }

    public void setTablename(String tablename) {
        this.tablename = tablename == null ? null : tablename.trim();
    }

    public String getMaxno() {
        return maxno;
    }

    public void setMaxno(String maxno) {
        this.maxno = maxno == null ? null : maxno.trim();
    }
}