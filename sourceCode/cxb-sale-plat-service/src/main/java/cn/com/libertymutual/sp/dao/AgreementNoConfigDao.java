package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.com.libertymutual.sp.bean.TbSpAgreementNoConfig;

public interface AgreementNoConfigDao extends PagingAndSortingRepository<TbSpAgreementNoConfig, Integer>, JpaSpecificationExecutor<TbSpAgreementNoConfig> {

	@Query("select t from TbSpAgreementNoConfig t where t.agreementNo = ?1 and t.productId=?2")
	List<TbSpAgreementNoConfig> findByAgreementNoAndProductId(String agreementNo, Integer productId);

	@Query("select t from TbSpAgreementNoConfig t where t.agreementNo = ?1")
	Page<TbSpAgreementNoConfig> findByAgreementNo(String agreementNo, Pageable pageable);
}
