package cn.com.libertymutual.production.service.api;

import java.util.List;

import com.github.pagehelper.PageInfo;

import cn.com.libertymutual.production.model.nomorcldatasource.TbSysBranch;
import cn.com.libertymutual.production.pojo.request.Request;

public interface BranchService {
	
	/**
	 * 查询所有机构
	 * @return
	 */
	public PageInfo<TbSysBranch> findBranchs(Request request);
	
	/**
	 * 查询分支机构
	 * @return
	 */
	public List<TbSysBranch> findNextBranchs();

	/**
	 * 查询分支机构
	 * @param comcode
	 * @return
     */
	List<TbSysBranch> findNextBranchsByCodes(List<String> comcodes);
}
