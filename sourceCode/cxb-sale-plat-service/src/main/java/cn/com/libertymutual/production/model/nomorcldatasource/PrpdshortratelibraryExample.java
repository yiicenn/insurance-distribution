package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PrpdshortratelibraryExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpdshortratelibraryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andShortratetypeIsNull() {
            addCriterion("SHORTRATETYPE is null");
            return (Criteria) this;
        }

        public Criteria andShortratetypeIsNotNull() {
            addCriterion("SHORTRATETYPE is not null");
            return (Criteria) this;
        }

        public Criteria andShortratetypeEqualTo(String value) {
            addCriterion("SHORTRATETYPE =", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeNotEqualTo(String value) {
            addCriterion("SHORTRATETYPE <>", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeGreaterThan(String value) {
            addCriterion("SHORTRATETYPE >", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeGreaterThanOrEqualTo(String value) {
            addCriterion("SHORTRATETYPE >=", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeLessThan(String value) {
            addCriterion("SHORTRATETYPE <", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeLessThanOrEqualTo(String value) {
            addCriterion("SHORTRATETYPE <=", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeLike(String value) {
            addCriterion("SHORTRATETYPE like", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeNotLike(String value) {
            addCriterion("SHORTRATETYPE not like", value, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeIn(List<String> values) {
            addCriterion("SHORTRATETYPE in", values, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeNotIn(List<String> values) {
            addCriterion("SHORTRATETYPE not in", values, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeBetween(String value1, String value2) {
            addCriterion("SHORTRATETYPE between", value1, value2, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andShortratetypeNotBetween(String value1, String value2) {
            addCriterion("SHORTRATETYPE not between", value1, value2, "shortratetype");
            return (Criteria) this;
        }

        public Criteria andRate1IsNull() {
            addCriterion("RATE1 is null");
            return (Criteria) this;
        }

        public Criteria andRate1IsNotNull() {
            addCriterion("RATE1 is not null");
            return (Criteria) this;
        }

        public Criteria andRate1EqualTo(BigDecimal value) {
            addCriterion("RATE1 =", value, "rate1");
            return (Criteria) this;
        }

        public Criteria andRate1NotEqualTo(BigDecimal value) {
            addCriterion("RATE1 <>", value, "rate1");
            return (Criteria) this;
        }

        public Criteria andRate1GreaterThan(BigDecimal value) {
            addCriterion("RATE1 >", value, "rate1");
            return (Criteria) this;
        }

        public Criteria andRate1GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE1 >=", value, "rate1");
            return (Criteria) this;
        }

        public Criteria andRate1LessThan(BigDecimal value) {
            addCriterion("RATE1 <", value, "rate1");
            return (Criteria) this;
        }

        public Criteria andRate1LessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE1 <=", value, "rate1");
            return (Criteria) this;
        }

        public Criteria andRate1In(List<BigDecimal> values) {
            addCriterion("RATE1 in", values, "rate1");
            return (Criteria) this;
        }

        public Criteria andRate1NotIn(List<BigDecimal> values) {
            addCriterion("RATE1 not in", values, "rate1");
            return (Criteria) this;
        }

        public Criteria andRate1Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE1 between", value1, value2, "rate1");
            return (Criteria) this;
        }

        public Criteria andRate1NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE1 not between", value1, value2, "rate1");
            return (Criteria) this;
        }

        public Criteria andRate2IsNull() {
            addCriterion("RATE2 is null");
            return (Criteria) this;
        }

        public Criteria andRate2IsNotNull() {
            addCriterion("RATE2 is not null");
            return (Criteria) this;
        }

        public Criteria andRate2EqualTo(BigDecimal value) {
            addCriterion("RATE2 =", value, "rate2");
            return (Criteria) this;
        }

        public Criteria andRate2NotEqualTo(BigDecimal value) {
            addCriterion("RATE2 <>", value, "rate2");
            return (Criteria) this;
        }

        public Criteria andRate2GreaterThan(BigDecimal value) {
            addCriterion("RATE2 >", value, "rate2");
            return (Criteria) this;
        }

        public Criteria andRate2GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE2 >=", value, "rate2");
            return (Criteria) this;
        }

        public Criteria andRate2LessThan(BigDecimal value) {
            addCriterion("RATE2 <", value, "rate2");
            return (Criteria) this;
        }

        public Criteria andRate2LessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE2 <=", value, "rate2");
            return (Criteria) this;
        }

        public Criteria andRate2In(List<BigDecimal> values) {
            addCriterion("RATE2 in", values, "rate2");
            return (Criteria) this;
        }

        public Criteria andRate2NotIn(List<BigDecimal> values) {
            addCriterion("RATE2 not in", values, "rate2");
            return (Criteria) this;
        }

        public Criteria andRate2Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE2 between", value1, value2, "rate2");
            return (Criteria) this;
        }

        public Criteria andRate2NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE2 not between", value1, value2, "rate2");
            return (Criteria) this;
        }

        public Criteria andRate3IsNull() {
            addCriterion("RATE3 is null");
            return (Criteria) this;
        }

        public Criteria andRate3IsNotNull() {
            addCriterion("RATE3 is not null");
            return (Criteria) this;
        }

        public Criteria andRate3EqualTo(BigDecimal value) {
            addCriterion("RATE3 =", value, "rate3");
            return (Criteria) this;
        }

        public Criteria andRate3NotEqualTo(BigDecimal value) {
            addCriterion("RATE3 <>", value, "rate3");
            return (Criteria) this;
        }

        public Criteria andRate3GreaterThan(BigDecimal value) {
            addCriterion("RATE3 >", value, "rate3");
            return (Criteria) this;
        }

        public Criteria andRate3GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE3 >=", value, "rate3");
            return (Criteria) this;
        }

        public Criteria andRate3LessThan(BigDecimal value) {
            addCriterion("RATE3 <", value, "rate3");
            return (Criteria) this;
        }

        public Criteria andRate3LessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE3 <=", value, "rate3");
            return (Criteria) this;
        }

        public Criteria andRate3In(List<BigDecimal> values) {
            addCriterion("RATE3 in", values, "rate3");
            return (Criteria) this;
        }

        public Criteria andRate3NotIn(List<BigDecimal> values) {
            addCriterion("RATE3 not in", values, "rate3");
            return (Criteria) this;
        }

        public Criteria andRate3Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE3 between", value1, value2, "rate3");
            return (Criteria) this;
        }

        public Criteria andRate3NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE3 not between", value1, value2, "rate3");
            return (Criteria) this;
        }

        public Criteria andRate4IsNull() {
            addCriterion("RATE4 is null");
            return (Criteria) this;
        }

        public Criteria andRate4IsNotNull() {
            addCriterion("RATE4 is not null");
            return (Criteria) this;
        }

        public Criteria andRate4EqualTo(BigDecimal value) {
            addCriterion("RATE4 =", value, "rate4");
            return (Criteria) this;
        }

        public Criteria andRate4NotEqualTo(BigDecimal value) {
            addCriterion("RATE4 <>", value, "rate4");
            return (Criteria) this;
        }

        public Criteria andRate4GreaterThan(BigDecimal value) {
            addCriterion("RATE4 >", value, "rate4");
            return (Criteria) this;
        }

        public Criteria andRate4GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE4 >=", value, "rate4");
            return (Criteria) this;
        }

        public Criteria andRate4LessThan(BigDecimal value) {
            addCriterion("RATE4 <", value, "rate4");
            return (Criteria) this;
        }

        public Criteria andRate4LessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE4 <=", value, "rate4");
            return (Criteria) this;
        }

        public Criteria andRate4In(List<BigDecimal> values) {
            addCriterion("RATE4 in", values, "rate4");
            return (Criteria) this;
        }

        public Criteria andRate4NotIn(List<BigDecimal> values) {
            addCriterion("RATE4 not in", values, "rate4");
            return (Criteria) this;
        }

        public Criteria andRate4Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE4 between", value1, value2, "rate4");
            return (Criteria) this;
        }

        public Criteria andRate4NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE4 not between", value1, value2, "rate4");
            return (Criteria) this;
        }

        public Criteria andRate5IsNull() {
            addCriterion("RATE5 is null");
            return (Criteria) this;
        }

        public Criteria andRate5IsNotNull() {
            addCriterion("RATE5 is not null");
            return (Criteria) this;
        }

        public Criteria andRate5EqualTo(BigDecimal value) {
            addCriterion("RATE5 =", value, "rate5");
            return (Criteria) this;
        }

        public Criteria andRate5NotEqualTo(BigDecimal value) {
            addCriterion("RATE5 <>", value, "rate5");
            return (Criteria) this;
        }

        public Criteria andRate5GreaterThan(BigDecimal value) {
            addCriterion("RATE5 >", value, "rate5");
            return (Criteria) this;
        }

        public Criteria andRate5GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE5 >=", value, "rate5");
            return (Criteria) this;
        }

        public Criteria andRate5LessThan(BigDecimal value) {
            addCriterion("RATE5 <", value, "rate5");
            return (Criteria) this;
        }

        public Criteria andRate5LessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE5 <=", value, "rate5");
            return (Criteria) this;
        }

        public Criteria andRate5In(List<BigDecimal> values) {
            addCriterion("RATE5 in", values, "rate5");
            return (Criteria) this;
        }

        public Criteria andRate5NotIn(List<BigDecimal> values) {
            addCriterion("RATE5 not in", values, "rate5");
            return (Criteria) this;
        }

        public Criteria andRate5Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE5 between", value1, value2, "rate5");
            return (Criteria) this;
        }

        public Criteria andRate5NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE5 not between", value1, value2, "rate5");
            return (Criteria) this;
        }

        public Criteria andRate6IsNull() {
            addCriterion("RATE6 is null");
            return (Criteria) this;
        }

        public Criteria andRate6IsNotNull() {
            addCriterion("RATE6 is not null");
            return (Criteria) this;
        }

        public Criteria andRate6EqualTo(BigDecimal value) {
            addCriterion("RATE6 =", value, "rate6");
            return (Criteria) this;
        }

        public Criteria andRate6NotEqualTo(BigDecimal value) {
            addCriterion("RATE6 <>", value, "rate6");
            return (Criteria) this;
        }

        public Criteria andRate6GreaterThan(BigDecimal value) {
            addCriterion("RATE6 >", value, "rate6");
            return (Criteria) this;
        }

        public Criteria andRate6GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE6 >=", value, "rate6");
            return (Criteria) this;
        }

        public Criteria andRate6LessThan(BigDecimal value) {
            addCriterion("RATE6 <", value, "rate6");
            return (Criteria) this;
        }

        public Criteria andRate6LessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE6 <=", value, "rate6");
            return (Criteria) this;
        }

        public Criteria andRate6In(List<BigDecimal> values) {
            addCriterion("RATE6 in", values, "rate6");
            return (Criteria) this;
        }

        public Criteria andRate6NotIn(List<BigDecimal> values) {
            addCriterion("RATE6 not in", values, "rate6");
            return (Criteria) this;
        }

        public Criteria andRate6Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE6 between", value1, value2, "rate6");
            return (Criteria) this;
        }

        public Criteria andRate6NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE6 not between", value1, value2, "rate6");
            return (Criteria) this;
        }

        public Criteria andRate7IsNull() {
            addCriterion("RATE7 is null");
            return (Criteria) this;
        }

        public Criteria andRate7IsNotNull() {
            addCriterion("RATE7 is not null");
            return (Criteria) this;
        }

        public Criteria andRate7EqualTo(BigDecimal value) {
            addCriterion("RATE7 =", value, "rate7");
            return (Criteria) this;
        }

        public Criteria andRate7NotEqualTo(BigDecimal value) {
            addCriterion("RATE7 <>", value, "rate7");
            return (Criteria) this;
        }

        public Criteria andRate7GreaterThan(BigDecimal value) {
            addCriterion("RATE7 >", value, "rate7");
            return (Criteria) this;
        }

        public Criteria andRate7GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE7 >=", value, "rate7");
            return (Criteria) this;
        }

        public Criteria andRate7LessThan(BigDecimal value) {
            addCriterion("RATE7 <", value, "rate7");
            return (Criteria) this;
        }

        public Criteria andRate7LessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE7 <=", value, "rate7");
            return (Criteria) this;
        }

        public Criteria andRate7In(List<BigDecimal> values) {
            addCriterion("RATE7 in", values, "rate7");
            return (Criteria) this;
        }

        public Criteria andRate7NotIn(List<BigDecimal> values) {
            addCriterion("RATE7 not in", values, "rate7");
            return (Criteria) this;
        }

        public Criteria andRate7Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE7 between", value1, value2, "rate7");
            return (Criteria) this;
        }

        public Criteria andRate7NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE7 not between", value1, value2, "rate7");
            return (Criteria) this;
        }

        public Criteria andRate8IsNull() {
            addCriterion("RATE8 is null");
            return (Criteria) this;
        }

        public Criteria andRate8IsNotNull() {
            addCriterion("RATE8 is not null");
            return (Criteria) this;
        }

        public Criteria andRate8EqualTo(BigDecimal value) {
            addCriterion("RATE8 =", value, "rate8");
            return (Criteria) this;
        }

        public Criteria andRate8NotEqualTo(BigDecimal value) {
            addCriterion("RATE8 <>", value, "rate8");
            return (Criteria) this;
        }

        public Criteria andRate8GreaterThan(BigDecimal value) {
            addCriterion("RATE8 >", value, "rate8");
            return (Criteria) this;
        }

        public Criteria andRate8GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE8 >=", value, "rate8");
            return (Criteria) this;
        }

        public Criteria andRate8LessThan(BigDecimal value) {
            addCriterion("RATE8 <", value, "rate8");
            return (Criteria) this;
        }

        public Criteria andRate8LessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE8 <=", value, "rate8");
            return (Criteria) this;
        }

        public Criteria andRate8In(List<BigDecimal> values) {
            addCriterion("RATE8 in", values, "rate8");
            return (Criteria) this;
        }

        public Criteria andRate8NotIn(List<BigDecimal> values) {
            addCriterion("RATE8 not in", values, "rate8");
            return (Criteria) this;
        }

        public Criteria andRate8Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE8 between", value1, value2, "rate8");
            return (Criteria) this;
        }

        public Criteria andRate8NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE8 not between", value1, value2, "rate8");
            return (Criteria) this;
        }

        public Criteria andRate9IsNull() {
            addCriterion("RATE9 is null");
            return (Criteria) this;
        }

        public Criteria andRate9IsNotNull() {
            addCriterion("RATE9 is not null");
            return (Criteria) this;
        }

        public Criteria andRate9EqualTo(BigDecimal value) {
            addCriterion("RATE9 =", value, "rate9");
            return (Criteria) this;
        }

        public Criteria andRate9NotEqualTo(BigDecimal value) {
            addCriterion("RATE9 <>", value, "rate9");
            return (Criteria) this;
        }

        public Criteria andRate9GreaterThan(BigDecimal value) {
            addCriterion("RATE9 >", value, "rate9");
            return (Criteria) this;
        }

        public Criteria andRate9GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE9 >=", value, "rate9");
            return (Criteria) this;
        }

        public Criteria andRate9LessThan(BigDecimal value) {
            addCriterion("RATE9 <", value, "rate9");
            return (Criteria) this;
        }

        public Criteria andRate9LessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE9 <=", value, "rate9");
            return (Criteria) this;
        }

        public Criteria andRate9In(List<BigDecimal> values) {
            addCriterion("RATE9 in", values, "rate9");
            return (Criteria) this;
        }

        public Criteria andRate9NotIn(List<BigDecimal> values) {
            addCriterion("RATE9 not in", values, "rate9");
            return (Criteria) this;
        }

        public Criteria andRate9Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE9 between", value1, value2, "rate9");
            return (Criteria) this;
        }

        public Criteria andRate9NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE9 not between", value1, value2, "rate9");
            return (Criteria) this;
        }

        public Criteria andRate10IsNull() {
            addCriterion("RATE10 is null");
            return (Criteria) this;
        }

        public Criteria andRate10IsNotNull() {
            addCriterion("RATE10 is not null");
            return (Criteria) this;
        }

        public Criteria andRate10EqualTo(BigDecimal value) {
            addCriterion("RATE10 =", value, "rate10");
            return (Criteria) this;
        }

        public Criteria andRate10NotEqualTo(BigDecimal value) {
            addCriterion("RATE10 <>", value, "rate10");
            return (Criteria) this;
        }

        public Criteria andRate10GreaterThan(BigDecimal value) {
            addCriterion("RATE10 >", value, "rate10");
            return (Criteria) this;
        }

        public Criteria andRate10GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE10 >=", value, "rate10");
            return (Criteria) this;
        }

        public Criteria andRate10LessThan(BigDecimal value) {
            addCriterion("RATE10 <", value, "rate10");
            return (Criteria) this;
        }

        public Criteria andRate10LessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE10 <=", value, "rate10");
            return (Criteria) this;
        }

        public Criteria andRate10In(List<BigDecimal> values) {
            addCriterion("RATE10 in", values, "rate10");
            return (Criteria) this;
        }

        public Criteria andRate10NotIn(List<BigDecimal> values) {
            addCriterion("RATE10 not in", values, "rate10");
            return (Criteria) this;
        }

        public Criteria andRate10Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE10 between", value1, value2, "rate10");
            return (Criteria) this;
        }

        public Criteria andRate10NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE10 not between", value1, value2, "rate10");
            return (Criteria) this;
        }

        public Criteria andRate11IsNull() {
            addCriterion("RATE11 is null");
            return (Criteria) this;
        }

        public Criteria andRate11IsNotNull() {
            addCriterion("RATE11 is not null");
            return (Criteria) this;
        }

        public Criteria andRate11EqualTo(BigDecimal value) {
            addCriterion("RATE11 =", value, "rate11");
            return (Criteria) this;
        }

        public Criteria andRate11NotEqualTo(BigDecimal value) {
            addCriterion("RATE11 <>", value, "rate11");
            return (Criteria) this;
        }

        public Criteria andRate11GreaterThan(BigDecimal value) {
            addCriterion("RATE11 >", value, "rate11");
            return (Criteria) this;
        }

        public Criteria andRate11GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE11 >=", value, "rate11");
            return (Criteria) this;
        }

        public Criteria andRate11LessThan(BigDecimal value) {
            addCriterion("RATE11 <", value, "rate11");
            return (Criteria) this;
        }

        public Criteria andRate11LessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE11 <=", value, "rate11");
            return (Criteria) this;
        }

        public Criteria andRate11In(List<BigDecimal> values) {
            addCriterion("RATE11 in", values, "rate11");
            return (Criteria) this;
        }

        public Criteria andRate11NotIn(List<BigDecimal> values) {
            addCriterion("RATE11 not in", values, "rate11");
            return (Criteria) this;
        }

        public Criteria andRate11Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE11 between", value1, value2, "rate11");
            return (Criteria) this;
        }

        public Criteria andRate11NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE11 not between", value1, value2, "rate11");
            return (Criteria) this;
        }

        public Criteria andRate12IsNull() {
            addCriterion("RATE12 is null");
            return (Criteria) this;
        }

        public Criteria andRate12IsNotNull() {
            addCriterion("RATE12 is not null");
            return (Criteria) this;
        }

        public Criteria andRate12EqualTo(BigDecimal value) {
            addCriterion("RATE12 =", value, "rate12");
            return (Criteria) this;
        }

        public Criteria andRate12NotEqualTo(BigDecimal value) {
            addCriterion("RATE12 <>", value, "rate12");
            return (Criteria) this;
        }

        public Criteria andRate12GreaterThan(BigDecimal value) {
            addCriterion("RATE12 >", value, "rate12");
            return (Criteria) this;
        }

        public Criteria andRate12GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE12 >=", value, "rate12");
            return (Criteria) this;
        }

        public Criteria andRate12LessThan(BigDecimal value) {
            addCriterion("RATE12 <", value, "rate12");
            return (Criteria) this;
        }

        public Criteria andRate12LessThanOrEqualTo(BigDecimal value) {
            addCriterion("RATE12 <=", value, "rate12");
            return (Criteria) this;
        }

        public Criteria andRate12In(List<BigDecimal> values) {
            addCriterion("RATE12 in", values, "rate12");
            return (Criteria) this;
        }

        public Criteria andRate12NotIn(List<BigDecimal> values) {
            addCriterion("RATE12 not in", values, "rate12");
            return (Criteria) this;
        }

        public Criteria andRate12Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE12 between", value1, value2, "rate12");
            return (Criteria) this;
        }

        public Criteria andRate12NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RATE12 not between", value1, value2, "rate12");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNull() {
            addCriterion("VALIDSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNotNull() {
            addCriterion("VALIDSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidstatusEqualTo(String value) {
            addCriterion("VALIDSTATUS =", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotEqualTo(String value) {
            addCriterion("VALIDSTATUS <>", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThan(String value) {
            addCriterion("VALIDSTATUS >", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS >=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThan(String value) {
            addCriterion("VALIDSTATUS <", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS <=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLike(String value) {
            addCriterion("VALIDSTATUS like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotLike(String value) {
            addCriterion("VALIDSTATUS not like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusIn(List<String> values) {
            addCriterion("VALIDSTATUS in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotIn(List<String> values) {
            addCriterion("VALIDSTATUS not in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS not between", value1, value2, "validstatus");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}