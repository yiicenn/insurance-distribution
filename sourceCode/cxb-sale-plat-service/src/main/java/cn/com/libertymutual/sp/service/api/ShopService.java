package cn.com.libertymutual.sp.service.api;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;

import cn.com.libertymutual.core.base.dto.PrpLmAgent;
import cn.com.libertymutual.core.base.dto.TQueryLmAgentResponseDto;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpStoreProduct;
import cn.com.libertymutual.sp.bean.TbSpUser;
import cn.com.libertymutual.sp.bean.TbSysHotarea;

public interface ShopService {
	TbSpUser mergeAccount(String userCode);

	ServiceResult setStateType(String userCode, String stateType);

	ServiceResult productManage(String userCode, String branchCode, String riskCode);

	Double getRate(Double rate, String userCode, String proId, boolean getRate);

	ServiceResult saveStoreProductList(List<TbSpStoreProduct> storeList);

	ServiceResult shopProductFind(String userCode, String type, Integer pageNumber, Integer pageSize);

	/**Remarks: 申请业务关系代码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月13日上午10:32:06<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr
	 * @param userCode 用户编码，非空
	 * @param applyType 申请类型
	 * @param areaId 地区ID
	 * @param areaCode 地区编码
	 * @return
	 */
	public ServiceResult updateAgreementNoByApply(HttpServletRequest request, ServiceResult sr, String userCode, String applyType, Integer areaId,
			String areaCode) throws IOException, ParseException, MessagingException;

	/**Remarks: 查询是否有专属佣金配置<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年2月6日上午11:38:42<br>
	 * Project：liberty_sale_plat<br>
	 * @param sr
	 * @param userCode
	 * @return
	 * @throws Exception
	 */
	public ServiceResult findEYongjCountByUserCode(ServiceResult sr, String userCode) throws Exception;

	/**Remarks: 查询业务关系代码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年10月17日下午4:13:40<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr
	 * @param agreementNo
	 * @return
	 * @throws Exception
	 */
	public ServiceResult findByAgreementNo(HttpServletRequest request, ServiceResult sr, String agreementNo) throws Exception;

	/**Remarks: 查询所有常驻地区<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月11日下午12:03:21<br>
	 * Project：liberty_sale_plat<br>
	 * @return
	 */
	public ServiceResult findAllHotArea();

	/**Remarks: 无业务关系代码人员--开店，保存地区信息，更新账户类型<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月12日下午1:58:10<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr
	 * @param userCode 用户编码，非空
	 * @param agreementNo 业务关系代码，非空
	 * @param areaCode 常驻地区编码，非空
	 * @param saleCode 销售人员编码
	 * @param saleName 销售人员名称
	 * @return
	 */
	public ServiceResult updateStoreStateByArea(HttpServletRequest request, ServiceResult sr, String userCode, String agreementNo, String areaCode,
			String saleCode, String saleName) throws Exception;

	/**Remarks: 有业务关系代码人员--开店，根据业务关系代码更新业务关系代码,销售人员，销售渠道,所属机构编码等信息，更新账户类型<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月12日下午1:58:24<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @param sr
	 * @param userCode 用户编码，非空
	 * @param agreementNo 业务关系代码，非空
	 * @param saleCode 销售人员编码
	 * @param saleName 销售人员名称
	 * @return
	 */
	public ServiceResult updateStoreStateByAgreement(HttpServletRequest request, ServiceResult sr, String userCode, String agreementNo,
			String saleCode, String saleName) throws Exception;

	/**
	 * 根据业务关系代码设置用户相关信息<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月27日上午12:01:20<br>
	 * @param request
	 * @param sr
	 * @param type 业务关系代码保存模式，录入或常住地选择
	 * @param prpLmAgent 录入业务关系代码的对象
	 * @param user 需要更新的用户
	 * @param agreementNo 录入的业务关系代码
	 * @param saleCode 销售人员编码
	 * @param saleName 销售人员名称
	 * @param hotarea 常住地信息
	 * @return 已设置好用户需要保存的业务关系代码相关字段后的对象
	 * @throws Exception
	 */
	public ServiceResult checkAgreementNo(HttpServletRequest request, ServiceResult sr, String type, PrpLmAgent prpLmAgent, TbSpUser user,
			String agreementNo, String saleCode, String saleName, TbSysHotarea hotarea) throws Exception;

	/**Remarks: 删除某用户的专属佣金配置<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年2月6日下午2:31:36<br>
	 * Project：liberty_sale_plat<br>
	 * @param sr
	 * @param userCode
	 * @return
	 * @throws Exception
	 */
	public ServiceResult deleteStoreConfigByUserCode(ServiceResult sr, String userCode) throws Exception;

	public ServiceResult findShopInfo(String userCode, HttpServletRequest request, String type);

	/**
	 * 保存分享信息<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年3月30日下午5:50:20<br>
	 * @param userCode
	 * @param type
	 * @param sub
	 * @return
	 */
	void saveShare(String userCode, String type, String sub);

	ServiceResult shopRate(String userCode, String branchCode);

	TQueryLmAgentResponseDto findAgreementNo(String agreementNo);

	Boolean isSetRate(String userCode);
}
