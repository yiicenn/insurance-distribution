package cn.com.libertymutual.sp.dto.savePlan;

import java.util.List;

import cn.com.libertymutual.sp.bean.TbSpUser;
import cn.com.libertymutual.sp.bean.TbSysHotarea;
import cn.com.libertymutual.sp.dto.InsureQuery;
import cn.com.libertymutual.sp.dto.PlanDto;
import cn.com.libertymutual.sp.dto.axtx.GuaranteeInfo;
import cn.com.libertymutual.sp.dto.car.dto.TMotorPremiumRequestDTO;
import cn.com.libertymutual.sp.dto.car.dto.TProposalSaveRequestDto;
import cn.com.libertymutual.sp.dto.queryplans.CrossSaleKind;

public class PropsalSaveRequestDto {
	private InsureQuery insureQuery;
	private ProposalSaveForRequestDto proposalSaveReq;
	private TbSysHotarea hotarea;
	private TbSpUser user; // 用户基本信息
	private String identCode;
	private PlanDto planDto;
	// 保障条框的基本信息
	private List<CrossSaleKind> kinds;
	private GuaranteeInfo axtx;
	private String log;
	private String comm1Rate;
	private TMotorPremiumRequestDTO tMotorPremiumRequestDTO;

	private TProposalSaveRequestDto tProposalSaveDTO;

	
	public synchronized PlanDto getPlanDto() {
		return planDto;
	}

	public synchronized void setPlanDto(PlanDto planDto) {
		this.planDto = planDto;
	}

	public InsureQuery getInsureQuery() {
		return insureQuery;
	}

	public void setInsureQuery(InsureQuery insureQuery) {
		this.insureQuery = insureQuery;
	}

	public ProposalSaveForRequestDto getProposalSaveReq() {
		return proposalSaveReq;
	}

	public void setProposalSaveReq(ProposalSaveForRequestDto proposalSaveReq) {
		this.proposalSaveReq = proposalSaveReq;
	}

	public TbSysHotarea getHotarea() {
		return hotarea;
	}

	public void setHotarea(TbSysHotarea hotarea) {
		this.hotarea = hotarea;
	}

	public TbSpUser getUser() {
		return user;
	}

	public void setUser(TbSpUser user) {
		this.user = user;
	}

	public String getIdentCode() {
		return identCode;
	}

	public void setIdentCode(String identCode) {
		this.identCode = identCode;
	}

	public List<CrossSaleKind> getKinds() {
		return kinds;
	}

	public void setKinds(List<CrossSaleKind> kinds) {
		this.kinds = kinds;
	}

	public GuaranteeInfo getAxtx() {
		return axtx;
	}

	public void setAxtx(GuaranteeInfo axtx) {
		this.axtx = axtx;
	}

	public String getLog() {
		return log;
	}

	public void setLog(String log) {
		this.log = log;
	}

	public String getComm1Rate() {
		return comm1Rate;
	}

	public void setComm1Rate(String comm1Rate) {
		this.comm1Rate = comm1Rate;
	}

	public TMotorPremiumRequestDTO gettMotorPremiumRequestDTO() {
		return tMotorPremiumRequestDTO;
	}

	public void settMotorPremiumRequestDTO(TMotorPremiumRequestDTO tMotorPremiumRequestDTO) {
		this.tMotorPremiumRequestDTO = tMotorPremiumRequestDTO;
	}

	public TProposalSaveRequestDto gettProposalSaveDTO() {
		return tProposalSaveDTO;
	}

	public void settProposalSaveDTO(TProposalSaveRequestDto tProposalSaveDTO) {
		this.tProposalSaveDTO = tProposalSaveDTO;
	}

}
