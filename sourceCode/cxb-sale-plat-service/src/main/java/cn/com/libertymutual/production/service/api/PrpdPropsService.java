package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdprops;
import cn.com.libertymutual.production.pojo.request.PrpdPropsRequest;

public interface PrpdPropsService {
	
	/**
	 * 查询因子
	 * @param request
	 * @return
	 */
	public List<Prpdprops> select(PrpdPropsRequest request);
	
}
