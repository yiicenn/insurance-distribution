package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrpdclauseExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpdclauseExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andClausecodeIsNull() {
            addCriterion("CLAUSECODE is null");
            return (Criteria) this;
        }

        public Criteria andClausecodeIsNotNull() {
            addCriterion("CLAUSECODE is not null");
            return (Criteria) this;
        }

        public Criteria andClausecodeEqualTo(String value) {
            addCriterion("CLAUSECODE =", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeNotEqualTo(String value) {
            addCriterion("CLAUSECODE <>", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeGreaterThan(String value) {
            addCriterion("CLAUSECODE >", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeGreaterThanOrEqualTo(String value) {
            addCriterion("CLAUSECODE >=", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeLessThan(String value) {
            addCriterion("CLAUSECODE <", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeLessThanOrEqualTo(String value) {
            addCriterion("CLAUSECODE <=", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeLike(String value) {
            addCriterion("CLAUSECODE like", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeNotLike(String value) {
            addCriterion("CLAUSECODE not like", value, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeIn(List<String> values) {
            addCriterion("CLAUSECODE in", values, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeNotIn(List<String> values) {
            addCriterion("CLAUSECODE not in", values, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeBetween(String value1, String value2) {
            addCriterion("CLAUSECODE between", value1, value2, "clausecode");
            return (Criteria) this;
        }

        public Criteria andClausecodeNotBetween(String value1, String value2) {
            addCriterion("CLAUSECODE not between", value1, value2, "clausecode");
            return (Criteria) this;
        }

        public Criteria andLinenoIsNull() {
            addCriterion("LINENO is null");
            return (Criteria) this;
        }

        public Criteria andLinenoIsNotNull() {
            addCriterion("LINENO is not null");
            return (Criteria) this;
        }

        public Criteria andLinenoEqualTo(String value) {
            addCriterion("LINENO =", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoNotEqualTo(String value) {
            addCriterion("LINENO <>", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoGreaterThan(String value) {
            addCriterion("LINENO >", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoGreaterThanOrEqualTo(String value) {
            addCriterion("LINENO >=", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoLessThan(String value) {
            addCriterion("LINENO <", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoLessThanOrEqualTo(String value) {
            addCriterion("LINENO <=", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoLike(String value) {
            addCriterion("LINENO like", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoNotLike(String value) {
            addCriterion("LINENO not like", value, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoIn(List<String> values) {
            addCriterion("LINENO in", values, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoNotIn(List<String> values) {
            addCriterion("LINENO not in", values, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoBetween(String value1, String value2) {
            addCriterion("LINENO between", value1, value2, "lineno");
            return (Criteria) this;
        }

        public Criteria andLinenoNotBetween(String value1, String value2) {
            addCriterion("LINENO not between", value1, value2, "lineno");
            return (Criteria) this;
        }

        public Criteria andClausenameIsNull() {
            addCriterion("CLAUSENAME is null");
            return (Criteria) this;
        }

        public Criteria andClausenameIsNotNull() {
            addCriterion("CLAUSENAME is not null");
            return (Criteria) this;
        }

        public Criteria andClausenameEqualTo(String value) {
            addCriterion("CLAUSENAME =", value, "clausename");
            return (Criteria) this;
        }

        public Criteria andClausenameNotEqualTo(String value) {
            addCriterion("CLAUSENAME <>", value, "clausename");
            return (Criteria) this;
        }

        public Criteria andClausenameGreaterThan(String value) {
            addCriterion("CLAUSENAME >", value, "clausename");
            return (Criteria) this;
        }

        public Criteria andClausenameGreaterThanOrEqualTo(String value) {
            addCriterion("CLAUSENAME >=", value, "clausename");
            return (Criteria) this;
        }

        public Criteria andClausenameLessThan(String value) {
            addCriterion("CLAUSENAME <", value, "clausename");
            return (Criteria) this;
        }

        public Criteria andClausenameLessThanOrEqualTo(String value) {
            addCriterion("CLAUSENAME <=", value, "clausename");
            return (Criteria) this;
        }

        public Criteria andClausenameLike(String value) {
            addCriterion("CLAUSENAME like", value, "clausename");
            return (Criteria) this;
        }

        public Criteria andClausenameNotLike(String value) {
            addCriterion("CLAUSENAME not like", value, "clausename");
            return (Criteria) this;
        }

        public Criteria andClausenameIn(List<String> values) {
            addCriterion("CLAUSENAME in", values, "clausename");
            return (Criteria) this;
        }

        public Criteria andClausenameNotIn(List<String> values) {
            addCriterion("CLAUSENAME not in", values, "clausename");
            return (Criteria) this;
        }

        public Criteria andClausenameBetween(String value1, String value2) {
            addCriterion("CLAUSENAME between", value1, value2, "clausename");
            return (Criteria) this;
        }

        public Criteria andClausenameNotBetween(String value1, String value2) {
            addCriterion("CLAUSENAME not between", value1, value2, "clausename");
            return (Criteria) this;
        }

        public Criteria andLanguageIsNull() {
            addCriterion("LANGUAGE is null");
            return (Criteria) this;
        }

        public Criteria andLanguageIsNotNull() {
            addCriterion("LANGUAGE is not null");
            return (Criteria) this;
        }

        public Criteria andLanguageEqualTo(String value) {
            addCriterion("LANGUAGE =", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageNotEqualTo(String value) {
            addCriterion("LANGUAGE <>", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageGreaterThan(String value) {
            addCriterion("LANGUAGE >", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageGreaterThanOrEqualTo(String value) {
            addCriterion("LANGUAGE >=", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageLessThan(String value) {
            addCriterion("LANGUAGE <", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageLessThanOrEqualTo(String value) {
            addCriterion("LANGUAGE <=", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageLike(String value) {
            addCriterion("LANGUAGE like", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageNotLike(String value) {
            addCriterion("LANGUAGE not like", value, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageIn(List<String> values) {
            addCriterion("LANGUAGE in", values, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageNotIn(List<String> values) {
            addCriterion("LANGUAGE not in", values, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageBetween(String value1, String value2) {
            addCriterion("LANGUAGE between", value1, value2, "language");
            return (Criteria) this;
        }

        public Criteria andLanguageNotBetween(String value1, String value2) {
            addCriterion("LANGUAGE not between", value1, value2, "language");
            return (Criteria) this;
        }

        public Criteria andTitleflagIsNull() {
            addCriterion("TITLEFLAG is null");
            return (Criteria) this;
        }

        public Criteria andTitleflagIsNotNull() {
            addCriterion("TITLEFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andTitleflagEqualTo(String value) {
            addCriterion("TITLEFLAG =", value, "titleflag");
            return (Criteria) this;
        }

        public Criteria andTitleflagNotEqualTo(String value) {
            addCriterion("TITLEFLAG <>", value, "titleflag");
            return (Criteria) this;
        }

        public Criteria andTitleflagGreaterThan(String value) {
            addCriterion("TITLEFLAG >", value, "titleflag");
            return (Criteria) this;
        }

        public Criteria andTitleflagGreaterThanOrEqualTo(String value) {
            addCriterion("TITLEFLAG >=", value, "titleflag");
            return (Criteria) this;
        }

        public Criteria andTitleflagLessThan(String value) {
            addCriterion("TITLEFLAG <", value, "titleflag");
            return (Criteria) this;
        }

        public Criteria andTitleflagLessThanOrEqualTo(String value) {
            addCriterion("TITLEFLAG <=", value, "titleflag");
            return (Criteria) this;
        }

        public Criteria andTitleflagLike(String value) {
            addCriterion("TITLEFLAG like", value, "titleflag");
            return (Criteria) this;
        }

        public Criteria andTitleflagNotLike(String value) {
            addCriterion("TITLEFLAG not like", value, "titleflag");
            return (Criteria) this;
        }

        public Criteria andTitleflagIn(List<String> values) {
            addCriterion("TITLEFLAG in", values, "titleflag");
            return (Criteria) this;
        }

        public Criteria andTitleflagNotIn(List<String> values) {
            addCriterion("TITLEFLAG not in", values, "titleflag");
            return (Criteria) this;
        }

        public Criteria andTitleflagBetween(String value1, String value2) {
            addCriterion("TITLEFLAG between", value1, value2, "titleflag");
            return (Criteria) this;
        }

        public Criteria andTitleflagNotBetween(String value1, String value2) {
            addCriterion("TITLEFLAG not between", value1, value2, "titleflag");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeIsNull() {
            addCriterion("NEWCLAUSECODE is null");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeIsNotNull() {
            addCriterion("NEWCLAUSECODE is not null");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeEqualTo(String value) {
            addCriterion("NEWCLAUSECODE =", value, "newclausecode");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeNotEqualTo(String value) {
            addCriterion("NEWCLAUSECODE <>", value, "newclausecode");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeGreaterThan(String value) {
            addCriterion("NEWCLAUSECODE >", value, "newclausecode");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeGreaterThanOrEqualTo(String value) {
            addCriterion("NEWCLAUSECODE >=", value, "newclausecode");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeLessThan(String value) {
            addCriterion("NEWCLAUSECODE <", value, "newclausecode");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeLessThanOrEqualTo(String value) {
            addCriterion("NEWCLAUSECODE <=", value, "newclausecode");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeLike(String value) {
            addCriterion("NEWCLAUSECODE like", value, "newclausecode");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeNotLike(String value) {
            addCriterion("NEWCLAUSECODE not like", value, "newclausecode");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeIn(List<String> values) {
            addCriterion("NEWCLAUSECODE in", values, "newclausecode");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeNotIn(List<String> values) {
            addCriterion("NEWCLAUSECODE not in", values, "newclausecode");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeBetween(String value1, String value2) {
            addCriterion("NEWCLAUSECODE between", value1, value2, "newclausecode");
            return (Criteria) this;
        }

        public Criteria andNewclausecodeNotBetween(String value1, String value2) {
            addCriterion("NEWCLAUSECODE not between", value1, value2, "newclausecode");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNull() {
            addCriterion("VALIDSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidstatusIsNotNull() {
            addCriterion("VALIDSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidstatusEqualTo(String value) {
            addCriterion("VALIDSTATUS =", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotEqualTo(String value) {
            addCriterion("VALIDSTATUS <>", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThan(String value) {
            addCriterion("VALIDSTATUS >", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS >=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThan(String value) {
            addCriterion("VALIDSTATUS <", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLessThanOrEqualTo(String value) {
            addCriterion("VALIDSTATUS <=", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusLike(String value) {
            addCriterion("VALIDSTATUS like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotLike(String value) {
            addCriterion("VALIDSTATUS not like", value, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusIn(List<String> values) {
            addCriterion("VALIDSTATUS in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotIn(List<String> values) {
            addCriterion("VALIDSTATUS not in", values, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andValidstatusNotBetween(String value1, String value2) {
            addCriterion("VALIDSTATUS not between", value1, value2, "validstatus");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andContflagIsNull() {
            addCriterion("CONTFLAG is null");
            return (Criteria) this;
        }

        public Criteria andContflagIsNotNull() {
            addCriterion("CONTFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andContflagEqualTo(String value) {
            addCriterion("CONTFLAG =", value, "contflag");
            return (Criteria) this;
        }

        public Criteria andContflagNotEqualTo(String value) {
            addCriterion("CONTFLAG <>", value, "contflag");
            return (Criteria) this;
        }

        public Criteria andContflagGreaterThan(String value) {
            addCriterion("CONTFLAG >", value, "contflag");
            return (Criteria) this;
        }

        public Criteria andContflagGreaterThanOrEqualTo(String value) {
            addCriterion("CONTFLAG >=", value, "contflag");
            return (Criteria) this;
        }

        public Criteria andContflagLessThan(String value) {
            addCriterion("CONTFLAG <", value, "contflag");
            return (Criteria) this;
        }

        public Criteria andContflagLessThanOrEqualTo(String value) {
            addCriterion("CONTFLAG <=", value, "contflag");
            return (Criteria) this;
        }

        public Criteria andContflagLike(String value) {
            addCriterion("CONTFLAG like", value, "contflag");
            return (Criteria) this;
        }

        public Criteria andContflagNotLike(String value) {
            addCriterion("CONTFLAG not like", value, "contflag");
            return (Criteria) this;
        }

        public Criteria andContflagIn(List<String> values) {
            addCriterion("CONTFLAG in", values, "contflag");
            return (Criteria) this;
        }

        public Criteria andContflagNotIn(List<String> values) {
            addCriterion("CONTFLAG not in", values, "contflag");
            return (Criteria) this;
        }

        public Criteria andContflagBetween(String value1, String value2) {
            addCriterion("CONTFLAG between", value1, value2, "contflag");
            return (Criteria) this;
        }

        public Criteria andContflagNotBetween(String value1, String value2) {
            addCriterion("CONTFLAG not between", value1, value2, "contflag");
            return (Criteria) this;
        }

        public Criteria andDelflagIsNull() {
            addCriterion("DELFLAG is null");
            return (Criteria) this;
        }

        public Criteria andDelflagIsNotNull() {
            addCriterion("DELFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andDelflagEqualTo(String value) {
            addCriterion("DELFLAG =", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagNotEqualTo(String value) {
            addCriterion("DELFLAG <>", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagGreaterThan(String value) {
            addCriterion("DELFLAG >", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagGreaterThanOrEqualTo(String value) {
            addCriterion("DELFLAG >=", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagLessThan(String value) {
            addCriterion("DELFLAG <", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagLessThanOrEqualTo(String value) {
            addCriterion("DELFLAG <=", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagLike(String value) {
            addCriterion("DELFLAG like", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagNotLike(String value) {
            addCriterion("DELFLAG not like", value, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagIn(List<String> values) {
            addCriterion("DELFLAG in", values, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagNotIn(List<String> values) {
            addCriterion("DELFLAG not in", values, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagBetween(String value1, String value2) {
            addCriterion("DELFLAG between", value1, value2, "delflag");
            return (Criteria) this;
        }

        public Criteria andDelflagNotBetween(String value1, String value2) {
            addCriterion("DELFLAG not between", value1, value2, "delflag");
            return (Criteria) this;
        }

        public Criteria andIniflagIsNull() {
            addCriterion("INIFLAG is null");
            return (Criteria) this;
        }

        public Criteria andIniflagIsNotNull() {
            addCriterion("INIFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andIniflagEqualTo(String value) {
            addCriterion("INIFLAG =", value, "iniflag");
            return (Criteria) this;
        }

        public Criteria andIniflagNotEqualTo(String value) {
            addCriterion("INIFLAG <>", value, "iniflag");
            return (Criteria) this;
        }

        public Criteria andIniflagGreaterThan(String value) {
            addCriterion("INIFLAG >", value, "iniflag");
            return (Criteria) this;
        }

        public Criteria andIniflagGreaterThanOrEqualTo(String value) {
            addCriterion("INIFLAG >=", value, "iniflag");
            return (Criteria) this;
        }

        public Criteria andIniflagLessThan(String value) {
            addCriterion("INIFLAG <", value, "iniflag");
            return (Criteria) this;
        }

        public Criteria andIniflagLessThanOrEqualTo(String value) {
            addCriterion("INIFLAG <=", value, "iniflag");
            return (Criteria) this;
        }

        public Criteria andIniflagLike(String value) {
            addCriterion("INIFLAG like", value, "iniflag");
            return (Criteria) this;
        }

        public Criteria andIniflagNotLike(String value) {
            addCriterion("INIFLAG not like", value, "iniflag");
            return (Criteria) this;
        }

        public Criteria andIniflagIn(List<String> values) {
            addCriterion("INIFLAG in", values, "iniflag");
            return (Criteria) this;
        }

        public Criteria andIniflagNotIn(List<String> values) {
            addCriterion("INIFLAG not in", values, "iniflag");
            return (Criteria) this;
        }

        public Criteria andIniflagBetween(String value1, String value2) {
            addCriterion("INIFLAG between", value1, value2, "iniflag");
            return (Criteria) this;
        }

        public Criteria andIniflagNotBetween(String value1, String value2) {
            addCriterion("INIFLAG not between", value1, value2, "iniflag");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNull() {
            addCriterion("CREATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andCreatedateIsNotNull() {
            addCriterion("CREATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andCreatedateEqualTo(Date value) {
            addCriterion("CREATEDATE =", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotEqualTo(Date value) {
            addCriterion("CREATEDATE <>", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThan(Date value) {
            addCriterion("CREATEDATE >", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE >=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThan(Date value) {
            addCriterion("CREATEDATE <", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateLessThanOrEqualTo(Date value) {
            addCriterion("CREATEDATE <=", value, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateIn(List<Date> values) {
            addCriterion("CREATEDATE in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotIn(List<Date> values) {
            addCriterion("CREATEDATE not in", values, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andCreatedateNotBetween(Date value1, Date value2) {
            addCriterion("CREATEDATE not between", value1, value2, "createdate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UPDATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UPDATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UPDATEDATE =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UPDATEDATE <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UPDATEDATE >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UPDATEDATE >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UPDATEDATE <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UPDATEDATE <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UPDATEDATE in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UPDATEDATE not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UPDATEDATE between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UPDATEDATE not between", value1, value2, "updatedate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}