package cn.com.libertymutual.sp.service.impl;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.sp.service.api.BaseService;

@Service("BaseService")
@RefreshScope // 刷新配置无需重启服务
public class BaseServiceImpl implements BaseService {

	private Logger log = LoggerFactory.getLogger(getClass());

	/**
	 * Remarks: 获取域名全路径<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年1月22日下午2:04:51<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@Override
	public String getServerNameFull(HttpServletRequest request) {
		// 当前域名根路径+文件夹路径
		String basePath = "https://" + request.getServerName() + "/sticcxb/";

		log.info("域名地址：{}", basePath);

		return basePath;
	}

	/**Remarks: 获取域名<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年1月22日下午2:05:19<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@Override
	public String getServerName(HttpServletRequest request) {
		// 当前域名根路径+文件夹路径
		String basePath = "https://" + request.getServerName() + "/";
		log.info("域名地址：{}", basePath);
		return basePath;
	}
}
