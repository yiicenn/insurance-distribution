package cn.com.libertymutual.sp.dto.queryplans;

import java.io.Serializable;
import java.util.List;

public class CrossSalePlan implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2944377213462047088L;

	private String proCode;//产品代码
	private String proName;//产品名称
	private String proEName;//产品英文名
	private int planId;//计划ID
	private String planCode;//计划编码
	private String planName;//计划名称
	private String planEName;//计划英文名
	private String planIcpcode;//
	private String planPrice;//计划保费
	private String sumAmount;//总保额
	private String insuredRelation;// 安行百万宝被保险人关系 " 1.同投保人 2.同被保险人 3.同车主 "
	private String agreementCode ;// 业务关系代码
	
	private String motorProposalNo;//车险投保单号
	private String motorPolicyNo;//车险保单号
	private String noMotorProposalNo;//非车投保单号
	private String noMotorPolicyNo;//非车保单号
	private int seatCount;//座位数 
	private String salerNumber;//销售人员  广东四川特有
	private String salerName;//销售人员名称
	
	// 2017 07 10 pengmin add
	private String rate;//费率
	// 2017 07 10 pengmin end
	
	private List<CrossSaleKind> kinds;
	
	
	private String minAge;
	private String maxAge;
	
	private String minDays;
	private String maxDays;
	public String getProCode() {
		return proCode;
	}
	public void setProCode(String proCode) {
		this.proCode = proCode;
	}
	public String getProName() {
		return proName;
	}
	public void setProName(String proName) {
		this.proName = proName;
	}
	public String getProEName() {
		return proEName;
	}
	public void setProEName(String proEName) {
		this.proEName = proEName;
	}
	public int getPlanId() {
		return planId;
	}
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	public String getPlanCode() {
		return planCode;
	}
	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPlanEName() {
		return planEName;
	}
	public void setPlanEName(String planEName) {
		this.planEName = planEName;
	}
	public String getPlanIcpcode() {
		return planIcpcode;
	}
	public void setPlanIcpcode(String planIcpcode) {
		this.planIcpcode = planIcpcode;
	}
	public String getPlanPrice() {
		return planPrice;
	}
	public void setPlanPrice(String planPrice) {
		this.planPrice = planPrice;
	}
	public List<CrossSaleKind> getKinds() {
		return kinds;
	}
	public void setKinds(List<CrossSaleKind> kinds) {
		this.kinds = kinds;
	}
	public String getInsuredRelation() {
		return insuredRelation;
	}
	public void setInsuredRelation(String insuredRelation) {
		this.insuredRelation = insuredRelation;
	}
	public String getAgreementCode() {
		return agreementCode;
	}
	public void setAgreementCode(String agreementCode) {
		this.agreementCode = agreementCode;
	}
	public String getMotorPolicyNo() {
		return motorPolicyNo;
	}
	public void setMotorPolicyNo(String motorPolicyNo) {
		this.motorPolicyNo = motorPolicyNo;
	}
	public String getNoMotorProposalNo() {
		return noMotorProposalNo;
	}
	public void setNoMotorProposalNo(String noMotorProposalNo) {
		this.noMotorProposalNo = noMotorProposalNo;
	}
	public String getNoMotorPolicyNo() {
		return noMotorPolicyNo;
	}
	public void setNoMotorPolicyNo(String noMotorPolicyNo) {
		this.noMotorPolicyNo = noMotorPolicyNo;
	}
	public int getSeatCount() {
		return seatCount;
	}
	public void setSeatCount(int seatCount) {
		this.seatCount = seatCount;
	}
	public String getSumAmount() {
		return sumAmount;
	}
	public void setSumAmount(String sumAmount) {
		this.sumAmount = sumAmount;
	}
	public String getSalerNumber() {
		return salerNumber;
	}
	public void setSalerNumber(String salerNumber) {
		this.salerNumber = salerNumber;
	}
	public String getMotorProposalNo() {
		return motorProposalNo;
	}
	public void setMotorProposalNo(String motorProposalNo) {
		this.motorProposalNo = motorProposalNo;
	}
	public String getSalerName() {
		return salerName;
	}
	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}
	public String getRate() {
		return rate;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public String getMinAge() {
		return minAge;
	}
	public void setMinAge(String minAge) {
		this.minAge = minAge;
	}
	public String getMaxAge() {
		return maxAge;
	}
	public void setMaxAge(String maxAge) {
		this.maxAge = maxAge;
	}
	public String getMinDays() {
		return minDays;
	}
	public void setMinDays(String minDays) {
		this.minDays = minDays;
	}
	public String getMaxDays() {
		return maxDays;
	}
	public void setMaxDays(String maxDays) {
		this.maxDays = maxDays;
	}
	
}


