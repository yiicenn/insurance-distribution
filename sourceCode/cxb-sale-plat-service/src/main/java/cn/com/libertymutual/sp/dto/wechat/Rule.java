package cn.com.libertymutual.sp.dto.wechat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * Entity - 规则
 * 
 * @author yeyc
 *
 */
@Entity
@Table(name = "t_rule")
@SequenceGenerator(name = "sequenceGenerator", sequenceName = "t_sequence")
public class Rule implements Serializable {

	private static final long serialVersionUID = -4644887854608704931L;
	private long id; 
	/** 规则名称 */
	private String name;
	
	/** 规则类型 (0-被添加自动回复，1-关键词自动回复)*/
	private Integer ruleType;
	
	/** 是否全部回复(0-否，1-是) */
	private Integer isReplyAll = 0;
	
	/** 公众号ID */
	private Long publicId;

	/** 关键字 */
	private List<RuleKeyword> keywords = new ArrayList<RuleKeyword>();
	
	/** 回复消息 */
	private List<RuleReplyMsg> replyMsgs = new ArrayList<RuleReplyMsg>();
	@Id
	 @Column(name = "ID")
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@NotNull
	@Column(nullable = false, length = 255)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@NotNull
	@Column(nullable = false)
	public Integer getRuleType() {
		return ruleType;
	}

	public void setRuleType(Integer ruleType) {
		this.ruleType = ruleType;
	}

	public Integer getIsReplyAll() {
		return isReplyAll;
	}

	public void setIsReplyAll(Integer isReplyAll) {
		this.isReplyAll = isReplyAll;
	}

	@NotNull
	@Column(nullable = false)
	public Long getPublicId() {
		return publicId;
	}

	public void setPublicId(Long publicId) {
		this.publicId = publicId;
	}

	@OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "rule", orphanRemoval = true)
	@Fetch(FetchMode.SUBSELECT)
	public List<RuleKeyword> getKeywords() {
		return keywords;
	}

	public void setKeywords(List<RuleKeyword> keywords) {
		this.keywords = keywords;
	}

	@OneToMany(fetch= FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "rule", orphanRemoval = true)
	@Fetch(FetchMode.SUBSELECT)
	public List<RuleReplyMsg> getReplyMsgs() {
		return replyMsgs;
	}

	public void setReplyMsgs(List<RuleReplyMsg> replyMsgs) {
		this.replyMsgs = replyMsgs;
	}
	
}
