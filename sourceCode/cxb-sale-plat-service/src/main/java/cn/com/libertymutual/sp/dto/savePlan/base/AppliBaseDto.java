package cn.com.libertymutual.sp.dto.savePlan.base;

/**
 * 销售平台基础投保单Dto
 */
public class AppliBaseDto {

	private String insuredType;//投保人类型
	private String insuredName;//投保人姓名
	private String identifyType;//投保人证件类型
	private String identifyNumber;//投保人证件号码
	private String linkMobile;//投保人手机
	private String sex;//性别
	private String birth;//出生日期
	private String email;//投保人邮箱地址
	private String payMth;//缴费方式
	
	public String getPayMth() {
		return payMth;
	}
	public void setPayMth(String payMth) {
		this.payMth = payMth;
	}
	public String getInsuredType() {
		return insuredType;
	}
	public void setInsuredType(String insuredType) {
		this.insuredType = insuredType;
	}
	public String getInsuredName() {
		return insuredName;
	}
	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}
	public String getIdentifyType() {
		return identifyType;
	}
	public void setIdentifyType(String identifyType) {
		this.identifyType = identifyType;
	}
	public String getIdentifyNumber() {
		return identifyNumber;
	}
	public void setIdentifyNumber(String identifyNumber) {
		this.identifyNumber = identifyNumber;
	}
	public String getLinkMobile() {
		return linkMobile;
	}
	public void setLinkMobile(String linkMobile) {
		this.linkMobile = linkMobile;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
}
