/*
 *	Copyright (c) liberty 2016
 *	cn.com.libertymutual.core.exception
 *  Create Date 2016年4月29日
 *	@author tracy.liao
 */
package cn.com.libertymutual.core.exception;

/**
 * 接口异常分类
 * @author tracy.liao
 * @date 2016年4月29日
 */
public enum InterfaceExceptionEnums implements ExceptionEnums {
	CONNECTION_EXCEPTION(4001,"连接异常"),
	CALL_INTERFACE_EXCEPTION(4002,"接口调用失败"),
	WEBSERVICE_TRANSPORT_EXCEPTION(4003,"接口异常"),
	PERON_NOT_FOUND_EXCEPTION(4004,"人员未找到"),
	RECALL_POLICY_FAIL_EXCEPTION(4005,"未查询到保单数据"),
	MANAGEMENT_QUERY_FAIL_EXCEPTION(4006,"签单查询数据超过1000条，无法正常显示，请添加查询条件"),
	CUSTOMER_QUERY_FAIL_EXCEPTION(4007,"未查询到客户信息"),
	CALL_PRINT_INTERFACE_EXCEPTION(4008,"未查询到打印数据"),
	MANAGEMENT_NOT_FIND_DATA_EXCEPTION(4009,"未取得单证信息"),
	VEHICLE_TYPE_QUERY_EXCEPTION(4010,"未查询到车型数据"),
	VIN_QUERY_EXCEPTION(4011,"未查询到VIN数据"),
	GET_CONNECTION_LOCAL_ERROR(4012,"未能获取到正确的接口地址"),
	GET_PAYMENT_QUERY_ERROR(4013,"获取支付状态失败"),
	GET_PAYMENT_ERROR(4014,"获取支付方式失败"),
	GET_PAYMENT_CODE_ERROR(4015,"支付失败"),
	QUERY_PAYMENT_ERROR(4017,"支付查询失败"),
	RENCALL_POLICY_ERROR(4016,"撤回保单失败");

	public int code;
	public String message;

	InterfaceExceptionEnums(int code, String message) {
		this.code = code;
		this.message = message;
	}

	@Override
	public int getCode() {
		// TODO Auto-generated method stub
		return code;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return message;
	}

}
