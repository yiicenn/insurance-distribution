package cn.com.libertymutual.core.base.dto;

import java.io.Serializable;
import java.util.List;

import cn.com.libertymutual.core.base.UserInfoBase;


public class UserInfo extends UserInfoBase implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2662577429428761735L;
	private String branchNo;//机构，对应的返回字段 comCode
	private String branchName;
	private String departmentCode;//部门   对应的返回字段  branchNo
	private String departmentName;
	private List<String> roleId;
	private List menus;
	private List<String> approveAuth;
	private List<String> configAuth;
	
	public List<String> getConfigAuth() {
		return configAuth;
	}
	public void setConfigAuth(List<String> configAuth) {
		this.configAuth = configAuth;
	}
	public List<String> getApprove() {
		return approveAuth;
	}
	public void setApprove(List<String> approveAuth) {
		this.approveAuth = approveAuth;
	}
	/**
	 * 登陆成功后的token，用于每笔交易的验证
	 * @return
	 */
	 
	 
	/*
	 * 机构编码	
	 */
	public String getBranchNo() {
		return branchNo;
	}
	public void setBranchNo(String branchNo) {
		this.branchNo = branchNo;
	}
	/**
	 * 机构名称	
	 * @return
	 */
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	/**
	 * 部门编码
	 * @return
	 */
	public String getDepartmentCode() {
		return departmentCode;
	}
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}
	/**
	 * 部门名称
	 * @return
	 */
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	/**
	 * 角色ID
	 * @return
	 */
	public List<String> getRoleId() {
		return roleId;
	}
	public void setRoleId(List<String> roleId) {
		this.roleId = roleId;
	}
	/**
	 * 角色名称
	 * @return
	 */
	/*public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}*/
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((branchName == null) ? 0 : branchName.hashCode());
		result = prime * result
				+ ((branchNo == null) ? 0 : branchNo.hashCode());
		result = prime * result
				+ ((departmentCode == null) ? 0 : departmentCode.hashCode());
		result = prime * result
				+ ((departmentName == null) ? 0 : departmentName.hashCode());
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserInfo other = (UserInfo) obj;
		if (branchName == null) {
			if (other.branchName != null)
				return false;
		} else if (!branchName.equals(other.branchName))
			return false;
		if (branchNo == null) {
			if (other.branchNo != null)
				return false;
		} else if (!branchNo.equals(other.branchNo))
			return false;
		if (departmentCode == null) {
			if (other.departmentCode != null)
				return false;
		} else if (!departmentCode.equals(other.departmentCode))
			return false;
		if (departmentName == null) {
			if (other.departmentName != null)
				return false;
		} else if (!departmentName.equals(other.departmentName))
			return false;
		
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		
		return true;
	}
	@Override
	public String toString() {
		return "UserInfo [branchNo=" + branchNo + ", branchName=" + branchName
				+ ", departmentCode=" + departmentCode + ", departmentName="
				+ departmentName + ", roleId=" + roleId + "]";
	}
	public List getMenus() {
		return menus;
	}
	public void setMenus(List menus) {
		this.menus = menus;
	}

	private String userCode;

	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	private String mobile;

	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	
}
