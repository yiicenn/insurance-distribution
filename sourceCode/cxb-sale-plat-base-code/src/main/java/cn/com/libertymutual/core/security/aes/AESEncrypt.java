package cn.com.libertymutual.core.security.aes;

/**
 * AES 加密
 * @author bob.kuang
 * @date 20170206 
 */

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AESEncrypt {
	private Logger log = LoggerFactory.getLogger(getClass());
	
	//算法名称
	private final String KEY_ALGORITHM = "AES";
	
	//加解密算法/模式/填充方式
	private final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
	
	private final String ALGORITHM = "SHA1PRNG";
	
	private final String CHARSET_NAME = "UTF-8";

	private Cipher cipherEncrypt;
	
	public AESEncrypt() {
		
	}
	
	public AESEncrypt( byte[] keyBytes, byte[] iv ) {
		
		try {
			KeyGenerator keyGenerator = KeyGenerator.getInstance( KEY_ALGORITHM );
			SecureRandom secureRandom = SecureRandom.getInstance( ALGORITHM );
	        secureRandom.setSeed(keyBytes);
	            
	        keyGenerator.init(128, secureRandom);
	        SecretKey key=keyGenerator.generateKey();
	        cipherEncrypt=Cipher.getInstance( TRANSFORMATION );
	        cipherEncrypt.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
	        
		} catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException e) {
			log.error("初始化AES失败", e);
		}
	}
	
	/**
	 * 加密
	 * @param content
	 * @return
	 */
	public String encryptForCBC(String content){  

        try{
            byte[] result=cipherEncrypt.doFinal(content.getBytes( CHARSET_NAME ));
            return byteToHexString( result );
        }catch (Exception e) {
            log.error("加密失败。", e);
        }

        return null;
    }


	/**
	 * 加密
	 * @param content
	 * @param keyBytes
	 * @param iv
	 * @return
	 */
	public String encryptForCBC(String content, byte[] keyBytes, byte[] iv){  

        try{
        	KeyGenerator keyGenerator = KeyGenerator.getInstance( KEY_ALGORITHM );
        	SecureRandom secureRandom = SecureRandom.getInstance( ALGORITHM );
	        secureRandom.setSeed(keyBytes);
	            
	        keyGenerator.init(128, secureRandom);
        	SecretKey key=keyGenerator.generateKey();
        	Cipher cipher=Cipher.getInstance( TRANSFORMATION );
        	cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(iv));
        
            byte[] result=cipher.doFinal(content.getBytes( CHARSET_NAME ));
            return byteToHexString( result );
        }catch (Exception e) {
            log.error("加密失败。", e);
        }

        return null;
    }
	
    private String byteToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder(bytes.length);
        String sTemp;

        for (byte b : bytes) {
        	sTemp = Integer.toHexString(0xFF & b);
            if (sTemp.length() < 2)
                sb.append(0);
            sb.append(sTemp.toUpperCase());
		}

        return sb.toString();
    }
    
}
