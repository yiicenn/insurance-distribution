package cn.com.libertymutual.core.util;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;


/**
 * 请求信息封装到对象
 * @author   yangjian1004
 * @Date     Aug 5, 2014     
 */
public class FormBeanUtil {
	 
    static {
        // 在封装之前 注册转换器
        ConvertUtils.register(new DateTimeConverter(), java.util.Date.class);
    }
 
    /**
     * 请求信息封装到对象
     * 
     * @param request
     *            请求信息
     * @param clazz
     *            封装对象
     */
    public static void populate(Object obj,HttpServletRequest request) {
        if (request == null)
            throw new IllegalArgumentException("FormBeanUtil.get中的request为空");
        try {
            Map<String, String[]> parameterMap = request.getParameterMap();
            BeanUtils.populate(obj, parameterMap);
        } catch (Exception e) {
            e.printStackTrace();
        }
         
    }
    /**
     * 请求信息封装到对象
     * 
     * @param 
     *            请求信息
     * @param clazz
     *            封装对象
     */
    public static  void populate( Object obj,Map<String, String[]> parameterMap) {
    	try {
    		BeanUtils.populate(obj, parameterMap); 
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
}
