/*
 *	Copyright (c) liberty 2016
 *	cn.com.libertymutual.insure.exception
 *  Create Date 2016年4月26日
 *	@author tracy.liao
 */
package cn.com.libertymutual.core.exception;

/**
 * 异常接口
 * @author tracy.liao
 * @date 2016年4月26日
 */
public interface ExceptionEnums {
	
	public int getCode();
	
	public String getMessage();

}
