package cn.com.libertymutual.core.query;

/**
 * 查询接口.每个方法的返回值应该是Query本身
 * 
 * @author LuoGang
 *
 */
public interface Query {
	/**
	 * 查询时调用的运算符,与Query的方法一一对应
	 * 
	 * @author LuoGang
	 * 
	 * @see Query
	 *
	 */
	public static enum Operator {
		/** 等于 */
		EQ,
		/** 不等于 */
		NOT_EQ,
		/** String不区分大小写等于 */
		IEQ,
		/** String不区分大小写不等于 */
		NOT_IEQ,
		/** IS NULL,等价于参数为null的EQ */
		IS_NULL,
		/** IS NOT NULL,等价于参数为null的NOT_EQ */
		IS_NOT_NULL,
		/** 小于 */
		LT,
		/** 小于等于 */
		LE,
		/** 大于 */
		GT,
		/** 大于等于 */
		GE,
		/** IN.. */
		IN,
		/** NOT IN */
		NOT_IN,
		/**
		 * between,
		 * 
		 * @see Query#between(String, Comparable, Comparable)
		 */
		BETWEEN,

		/** LIKE,不忽略大小写,但是如果数据库自身对like可能忽略大小写,比如SQL Server的like查询默认就是忽略大小写的 */
		LIKE,
		/** LIKE,忽略大小写 */
		ILIKE,
		/** NOT LIKE */
		NOT_LIKE,
		/** NOT_LIKE,忽略大小写 */
		NOT_ILIKE
	}

	/**
	 * name属性的值等于value
	 * 
	 * @param name
	 * @param value 可以为null,等价于isNull
	 * @return this
	 */
	Query eq(String name, Object value);

	/**
	 * name属性的值不等于value
	 * 
	 * @param name
	 * @param value 可以为null,等价于isNotNull
	 * @return this
	 */
	Query notEq(String name, Object value);

	/**
	 * name属性的值等于value,不区分大小写
	 * 
	 * @param name
	 * @param value 可以为null,等价于isNull
	 * @return this
	 */
	Query ieq(String name, String value);

	/**
	 * name属性的值不等于value,不区分大小写
	 * 
	 * @param name
	 * @param value 可以为null,等价于isNotNull
	 * @return this
	 */
	Query notIeq(String name, String value);

	/**
	 * name属性的为null
	 * 
	 * @param name
	 * @return this
	 */
	Query isNull(String name);

	/**
	 * name属性的不为null
	 * 
	 * @param name
	 * @return this
	 */
	Query isNotNull(String name);

	/**
	 * name属性的值小于value
	 * 
	 * @param name
	 * @param value
	 * @return this
	 */
	Query lt(String name, Comparable<?> value);

	/**
	 * 小于等于
	 * 
	 * @param name
	 * @param value
	 * @return this
	 */
	Query le(String name, Comparable<?> value);

	/**
	 * 大于
	 * 
	 * @param name
	 * @param value
	 * @return this
	 */
	Query gt(String name, Comparable<?> value);

	/**
	 * 大于等于
	 * 
	 * @param name
	 * @param value
	 * @return this
	 */
	Query ge(String name, Comparable<?> value);

	/**
	 * 查询介于min和max中的值，并且包含min和max.
	 * 为了防止数据库对between的差异,此方式实现的时候不使用between,应该使用name >= min && name <= max的形式.
	 * 即依次调用ge(name,min),le(name,max)
	 * 
	 * @param name
	 * @param min 可以为null,不限制下限查询
	 * @param max 可以为null,不限制上限查询
	 * @return
	 */
	Query between(String name, Comparable<?> min, Comparable<?> max);

	/**
	 * in查询
	 * 
	 * @param name
	 * @param values 查询参数,可以是一个collection或普通数组,如int[],不能为空
	 * @return this
	 */
	Query in(String name, Object... values);

	/**
	 * not in 查询
	 * 
	 * @param name
	 * @param values 查询参数,可以是一个collection或普通数组,如int[],不能为空
	 * @return this
	 */
	Query notIn(String name, Object... values);

	/**
	 * like查询，是否忽略大小写以数据库的设置为准，程序不特别处理
	 * 
	 * @param name
	 * @param values 如果有多个value，则每个之间是or的关系
	 * @return this
	 */
	Query like(String name, String... values);

	/**
	 * not like查询
	 * 
	 * @param name
	 * @param values 如果value有多个，则每个之间是and的关系
	 * @return this
	 */
	Query notLike(String name, String... values);

	/**
	 * like查询，忽略大小写
	 * 
	 * @param name
	 * @param values
	 * @return this
	 */
	Query ilike(String name, String... values);

	/**
	 * not like查询，忽略大小写
	 * 
	 * @param name
	 * @param values
	 * @return this
	 */
	Query notIlike(String name, String... values);

	/**
	 * 两个查询之间and
	 * 
	 * @param query
	 * @return this
	 */
	Query and(Query query);

	/**
	 * 两个查询之间or
	 * 
	 * @param query
	 * @return this
	 */
	Query or(Query query);

	// /**
	// * 对当前所有条件进行not
	// *
	// * @return this
	// */
	// Query not();

	// /**
	// * 返回查询条件
	// *
	// * @return
	// */
	// Predicate toPredicate();
}
