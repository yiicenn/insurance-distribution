package cn.com.libertymutual.core.sftp;

import org.springframework.stereotype.Component;

@Component
public class SFtpConfig {

	private String host;
	private int port;
	private String username;
	private String password;
	private String charset;
	private String remoteDirectory;
	private String localDirectory;
	private int connectTimeout = 0;

	private String remoteFileSeparator = "/";
	private String strictHostKeyChecking = "no";
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public String getRemoteDirectory() {
		return remoteDirectory;
	}
	public void setRemoteDirectory(String remoteDirectory) {
		this.remoteDirectory = remoteDirectory;
	}
	public String getLocalDirectory() {
		return localDirectory;
	}
	public void setLocalDirectory(String localDirectory) {
		this.localDirectory = localDirectory;
	}
	public String getRemoteFileSeparator() {
		return remoteFileSeparator;
	}
	public void setRemoteFileSeparator(String remoteFileSeparator) {
		this.remoteFileSeparator = remoteFileSeparator;
	}
	public String getStrictHostKeyChecking() {
		return strictHostKeyChecking;
	}
	public void setStrictHostKeyChecking(String strictHostKeyChecking) {
		this.strictHostKeyChecking = strictHostKeyChecking;
	}
	public int getConnectTimeout() {
		return connectTimeout;
	}
	public void setConnectTimeout(int connectTimeout) {
		this.connectTimeout = connectTimeout;
	}
	
	@Override
	public String toString() {
		return "SFtpConfig [host=" + host + ", port=" + port + ", username="
				+ username + ", password=" + password + ", charset=" + charset
				+ ", remoteDirectory=" + remoteDirectory + ", localDirectory="
				+ localDirectory +",remoteFileSeparator="+remoteFileSeparator + ", strictHostKeyChecking="
				+ strictHostKeyChecking + ",connectTimeout="+connectTimeout+"]";
	}

}
