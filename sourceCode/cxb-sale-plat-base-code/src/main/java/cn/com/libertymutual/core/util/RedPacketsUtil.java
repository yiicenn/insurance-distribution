package cn.com.libertymutual.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.google.common.collect.Lists;

public class RedPacketsUtil {
	//红包最小值  
	private static final double MINVALUE = 0.01d;  
    //红包最大值  
       private static final double MAXVALUE = 200d;  
  
    /** 
     * 这里为了避免某一个红包占用大量资金，我们需要设定非最后一个红包的最大金额，我们把他设置为红包金额平均值的N倍； 
     */  
    private static final int TIMES = 2;  
  
    public static void main(String[] args) {
    	
    	List<String> toEmils=Lists.newArrayList();
    	String emails="bob.kuang@libertymutual.com.cn,kfwang@isoftstone.com,George.zhu@libertymutual.com.cn,Leo.Wang@libertymutual.com.cn,William.Guo@libertymutual.com.cn,Menny.Du@libertymutual.com.cn";
    	toEmils.toArray(emails.split(","));
    	System.out.println(toEmils.size());
		for(String e:toEmils){
			System.out.println(e);
		}
    	//测试
		//System.out.println(spiltRedPackets(20,5,0.01d,200d,2));
    	for(int i=0;i<20;i++){
    		System.out.print(spiltRedPacket(2000,266,1,5)+",");
    		
    	}
    	System.out.println("");
    	for(int i=0;i<20;i++){
    		System.out.print(Math.random()*(34)+1+",");
    	}
		
		//Random r =new Random();
		//System.out.println(r.nextInt(2));
	}
   
    /**
     * 抽奖
     * @param money 剩余钱
     * @param count 剩余次数
     * @param minValue 最小值
     * @param maxValue 最大值
     * @param times  平均值的倍数
     * @return
     */
    public static double  spiltRedPacket(double money,int count,double minValue,double maxValue,int times) { 
    	double avg = money/count;  
        if(avg <= minValue) {  
            return minValue;  
        } else if(avg >= maxValue) {  
            return maxValue;  
        }  
 	   double max = (money/count)*times;  
        max = max>money?money:max;  
 	double value = randomRedPacket(money,minValue,max,count);
     BigDecimal amount=new BigDecimal(value).setScale(2, BigDecimal.ROUND_HALF_UP);
 	return amount.doubleValue();
 }
    public static double spiltRedPacket(double money,int count) { 
    	return spiltRedPacket( money,count,MINVALUE,TIMES);
    }
    
    
    public static double  spiltRedPacket(double money,int count,double minvalue,int times) { 
    	   double max = (money/count)*times;  
           max = max>money?money:max;  
    	double value = randomRedPacket(money,minvalue,max,count);
        BigDecimal amount=new BigDecimal(value).setScale(2, BigDecimal.ROUND_HALF_UP);
    	return amount.doubleValue();
    }
    public static List<Double>  spiltRedPackets(double money,int count) { 
    	return spiltRedPackets( money,count,MINVALUE,MAXVALUE,TIMES);
    }
    
    /**
     * 拆分红包
     * @param money 金额
     * @param count 个数
     * @param minvalue  最小值
     * @param maxValue  最大值
     * @param times  这里为了避免某一个红包占用大量资金，我们需要设定非最后一个红包的最大金额，我们把他设置为红包金额平均值的N倍；
     * @return
     */
    
    public static List<Double>  spiltRedPackets(double money,int count,double minvalue,double maxValue,int times) {  
        //首先判断红包是否合情理  
        if(!isRight(money,count,minvalue,maxValue)) {  
            return null;  
        }  
        List<Double> list = new ArrayList<Double>();  
        double max = money/count*times;  
        max = max>money?money:max;  
        for(int i = 0 ; i < count; i++) {  
        	double value = randomRedPacket(money,minvalue,max,count-i);
            BigDecimal amount=new BigDecimal(value).setScale(2, BigDecimal.ROUND_HALF_UP);
            list.add(amount.doubleValue());  
            money -= value;  
        }  
        return list;  
    } 
    /** 
     * 判断红包是否合情理 
     * @param money 
     * @param count 
     * @return 
     */  
    public static boolean isRight(double money,int count,double minvalue,double maxValue) {  
    	double avg = money/count;  
        if(avg < minvalue) {  
            return false;  
        } else if(avg > maxValue) {  
            return false;  
        }  
        return true;  
    }  
  
    /** 
     * 分红包核心算法 
     * @param money 
     * @param minS 
     * @param maxS 
     * @param count 
     * @return 
     */  
    public static double randomRedPacket(double money,double minS,double maxS,int count) {  
        //当人数剩余一个时，把当前剩余全部返回  
        if(count == 1) {  
            return money;  
        }  
        //如果当前最小红包等于最大红包，之间返回当前红包  
        if(minS == maxS) {  
            return minS;  
        }  
        double max = maxS>money?money:maxS;  
        //随机产生一个红包  
        double one = Math.random()*(max-minS)+minS; 
      
        double balance = money - one;  
        //判断此次分配后，后续是否合理  
        if(isRight(balance,count-1,minS,max)) {  
            return one;  
        } else {  
            //重新分配  
        	double avg = balance/(count-1);  
            //如果本次红包过大，导致下次不够分，走这一条  
            if(avg < minS) {  
                return randomRedPacket(money, minS, one, count);  
            } else {  
                return randomRedPacket(money, one, maxS, count);  
            }  
        }  
    }  
  
    /**
	 *  1/times 概率产生
	 * @param times
	 * @return
	 */
	public static boolean random(int times){
		Random r =new Random();
		if(1==r.nextInt(times)+1){
			return true;
		}
		return false;
	}

}
