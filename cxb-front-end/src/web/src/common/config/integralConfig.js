
export default {
    /**
     * 获得积分类型-全部
     */
    getIntegralTypeAll() {
        return INTEGRAL_TYPE_LIST;
    },
    /**
     * 获得积分类型
     * @param {*} index 
     */
    getIntegralType(index) {
        return INTEGRAL_TYPE_LIST[index];
    },
    /**
     * 获得积分变更类型
     * @param {*} index 
     */
    getIntegralChangeType(index) {
        return INTEGRAL_CHANGE_TYPE[parseInt(index)];
    },
}
// 积分类别,1.获取 2.扣减
const INTEGRAL_TYPE_LIST = [
    { key: "all", value: "全部" },
    { key: "in", value: "获取" },
    { key: "out", value: "扣减" },
];
// 积分变更类型
const INTEGRAL_CHANGE_TYPE = [
    "",
    "活动",//1
    "签单",//2，或者叫 规则
    "其他",//3，原本叫 人工发放
    "兑换",//4
    "退保",//5
    "过期",//6
    "商城",//7，商城退还
    "退保差额",//8
    "提现",//9
    "扣减差额",//10
    "直接好友出单",//11
    "间接好友出单",//12
    "渠道出单",//13
    "直接好友退保",//14
    "间接好友退保",//15
    "渠道退保",//16
    "其他",//17
];