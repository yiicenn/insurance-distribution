import {
    policyHolderType
} from 'src/common/const';
export default {
    /*
        获得支付的状态
    */
    getOrderState(index) {
        return ORDER_STATE[index];
    },
    getOrderStateTypeColor(index) {
        return ORDER_STATE_COLOR[index];
    },
    getOrderStateColor(index) {
        return COLOR_STATE[index];
    },
    /*
         获得状态显示的对应功能按钮
    */
    getOrderStateShowMenu(index) {
        return STATE_SHOW_MENU[index];
    },
    /*
         获得订单状态
    */
    getOrderStateType() {
        return ORDER_STATE_TYPE;
    },
    /*
         获得订单状态
    */
    getPolicyStateType(index) {
        if (typeof (POLICY_STATE[index]) == "undefined") {
            return "无效";
        } else {
            return POLICY_STATE[index];
        }
    },
    getCliaimsType(index) {
        return CLAIMS_STATE[index];
    },
}

const POLICY_STATE = ['未生效', '在期', '已撤单', '已注销', '全单退保', '保单遗失', '保险标志遗失', '保单和保险标志遗失', '支付', '失效'];


const COLOR_STATE = ['#7799AE', '#0691CD', '#ACD0CE', '#FFFDD6', '#E70E91', '#84C2B7', '#9EC4F6', "#F83149", '#D4237A', '#dedede']
//订单状态 
//0 支付失败
//1 支付成功 =  有效
//2 没有支付
//3 无效
const ORDER_STATE = ['支付失败', '有效', '未支付', '无效', '新保', '下发修改或拒保', '自动核保', '等待核保', '主动撤回', '查勘岗', '单证审核不通过', '待单证审核', '待绩效审核', '绩效审核不通过', '批退', '批改', "保单遗失", "保单注销", "生成保单失败,需要补登操作"];

const ORDER_STATE_COLOR = ['#F83149', '#0691CD', '#7799AE', '#dedede'];

const ORDER_STATE_TYPE = {
    FAIL: '0',
    EFFECTIVE: '1',
    NO_PAY: '2',
    INVALID: '3'
}
// 新建 打开 关闭 重开
// 1     2    0   3
const CLAIMS_STATE = {
    "0": {
        openIsDone: true,
        closeIsDone: true,
        newIsDone: true,
        isReTest: false,
    },
    "1": {
        openIsDone: false,
        closeIsDone: false,
        newIsDone: false,
        isReTest: false,
    },
    "2": {
        openIsDone: true,
        closeIsDone: false,
        newIsDone: false,
        isReTest: false,
    },
    "3": {
        openIsDone: true,
        closeIsDone: true,
        newIsDone: true,
        isReTest: true,
    },
}

//状态展示菜单
const STATE_SHOW_MENU = {
    "0": {    //支付失败
        policy: false,  // 电子保单
        invoice: false, // 电子发票
        case: false, //马上报案
        history: false, //历史赔案
        pay: true, //付款
        clause: true, //条款下载
    },
    "1": {     //有效
        policy: true,  // 电子保单
        invoice: true, // 电子发票
        case: true, //马上报案
        history: true, //历史赔案
        pay: false, //付款
        clause: true, //条款下载
    },
    "2": {     //未支付
        policy: false,  // 电子保单
        invoice: false, // 电子发票
        case: false, //马上报案
        history: false, //历史赔案
        pay: true, //付款
        clause: true, //条款下载
    },
    "3": {     //无效
        policy: false,  // 电子保单
        invoice: false, // 电子发票
        case: false, //马上报案
        history: false, //历史赔案
        pay: false, //付款
        clause: true, //条款下载
    },
    "15": {     //无效
        policy: false,  // 电子保单
        invoice: false, // 电子发票
        case: false, //马上报案
        history: false, //历史赔案
        pay: false, //付款
        clause: true, //条款下载
    },
};

