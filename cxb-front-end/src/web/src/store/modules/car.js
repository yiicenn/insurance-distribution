import { Car_Mutations, OWNER_TYPE, RouteUrl, Mutations, DEFAULT_VEHICLE_CATEGORY_CODE, PerIdentifyType, CAR_INSURED_RELATION } from 'src/common/const'
import { RequestUrl } from "src/common/carUrl";
import DateUtil from "src/common/util/dateUtil";
import Validation from "src/common/util/validation";
import CarConfig from "src/common/config/carConfig";
export default {
    state: {
        carConfig: {},

        carInfoList: [],
        isQueryCar: false,
        curInfo: {},
        licenseNo: "",
        carInsureData: {
            licenseNo: "",
            carOwner: "",
            isNewCar: false,
        },
        newCarLicenseNo: "浙",
        queryModelName: "",
        vinList: [],
        vehicleList: [],

        carCurShowList: [],

        carSelectSave: [],
        carSelect: {},

        addCarInfoSave: [],
        curAddCarInfo: {},

        vinNoQueryFlag: 0,    //是否vin解析

        tMotorPremiumRequestDTO: {   //保费计算
        },
        tdistributionDto: {           //提交核保信息
        },


        saveKind: {},
        relationSave: {},
        mainRiskSave: [],    //主险
        additionalInsurance: [],  //附加险
        seleRateSave: {},
        addMain: {},

        baseUserSeleList: {},
        userSeleList: {},

        isImmInsure: false,

        // 保费计算结果
        combinecalculate: {},

        isGetCarOfferInit: false,

        carKindList: [],   //车辆种类
        fuelTypeList: [],  //能源种类
        vehicleCategoryList: [], //交管车种类
        licenseKindCodeList: [], //号牌种类

        userNatureList: {}, //获得车辆使用性质

        seleSetSatateList: {},


        DistributionSave: {
            name: "",
            cellPhoneNo: "",
            address: "",
            email: "",
        },

        commitDisData: {
            name: "",
            cellPhoneNo: "",
            address: "",
            email: "",
        },
        isSetCommitDis: false,

        downTprpTmainDto: {},
        isGetNalInit: false,

        premium: "",
        mtplPremium: "",

        tprpTitemKindListDto: [],

        queryVehicleList: {},

        carSeleInitSave: {},

        baseCarOffer: {},

        packList: {},
        packNameList: [],

        crossSalePlan: {},
        curCrossSalePlan: [],
        seleAxtxList: [],
        seleAtqqbList: [],

        carAgrInfo: {
            agreementCode: "",
            salerName: "",
            saleCode: "",
            channelName: "",
        },

        verifyRes: {
            status: true,
            resultCode: "",
            flowId: "",
            uuId: "",
            renewalFlag: "",
            querySequenceNo: "",
            renewalQuestion: ""
        },

        saveDataIdList: {}
    },
    mutations: {
        [Car_Mutations.CAR_GET_SAVE_FIND_ID](state, data) {
            let _this = data._this;
            let id = data.id;
            let setCarInfoData = function (state, saveData, _this) {
                try {
                    // debugger
                    let date = new Date();
                    date.setTime(date.getTime() + 24 * 60 * 60 * 1000);
                    let dataTime = date.getTime();
                    let carInsureData = saveData.carInsureData;
                    let carInfoList = saveData.carInfoList;
                    let combinecalculate = saveData.combinecalculate;
                    let startData = "";
                    // debugger
                    state.carInsureData = saveData.carInsureData;
                    state.licenseNo = carInsureData.licenseNo;
                    state.carInfoList[state.licenseNo] = carInfoList;
                    state.combinecalculate[state.licenseNo] = combinecalculate;
                    let query = {
                        _this: _this,
                        insureType: carInfoList.seleCrossValue,
                        type: "query"
                    }
                    _this.$common.storeCommit(_this, Car_Mutations.CROSS_SALE_PLAN, query);

                    // _this.$common.goUrl(_this, RouteUrl.OFFER_RESULT);

                    if (combinecalculate.status && typeof (combinecalculate.subPremium) != "undefined") {
                        if (combinecalculate.combineFlag == "COMBINE") {
                            let stDate = DateUtil.getDateByStrYmdOrYmdhms(combinecalculate.startDate).getTime();
                            let stMtplDate = DateUtil.getDateByStrYmdOrYmdhms(combinecalculate.mtplStartDate).getTime();
                            if (stDate > stMtplDate) {
                                startData = stMtplDate;
                            } else {
                                startData = startData;
                            }
                        }
                        else if (combinecalculate.combineFlag == "MTPL") {
                            let stMtplDate = DateUtil.getDateByStrYmdOrYmdhms(combinecalculate.mtplStartDate).getTime();
                            startData = stMtplDate;
                        } else {
                            let stDate = DateUtil.getDateByStrYmdOrYmdhms(combinecalculate.startDate).getTime();
                            startData = stDate;
                        }
                        startData = DateUtil.getDateByStrYmdOrYmdhms(startData).getTime();
                        if (startData < dataTime) {
                            _this.$common.goUrl(_this, RouteUrl.CAR_OFFER);
                        } else {
                            _this.$common.goUrl(_this, RouteUrl.OFFER_RESULT);
                        }
                    } else {
                        _this.$common.goUrl(_this, RouteUrl.CAR_OFFER);
                    }

                } catch (e) {
                    console.log(e);
                }
            }
            if (typeof (state.saveDataIdList[id]) == "undefined") {
                let fm = new FormData();
                fm.append('id', id);

                _this.$http.post(RequestUrl.FIND_SAVE_DATA_ID, fm).then(function (res) {
                    if (res.success) {
                        let saveData = res.result;
                        saveData.saveData = JSON.parse(saveData.saveData);
                        state.saveDataIdList[id] = saveData;
                        setCarInfoData(state, saveData.saveData, _this);
                    } else {
                        _this.$common.showErrorMsg(_this, "未查询到对应的报价信息!");
                    }
                });
            } else {
                setCarInfoData(state, state.saveDataIdList[id].saveData, _this);
            }
        },
        [Car_Mutations.SET_CAR_OFFER_DATA](state, data) {
            let _this = data._this;
            let res = data.res;
            let saveCarInfo = function (type, _this, state) {
                // debugger
                let carInfoList = state.carInfoList[state.licenseNo];
                let saveData = {
                    carInsureData: state.carInsureData,
                    carInfoList: state.carInfoList[state.licenseNo],
                    combinecalculate: state.combinecalculate[state.licenseNo]
                }
                // let markText = state.carInsureData.licenseNo + "        " + carInfoList.tprptCarOwnerDto.name;
                // debugger
                let markText = carInfoList.tprptCarOwnerDto.name + "-" + state.carInsureData.licenseNo;
                let riskCode = "0502";
                let price = "";
                let stateNum = "";
                let startDate = "";
                let endTime = "";
                if (type == 1) {   //报价成功
                    let tprpTmainDto = saveData.combinecalculate.tprpTmainDto;
                    if (tprpTmainDto.mtplStartDate != "" && tprpTmainDto.mtplStartDate != null) {
                        startDate = tprpTmainDto.mtplStartDate;
                        endTime = tprpTmainDto.mtplEndDate;
                    } else {
                        startDate = tprpTmainDto.startDate;
                        endTime = tprpTmainDto.endDate;
                    }
                    startDate = DateUtil.getDateStr_YmdhmsByTs(_this.$common.getDateCurrency(startDate));
                    endTime = DateUtil.getDateStr_YmdhmsByTs(_this.$common.getDateCurrency(endTime));
                    // debugger
                    price = saveData.combinecalculate.showPrice;
                    stateNum = "1";
                } else {   //报价失败
                    stateNum = "2"
                }
                let queryData = {
                    saveData: JSON.stringify(saveData),
                    markText: markText,
                    startDate: startDate,
                    endTime: endTime,
                    riskCode: riskCode,
                    price: price,
                    state: stateNum,
                    _this: _this
                }
                _this.$common.storeCommit(_this, Car_Mutations.SET_PRESERVATION, queryData);

            }

            if (res.success) {
                if (typeof (res.result.querySequenceNo) != "undefined") {
                    state.verifyRes = res.result;
                    state.verifyRes.renewalQuestion = "data:image/png;base64," + state.verifyRes.renewalQuestion;
                    _this.showVal = true;
                }
                else if (res.result.status && (res.result.resultCode == "0" || res.result.resultCode == "0001")) {
                    state.combinecalculate[state.licenseNo] = res.result;
                    state.premium = res.result.tprpTmainDto.premium;
                    state.mtplPremium = res.result.tprpTmainDto.mtplPremium;
                    state.tprpTitemKindListDto = res.result.tprpTitemKindListDto;

                    //交叉销售计划保存
                    let proposalNo = "";
                    if (_this.commInsure) {
                        proposalNo = res.result.tprpTmainDto.proposalNo;
                    } else {
                        proposalNo = res.result.tprpTmainDto.mtplProposalNo;
                    }
                    let planList = [];
                    if (_this.axtxInsure) {
                        let saveAxtxPlan = _this.$common.getNewObject(_this.seleAxtx);
                        saveAxtxPlan.motorProposalNo = proposalNo;
                        saveAxtxPlan.insuredRelation = _this.seleCrossValue;
                        planList.push(saveAxtxPlan);
                        state.combinecalculate[state.licenseNo].axtxPrice = saveAxtxPlan.planPrice;
                        state.combinecalculate[state.licenseNo].axtxKind = saveAxtxPlan;
                    } else {
                        state.combinecalculate[state.licenseNo].axtxPrice = 0;
                        state.combinecalculate[state.licenseNo].axtxKind = [];
                    }
                    if (_this.atqqbInsure) {
                        let saveAtqbbPlan = _this.$common.getNewObject(_this.seleAtqqb);
                        saveAtqbbPlan.motorProposalNo = proposalNo;
                        saveAtqbbPlan.insuredRelation = _this.seleCrossValue;
                        planList.push(saveAtqbbPlan);
                        state.combinecalculate[state.licenseNo].atqqbPrice = saveAtqbbPlan.planPrice;
                        state.combinecalculate[state.licenseNo].atqqbKind = saveAtqbbPlan;
                    } else {
                        state.combinecalculate[state.licenseNo].atqqbPrice = 0;
                        state.combinecalculate[state.licenseNo].atqqbKind = [];
                    }
                    let subPremium = parseFloat(state.combinecalculate[state.licenseNo].subPremium);
                    let axtxPrice = parseFloat(state.combinecalculate[state.licenseNo].axtxPrice);
                    let atqqbPrice = parseFloat(state.combinecalculate[state.licenseNo].atqqbPrice);
                    state.combinecalculate[state.licenseNo].showPrice = (subPremium + axtxPrice + atqqbPrice).toFixed(2);

                    saveCarInfo(1, _this, state);
                    if ((_this.atqqbInsure || _this.axtxInsure) && _this.seleCrossList.length > 0) {
                        let query = {
                            planList: planList,
                            _this: _this,
                            proposalNo: proposalNo,
                            res: res
                        }
                        _this.$common.storeCommit(_this, Car_Mutations.CROSS_SALE_PLAN_SAVE, query);
                    } else {
                        if (res.result.resultCode == "0001") {
                            res.result.resultMessage = res.result.resultMessage.replace("投保车辆信息中,未提供国产进口车标识,将按国产车进行险别信息校验.商业险发生重复投保,", "");
                            let msgComData = {
                                title: "重复投保",
                                content: res.result.resultMessage,
                                alertBtn: "继续投保",
                                alertBtnUrl: RouteUrl.OFFER_RESULT,
                                backBtn: "重新选择",
                                backBtnUrl: "",
                            };
                            _this.$common.storeCommit(_this, Mutations.IS_SHOW_MSG_COM_DATA, msgComData);
                        } else {
                            _this.$common.goUrl(_this, RouteUrl.OFFER_RESULT);
                        }
                    }
                    //交叉销售计划保存
                } else {
                    saveCarInfo(2, _this, state);
                    _this.$common.showErrorMsg(_this, res.result.resultMessage);
                    // }
                }
            } else {
                if (typeof (res.result.resultMessage) != "undefined") {
                    _this.$common.showErrorMsg(_this, res.result.resultMessage);
                } else {
                    _this.$common.showErrorMsg(_this, res.result);
                }
            }

            _this.$common.storeCommit(_this, Car_Mutations.SET_QUERY_STATE, false);
        },
        [Car_Mutations.VERIFY](state, data) {
            let _this = data._this;
            let answer = data.answer;
            let verifyRequstDTO = {
                renewalFlag: state.verifyRes.renewalFlag,
                querySequenceNo: state.verifyRes.querySequenceNo,
                answer: answer,
                agreementNo: state.carAgrInfo.agreementCode,
                flowId: state.verifyRes.flowId,
            }
            _this.$common.storeCommit(_this, Car_Mutations.SET_QUERY_STATE, true);
            _this.$http.post(RequestUrl.VERIFY, verifyRequstDTO).then(function (res) {
                _this.$common.storeCommit(_this, Car_Mutations.SET_QUERY_STATE, false);
                if (res.success) {
                    let query = {
                        _this: _this,
                        res: res
                    }
                    _this.$common.storeCommit(_this, Car_Mutations.SET_CAR_OFFER_DATA, query);
                } else {
                    _this.haveError = true;
                    if (typeof (res.result.resultMessage) != "undefined" && res.result.resultCode == "9997") {
                        // _this.errorMessage = res.result.resultMessage;
                        _this.errorMessage = "输入验证码有误!";
                    } else {
                        _this.errorMessage = "校验失败！";
                    }
                    let queryData = {
                        _this: _this,
                        seleValue: _this.seleValue
                    }
                    _this.$common.storeCommit(_this, Car_Mutations.CAR_OFFER, queryData);
                }
            });
        },
        [Car_Mutations.SET_PRESERVATION](state, data) {
            // debugger
            let _this = data._this;
            let userCode = "";
            if (_this.user.isLogin) {
                userCode = _this.user.userDto.userCode;
            }
            let query = {
                userCode: userCode,
                saveData: data.saveData,
                markText: data.markText,
                startTime: "",
                endTime: "",
                riskCode: data.riskCode,
                price: data.price,
                state: data.state,
            }
            _this.$http.post(RequestUrl.SAVE_DATA, query).then(function (res) {
            });
        },
        [Car_Mutations.SET_CAR_AGR_INFO](state, data) {
            state.carAgrInfo = data;
        },
        /**
         * 请求交叉销售计划
         */
        [Car_Mutations.CROSS_SALE_PLAN](state, data) {
            // debugger
            let _this = data._this;
            let seatCount = "";
            let saveData = state.carInfoList[state.licenseNo];
            if (typeof (data.seatCount) == "undefined") {
                seatCount = saveData.tprpTitemCarDto.seatCount;
            } else {
                seatCount = data.seatCount;
            }
            let brithday = "";
            let age = 18;
            let insureType = data.insureType;
            let type = data.type;
            if (type == "init") {
                insureType = 1;
                let indexOfType = "";
                state.carInfoList[state.licenseNo].seleCross = [];
                if (saveData.tprptCarOwnerDto.natureOfRole == OWNER_TYPE.PERSONAL) {
                    let seleOwner = { key: 3, value: "同车主" }
                    state.carInfoList[state.licenseNo].seleCross.push(seleOwner);
                    insureType = 3;
                    indexOfType += "3";
                }
                if (saveData.tprptInsuredDto.natureOfRole == OWNER_TYPE.PERSONAL) {
                    let seleInsure = { key: 2, value: "同被保人" }
                    state.carInfoList[state.licenseNo].seleCross.push(seleInsure);
                    insureType = 2;
                    indexOfType += "2";
                }
                if (saveData.tprptApplicantDto.natureOfRole == OWNER_TYPE.PERSONAL) {
                    let seleAppli = { key: 1, value: "同投保人" }
                    state.carInfoList[state.licenseNo].seleCross.push(seleAppli);
                    insureType = 1;
                    indexOfType += "1";
                }
                // debugger
                if (typeof (state.carInfoList[state.licenseNo].seleCrossValue) == "undefined" || indexOfType.indexOf(state.carInfoList[state.licenseNo].seleCrossValue) < 0) {
                    state.carInfoList[state.licenseNo].seleCrossValue = insureType;
                }

            }
            if (insureType == 3) {
                brithday = saveData.tprptCarOwnerDto.brithday;
            } else if (insureType == 2) {
                brithday = saveData.tprptInsuredDto.brithday;
            } else if (insureType == 1) {
                brithday = saveData.tprptApplicantDto.brithday;
            } else {
                return;
            }
            age = DateUtil.jsGetAge(brithday);
            let setCroPlan = function () {
                let queryPlan = state.crossSalePlan[seatCount + age];
                if (typeof (queryPlan['2724']) != "undefined") {
                    state.seleAxtxList = queryPlan['2724'];
                    if (state.seleAxtxList.length > 0 && typeof (state.carInfoList[state.licenseNo].sele2724.planPrice) == "undefined") {
                        state.carInfoList[state.licenseNo].sele2724 = state.seleAxtxList[0].key;
                    }
                } else {
                    state.seleAxtxList = [];
                }
                if (typeof (queryPlan['3105']) != "undefined") {
                    state.seleAtqqbList = queryPlan['3105'];
                    if (state.seleAtqqbList.length > 0 && typeof (state.carInfoList[state.licenseNo].sele3105.planPrice) == "undefined") {
                        state.carInfoList[state.licenseNo].sele3105 = state.seleAtqqbList[0].key;
                    }
                } else {
                    state.seleAtqqbList = [];
                }
                console.log('-------------1-----------------');
                console.log(state.carInfoList[state.licenseNo].sele2724);
                console.log(state.carInfoList[state.licenseNo].sele3105);
            };
            //初始化默认选项值 2724
            if (typeof (state.carInfoList[state.licenseNo].sele2724) == "undefined") {
                state.carInfoList[state.licenseNo].sele2724 = {};
            }
            //初始化默认选项值  3105
            if (typeof (state.carInfoList[state.licenseNo].sele3105) == "undefined") {
                state.carInfoList[state.licenseNo].sele3105 = {};
            }
            // let age = data.age;
            if (typeof (state.crossSalePlan[seatCount + age]) == "undefined") {
                let fromData = new FormData();
                fromData.append('seatCount', seatCount);
                fromData.append('age', age);
                fromData.append('agreementCode', state.carAgrInfo.agreementCode);
                _this.$http.post(RequestUrl.CROSS_SALE_PLAN, fromData).then(function (res) {
                    if (res.success) {
                        let saveData = {};
                        let resData = res.result;
                        for (let i = 0; i < resData.length; i++) {
                            let resItem = resData[i];
                            if (typeof (saveData[resItem.proCode]) == "undefined") {
                                saveData[resItem.proCode] = [];
                            }
                            let saveSeleItem = {
                                key: resItem,
                                value: "¥ " + resItem.planPrice
                            }
                            saveData[resItem.proCode].push(saveSeleItem);
                        }
                        // console.log(saveData);
                        state.crossSalePlan[seatCount + age] = saveData;
                        state.curCrossSalePlan = saveData;
                        setCroPlan();
                    } else {
                        _this.$common.showErrorMsg(_this, "未查询到该交叉销售计划!");
                    }
                });
            } else {
                state.curCrossSalePlan = state.crossSalePlan[seatCount + age];
                setCroPlan();
            }

        },

        [Car_Mutations.COMMIT_DISTRIBUTION](state, data) {
            state.commitDisData = data;
            state.isSetCommitDis = true;
        },
        [Car_Mutations.SAVE_DISTRIBUTION](state, data) {
            // debugger
            state.DistributionSave = data;
        },
        [Car_Mutations.GET_USER_NATURE_LIST](state, data) {
            let _this = data._this;
            let insuredNature = data.insuredNature; //车主类型
            let carKind = data.carKind;
            let riskCode = "0502";
            let query = {
                insuredNature: insuredNature,
                carKind: carKind,
                riskCode: riskCode,
                TYPE: "FORM"
            }
            // debugger
            let setBaseData = function (userNatureList) {
                let dataList = state.userNatureList[saveSt];
                let userNa = _this.tprpTitemCarDto.useNatureCode;
                if (_this.tprpTitemCarDto.useNatureCode == "") {
                    _this.tprpTitemCarDto.useNatureCode = _this.useNatureList[0].key;
                } else {
                    let isCon = false;
                    for (let i = 0; i < dataList.length; i++) {
                        if (dataList[i].key == userNa) {
                            isCon = true;
                        }
                    }
                    if (!isCon) {
                        _this.tprpTitemCarDto.useNatureCode = dataList[0].key;
                    }
                }
            }

            let saveSt = insuredNature + carKind + riskCode;
            if (typeof (state.userNatureList[saveSt]) == "undefined") {
                _this.$http.post(RequestUrl.GET_USER_NATURE, query)
                    .then(function (res) {
                        state.isGetNalInit = true;
                        if (res.success) {
                            state.userNatureList[saveSt] = res.result;
                            _this.useNatureList = res.result;
                            setBaseData();
                        }
                    })
            } else {
                _this.useNatureList = state.userNatureList[saveSt];
                setBaseData();
            }

        },
        [Car_Mutations.SET_INIT_SELE_CAR](state, data) {
            // debugger
            let _this = data._this;
            let riskCode = "0502";
            let branchCode = "50000000";
            let query = {
                TYPE: "FORM",
                riskCode: riskCode,
                branchCode: branchCode
            }
            if (typeof (state.carSeleInitSave[riskCode + branchCode]) == "undefined") {
                _this.$http.post(RequestUrl.GET_CAR_SELE_INIT, query)
                    .then(function (res) {
                        if (res.success) {
                            let result = res.result;
                            state.carSeleInitSave[riskCode + branchCode] = result;
                            state.carKindList = result.CarKind;
                            state.fuelTypeList = result.FuelType;
                            state.vehicleCategoryList = result.VehicleCategory;
                            state.licenseKindCodeList = result.LicenseKindCode;
                            let packList = result.CarPackage;
                            let savePack = {};
                            let packNameList = [];
                            for (let i = 0; i < packList.length; i++) {
                                let pack = packList[i];
                                if (typeof (savePack[pack.packName]) == "undefined") {
                                    packNameList.push(pack.packName);
                                    savePack[pack.packName] = {};
                                }
                                savePack[pack.packName][pack.packKind] = pack;
                            }
                            state.packNameList = packNameList;
                            state.packList = savePack;
                            result.packList = savePack;
                            result.packNameList = packNameList;
                            // debugger
                        }
                    })
            } else {
                let result = state.carSeleInitSave[riskCode + branchCode];
                state.carKindList = result.CarKind;
                state.fuelTypeList = result.FuelType;
                state.vehicleCategoryList = result.VehicleCategory;
                state.licenseKindCodeList = result.LicenseKindCode;
                state.packList = result.packList;
                state.packNameList = result.packNameList;
            }

        },

        //  续保查询接口
        [Car_Mutations.CAR_RENEWAL_QUERY](state, data) {
            // debugger
            state.isQueryCar = true;
            let _this = data._this;
            let isNewCar = data.isNewCar;
            let query = {
                licenseNo: data.licenseNo,
                agreementNo: state.carAgrInfo.agreementCode
            }
            state.licenseNo = data.licenseNo;
            let setInit = function () {
                let sr = {
                    renewalFlag: "0",
                }
                state.carInfoList[query.licenseNo] = sr;
                let tprpTmainDtoQuery = {
                    combineFlag: "",
                    proposalNo: "",
                    mtplProposalNo: "",
                    autonomyAdjustValue: "",
                    channelAdjustValue: "",
                    salerNumber: "",
                    salerName: "",
                    trdSalesCode: "",
                    startDate: DateUtil.getDateTomorrow(1),
                    startHour: "0",
                    endDate: "",
                    endHour: "24",
                    mtplStartDate: DateUtil.getDateTomorrow(1),
                    mtplStartHour: "0",
                    mtplEndDate: "",
                    mtplEndHour: "24"
                };
                // debugger
                let tprpTmainDto = {               //投保单信息
                    flowId: "",
                    partnerAccountCode: "",
                    operatorTime: "",
                    recordCode: "",
                    agreementNo: "",
                    renewalFlag: "",
                    proposalNo: "",   //商业险投保单号
                    mtplProposalNo: "",   //交强险投保单号
                    policyType: "",
                    papolicyNo: "",
                    mtplPapolicyNo: "",
                    startDate: DateUtil.getDateTomorrow(1),
                    endDate: "",
                    startHour: "",
                    endHour: "",
                    mtplStartDate: DateUtil.getDateTomorrow(1),
                    mtplEndDate: "",
                    mtplStartHour: "",
                    mtplEndHour: "",
                    premium: 0,
                    discountMain: 0,
                    mtplPremium: 0,
                    mtplDiscountMain: 0,
                    tax: 0,
                    plateno: "",
                    vinno: "",
                    applicantId: "",
                    applicantName: "",
                    insuredId: "",
                    insuredName: "",
                    carholderId: "",
                    carholderName: "",
                    status: "",
                    updateUser: "",
                    updateTime: "",
                    createTime: "",
                    createUser: "",
                    remark: "",
                    combineFlag: "",   //联合投保标识
                    lastyearClaimTime: "",
                    businessType: "",
                    autonomyAdjustValue: "",
                    channelAdjustValue: "",
                    autonomyAdjust: "",
                    channelAdjust: "",
                    salerNumber: "",
                    salerName: "",
                    trdSalesCode: ""
                };
                let tprpTitemCarDto = {            //投保标的信息
                    id: 0,
                    flowId: "",
                    countryCode: "",
                    runAreaCode: "",
                    whethelicenses: "",
                    whethercar: "",
                    newOldLogo: "",
                    licenseNo: "",
                    carKindCode: "",
                    useNatureCode: "",
                    engineNo: "",
                    vinNo: "",
                    licenseKindCode: "",
                    enrollDate: DateUtil.getNowDateStr_Ymd(),
                    purchasePrice: 0,
                    tonCount: 0,
                    exhaustScale: 0,
                    seatCount: 0,
                    fuelType: "",
                    carOwner: "",
                    purchaseDate: DateUtil.getNowDateStr_Ymd(),
                    actualValue: "",
                    modelCode: "",
                    vehicleModel: "",
                    vehicleCategoryCode: DEFAULT_VEHICLE_CATEGORY_CODE,
                    chgOwnerFlag: "0",
                    loanVehicleFlag: false,
                    transferDate: DateUtil.getNowDateStr_Ymd(),
                    poWeight: "",
                    vinNoQueryFlag: "",
                    certificateDate: "",
                    certificateNo: "",
                    certificateType: "",
                    madeDate: "",
                    vehicleStyleDesc: "",
                    madeFactory: "",
                    vehicleBrand: "",
                    certificateDate2: "",
                    carInsuredRelation: "",
                    companyType: "",
                    loanBeneficiary: ""
                };
                let tprptCarOwnerDto = {           //车主信息..
                    applicantId: 0,
                    flowId: "",
                    name: "",
                    idType: PerIdentifyType.ID_CAR,
                    idNo: "",
                    cellPhoneNo: "",
                    address: "",
                    email: "",
                    gental: "",
                    brithday: "",
                    applicantType: "",
                    natureOfRole: OWNER_TYPE.PERSONAL,
                    groupType: "0",
                    carInsuredRelation: CAR_INSURED_RELATION.NATURAL_PERSON
                };
                let tprptApplicantDto = {        //投保人信息
                    applicantId: "",
                    flowId: "",
                    name: "",
                    idType: PerIdentifyType.ID_CAR,
                    idNo: "",
                    cellPhoneNo: "",
                    address: "",
                    email: "",
                    gental: "",
                    brithday: "",
                    applicantType: "",
                    natureOfRole: OWNER_TYPE.PERSONAL
                };
                let tprptInsuredDto = {        //被保险人
                    applicantId: 0,
                    flowId: "",
                    name: "",
                    idType: PerIdentifyType.ID_CAR,
                    idNo: "",
                    cellPhoneNo: "",
                    address: "",
                    email: "",
                    gental: "",
                    brithday: "",
                    applicantType: "",
                    natureOfRole: OWNER_TYPE.PERSONAL
                };
                let tprpTitemKindListDto = [   //  险别标的信息List
                    {
                        id: 0,                 //险别标的信息
                        flowId: "",
                        itemKindNo: "",
                        kindCodeMain: "",
                        kindNameMain: "",
                        unitAmountMain: "",
                        quantityMain: "",
                        amountMain: "",
                        benchMarkPremiumMain: "",
                        premiumMain: "",
                        modeCode: "",
                        modeName: ""
                    },
                ];
                let prpTcarshipTaxDto = {              //车船税信息
                    id: 0,
                    flowId: "",
                    taxConditionCode: "",
                    taxpayerName: "",
                    taxPayerIdentificationCode: "",
                    vehicleStatus: "",
                    cutoffDate: "",
                    taxEndDate: "",
                    outstandingTax: "",
                    dealyPayPenylty: "",
                    vehicleTax: "",
                    sumTax: 275,
                    taxCutsPlan: "",
                    taxCutsAmount: "",
                    taxAuthorityName: "",
                    taxCutsReason: "",
                    taxCutsCertificateNumber: "",
                    deductionRate: 0,
                    calcTaxFlag: "",
                    declareStatusIa: ""

                };
                let renewalKindCode = {};
                let isSetrenewalKindCode = false;
                state.carInfoList[query.licenseNo].tprpTmainDtoQuery = tprpTmainDtoQuery;
                state.carInfoList[query.licenseNo].isSetrenewalKindCode = isSetrenewalKindCode;
                state.carInfoList[query.licenseNo].renewalKindCode = renewalKindCode;
                state.carInfoList[query.licenseNo].tprpTmainDto = tprpTmainDto;
                state.carInfoList[query.licenseNo].tprpTitemCarDto = tprpTitemCarDto;
                state.carInfoList[query.licenseNo].tprptCarOwnerDto = tprptCarOwnerDto;
                state.carInfoList[query.licenseNo].tprptApplicantDto = tprptApplicantDto;
                state.carInfoList[query.licenseNo].tprptInsuredDto = tprptInsuredDto;
                state.carInfoList[query.licenseNo].tprpTitemKindListDto = tprpTitemKindListDto;
                state.carInfoList[query.licenseNo].prpTcarshipTaxDto = prpTcarshipTaxDto;
                state.carInfoList[query.licenseNo].appliIsOwner = true;
                state.carInfoList[query.licenseNo].insureIsAppli = true;
                state.carInfoList[query.licenseNo].insureIsOwner = true;
                state.carInfoList[query.licenseNo].renewalFlag = "0";
                state.carInfoList[query.licenseNo].gruopType = "";

                //续保是否改变了日期  用于判断续保日期改变给予提示
                state.carInfoList[query.licenseNo].renewalDataChange = {
                    renewalStartDate: "",
                    renewalMtplStartDate: "",
                    isChangeData: false,
                    isChangeMtplData: false,
                    renewalFlag: "0"
                };

            };
            if (isNewCar && typeof (state.carInfoList[query.licenseNo]) == "undefined") {
                setInit();
            }
            if (typeof (state.carInfoList[query.licenseNo]) == "undefined" && !isNewCar) {
                setInit();
                state.curInfo = state.carInfoList[query.licenseNo];
                let tprpTmainDtoQuery = {
                    combineFlag: "",
                    proposalNo: "",
                    mtplProposalNo: "",
                    autonomyAdjustValue: "",
                    channelAdjustValue: "",
                    salerNumber: "",
                    salerName: "",
                    trdSalesCode: "",
                    startDate: DateUtil.getDateTomorrow(1),
                    startHour: "0",
                    endDate: "",
                    endHour: "24",
                    mtplStartDate: DateUtil.getDateTomorrow(1),
                    mtplStartHour: "0",
                    mtplEndDate: "",
                    mtplEndHour: "24"
                };
                _this.$http.post(RequestUrl.RENEWAL_QUERY, query)
                    .then(function (res) {
                        if (res.success && res.result.renewalFlag == "1") {
                            let tprptCarOwnerDto = res.result.tprptCarOwnerDto;
                            let renewalKindCode = {};
                            let tprpTitemKindListDto = res.result.tprpTitemKindListDto;
                            let tprptApplicantDto = res.result.tprptApplicantDto;
                            let tprptInsuredDto = res.result.tprptInsuredDto;
                            let isSetrenewalKindCode = false;

                            res.result.tprpTmainDtoQuery = tprpTmainDtoQuery;
                            for (let i = 0; i < tprpTitemKindListDto.length; i++) {
                                // if (tprpTitemKindListDto[i].kindCodeMain != "BZ") {
                                renewalKindCode[tprpTitemKindListDto[i].kindCodeMain] = tprpTitemKindListDto[i];
                                isSetrenewalKindCode = true;
                                // }
                            }

                            res.result.isSetrenewalKindCode = isSetrenewalKindCode;
                            res.result.renewalKindCode = renewalKindCode;
                            //投保人同车主
                            if (tprptCarOwnerDto.name == tprptApplicantDto.name && tprptCarOwnerDto.idNo == tprptApplicantDto.idNo) {
                                res.result.appliIsOwner = true;
                            } else {
                                res.result.appliIsOwner = false;
                            }
                            //被保人同车主
                            if (tprptInsuredDto.name == tprptCarOwnerDto.name && tprptInsuredDto.idNo == tprptCarOwnerDto.idNo) {
                                res.result.insureIsOwner = true;
                                res.result.insureIsAppli = false;
                            } else {
                                res.result.insureIsOwner = false;
                                //被保人同投保人
                                if (tprptInsuredDto.name == tprptApplicantDto.name && tprptInsuredDto.idNo == tprptApplicantDto.idNo) {
                                    res.result.insureIsAppli = true;
                                } else {
                                    res.result.insureIsAppli = false;
                                }
                            }
                            res.result.gruopType = "";

                            res.result.tprpTitemCarDto.enrollDate = DateUtil.getDateStr_YmdByTs(res.result.tprpTitemCarDto.enrollDate);
                            // debugger
                            if (res.result.tprpTitemCarDto.carInsuredRelation == "" || res.result.tprpTitemCarDto.carInsuredRelation == null) {
                                res.result.tprpTitemCarDto.carInsuredRelation = CAR_INSURED_RELATION.NATURAL_PERSON;
                            }
                            res.result.tprptCarOwnerDto.carInsuredRelation = res.result.tprpTitemCarDto.carInsuredRelation;


                            if (res.result.tprpTitemCarDto.companyType == "" || res.result.tprpTitemCarDto.companyType == null) {
                                res.result.tprpTitemCarDto.groupType = "A0";
                            }

                            res.result.tprptCarOwnerDto.groupType = res.result.tprpTitemCarDto.companyType;

                            //续保是否改变了日期  用于判断续保日期改变给予提示
                            res.result.renewalDataChange = {
                                renewalStartDate: "",
                                renewalMtplStartDate: "",
                                isChangeData: false,
                                isChangeMtplData: false,
                                renewalFlag: "0"
                            };

                            state.carInfoList[query.licenseNo] = res.result;
                            // debugger
                            // debugger

                            // let test = res.result.tprpTitemCarDto.enrollDate;
                            // let test2 = res.result.tprpTitemCarDto.purchaseDate;

                            // debugger
                            // console.log(typeof (res.result.tprpTitemCarDto.enrollDate));
                            // console.log(res.result.tprpTitemCarDto.enrollDate);
                            // console.log(typeof (res.result.tprpTitemCarDto.purchaseDate));
                            // console.log(res.result.tprpTitemCarDto.purchaseDate);
                            // debugger
                            state.curInfo = res.result;
                            // if (res.renewalFlag == "1") {  //允许续保
                            //     state.carInfoList[query.licenseNo]=res;
                            // } else {
                            //     state.carInfoList[query.licenseNo]=res;
                            // }
                        } else {
                            // let sr = {
                            //     renewalFlag: "0",
                            // }
                            // state.carInfoList[query.licenseNo] = sr;

                            setInit();
                        }
                        // if (state.carInfoList[query.licenseNo].renewalFlag != "1") {
                        //     setInit();
                        // }
                        state.isQueryCar = false;
                    })
            } else {
                state.isQueryCar = false;
                // setInit();
                state.curInfo = state.carInfoList[query.licenseNo];
            }

            // let initQuery = {
            //     _this: _this,
            //     riskCode: "0502"
            // }
            // initQuery.riskCode = "0503"
            // _this.$common.storeCommit(_this, Car_Mutations.CAR_OFFER_INIT, initQuery);


        },
        // 保存 信息
        [Car_Mutations.SAVE_DATA_CAR_INFO](state, _this) {
            // debugger
            //投保信息保存
            if (_this.type == "insure") {
                if (typeof (state.carInfoList[state.licenseNo]) == "undefined") {
                    _this.$common.goUrl(_this, RouteUrl.CAR_INSURE);
                    return;
                }
                if (_this.tprptCarOwnerDto.chgOwnerFlag == "false") {
                    _this.tprptCarOwnerDto.chgOwnerFlag = '0';
                } else {
                    _this.tprptCarOwnerDto.chgOwnerFlag = '1';
                }
                // 设置车主信息 
                state.carInfoList[state.licenseNo].tprptCarOwnerDto = _this.tprptCarOwnerDto;
                // //设置企业类型
                // if (_this.tprptCarOwnerDto.carInsuredRelation == CAR_INSURED_RELATION.NO_NATURAL_PERSON) {
                //     //如果为企业  设置企业类型
                //     state.carInfoList[state.licenseNo].gruopType = _this.groupType;
                // } else {
                //     state.carInfoList[state.licenseNo].gruopType = "";
                // }

                //设置是否同车主 投保人
                state.carInfoList[state.licenseNo].appliIsOwner = _this.appliIsOwner;
                if (_this.appliIsOwner) {
                    let tprpData = _this.$common.getNewObject(_this.tprptCarOwnerDto);
                    state.carInfoList[state.licenseNo].tprptApplicantDto = tprpData;
                    _this.tprptApplicantDto = tprpData;
                } else {
                    // 设置投保人
                    state.carInfoList[state.licenseNo].tprptApplicantDto = _this.tprptApplicantDto;
                }
                //设置车主标的信息类型
                state.carInfoList[state.licenseNo].tprpTitemCarDto.carInsuredRelation = _this.tprptCarOwnerDto.carInsuredRelation;
                if (_this.tprptCarOwnerDto.carInsuredRelation == CAR_INSURED_RELATION.NO_NATURAL_PERSON) {
                    state.carInfoList[state.licenseNo].tprpTitemCarDto.companyType = _this.tprptCarOwnerDto.groupType;
                } else {
                    state.carInfoList[state.licenseNo].tprpTitemCarDto.companyType = "";
                }


                //投保人是否同投保人
                state.carInfoList[state.licenseNo].insureIsAppli = _this.insureIsAppli;
                //被保人是否同车主
                state.carInfoList[state.licenseNo].insureIsOwner = _this.insureIsOwner;
                if (_this.insureIsAppli) {
                    let insureDtoApp = _this.$common.getNewObject(_this.tprptApplicantDto);
                    state.carInfoList[state.licenseNo].tprptInsuredDto = insureDtoApp;

                } else if (_this.insureIsOwner) {
                    let insureDtoOwner = _this.$common.getNewObject(_this.tprptCarOwnerDto);
                    state.carInfoList[state.licenseNo].tprptInsuredDto = insureDtoOwner;
                }

            } else {

                // 车辆信息填写保存
                state.carInfoList[state.licenseNo].tprpTitemCarDto = _this.tprpTitemCarDto;
                console.log(state.carInfoList[state.licenseNo].tprpTitemCarDto);
            }
        },
        //查询车辆信息
        [Car_Mutations.GET_SALE_QUERY_VIN_NO](state, data) {
            let _this = data._this;
            let queryVin = {
                vinNo: data.vinNo
            }
            let queryVehicle = {
                query: {
                    modelName: data.modelName,    // 厂牌型号          
                },
                type: data.type,
                _this: _this
            }

            // if (typeof (state.vinList[data.vinNo]) == "undefined") {
            //     _this.$common.storeCommit(_this, Car_Mutations.SET_QUERY_STATE, true);
            //     _this.$http.post(RequestUrl.SALE_QUERY_VIN, queryVin)
            //         .then(function (res) {
            // if (res.success) {
            //     state.vinList[data.vinNo] = res.result;
            // state.vinNoQueryFlag = 1;
            // } else {
            state.vinNoQueryFlag = 0;
            _this.$common.storeCommit(_this, Car_Mutations.GET_SALE_QUERY_VEHICLE, queryVehicle);
            // setTimeout(() => {
            //     _this.$common.storeCommit(_this, Car_Mutations.SET_QUERY_STATE, false);
            // }, 200);
            // }
            //         })
            // } else {
            //     state.vinNoQueryFlag = 1;
            // }
        },
        [Car_Mutations.GET_SALE_QUERY_VEHICLE](state, data) {
            let _this = data._this;
            let name = data.query.modelName;
            let type = data.type;
            let isThisQuery = true;
            if (type == "des") {
                isThisQuery = false;
            }
            state.queryModelName = name;

            if (typeof (state.queryVehicleList[name]) == "undefined") {
                let initQuery = {
                    intPageCount: 10,
                    pageNum: 0,
                    modelName: name,
                    isEnd: false,
                    showCarList: []
                }
                state.queryVehicleList[name] = initQuery;
            }
            state.queryVehicleList[name].pageNum++;
            let query = state.queryVehicleList[name];
            let queryData = {
                pageNum: query.pageNum,
                modelName: name,
                intPageCount: query.intPageCount,
                agreementNo: state.carAgrInfo.agreementCode
            };

            if (!query.isEnd) {
                if (typeof (state.vehicleList[name]) == "undefined") {
                    state.vehicleList[name] = [];
                }
                if (isThisQuery) {
                    _this.$common.storeCommit(_this, Car_Mutations.SET_QUERY_STATE, true);
                }
                _this.$http.post(RequestUrl.SALE_QUERY_VEHICLE, queryData)
                    .then(function (res) {
                        if (res.success && res.result.motorList.length != 0) {
                            // if (type == 'init') {
                            //     _this.isShowCarDetai = state.queryVehicleList[name].showCarList;
                            // }
                            let resList = res.result.motorList;
                            for (let i = 0; i < resList.length; i++) {
                                resList[i].isShowCarDetai = false;
                                //含税
                                let carInfoPurchasePrice = _this.$common.getNewObject(resList[i]);
                                carInfoPurchasePrice.showName = carInfoPurchasePrice.modelName + "(含税)";

                                carInfoPurchasePrice.price = carInfoPurchasePrice.purchasePrice;
                                state.vehicleList[name].push(carInfoPurchasePrice);
                                //不含税
                                let carInfoVe = _this.$common.getNewObject(resList[i]);
                                carInfoVe.showName = carInfoVe.modelName + "(不含税)";
                                carInfoVe.price = carInfoVe.vehiclePurchasePrice;
                                state.vehicleList[name].push(carInfoVe);

                                // state.vehicleList[name].push(resList[i]);
                                state.queryVehicleList[name].showCarList.push(false);
                                state.queryVehicleList[name].showCarList.push(false);
                                // state.isShowCarDetai = false;
                                // _this.isShowCarDetai.push(false);
                            }
                            console.log(state.vehicleList[name]);
                            if (res.result.motorList.length < query.intPageCount) {
                                state.queryVehicleList[name].isEnd = true;
                            }
                            state.carCurShowList = state.vehicleList[name];
                            if (isThisQuery) {
                                _this.showCarInfo = state.carCurShowList;
                            }
                        }
                        if (isThisQuery) {
                            _this.$common.storeCommit(_this, Car_Mutations.SET_QUERY_STATE, false);
                            if (res.state != 0) {
                                _this.showErrorMsg("查询车型信息出错,请联系管理员!");
                                _this.$common.goUrl(_this, RouteUrl.CAR_INFO);
                            } else if (res.result.motorList.length == 0) {
                                _this.showCarInfo = [];
                                _this.showErrorMsg("未查询到对应的车型信息!");
                                _this.$common.goUrl(_this, RouteUrl.CAR_INFO);
                            }
                        }
                        sessionStorage["IS_SET_CAR_SELECT_SCROLL"] = "F";
                    })
            } else {
                state.carCurShowList = state.vehicleList[name];
                if (isThisQuery) {
                    _this.showCarInfo = state.carCurShowList;
                }
                sessionStorage["IS_SET_CAR_SELECT_SCROLL"] = "F";
            }
        },
        [Car_Mutations.CAR_INSURE_SAVE](state, data) {
            state.carInsureData = data;
        },
        //车型选择后保存数据 组装保费计算数据
        [Car_Mutations.CAR_SELECT_SAVE](state, data) {
            let index = data.seleId;
            let _this = data._this;
            if (typeof (index) == "undefined" || index == "undefined" || index == null || index == "null") {
                index = 0;
            }
            // debugger
            state.carSelectSave[state.queryModelName] = index;
            state.carSelect = state.vehicleList[state.queryModelName][index];
            let carSelect = state.vehicleList[state.queryModelName][index];
            let tprpTitemCarDto = state.carInfoList[state.licenseNo].tprpTitemCarDto;
            state.queryModelName = state.carSelect.modelName;
            _this.getCarSele("des", state.queryModelName);
            //查看是否为新车 完善车辆保费查询信息
            if (state.carInsureData.isNewCar) {
                tprpTitemCarDto.licenseNo = "";
                tprpTitemCarDto.licenseKindCode = "";
                tprpTitemCarDto.whethelicenses = 0;
            } else {
                tprpTitemCarDto.whethelicenses = 1;
                tprpTitemCarDto.licenseNo = state.carInsureData.licenseNo;
                // tprpTitemCarDto.licenseKindCode = "";   //！  车牌号码种类
            }
            tprpTitemCarDto.exhaustScale = carSelect.exHaustScale;         //排量
            tprpTitemCarDto.tonCount = carSelect.tonCount;                 //吨位
            tprpTitemCarDto.seatCount = carSelect.seatCount;               //座位
            tprpTitemCarDto.modelCode = carSelect.modeCode;                   //车型代码
            tprpTitemCarDto.purchasePrice = carSelect.price; //新车购置价
            tprpTitemCarDto.vehicleModel = carSelect.modelName;                //车辆型号名称
            tprpTitemCarDto.showName = tprpTitemCarDto.showName;
            tprpTitemCarDto.vinNoQueryFlag = state.vinNoQueryFlag;      //是否已使用VIN码解析
            tprpTitemCarDto.countryCode = carSelect.countryCode;          //进口/国产
            state.carInfoList[state.licenseNo].tprpTitemCarDto = tprpTitemCarDto;
        },
        // [Car_Mutations.ADD_CAR_INFO_SAVE](state, data) {
        //     state.addCarInfoSave[state.queryModelName] = data;
        //     state.curAddCarInfo = data;
        // },
        [Car_Mutations.CAR_CONFIG_SAVE](state, data) {
            state.carConfig = data;
        },
        [Car_Mutations.ADD_CAR_INFO_SAVE](state, data) {
            state.carInfoList[state.licenseNo].tprpTitemCarDto = data;
        },
        [Car_Mutations.CAR_OFFER_INIT](state, data) {
            let _this = data._this;
            let query = {
                TYPE: "FORM",
                riskCode: data.riskCode
            }
            state.isGetCarOfferInit = true;
            _this.$http.post(RequestUrl.CAR_OFFER_INIT, query)
                .then(function (res) {
                    if (res.success) {
                        let kindList = res.result.kind;
                        // let saveKind = {};

                        let relationList = res.result.relation;
                        // let relationSave = {};
                        // let mainRiskSave = []; // 主险种

                        let seleRateList = res.result.seleRate;
                        // let seleRateSave = {};  
                        let userSeleList = {};

                        for (let key in kindList) {

                            state.saveKind[kindList[key].kindCode] = kindList[key];
                            let userSele = {
                                mainSele: true, //险种选择
                                nonSele: true,  //不计免赔
                                defValue: "",   //选择值
                                mainKindCode: "",
                                nonDeductibleKindCode: "",
                                idMainKind: false,
                                seleSetSatate: false,
                                excludeCode: ""
                            }
                            userSeleList[kindList[key].kindCode] = userSele;
                        }

                        for (let key in relationList) {
                            let relation = relationList[key];
                            //初始化选择默认值
                            if (typeof (state.seleRateSave[relation.kindCode]) == "undefined") {
                                let save = {
                                    value: "",
                                    seleData: [],
                                    isSetData: false,
                                }
                                state.seleRateSave[relation.kindCode] = save;
                            }

                            state.relationSave[relation.kindCode] = relation;
                            //设置默认选中
                            if (relation.isDefault == "1") {
                                userSeleList[relation.kindCode].mainSele = true;
                            } else {
                                userSeleList[relation.kindCode].mainSele = false;
                            }
                            if (relation.excludeCode != "" && typeof (userSeleList[relation.excludeCode]) != "undefined") {
                                userSeleList[relation.excludeCode].mainSele = false;
                                userSeleList[relation.excludeCode].nonSele = false;
                            }
                            //设置主险
                            if (relation.mainKindCode == null || relation.mainKindCode == "") {
                                state.mainRiskSave.push(relation);
                            } else {
                                //设置附加险
                                state.additionalInsurance.push(relation);
                                //设置附加险为不默认选择
                                userSeleList[relation.kindCode].nonSele = false; //主险种没有不计免赔

                                //设置主险种和附加险关系 
                                if (typeof (state.addMain[relation.mainKindCode]) == "undefined") {
                                    state.addMain[relation.mainKindCode] = [];
                                }
                                state.addMain[relation.mainKindCode].push(relation.kindCode);
                            }
                            userSeleList[relation.kindCode].mainKindCode = relation.mainKindCode;
                            userSeleList[relation.kindCode].nonDeductibleKindCode = relation.nonDeductibleKindCode;
                            userSeleList[relation.kindCode].excludeCode = relation.excludeCode;
                            userSeleList[relation.kindCode].idMainKind = true;
                        }

                        //设置选择默认值默认值
                        for (let key in seleRateList) {
                            let seleRate = seleRateList[key];
                            //设置选择的默认值
                            if (seleRate.isDefault == "Y") {
                                state.seleRateSave[seleRate.kindCode].value = seleRate.sumInsured;
                                userSeleList[seleRate.kindCode].defValue = seleRate.sumInsured;
                            }
                            if (seleRate.isDefault == "R") {
                                state.seleRateSave[seleRate.kindCode].value = seleRate.sumInsured;
                                userSeleList[seleRate.kindCode].defValue = seleRate.sumInsured;
                            }
                            if (seleRate.isDefault == "T" || seleRate.isDefault == "R") {
                                userSeleList[seleRate.kindCode].seleSetSatate = true;
                                state.seleSetSatateList[seleRate.kindCode + seleRate.sumInsured] = seleRate.sumInsuredName;
                            }
                            //转换为keyValue形式
                            let keyValue = {
                                key: seleRate.sumInsured,
                                value: seleRate.sumInsuredName
                            }
                            state.seleRateSave[seleRate.kindCode].isSetData = true;
                            state.seleRateSave[seleRate.kindCode].seleData.push(keyValue);
                        }
                        state.baseUserSeleList = userSeleList;

                    }
                    // debugger
                })
        },
        //报价页面保存操作
        [Car_Mutations.CAR_OFFER_SAVE](state, data) {
            state.userSeleList[state.licenseNo] = data.seleValue;
            state.carInfoList[state.licenseNo].tprpTmainDtoQuery = data.tprpTmainDto;
            state.carInfoList[state.licenseNo].sele2724 = data.seleAxtx;
            state.carInfoList[state.licenseNo].sele3105 = data.seleAtqqb;
            state.carInfoList[state.licenseNo].axtxInsure = data.axtxInsure;
            state.carInfoList[state.licenseNo].atqqbInsure = data.atqqbInsure;
            state.carInfoList[state.licenseNo].renewalDataChange = data.renewalDataChange;
            console.log(data.seleAxtx);
            console.log(data.seleAtqqb);
            if (typeof (state.baseCarOffer[state.licenseNo]) == "undefined") {
                let base = {
                    strongInsure: true,
                    commInsure: true,
                    packIndex: 0,
                }
                state.baseCarOffer[state.licenseNo] = base;
            }
            state.baseCarOffer[state.licenseNo].packIndex = data.packIndex;
            state.baseCarOffer[state.licenseNo].strongInsure = data.strongInsure;
            state.baseCarOffer[state.licenseNo].commInsure = data.commInsure;
            // debugger
            state.carInfoList[state.licenseNo].seleCrossValue = data.seleCrossValue;
        },
        [Car_Mutations.SAVE_OFFER_RESULT](state, data) {
            // state.combinecalculate[state.licenseNo] =
        },
        [Car_Mutations.SET_OFFER_RESULT](state, data) {
            let _this = data;
            let combinecalculate = state.combinecalculate[state.licenseNo];
            _this.subPremium = parseFloat(combinecalculate.subPremium);
            _this.tprpTmainDto = combinecalculate.tprpTmainDto;
            _this.tprpTitemKindListDto = combinecalculate.tprpTitemKindListDto;
            _this.axtxPrice = parseFloat(combinecalculate.axtxPrice);
            _this.axtxKind = combinecalculate.axtxKind;
            _this.atqqbPrice = parseFloat(combinecalculate.atqqbPrice);
            _this.atqqbKind = combinecalculate.atqqbKind;
            // debuggerdebugger
            // _this.showPrice = (_this.subPremium + _this.axtxPrice + _this.atqqbPrice).toFixed(2);

            _this.showPrice = combinecalculate.showPrice;
            _this.tprptCarOwnerDto = state.carInfoList[state.licenseNo].tprptCarOwnerDto;
            _this.tprptApplicantDto = state.carInfoList[state.licenseNo].tprptApplicantDto;
            _this.tprptInsuredDto = state.carInfoList[state.licenseNo].tprptInsuredDto;
            _this.appliIsOwner = state.carInfoList[state.licenseNo].appliIsOwner;
            _this.insureIsAppli = state.carInfoList[state.licenseNo].insureIsAppli;
            _this.insureIsOwner = state.carInfoList[state.licenseNo].insureIsOwner;
        },

        [Car_Mutations.SET_SPECIAL_AGREEMENT](state, _this) {
            // let tprptEngageDTOList = [];
            // tprptEngageDTOList = state.combinecalculate[state.licenseNo].tprptEngageDTOList;
            // if (tprptEngageDTOList.length > 0 && typeof (tprptEngageDTOList[0].direction) == "undefined") {
            //     for (let i = 0; i < tprptEngageDTOList.length; i++) {
            //         tprptEngageDTOList[i].direction = false;
            //         if (tprptEngageDTOList[i].mustFlag == "0") {
            //             tprptEngageDTOList[i].seleFlage = true;
            //         } else {
            //             tprptEngageDTOList[i].seleFlage = false;
            //         }
            //     }
            // }
            // _this.tprptEngageDTOList = tprptEngageDTOList;

            let tprptEngageDTOList = [];
            tprptEngageDTOList = state.combinecalculate[state.licenseNo].tprptEngageDTOList;
            // tprptEngageDTOList = [{ "id": 268972, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "5", "mustFlag": "1", "updateFlag": "1", "clauseCode": "SE0394", "clausesName": "投保告知", "clausesContext": "为保障您的利益，请在收到本保险单一周内拨打我公司24小时服务热线4008882008核实保险单资料。" }, { "id": 268973, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0508", "engageSerialNo": "5", "mustFlag": "1", "updateFlag": "1", "clauseCode": "SE0394", "clausesName": "投保告知", "clausesContext": "为保障您的利益，请在收到本保险单一周内拨打我公司24小时服务热线4008882008核实保险单资料。" }, { "id": 268974, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "6", "mustFlag": "1", "updateFlag": "1", "clauseCode": "SE0395", "clausesName": "核对保单内容", "clausesContext": "收到本保单请即核对，若无疑义，即视为同意合同条款及约定的全部内容。本保单所列各险种均适用《利宝保险有限公司机动车综合商业保险示范条款》（本保单已附条款一份）。投保人与被保险人确认已充分理解保险条款的内容与含义，尤其是免除保险人责任条款的内容与含义。" }, { "id": 268975, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "7", "mustFlag": "1", "updateFlag": "1", "clauseCode": "SE0396", "clausesName": "人伤赔付标准", "clausesContext": "本公司按照《道路交通事故受伤人员临床诊疗指南》和国家基本医疗保险的同类医疗费用标准核定医疗费用的赔偿金额。" }, { "id": 268976, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "8", "mustFlag": "1", "updateFlag": "1", "clauseCode": "SE0397", "clausesName": "营业性使用免责", "clausesContext": "非营业车辆如从事营业性运输或租赁活动，发生保险责任范围内的事故的，本公司不负责赔偿。" }, { "id": 268977, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "9", "mustFlag": "1", "updateFlag": "1", "clauseCode": "SE0400", "clausesName": "现场报案", "clausesContext": "发生保险事故后，请保留现场并立即拨打4008882008报案。故意或者因重大过失未及时报案造成本公司无法对事故原因、经过、损失程度进行查勘或事故调查的，保险人对无法确定的部分不承担赔偿责任。" }, { "id": 268978, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "10", "mustFlag": "1", "updateFlag": "1", "clauseCode": "SE0403", "clausesName": "未投保交强险", "clausesContext": "发生意外事故时，保险车辆未投保交强险或交强险合同已经失效的，对于交强险责任各分项限额以内的损失和费用，本公司不负责赔偿。" }, { "id": 268979, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "11", "mustFlag": "1", "updateFlag": "1", "clauseCode": "SE0405", "clausesName": "拓印号码", "clausesContext": "本保单以拓印的发动机号及车架号确认保险车辆。" }, { "id": 268980, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "12", "mustFlag": "1", "updateFlag": "1", "clauseCode": "SE0411", "clausesName": "投保告知2", "clausesContext": "为了保障您的合法权益，请选择拨打客户服务电话4008882008/登陆网址www.libertymutual.com.cn/至公司柜面三种方式之一进行保单/赔案信息查询。" }, { "id": 268981, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0508", "engageSerialNo": "12", "mustFlag": "1", "updateFlag": "1", "clauseCode": "SE0411", "clausesName": "投保告知2", "clausesContext": "为了保障您的合法权益，请选择拨打客户服务电话4008882008/登陆网址www.libertymutual.com.cn/至公司柜面三种方式之一进行保单/赔案信息查询。" }, { "id": 268982, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "1", "mustFlag": "1", "updateFlag": "0", "clauseCode": "SE0414", "clausesName": "贷款车受益人", "clausesContext": "被保险人授权保险人将按照保险单规定理赔后应付给被保险人的赔款直接全额支付给********。同时，被保险人委托********在保险事故发生后向保险人进行索赔。" }, { "id": 268983, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "2", "mustFlag": "1", "updateFlag": "0", "clauseCode": "SE0415", "clausesName": "实际车型", "clausesContext": "本保险承保车辆的实际车型为：********。" }, { "id": 268984, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "13", "mustFlag": "1", "updateFlag": "0", "clauseCode": "SE0417", "clausesName": "受损车辆特约", "clausesContext": "投保时该车********处已受损（如验车相片所示），在以上受损部位修复并报保险人验车合格批改前，保险人对以上损失不承担赔偿责任。" }, { "id": 268985, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "4", "mustFlag": "1", "updateFlag": "0", "clauseCode": "SE0419", "clausesName": "被保险人与行驶证车主不一致", "clausesContext": "该车投保时行驶证车主为212121" }, { "id": 268986, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0508", "engageSerialNo": "4", "mustFlag": "1", "updateFlag": "0", "clauseCode": "SE0419", "clausesName": "被保险人与行驶证车主不一致", "clausesContext": "该车投保时行驶证车主为212121" }, { "id": 268987, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0508", "engageSerialNo": "15", "mustFlag": "1", "updateFlag": "0", "clauseCode": "SE0420", "clausesName": "实际车型", "clausesContext": "本保险承保车辆的实际车型为：********。" }, { "id": 268988, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0508", "engageSerialNo": "18", "mustFlag": "1", "updateFlag": "1", "clauseCode": "SE0424", "clausesName": "交强险即时起保2", "clausesContext": "本保单自缴费时起即时生效" }, { "id": 268989, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "17", "mustFlag": "1", "updateFlag": "0", "clauseCode": "SE0774", "clausesName": "贷款车受益人-重庆专用1", "clausesContext": "鉴于被保险车辆已设定抵押，经投保人（被保险人）声明和授权，本保单车辆损失险、盗抢险及该两项险别项下附加险的第一受益人为********           ，未经第一受益人书面同意，本保单涉及的前述险种不得被退保、减保或批改（不影响第一受益人权益的批改除外）；发生前述约定险种项下的保险事故后，除第一受益人明确同意将保险赔款支付被保险人外，原则上保险赔款应支付第一受益人。" }, { "id": 268990, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "18", "mustFlag": "1", "updateFlag": "0", "clauseCode": "SE0775", "clausesName": "贷款车受益人-重庆专用2", "clausesContext": "鉴于被保险车辆已设定抵押，经投保人（被保险人）声明和授权，本保单车辆损失险、盗抢险及该两项险别项下附加险的第一受益人为********          ，未经第一受益人书面同意，本保单涉及的前述险种不得被退保、减保或批改（不影响第一受益人权益的批改除外）； 发生前述约定险种项下的保险事故且一次事故的保险赔款超过人民币********元时，保险人同意根据第一受益人的书面指示支付,若一次事故的保险赔款低于前述约定金额时，根据法律规定及保险合同约定支付。" }, { "id": 268991, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "30", "mustFlag": "1", "updateFlag": "1", "clauseCode": "SE0429", "clausesName": "绝对免配额", "clausesContext": "投保人与保险人协商同意，机动车损失保险在实行免赔率的基础上增加每次事故绝对免赔额，每次事故绝对免赔额的具体金额由投保人与保险人协商确定并在保险单中载明。--修改" }, { "id": 268992, "flowId": "LBISLE180403W04mY9Kv", "riskCode": "0502", "engageSerialNo": "22", "mustFlag": "0", "updateFlag": "0", "clauseCode": "SE0860", "clausesName": "测试环境特约编码", "clausesContext": "享受非事故全国道路救援服务，如需服务，请致电400 888 2008转尊客会" }];
            if (tprptEngageDTOList.length > 0 && typeof (tprptEngageDTOList[0].direction) == "undefined") {
                for (let i = 0; i < tprptEngageDTOList.length; i++) {
                    tprptEngageDTOList[i].direction = false;
                    if (tprptEngageDTOList[i].mustFlag == "0") {
                        tprptEngageDTOList[i].seleFlage = true;
                    } else {
                        tprptEngageDTOList[i].seleFlage = false;
                    }
                }
            }
            _this.tprptEngageDTOList = tprptEngageDTOList;
        },
        [Car_Mutations.SAVE_SPECIAL_AGREEMENT](state, _this) {
            // debugger
            state.combinecalculate[state.licenseNo].tprptEngageDTOList = _this.tprptEngageDTOList;
        },
        [Car_Mutations.DOWN_LIST](state, _this) {
            // debugger
            _this.$common.storeCommit(_this, Car_Mutations.SET_QUERY_STATE, true);
            let queryData = {
                flowId: "",
                tdistributionDto: {
                    name: "",
                    cellPhoneNo: "",
                    address: "",
                    email: "",
                    callbackUrl: "",
                },     //配送信息
                tprptCarOwnerDto: {},     //车主信息
                tprptApplicantDto: {},    //投保人
                tprptInsuredDto: {},      //被保险人
                tprptEngageDTOList: [],   //特约及附加条款列表
            }
            let carInfo = state.carInfoList[state.licenseNo];
            // queryData.flowId = carInfo.flowId;
            queryData.tprptCarOwnerDto = carInfo.tprptCarOwnerDto;
            queryData.tprptApplicantDto = carInfo.tprptApplicantDto;
            queryData.tprptInsuredDto = carInfo.tprptInsuredDto;
            let combinecalculate = state.combinecalculate[state.licenseNo];
            let tprptEngageDTOList = combinecalculate.tprptEngageDTOList;
            queryData.flowId = combinecalculate.flowId;
            for (let i = 0; i < tprptEngageDTOList.length; i++) {
                if (tprptEngageDTOList[i].seleFlage) {
                    let tprptEngageDTO = tprptEngageDTOList[i];
                    let base = {
                        riskCode: "",          //险种
                        engageSerialNo: "",    //序号
                        clauseCode: "",        //代码
                        clausesName: "",       //名称
                        clausesContext: "",    //内容
                    }
                    base.riskCode = tprptEngageDTO.riskCode;
                    base.engageSerialNo = tprptEngageDTO.engageSerialNo;
                    base.clauseCode = tprptEngageDTO.clauseCode;
                    base.clausesName = tprptEngageDTO.clausesName;
                    base.clausesContext = tprptEngageDTO.clausesContext;
                    queryData.tprptEngageDTOList.push(base);
                }
            }
            //添加上配送信息
            if (state.isSetCommitDis) {
                queryData.tdistributionDto = state.commitDisData;
            }
            let userId = "";
            if (_this.user.isLogin) {
                userId = _this.user.userDto.userCode;
            }

            let refereeId = "";
            if (_this.saveInsure.refereeData != false && _this.$common.isNotEmpty(_this.saveInsure.refereeData) && _this.$common.isNotEmpty(_this.saveInsure.refereeData.userCode)) {
                refereeId = _this.saveInsure.refereeData.userCode
            }



            let insureQuery = {
                accessUrl: _this.accessUrl + "  ver:" + localStorage["version"] + "INSURE_TIME:" + DateUtil.getNowDateStr_Ymdhm(new Date()) + "  userId:" + userId + "  refId:" + refereeId,
                city: _this.initIndexData.region,
                shareUid: _this.saveInsure.shareUid,
                callBackUrl: _this.shareData.callBackUrl,
                singleMember: _this.shareData.singleMember,
                successfulUrl: _this.shareData.successfulUrl,
                planId: "",
                risk: "",
                riskName: _this.saveInsure.indexData.productType,
                proRiskCode: _this.saveInsure.indexData.riskCode,
                productId: _this.saveInsure.indexData.id,
                branchCode: _this.saveInsure.hotArea.branchCode,
                refereeId: refereeId,
                productName: _this.saveInsure.indexData.productCname,
                premium: state.premium,
                mtplPremium: state.mtplPremium,
                clauseUrl: "",
                isHotArea: _this.saveInsure.car.isHotArea,
                areaCode: _this.saveInsure.car.areaCode,
                dealUid: _this.dealUid,
                planName: "",
                deadLine: "",
                channelName: _this.saveInsure.car.channelName,
                tprpTitemKindListDto: state.tprpTitemKindListDto,
                agreementCode: _this.saveInsure.car.agreementCode,
                salerName: _this.saveInsure.car.salerName,
                saleCode: _this.saveInsure.car.saleCode,
            }
            let query = {
                hotarea: _this.saveInsure.hotArea,
                insureQuery: insureQuery,
                tProposalSaveDTO: queryData,
                tMotorPremiumRequestDTO: {
                    tprpTmainDto: state.downTprpTmainDto
                },
                user: {
                    userCode: userId
                },
            }

            _this.$http.post(RequestUrl.PREMINUM_COMMIT, query)
                .then(function (res) {
                    // debugger
                    if (res.success && res.result.status) {
                        // if (res.result.proposalStatus == "9") {
                        //     _this.showErrorMsg("需要人工核保", true);
                        // } else if (res.result.proposalStatus == "8") {
                        if (CarConfig.getPreminumCommitResCode(_this, res.result.proposalStatus)) {
                            let payNo = "";
                            let payRiskCode = "";
                            if (res.result.proposalNo != null && res.result.proposalNo != "") {
                                payNo = res.result.proposalNo;
                                payRiskCode = res.result.proposalNo.substring(2, 6);
                            } else {
                                payNo = res.result.proposalMTPLNo;
                                payRiskCode = res.result.proposalMTPLNo.substring(2, 6);
                            }
                            let payData = {
                                _this: _this,
                                paymentNo: payNo,
                                riskCode: payRiskCode
                            };
                            // debugger
                            _this.$common.storeCommit(_this, Mutations.PAY_SET_DATA, payData);
                        }
                        // }
                    } else {
                        if (typeof (res.result.resultMessage) != "undefined") {
                            _this.$common.showErrorMsg(_this, res.result.resultMessage);
                        } else {
                            _this.$common.showErrorMsg(_this, res.result);
                        }
                        _this.$common.goUrl(_this, RouteUrl.CAR_OFFER);
                    }
                    _this.$common.storeCommit(_this, Car_Mutations.SET_QUERY_STATE, false);
                })


        },
        //报价操作
        [Car_Mutations.CAR_OFFER](state, data) {
            let _this = data._this;
            _this.$common.storeCommit(_this, Car_Mutations.SET_QUERY_STATE, true);
            let seleValue = _this.seleValue;
            //商业险对象
            let query = {
                tprpTmainDto: {},
                tprpTitemCarDto: {},
                tprptCarOwnerDto: {},
                tprptApplicantDto: {},
                tprptInsuredDto: {},
                prpTcarshipTaxDto: {},
                tprpTitemKindListDto: [],
            }
            //设置投保单信息
            query.tprpTmainDto = _this.tprpTmainDto;
            //TEST
            // query.tprpTmainDto.salerNumber = '234';
            // query.tprpTmainDto.salerName = 'motoui';



            if (_this.strongInsure && _this.commInsure) {
                //联合单子
                query.tprpTmainDto.combineFlag = "COMBINE";
                //设置结束日期
                if (!state.isImmInsure) {
                    query.tprpTmainDto.endDate = DateUtil.getDateInsure(query.tprpTmainDto.startDate);
                    query.tprpTmainDto.mtplEndDate = DateUtil.getDateInsure(query.tprpTmainDto.mtplStartDate);
                }
            } else if (_this.strongInsure) {
                //单交强
                query.tprpTmainDto.combineFlag = "MTPL";
                if (!state.isImmInsure) {
                    query.tprpTmainDto.mtplEndDate = DateUtil.getDateInsure(query.tprpTmainDto.mtplStartDate);
                    // query.tprpTmainDto.startDate = "";
                    // query.tprpTmainDto.startHour = "";
                    // query.tprpTmainDto.endDate = "";
                    // query.tprpTmainDto.endHour = "";
                }
            } else if (_this.commInsure) {
                //单商业
                query.tprpTmainDto.combineFlag = "MOTOR";
                query.tprpTmainDto.endDate = DateUtil.getDateInsure(query.tprpTmainDto.startDate);
                // query.tprpTmainDto.mtplStartDate = "";
                // query.tprpTmainDto.mtplStartHour = "";
                // query.tprpTmainDto.mtplEndDate = "";
                // query.tprpTmainDto.mtplEndHour = "";
            }
            state.downTprpTmainDto = query.tprpTmainDto;
            let res = state.carInfoList[state.licenseNo];

            //设置 投保标的信息
            query.tprpTitemCarDto = res.tprpTitemCarDto;
            //车主信息
            query.tprptCarOwnerDto = res.tprptCarOwnerDto;
            //车主为个人 客户类型为单位,不传
            if (query.tprptCarOwnerDto.natureOfRole != OWNER_TYPE.PERSONAL) {
                query.tprptCarOwnerDto.gental = "";
                query.tprptCarOwnerDto.brithday = "";
            }
            //投保人
            query.tprptApplicantDto = res.tprptApplicantDto;
            //被保险人
            query.tprptInsuredDto = res.tprptInsuredDto;

            //投保人同车主
            if (res.appliIsOwner) {
                query.tprptApplicantDto = query.tprptCarOwnerDto;
            } else if (query.tprptApplicantDto.natureOfRole != OWNER_TYPE.PERSONAL) {
                query.tprptApplicantDto.gental = "";
                query.tprptApplicantDto.brithday = "";
            }
            state.carInfoList[state.licenseNo].tprptApplicantDto = query.tprptApplicantDto;
            //被保人同投保人
            if (res.insureIsAppli) {
                query.tprptInsuredDto = query.tprptApplicantDto;
            } else if (res.insureIsOwner) {
                //被保险人同车主
                query.tprptInsuredDto = query.tprptCarOwnerDto;
            } else if (query.tprptInsuredDto.natureOfRole != OWNER_TYPE.PERSONAL) {
                query.tprptInsuredDto.gental = "";
                query.tprptInsuredDto.brithday = "";
            }
            state.carInfoList[state.licenseNo].tprptInsuredDto = query.tprptInsuredDto;
            let tprpTitemKindListDto = [];
            // 设置险别标的信息
            if (_this.commInsure) {
                for (let key in seleValue) {
                    let sele = seleValue[key];
                    if (sele.idMainKind) {
                        // debugger
                        //主险
                        // debugger
                        if (sele.mainSele) {
                            let base = {
                                kindCodeMain: "",
                                kindNameMain: "",
                                unitAmountMain: "",
                                amountMain: "",
                                modeCode: "",
                                modeName: "",
                                quantityMain: "0.0",
                            }
                            let kind = state.saveKind[key];
                            base.kindNameMain = kind.kindCName;
                            if (sele.seleSetSatate) {
                                // debugger
                                base.modeCode = sele.defValue;
                                base.unitAmountMain = "";
                                base.modeName = state.seleSetSatateList[key + sele.defValue]
                            } else if (sele.defValue != true) {
                                base.unitAmountMain = sele.defValue;
                            }
                            base.kindCodeMain = kind.kindCode;
                            if (kind.kindCName.indexOf("乘客") > -1) {
                                base.quantityMain = query.tprpTitemCarDto.seatCount - 1;
                                // if (base.kindCodeMain == "A701" || base.kindCodeMain == "A702") {
                                //     base.quantityMain = 4;
                                // }
                                // base.quantityMain = kind.quantityMain;
                                base.amountMain = sele.defValue * base.quantityMain;
                            } else if (sele.defValue != true) {
                                base.amountMain = sele.defValue;
                            }
                            tprpTitemKindListDto.push(base);
                        }
                        //不计免赔
                        if (sele.nonSele && sele.nonDeductibleKindCode != "" && sele.nonDeductibleKindCode != null) {
                            let base = {
                                kindCodeMain: "",
                                kindNameMain: "",
                                unitAmountMain: "",
                                amountMain: "",
                                quantityMain: ""
                            }
                            let kindNon = state.saveKind[sele.nonDeductibleKindCode];
                            base.kindCodeMain = kindNon.kindCode;
                            base.kindNameMain = kindNon.kindCName;
                            // base.quantityMain = kindNon.quantityMain;
                            base.amountMain = "";
                            base.unitAmountMain = "";
                            tprpTitemKindListDto.push(base);
                        }
                    }

                }
            }

            query.tprpTitemKindListDto = tprpTitemKindListDto;
            // debugger
            //设置行驶区域代码
            // if (!state.carInsureData.isNewCar) {
            query.tprpTitemCarDto.runAreaCode = CarConfig.getAreaSort(state.licenseNo);
            // }
            if (state.carInsureData.isNewCar) {
                query.tprpTitemCarDto.licenseKindCode = "";
            }
            //是否车贷投保多年标志
            // debugger
            // if (query.tprpTitemCarDto.loanVehicleFlag != "0" && query.tprpTitemCarDto.loanVehicleFlag != "1" && query.tprpTitemCarDto.loanVehicleFlag != 1 && query.tprpTitemCarDto.loanVehicleFlag != 0) {
            // debugger
            if (query.tprpTitemCarDto.loanVehicleFlag == true) {
                query.tprpTitemCarDto.loanVehicleFlag = "1";
            } else {
                query.tprpTitemCarDto.loanVehicleFlag = "0";
            }
            //车辆过户标志
            if (query.tprpTitemCarDto.chgOwnerFlag == true) {
                query.tprpTitemCarDto.chgOwnerFlag = "1";
            } else {
                query.tprpTitemCarDto.chgOwnerFlag = "0";
                query.tprpTitemCarDto.transferDate = "";
            }
            // }
            //重新设置车主类型  3自然人 4 非自然人
            // if (query.tprpTitemCarDto.carInsuredRelation == 2) {
            //     query.tprpTitemCarDto.carInsuredRelation = 4;
            // } else {
            //     query.tprpTitemCarDto.carInsuredRelation = 3;
            // }

            let prpTcarshipTaxDto = {
                taxpayerName: "",
                taxPayerIdentificationCode: ""
            }
            query.prpTcarshipTaxDto = prpTcarshipTaxDto;
            let insureQuery = {
                agreementCode: state.carAgrInfo.agreementCode
            }
            query.tprpTmainDto.agreementCode = state.carAgrInfo.agreementCode;

            // console.log(query);
            let requestDto = {
                insureQuery: insureQuery,
                tMotorPremiumRequestDTO: query
            }
            // console.log('-----------carInsureData---------------');
            // console.log(JSON.stringify(state.carInsureData));
            // console.log('-----------carInfoList---------------');
            // console.log(JSON.stringify(state.carInfoList[state.licenseNo]));


            _this.$http.post(RequestUrl.COMBINE_CALCULATE, requestDto)
                .then(function (res) {
                    let query = {
                        _this: _this,
                        res: res
                    }
                    _this.$common.storeCommit(_this, Car_Mutations.SET_CAR_OFFER_DATA, query);
                })
        },
        /**
         * 交叉销售计划保存
         */
        [Car_Mutations.CROSS_SALE_PLAN_SAVE](state, data) {
            let _this = data._this;
            let planList = data.planList;
            let proposalNo = data.proposalNo;
            let resData = data.res;
            let requestDto = {
                motorProposalNo: proposalNo,
                planLists: planList,
                agreementNo: state.carAgrInfo.agreementCode
            }
            _this.$http.post(RequestUrl.CROSS_SALE_PLAN_SAVE, requestDto).then(function (res) {
                if (res.success) {
                    if (resData.result.resultCode == "0001") {
                        let msgComData = {
                            title: "重复投保",
                            content: resData.result.resultMessage,
                            alertBtn: "继续投保",
                            alertBtnUrl: RouteUrl.OFFER_RESULT,
                            backBtn: "重新选择",
                            backBtnUrl: "",
                        };
                        _this.$common.storeCommit(_this, Mutations.IS_SHOW_MSG_COM_DATA, msgComData);
                    } else {
                        _this.$common.goUrl(_this, RouteUrl.OFFER_RESULT);
                    }
                } else {
                    _this.$common.showErrorMsg("保存意外险信息出错,请联系管理员处理！");
                }
            });
        },

    },
    actions: {

    },
    getters: {

    }
}
