import {
  Mutations,
  policyHolderType,
  Gender,
  PerIdentifyType,
  EntIdentifyType,
  benefType,
  insuredidentity,
} from 'src/common/const';

import {
  RequestUrl
} from 'src/common/url';
import DateUtil from 'src/common/util/dateUtil';

export default {
  state: {
    curShowinsurance: [],
    isQuery: false, //是否在进行请求，一直刷新操作  从而重复请求
    isEnd: false,
    insuranceData: {},
    insureType: "all",
    typeIsShop: false,
  },
  mutations: {
    [Mutations.SET_CUR_SHOWINSURANCE](state, insuranceData) {
      if (!state.typeIsShop) {
        state.curShowinsurance = [];
        state.isEnd = false;
      }

    },
    [Mutations.INSURANCE_OPERATION](state, insuranceData) {
      // let isShop = insuranceData.isShop;
      let _this = insuranceData._this;
      let operation = insuranceData.operation;
      let riskCode = insuranceData.riskCode;
      let type = insuranceData.type;
      state.typeIsShop = type;
      state.insureType = riskCode;
      let baseQueryData = {
        TYPE: 'FORM',
        type: riskCode,
        sorttype: 'ASC',
        pageNumber: 0,
        pageSize: 10,
        userCode: insuranceData.userCode,
      }

      //如果没有定义这个险种信息 就进行创建
      //   alert(typeof (state.insuranceData[type+riskCode]));
      //   let test = (typeof (state.insuranceData[type+riskCode]) == 'undefine');
      if (typeof (state.insuranceData[type + riskCode]) == 'undefined') {
        let baseDataInfo = {
          isisEnd: false,
          queryData: baseQueryData,
          insurance: []
        }
        state.curShowinsurance = [];
        state.isEnd = false;
        state.insuranceData[type + riskCode] = baseDataInfo;
      }

      if (operation == "refresh") {
        //上拉刷新操作  赋初始值
        state.isEnd = false;
        state.insuranceData[type + riskCode].queryData = baseQueryData;
        state.insuranceData[type + riskCode].isisEnd = false;
        state.insuranceData[type + riskCode].insurance = [];
        state.insuranceData[type + riskCode].queryData.pageNumber++;
      }

      if (!state.isQuery && !state.insuranceData[type + riskCode].isisEnd) {
        if (operation == "infinite") {
          //下拉刷新操作 页码加一
          state.insuranceData[type + riskCode].queryData.pageNumber++;
        }
        let url = "";
        if (type) {
          url = RequestUrl.SHOP_PRODUCT_LIST;
        } else {
          url = RequestUrl.PRODUCT_LIST;
        }
        //开始进行请求操作
        state.isQuery = true;
        _this.$http.post(url, state.insuranceData[type + riskCode].queryData)
          .then(function (res) {

            let insure
            if (type) {
              insure = res.result;
            } else {
              insure = res.result.content;
            }
            for (let i = 0; i < insure.length; i++) {
              let parm = {
                type: 'add',
                id: insure[i].id,
                product: insure[i]
              }
              _this.$common.storeCommit(_this, Mutations.SET_PRODUCT_LIST, parm);
              state.insuranceData[type + riskCode].insurance.push(insure[i]);
            }
            if (insure.length < state.insuranceData[type + riskCode].queryData.pageSize) {
              state.insuranceData[type + riskCode].isisEnd = true;
              state.isEnd = true;
            }
            state.curShowinsurance = state.insuranceData[type + riskCode].insurance.slice();

            state.isQuery = false;

            // insuranceData.done(state.isEnd);
          })

        //请求标识码
        // $.ajax({
        //   type: 'POST',
        //   url: RequestUrl.PRODUCT_LIST,
        //   data: state.insuranceData[type+riskCode].queryData,
        //   async: false,
        //   success: function (res) {
        //     let insure = res.result.content;
        //     for (let i = 0; i < insure.length; i++) {
        //       state.insuranceData[type+riskCode].insurance.push(insure[i]);
        //     }
        //     if (res.result.size < state.insuranceData[type+riskCode].queryData.pageSize) {
        //       state.insuranceData[type+riskCode].isisEnd = true;
        //       state.isEnd = true;
        //     }
        //     state.curShowinsurance = state.insuranceData[type+riskCode].insurance;

        //     state.isQuery = false;
        //   }
        // });


      } else {
        state.isEnd = state.insuranceData[type + riskCode].isisEnd;
        state.curShowinsurance = state.insuranceData[type + riskCode].insurance.slice();
        // insuranceData.done(state.isEnd);
      }

    },
  },
  actions: {

  },
  getters: {

  }
}
