export const TRANSLATIONS_CH = {
  "content": "中文测试 {type}",
  "index": "首页",
  "insurance": "保险",
  "service": "服务",
  "my": "我的",
  "henghua": "恒华出行宝",
  "more": "查看更多",
  "hotActivity": "热门产品",
  "moreInsurance": "更多保险",
  "moreService": "更多服务"
};
